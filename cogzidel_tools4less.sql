-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 28, 2017 at 11:19 AM
-- Server version: 5.6.37
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cogzidel_tools4less`
--

-- --------------------------------------------------------

--
-- Table structure for table `accept_pay`
--

CREATE TABLE `accept_pay` (
  `id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `transaction_id` varchar(50) NOT NULL,
  `amount` float NOT NULL,
  `currency` varchar(25) NOT NULL,
  `status` int(1) NOT NULL,
  `created` varchar(25) NOT NULL,
  `payout_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_key`
--

CREATE TABLE `admin_key` (
  `id` int(10) NOT NULL,
  `page_refer` varchar(150) NOT NULL,
  `page_key` varchar(150) NOT NULL,
  `created` varchar(150) NOT NULL,
  `status` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_key`
--

INSERT INTO `admin_key` (`id`, `page_refer`, `page_key`, `created`, `status`) VALUES
(1, '0', 'Book Your Accommodation', '1375281419', 0);

-- --------------------------------------------------------

--
-- Table structure for table `amnities`
--

CREATE TABLE `amnities` (
  `id` smallint(6) NOT NULL,
  `name` varchar(111) NOT NULL,
  `description` varchar(333) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amnities`
--

INSERT INTO `amnities` (`id`, `name`, `description`) VALUES
(1, 'Smoking Allowed ', 'Smoking is allowed '),
(2, 'Pets Allowed', 'Pets is allowed'),
(4, 'Cable TV ', 'Cable TV  is available'),
(6, 'Wireless Internet', 'A wireless router that guests can access 24/7.'),
(7, 'Air Conditioning', 'Air Conditioning is available'),
(8, 'Heating', 'Heating is available'),
(9, 'Elevator in Building ', 'Elevator is available in the building '),
(10, 'Handicap Accessible', 'The property is easily accessible.  Guests should communicate about individual needs.'),
(11, 'Pool', 'A private swimming pool'),
(12, 'Kitchen', 'Kitchen is available for guest use'),
(13, 'Parking Included', 'Parking Included'),
(14, 'Washer / Dryer', 'Washer / Dryer'),
(15, 'Doorman', 'Doorman'),
(16, 'Gym', 'Gym'),
(17, 'Hot Tub', 'Hot Tub'),
(18, 'Indoor Fireplace', 'Indoor Fireplace'),
(19, 'Buzzer/Wireless Intercom ', 'Buzzer/Wireless Intercom '),
(20, 'Breakfast', 'Breakfast is provided.'),
(21, 'Family/Kid Friendly', 'The property is suitable for hosting families with children.'),
(22, 'Suitable for Events', 'The property can accommodate a gathering of 25 or more attendees.'),
(24, 'Swimming', 'Swimming pool');

-- --------------------------------------------------------

--
-- Table structure for table `booktime_policy`
--

CREATE TABLE `booktime_policy` (
  `id` int(25) NOT NULL,
  `name` varchar(155) NOT NULL,
  `cancellation_title` varchar(155) NOT NULL,
  `cancellation_content` text NOT NULL,
  `cleaning_status` int(1) NOT NULL,
  `security_status` int(1) NOT NULL,
  `additional_status` int(1) NOT NULL,
  `list_days_prior_status` int(1) NOT NULL,
  `list_days_prior_days` int(2) NOT NULL,
  `list_days_prior_percentage` int(3) NOT NULL,
  `list_before_status` int(1) NOT NULL,
  `list_before_days` int(2) NOT NULL,
  `list_before_percentage` int(2) NOT NULL,
  `list_non_refundable_nights` int(2) NOT NULL,
  `list_after_status` int(1) NOT NULL,
  `list_after_days` int(2) NOT NULL,
  `list_after_percentage` int(2) NOT NULL,
  `is_standard` enum('0','1') NOT NULL,
  `reservation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `calendar`
--

CREATE TABLE `calendar` (
  `id` bigint(20) NOT NULL,
  `list_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `availability` varchar(31) NOT NULL,
  `value` varchar(30) NOT NULL,
  `currency` varchar(7) NOT NULL,
  `notes` text NOT NULL,
  `style` varchar(11) NOT NULL,
  `booked_using` varchar(30) NOT NULL,
  `booked_days` int(31) NOT NULL,
  `created` int(31) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cancellation_policy`
--

CREATE TABLE `cancellation_policy` (
  `id` int(25) NOT NULL,
  `name` varchar(155) NOT NULL,
  `cancellation_title` varchar(155) NOT NULL,
  `cancellation_content` text NOT NULL,
  `cleaning_status` int(1) NOT NULL,
  `security_status` int(1) NOT NULL,
  `additional_status` int(1) NOT NULL,
  `list_days_prior_status` int(1) NOT NULL,
  `list_days_prior_days` int(2) NOT NULL,
  `list_days_prior_percentage` int(3) NOT NULL,
  `list_before_status` int(1) NOT NULL,
  `list_before_days` int(2) NOT NULL,
  `list_before_percentage` int(2) NOT NULL,
  `list_non_refundable_nights` int(2) NOT NULL,
  `list_after_status` int(1) NOT NULL,
  `list_after_days` int(2) NOT NULL,
  `list_after_percentage` int(2) NOT NULL,
  `is_standard` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancellation_policy`
--

INSERT INTO `cancellation_policy` (`id`, `name`, `cancellation_title`, `cancellation_content`, `cleaning_status`, `security_status`, `additional_status`, `list_days_prior_status`, `list_days_prior_days`, `list_days_prior_percentage`, `list_before_status`, `list_before_days`, `list_before_percentage`, `list_non_refundable_nights`, `list_after_status`, `list_after_days`, `list_after_percentage`, `is_standard`) VALUES
(1, 'Flexible', 'Flexible: {percentage} refund {day} prior to arrival, except fees', '<ul>\n<li>Cleaning fees, Security fees and Additional Guest fees are always refunded if the guest did not check in.</li>\n<li>The {site_title} service fee is non-refundable.</li>\n<li>If there is a complaint from either party, notice must be given to&nbsp;{site_title} within 24 hours of check-in.</li>\n<li>{site_title}&nbsp;will mediate when necessary, and has the final say in all disputes.</li>\n<li>A reservation is officially cancelled when the guest clicks the cancellation button on the cancellation confirmation page, which they can find in Dashboard &gt; Your Trips &gt; Cancel.</li>\n<li>Cancellation policies may be superseded by the Guest Refund Policy, safety cancellations, or extenuating circumstances. Please review these exceptions.</li>\n<li>Applicable taxes will be retained and remitted.</li>\n</ul>', 0, 0, 0, 1, 1, 100, 1, 1, 100, 1, 1, 1, 100, '1'),
(2, 'Moderate', 'Moderate: Full refund {day} prior to arrival, except fees', '<ul>\n<li>Cleaning fees, Security fees and Additional Guest fees are always refunded if the guest did not check in.</li>\n<li>The {site_title} service fee is non-refundable.</li>\n<li>If there is a complaint from either party, notice must be given to&nbsp;{site_title} within 24 hours of check-in.</li>\n<li>{site_title}&nbsp;will mediate when necessary, and has the final say in all disputes.</li>\n<li>A reservation is officially cancelled when the guest clicks the cancellation button on the cancellation confirmation page, which they can find in Dashboard &gt; Your Trips &gt; Cancel.</li>\n<li>Cancellation policies may be superseded by the Guest Refund Policy, safety cancellations, or extenuating circumstances. Please review these exceptions.</li>\n<li>Applicable taxes will be retained and remitted.</li>\n</ul>', 0, 0, 0, 1, 5, 100, 1, 5, 100, 1, 1, 1, 50, '1'),
(3, 'Strict', 'Strict: {percentage} refund up until {day} prior to arrival, except fees', '<ul>\n<li>Cleaning fees, Security fees and Additional Guest fees are always refunded if the guest did not check in.</li>\n<li>The {site_title} service fee is non-refundable.</li>\n<li>If there is a complaint from either party, notice must be given to&nbsp;{site_title} within 24 hours of check-in.</li>\n<li>{site_title}&nbsp;will mediate when necessary, and has the final say in all disputes.</li>\n<li>A reservation is officially cancelled when the guest clicks the cancellation button on the cancellation confirmation page, which they can find in Dashboard &gt; Your Trips &gt; Cancel.</li>\n<li>Cancellation policies may be superseded by the Guest Refund Policy, safety cancellations, or extenuating circumstances. Please review these exceptions.</li>\n<li>Applicable taxes will be retained and remitted.</li>\n</ul>', 0, 0, 0, 1, 7, 50, 0, 7, 50, 1, 0, 1, 0, '1'),
(4, 'Super Strict', 'Super Strict: {percentage} refund up until {day} prior to arrival, except fees', '<ul>\n<li>Cleaning fees, Security fees and Additional Guest fees are always refunded if the guest did not check in.</li>\n<li>The {site_title} service fee is non-refundable.</li>\n<li>If there is a complaint from either party, notice must be given to&nbsp;{site_title} within 24 hours of check-in.</li>\n<li>{site_title}&nbsp;will mediate when necessary, and has the final say in all disputes.</li>\n<li>A reservation is officially cancelled when the guest clicks the cancellation button on the cancellation confirmation page, which they can find in Dashboard &gt; Your Trips &gt; Cancel.</li>\n<li>Cancellation policies may be superseded by the Guest Refund Policy, safety cancellations, or extenuating circumstances. Please review these exceptions.</li>\n<li>Applicable taxes will be retained and remitted.</li>\n</ul>', 0, 0, 0, 1, 30, 50, 0, 30, 50, 1, 0, 1, 0, '0'),
(5, 'Long Term', 'Long Term: First month down payment, {day} notice for lease, termination', '<ul>\n<li>Cleaning fees, Security fees and Additional Guest fees are always refunded if the guest did not check in.</li>\n<li>The {site_title} service fee is non-refundable.</li>\n<li>If there is a complaint from either party, notice must be given to&nbsp;{site_title} within 24 hours of check-in.</li>\n<li>{site_title}&nbsp;will mediate when necessary, and has the final say in all disputes.</li>\n<li>A reservation is officially cancelled when the guest clicks the cancellation button on the cancellation confirmation page, which they can find in Dashboard &gt; Your Trips &gt; Cancel.</li>\n<li>Cancellation policies may be superseded by the Guest Refund Policy, safety cancellations, or extenuating circumstances. Please review these exceptions.</li>\n<li>Applicable taxes will be retained and remitted.</li>\n</ul>', 0, 0, 0, 0, 1, 10, 1, 30, 100, 1, 0, 30, 12, '0');

-- --------------------------------------------------------

--
-- Table structure for table `cancel_my_account_details`
--

CREATE TABLE `cancel_my_account_details` (
  `clientid` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tell_why_your_leaving` varchar(255) NOT NULL,
  `comment` int(255) NOT NULL,
  `contact_ok` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_my_account_details`
--

INSERT INTO `cancel_my_account_details` (`clientid`, `username`, `user_id`, `email`, `tell_why_your_leaving`, `comment`, `contact_ok`) VALUES
(0, '', '0', '', '0', 0, '0'),
(0, '', '0', '', '0', 0, '0'),
(0, '', '0', '', '0', 0, '0'),
(0, '', '0', '', '0', 0, '0'),
(0, '', '0', '', 'Other', 0, '0'),
(0, '', '0', '', '0', 0, '0'),
(0, '', '0', '', '0', 0, '0'),
(0, '', '0', '', '0', 0, '0'),
(0, '', '0', '', '0', 0, '0'),
(0, '', '0', '', 'User don\'t understand how to use it', 0, 'Yes'),
(0, '', '0', '', '0', 0, '0'),
(0, '', '0', '', '0', 0, '0'),
(0, '', '0', '', 'It\'s temporary; User will be back', 0, 'No'),
(0, '', '0', '', 'User have privacy concerns', 0, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('7513aadacc2258b7b87216906e356eb0', '0.0.0.0', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv', 1307957325, 'a:12:{s:10:\"DX_user_id\";s:2:\"17\";s:11:\"DX_username\";s:6:\"magesh\";s:10:\"DX_role_id\";s:1:\"1\";s:12:\"DX_role_name\";s:4:\"User\";s:18:\"DX_parent_roles_id\";a:0:{}s:20:\"DX_parent_roles_name\";a:0:{}s:13:\"DX_permission\";a:0:{}s:21:\"DX_parent_permissions\";a:0:{}s:12:\"DX_logged_in\";b:1;s:4:\"user\";s:2:\"17\";s:8:\"username\";s:6:\"magesh\";s:9:\"logged_in\";b:1;}'),
('d9fb989e2b792a7964d3caea86bcead0', '0.0.0.0', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv', 1307978034, 'a:12:{s:10:\"DX_user_id\";s:1:\"1\";s:11:\"DX_username\";s:5:\"admin\";s:10:\"DX_role_id\";s:1:\"2\";s:12:\"DX_role_name\";s:5:\"Admin\";s:18:\"DX_parent_roles_id\";a:0:{}s:20:\"DX_parent_roles_name\";a:0:{}s:13:\"DX_permission\";a:0:{}s:21:\"DX_parent_permissions\";a:0:{}s:12:\"DX_logged_in\";b:1;s:4:\"user\";s:1:\"1\";s:8:\"username\";s:5:\"admin\";s:9:\"logged_in\";b:1;}'),
('22ce146f669e0be9fbc77de305a3b208', '27.250.3.186', 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0', 1503909642, 'a:20:{s:9:\"user_data\";s:0:\"\";s:15:\"_facebook_scope\";s:5:\"email\";s:18:\"_facebook_callback\";s:65:\"http://demo.cogzideltemplates.com/tools4less/cron/cloudinary_cron\";s:14:\"locale_country\";s:5:\"India\";s:15:\"locale_currency\";s:3:\"INR\";s:10:\"DX_user_id\";s:1:\"1\";s:11:\"DX_username\";s:5:\"admin\";s:10:\"DX_emailId\";s:20:\"systems@cogzidel.com\";s:8:\"DX_refId\";s:32:\"21232f297a57a5a743894a0e4a801fc3\";s:10:\"DX_role_id\";s:1:\"2\";s:12:\"DX_role_name\";s:5:\"Admin\";s:18:\"DX_parent_roles_id\";a:0:{}s:20:\"DX_parent_roles_name\";a:0:{}s:13:\"DX_permission\";a:0:{}s:21:\"DX_parent_permissions\";a:0:{}s:12:\"DX_logged_in\";b:1;s:4:\"user\";s:1:\"1\";s:8:\"username\";s:5:\"admin\";s:9:\"logged_in\";b:1;s:11:\"redirect_to\";s:0:\"\";}'),
('25ae5d1156da1ff4bee2cfd8b684b525', '66.249.89.137', 'Mediapartners-Google', 1503909347, 'a:6:{s:9:\"user_data\";s:0:\"\";s:15:\"_facebook_scope\";s:5:\"email\";s:18:\"_facebook_callback\";s:45:\"http://demo.cogzideltemplates.com/tools4less/\";s:11:\"redirect_to\";s:0:\"\";s:14:\"locale_country\";s:13:\"United States\";s:15:\"locale_currency\";s:3:\"USD\";}'),
('ee0137c963c540eb7eb6617040cb86e6', '66.249.64.73', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1503909354, 'a:6:{s:9:\"user_data\";s:0:\"\";s:15:\"_facebook_scope\";s:5:\"email\";s:18:\"_facebook_callback\";s:73:\"http://demo.cogzideltemplates.com/tools4less//jquery.city-autocomplete.js\";s:11:\"redirect_to\";s:45:\"http://demo.cogzideltemplates.com/tools4less/\";s:14:\"locale_country\";s:13:\"United States\";s:15:\"locale_currency\";s:3:\"USD\";}'),
('511c048a3648e7373d8f4753f1eeae74', '61.0.242.140', 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36', 1503914979, 'a:6:{s:9:\"user_data\";s:0:\"\";s:15:\"_facebook_scope\";s:5:\"email\";s:18:\"_facebook_callback\";s:73:\"http://demo.cogzideltemplates.com/tools4less//jquery.city-autocomplete.js\";s:11:\"redirect_to\";s:45:\"http://demo.cogzideltemplates.com/tools4less/\";s:14:\"locale_country\";s:5:\"India\";s:15:\"locale_currency\";s:3:\"INR\";}'),
('4f1207790776319c1425b0ba82016e13', '27.250.3.186', 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0', 1503919028, 'a:6:{s:9:\"user_data\";s:0:\"\";s:15:\"_facebook_scope\";s:5:\"email\";s:18:\"_facebook_callback\";s:73:\"http://demo.cogzideltemplates.com/tools4less//jquery.city-autocomplete.js\";s:11:\"redirect_to\";s:45:\"http://demo.cogzideltemplates.com/tools4less/\";s:14:\"locale_country\";s:5:\"India\";s:15:\"locale_currency\";s:3:\"INR\";}');

--
-- Triggers `ci_sessions`
--
DELIMITER $$
CREATE TRIGGER `login_history` AFTER INSERT ON `ci_sessions` FOR EACH ROW insert into login_history (session_id,ip_address,user_agent,last_activity,user_id,logout) values (new.session_id,new.ip_address,new.user_agent,new.last_activity,0,0)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `login_history_update` AFTER UPDATE ON `ci_sessions` FOR EACH ROW update login_history set session_id = new.session_id, last_activity = new.last_activity where session_id = old.session_id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) NOT NULL,
  `list_id` int(111) NOT NULL,
  `contact_key` varchar(100) NOT NULL,
  `userby` int(11) NOT NULL,
  `userto` int(111) NOT NULL,
  `checkin` varchar(50) NOT NULL,
  `checkout` varchar(50) NOT NULL,
  `no_quest` tinyint(4) NOT NULL,
  `currency` varchar(11) NOT NULL,
  `price` float NOT NULL,
  `original_price` float NOT NULL,
  `topay` float NOT NULL,
  `admin_commission` float NOT NULL,
  `status` tinyint(4) NOT NULL,
  `send_date` int(31) NOT NULL,
  `offer` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_info`
--

CREATE TABLE `contact_info` (
  `id` int(11) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `street` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `country` varchar(30) NOT NULL,
  `pincode` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_info`
--

INSERT INTO `contact_info` (`id`, `phone`, `email`, `name`, `street`, `city`, `state`, `country`, `pincode`) VALUES
(1, '04524282000', 'support@cogzidel.com', 'Cogzidel Technologies', 'Simakkal', 'Madurai', 'TamilNadu', 'India', 625001);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `country_symbol` char(2) NOT NULL,
  `caps_name` varchar(80) NOT NULL,
  `country_name` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country_symbol`, `caps_name`, `country_name`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(12) NOT NULL,
  `couponcode` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `coupon_price` float NOT NULL,
  `expirein` varchar(12) NOT NULL,
  `status` int(20) NOT NULL,
  `currency` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_users`
--

CREATE TABLE `coupon_users` (
  `id` int(11) NOT NULL,
  `list_id` bigint(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `used_coupon_code` varchar(50) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `currency_name` varchar(150) NOT NULL,
  `currency_code` varchar(5) NOT NULL,
  `currency_symbol` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `default` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency_name`, `currency_code`, `currency_symbol`, `status`, `default`) VALUES
(1, 'US Dollar', 'USD', '&#36;', 1, 1),
(2, 'Pound Sterling', 'GBP', '&pound;', 1, 0),
(3, 'Europe', 'EUR', '&euro;', 1, 0),
(4, 'Australian Dollar', 'AUD', '&#36;', 1, 0),
(5, 'Singapore', 'SGD', '&#36;', 1, 0),
(6, 'Swedish Krona', 'SEK', 'kr', 1, 0),
(7, 'Danish Krone', 'DKK', 'kr', 1, 0),
(8, 'Mexican Peso', 'MXN', '$', 1, 0),
(9, 'Brazilian Real', 'BRL', 'R$', 1, 0),
(10, 'Malaysian Ringgit', 'MYR', 'RM', 1, 0),
(11, 'Philippine Peso', 'PHP', 'P', 1, 0),
(12, 'Swiss Franc', 'CHF', '&euro;', 1, 0),
(13, 'India', 'INR', '&#x20B9;', 1, 0),
(14, 'Argentine Peso', 'ARS', '&#36;', 1, 0),
(15, 'Canadian Dollar', 'CAD', '&#36;', 1, 0),
(16, 'Chinese Yuan', 'CNY', '&#165;', 1, 0),
(17, 'Czech Republic Koruna', 'CZK', 'K&#269;', 1, 0),
(18, 'Hong Kong Dollar', 'HKD', '&#36;', 1, 0),
(19, 'Hungarian Forint', 'HUF', 'Ft', 1, 0),
(20, 'Indonesian Rupiah', 'IDR', 'Rp', 1, 0),
(21, 'Israeli New Sheqel', 'ILS', '&#8362;', 1, 0),
(22, 'Japanese Yen', 'JPY', '&#165;', 1, 0),
(23, 'South Korean Won', 'KRW', '&#8361;', 1, 0),
(24, 'Norwegian Krone', 'NOK', 'kr', 1, 0),
(25, 'New Zealand Dollar', 'NZD', '&#36;', 1, 0),
(26, 'Polish Zloty', 'PLN', 'z&#322;', 1, 0),
(27, 'Russian Ruble', 'RUB', 'p', 1, 0),
(28, 'Thai Baht', 'THB', '&#3647;', 1, 0),
(29, 'Turkish Lira', 'TRY', '&#8378;', 1, 0),
(30, 'New Taiwan Dollar', 'TWD', '&#36;', 1, 0),
(31, 'Vietnamese Dong', 'VND', '&#8363;', 1, 0),
(32, 'South African Rand', 'ZAR', 'R', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `currency_converter`
--

CREATE TABLE `currency_converter` (
  `id` int(3) NOT NULL,
  `currency_code` varchar(10) NOT NULL,
  `currency_value` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency_converter`
--

INSERT INTO `currency_converter` (`id`, `currency_code`, `currency_value`) VALUES
(1, 'USD', '1'),
(2, 'GBP', '0.594438'),
(3, 'EUR', '0.744682'),
(4, 'AUD', '1.073841'),
(5, 'SGD', '1.24696'),
(6, 'SEK', '6.863501'),
(7, 'DKK', '5.554448'),
(8, 'MXN', '13.18913'),
(9, 'BRL', '2.259438'),
(10, 'MYR', '3.209797'),
(11, 'PHP', '43.75468'),
(12, 'CHF', '0.90628'),
(13, 'INR', '60.97431'),
(14, 'ARS', '8.230843'),
(15, 'CAD', '1.09174'),
(16, 'CNY', '6.178215'),
(17, 'CZK', '20.61438'),
(18, 'HKD', '7.750034'),
(19, 'HUF', '233.3027'),
(20, 'IDR', '11771.4166'),
(21, 'ILS', '3.420409'),
(22, 'JPY', '102.642499'),
(23, 'KRW', '1035.65165'),
(24, 'NOK', '6.270852'),
(25, 'NZD', '1.174948'),
(26, 'PLN', '3.116912'),
(27, 'RUB', '35.7553'),
(28, 'THB', '32.14812'),
(29, 'TRY', '2.135574'),
(30, 'TWD', '30.02386'),
(31, 'VND', '21219.8333'),
(32, 'ZAR', '10.68457');

-- --------------------------------------------------------

--
-- Table structure for table `discover`
--

CREATE TABLE `discover` (
  `id` int(200) NOT NULL,
  `country` varchar(200) NOT NULL,
  `image` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_settings`
--

CREATE TABLE `email_settings` (
  `id` int(10) NOT NULL,
  `code` varchar(111) NOT NULL,
  `name` varchar(111) NOT NULL,
  `value` varchar(111) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_settings`
--

INSERT INTO `email_settings` (`id`, `code`, `name`, `value`, `created`) VALUES
(1, 'MAILER_TYPE', 'Mailer Type', '1', 2011),
(2, 'SMTP_PORT', 'SMTP Port', '', 2011),
(3, 'SMTP_USER', 'SMTP Username', '', 2011),
(4, 'SMTP_PASS', 'SMTP Password', '', 2011),
(5, 'MAILER_MODE', 'Mailer Mode', 'html', 2011);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(64) CHARACTER SET utf8 NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `mail_subject` text CHARACTER SET utf8 NOT NULL,
  `email_body_text` text CHARACTER SET utf8 NOT NULL,
  `email_body_html` text CHARACTER SET ucs2 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `type`, `title`, `mail_subject`, `email_body_text`, `email_body_html`) VALUES
(44, 'tc_book_to_admin', 'Admin notification for  Travel cretid booking', ' {traveler_name} sent the reservation request by using his Travel Cretids', 'Hello Admin,\r\n\r\n{traveler_name}sent the reservation request to book the {list_title} place on {book_date} at {book_time} by using his Travel Credits.\r\n\r\nDetails as follows,\r\n\r\nTraveler Name : {traveler_name}\r\nContact Email Id : {traveler_email_id}\r\nPlace Name : {list_title}\r\nCheck in : {checkin}\r\nCheck out : {checkout}\r\nMarket Price : {market_price}\r\nPayed Amount : {payed_amount}\r\nTravel Credits : {travel_credits} \r\nHost Name : {host_name}\r\nHost Email Id : {host_email_id} \r\n\r\n--\r\nThanks and Regards,\r\n\r\n{site_name} Team', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi Admin,</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>{traveler_name}sent the reservation request to book the {list_title} place on {book_date} at {book_time} by using his Travel Credits.</p>\r\n<p>Details as follows,</p>\r\n<p>Traveler Name : {traveler_name}</p>\r\n<p>Contact Email Id : {traveler_email_id}</p>\r\n<p>Place Name : {list_title}</p>\r\n<p>Check in : {checkin}</p>\r\n<p>Check out : {checkout}</p>\r\n<p>Market Price : {market_price}</p>\r\n<p>Payed Amount : {payed_amount}</p>\r\n<p>Travel Credits : {travel_credits}</p>\r\n<p>Host Name : {host_name}</p>\r\n<p>Host Email Id : {host_email_id}</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0px;\">{site_name} Team</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(45, 'tc_book_to_host', 'Host notification for  Travel cretid booking', ' {traveler_name} sent the reservation request by using his Travel Cretids', 'Hello {username},\r\n\r\n{traveler_name}sent the reservation request to book your {list_title} place on {book_date} at {book_time} by using his Travel Credits.\r\n\r\nWe will contact you with the appropriate payment.\r\n\r\nDetails as follows,\r\n\r\nTraveler Name : {traveler_name}\r\nContact Email Id : {traveler_email_id}\r\nPlace Name : {list_title}\r\nCheck in : {checkin}\r\nCheck out : {checkout}\r\nPrice : {market_price}\r\n\r\n--\r\nThanks and Regards,\r\n\r\nAdmin\r\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi {username} ,</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>{traveler_name}sent the reservation request to book your {list_title} place on {book_date} at {book_time} by using his Travel Credits.</p>\r\n<p>Details as follows,</p>\r\n<p>Traveler Name : {traveler_name}</p>\r\n<p>Contact Email Id : {traveler_email_id}</p>\r\n<p>Place Name : {list_title}</p>\r\n<p>Check in : {checkin}</p>\r\n<p>Check out : {checkout}</p>\r\n<p>Price : {market_price}</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\r\n<p style=\"margin: 0px;\">{site_name}</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(46, 'admin_mass_email', 'Admin mass email', '{subject}', 'Hi User,\r\n\r\n{dynamic_content}\r\n\r\n--\r\nThanks and Regards,\r\n\r\nAdmin\r\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi User,</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>{dynamic_content}</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\r\n<p style=\"margin: 0px;\">{site_name}</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(47, 'user_reference', 'Reference', 'Write {username} a reference on {site_name}', 'Hi,\n\n{username} is asking you to provide a reference that they can display publicly on their {site_name} profile. {site_name} is a community marketplace for accommodations that is built on trust and reputation. A reference from you would really help build their reputation with the community.\nA reference on {site_name} can help {username} find a cool place to stay or host interesting travelers. \n\n{click_here} to have a Reference for {username}.\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi,</td>\n</tr>\n<tr>\n<td>\n<p>{username} is asking you to provide a reference that they can display publicly on their {site_name} profile. {site_name} is a community marketplace for accommodations that is built on trust and reputation. A reference from you would really help build their reputation with the community. A reference on {site_name} can help {username} find a cool place to stay or host interesting travelers.</p>\n<p>{click_here} to have a Reference for {username}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(101, 'reference_receive', 'Reference Receive', 'You have received a new reference on {site_name}!', 'Hi {username},\r\n\r\n{other_username} has written a reference for you. Before it goes to your public profile, you can review it and either accept or ignore it. {click_here} to view the reference. \r\n\r\n--\r\nThanks and Regards,\r\n\r\nAdmin\r\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {username},</td>\n</tr>\n<tr>\n<td>\n<p>{other_username} has written a reference for you. Before it goes to your public profile, you can review it and either accept or ignore it. {click_here} to view the reference.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(48, 'host_reservation_notification', 'Reservation notification for host', 'The Reservation was requested by  {traveler_name}', 'Hello {username},\n\n{traveler_name} booked the {list_title} place on {book_date} at {book_time}.\n\nDetails as follows,\n\nTraveler Name : {traveler_name}\nContact Email Id : {traveler_email_id}\nPlace Name : {list_title}\nCheck in : {checkin}\nCheck out : {checkout}\nPrice : {market_price}\n\nPlease give the confirmation by clicking the below action.\n{action_url}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p>Hi {username} ,</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>{traveler_name} booked the {list_title} place on {book_date} at {book_time}.</p>\r\n<br />\r\n<p>Details as follows,</p>\r\n<p>Traveler Name : {traveler_name}</p>\r\n<p>Contact Email Id : {traveler_email_id}</p>\r\n<p>Place Name : {list_title}</p>\r\n<p>Check in : {checkin}</p>\r\n<p>Check out : {checkout}</p>\r\n<p>Price : {market_price}</p>\r\n<br />\r\n<p>Please give the confirmation by clicking the below action.</p>\r\n<p>{action_url}</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0px;\">{site_name} Team</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(49, 'traveller_reservation_notification', 'Reservation notification for  traveller', 'Your Reservation Request Is Succesfully Sent', 'Hello {traveler_name},\n\nYour reservation request is successfully sent to the appropriate host.\n\nPlease wait for his confirmation.\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td><span style=\"font-family: helvetica;\">Hi {traveler_name} ,</span></td>\n</tr>\n<tr>\n<td>\n<p><span style=\"font-family: helvetica;\">Your reservation request is successfully sent to the appropriate host.</span></p>\n<p><span style=\"font-family: helvetica;\">Please wait for his confirmation.</span></p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\"><span style=\"font-family: helvetica;\">--</span></p>\n<p style=\"margin: 0 0 10px 0;\"><span style=\"font-family: helvetica;\">Thanks and Regards,</span></p>\n<p style=\"margin: 0 0 10px 0;\"><span style=\"font-family: helvetica;\">Admin,</span></p>\n<p style=\"margin: 0px;\"><span style=\"font-family: helvetica;\">{site_name}</span></p>\n</td>\n</tr>\n</tbody>\n</table>'),
(50, 'admin_reservation_notification', 'Reservation notification for  administrator', '{traveler_name} sent the reservation request to {host_name}', 'Hello Admin,\n\n{traveler_name} sent the reservation request for {list_title} place on {book_date} at {book_time}.\n\nDetails as follows,\n\nTraveler Name : {traveler_name}\nContact Email Id : {traveler_email_id}\nPlace Name : {list_title}\nCheck in : {checkin}\nCheck out : {checkout}\nPrice : {market_price}\nHost Name : {host_name}\nHost Email Id : {host_email_id} \n\n--\nThanks and Regards,\n\n{site_name} Team', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td><span style=\"font-family: helvetica;\">Hi Admin,</span></td>\n</tr>\n<tr>\n<td>\n<p><span style=\"font-family: helvetica;\">{traveler_name} sent the reservation request for {list_title} place on {book_date} at {book_time}.</span></p>\n<p><span style=\"font-family: helvetica;\">Details as follows,</span></p>\n<p><span style=\"font-family: helvetica;\">Traveler Name : {traveler_name}</span></p>\n<p><span style=\"font-family: helvetica;\">Contact Email Id : {traveler_email_id}</span></p>\n<p><span style=\"font-family: helvetica;\">Place Name : {list_title}</span></p>\n<p><span style=\"font-family: helvetica;\">Check in : {checkin}</span></p>\n<p><span style=\"font-family: helvetica;\">Check out : {checkout}</span></p>\n<p><span style=\"font-family: helvetica;\">Price : {market_price}</span></p>\n<p><span style=\"font-family: helvetica;\">Host Name : {host_name}</span></p>\n<p><span style=\"font-family: helvetica;\">Host Email Id : {host_email_id}</span></p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\"><span style=\"font-family: helvetica;\">--</span></p>\n<p style=\"margin: 0 0 10px 0;\"><span style=\"font-family: helvetica;\">Thanks and Regards,</span></p>\n<p style=\"margin: 0px;\"><span style=\"font-family: helvetica;\">{site_name} Team</span></p>\n</td>\n</tr>\n</tbody>\n</table>'),
(51, 'traveler_reservation_granted', 'Traveler : After Reservation granted', 'Congrats! Your reservation request is granted.', 'Hi {traveler_name},\n\nCongratulation, Your reservation request is granted by {host_name} for {list_name}.\n\nBelow we mentioned his contact details,\n\nFirst Name : {Fname}\nLast Name : {Lname}\nLive In : {livein}\nPhone No : {phnum}\n\nExact Address is,\n\nStreet Address : {street_address}\nOptional Address : {optional_address}\nCity : {city}\nState : {state}\nCountry : {country}\nZip Code : {zipcode}\n\nHost Message : {comment} \n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Congratulation, Your reservation request is granted by {host_name} for {list_name}.</p>\n<p>Below we mentioned his contact details,</p>\n<p>First Name : {Fname}</p>\n<p>Last Name : {Lname}</p>\n<p>Live In : {livein}</p>\n<p>Phone No : {phnum}</p>\n<p>Exact Address is,</p>\n<p>Street Address : {street_address},</p>\n<p>Optional Address :{optional_address},</p>\n<p>City : {city},</p>\n<p>State : {state},</p>\n<p>Country : {country},</p>\n<p>Zip code : {zipcode}</p>\n<p>Host Message : {comment}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(52, 'traveler_reservation_declined', 'Traveler : After reservation declined', 'Sorry! Your reservation request is denied', 'Hi {traveler_name},\n\nSorry, Your reservation request is denied by {host_name} for {list_title}.\n\nHost Message : {comment} \n\nSoon, We will contact you with the appropriate payment.\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Sorry, Your reservation request is dined by {host_name} for {list_title}.</p>\n<p>Host Message : {comment}</p>\n<p>Soon, We will contact you with the appropriate payment.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(60, 'traveler_reservation_cancel', 'Traveler : After reservation canceled', '{traveler_name} cancelled your reservation list', 'Hi {host_name},\n\nSorry, Your ({status}) reservation list is cancelled by {traveler_name} for {list_title}.\n\n{user_type} Message : {comment} \n\nSure we will contact you soon, if there is any payment balance.\n\nAnd also, if you have any other queries, please feel free to contact us.\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Sorry, Your ({status}) reservation list is canceled by {traveler_name} for {list_title}.</p>\n<p>{user_type} Message : {comment}</p>\n<p>Sure we will contact you soon, if there is any payment balance.</p>\n<p>And also, if you have any other queries, please feel free to contact us.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(53, 'traveler_reservation_expire', 'Traveler : Reservation Expire', 'Sorry! Your {type} request is expired', 'Hi {traveler_name},\n\nYour {type} request is expired.\n\nHost Name : {host_name}\nList Name : {list_title}\nPrice : {currency}{price}\nCheckin Date : {checkin}\nCheckout Date : {checkout}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nYour {type} request is expired.<br /><br />\nHost Name : {host_name}.<br /><br />\nList Name : {list_title}.<br /><br />\nPrice : {currency}{price}.<br /><br />\nCheckin Date : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div>&nbsp;</div>\n</td>\n</tr>\n</tbody>\n</table>'),
(54, 'host_reservation_expire', 'Host : Reservation Expire', '{type} request expired for your list', 'Hi {host_name},\n\n{type} request expired for {list_title} booked by {traveler_name}.\n\nGuest Name : {traveler_name}\nList Name : {list_title}\nPrice : {currency}{price}\nCheckin Date : {checkin}\nCheckout Date : {checkout}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{type} request expired for {list_title} booked by {traveler_name}.<br /><br />\nGuest Name : {traveler_name}.<br /><br />\nList Name : {list_title}.<br /><br />\nPrice : {currency}{price}.<br /><br />\nCheckin Date : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div>&nbsp;</div>\n</td>\n</tr>\n</tbody>\n</table>'),
(55, 'admin_reservation_expire', 'Admin : Reservation Expire', '{type} request is expired', 'Hi Admin,\n\n{traveler_name} {type} request is expired for {list_title}.\n\nGuest Name : {traveler_name}\nGuest Email : {traveler_email}\nHost Name : {host_name}\nHost Email : {host_email}\nList Name : {list_title}\nPrice : {currency}{price}\nCheckin Date : {checkin}\nCheckout Date : {checkout}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} {type} request is expired for {list_title}.<br /><br />\nGuest Name : {traveler_name}.<br /><br />\nGuest Email : {traveler_email}.<br /><br />\nHost Name : {host_name}.<br /><br />\nHost Email : {host_email}.<br /><br />\nList Name : {list_title}.<br /><br />\nPrice : {currency}{price}.<br /><br />\nCheckin Date : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div>&nbsp;</div>\n</td>\n</tr>\n</tbody>\n</table>'),
(56, 'host_reservation_granted', 'Host : After Reservation Granted', 'You have accepted the {traveler_name} reservation request', 'Hi {host_name},\r\n\r\nYou have accepted the {traveler_name} reservation request for {list_title}.\r\n\r\nBelow we mentioned his contact details,\r\n\r\nFirst Name : {Fname}\r\nLast Name : {Lname}\r\nLive In : {livein}\r\nPhone No : {phnum}\r\n\r\n--\r\nThanks and Regards,\r\n\r\nAdmin\r\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi {host_name} ,</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>You have accepted the {traveler_name} reservation request for {list_title}.</p>\r\n<p>Below we mentioned his contact details,</p>\r\n<p>First Name : {Fname}</p>\r\n<p>Last Name : {Lname}</p>\r\n<p>Live In : {livein}</p>\r\n<p>Phone No : {phnum}</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\r\n<p style=\"margin: 0px;\">{site_name}</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(57, 'admin_reservation_granted', 'Admin : After Reservation granted', '{host_name} accepted the {traveler_name} reservation request', 'Hi Admin,\r\n\r\n{host_name} accepted the {traveler_name} reservation request for {list_title}.\r\n\r\n--\r\nThanks and Regards,\r\n\r\n{site_name} Team', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi Admin,</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>{host_name} accepted the {traveler_name} reservation request for {list_title}.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0px;\">{site_name} Team</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(58, 'host_reservation_declined', 'Host : After Reservation Declined', 'You have declined the {traveler_name} reservation request', 'Hi {host_name},\r\n\r\nYou have declined the {traveler_name} reservation request for {list_title}.\r\n\r\n--\r\nThanks and Regards,\r\n\r\nAdmin\r\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi {host_name} ,</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>You have declined the {traveler_name} reservation request} for {list_title}.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\r\n<p style=\"margin: 0px;\">{site_name}</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(59, 'admin_reservation_declined', 'Admin : After Reservation Declined', '{host_name} declined the {traveler_name} reservation request', 'Hi Admin,\r\n\r\n{host_name} declined the {traveler_name} reservation request for {list_title}.\r\n\r\n--\r\nThanks and Regards,\r\n\r\n{site_name} Team', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi Admin,</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>{host_name} declined the {traveler_name} reservation request for {list_title}.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0px;\">{site_name} Team</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(61, 'admin_reservation_cancel', 'Admin : After reservation canceled', '{traveler_name} cancelled the {host_name} reservation list', 'Hi Admin,\n\n{traveler_name} cancelled the {host_name} reservation list ({status}) for {list_title}.\n\n{penalty}\n\n--\nThanks and Regards,\n\n{site_name} Team', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} canceled the {host_name}&nbsp; reservation list ({status}) for {list_title}.\n<br/><br/>{penalty}\n</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0px;\">{site_name} Team</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(62, 'host_reservation_cancel', 'Host : After reservation canceled', '{host_name} cancelled Your reservation list', 'Hi {traveler_name},\n\n{host_name}  cancel the your reservation list ({status}) for {list_title}.\n\n\nIf you have any other queries, please feel free to contact us. \n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name}  cancel the your reservation list ({status}) for {list_title}.<br /></p>\n<p>If you have any other queries, please feel free to contact us.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(63, 'forgot_password', 'Forgot Password', 'Forgot Password', 'Dear Member,\n\nBelow we have mentioned your account details.\n\nHere we go,\n\nEmail_id : {email}\n\nPassword : {password}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Dear Member,</td>\n</tr>\n<tr>\n<td>\n<p>Below we have mentioned your account details.</p>\n<p>Here we go,</p>\n<p>Email_id : {email}</p>\n<p>Password : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(91, 'checkin_host', 'Host: Check In', 'Traveler Check In', 'Hi {host_name},\n\n{traveler_name} is checkin to {list_title}.\n\nGuest Name : {guest_name}\nList Name : {list_title}\nPrice : {currency}{price}\nCheckin Date : {checkin}\nCheckout Date : {checkout}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} is checkin toÂ Â {list_title} Â .<br /><br />\nGuest Name : {traveler_name}.<br /><br />\nList Name : {list_title}.<br /><br />\nPrice : {currency}{price}.<br /><br />\nCheckin Date : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(64, 'users_signin', 'Users Signin', 'Welcome to  {site_name}', 'Dear Member,\r\n\r\nPleasure to meet you and welcome to the {site_name}.\r\n\r\nBelow we have mentioned your account details.\r\n\r\nHere we go,\r\n\r\nEmail_id : {email}\r\n\r\nPassword : {password}\r\n\r\n--\r\nThanks and Regards,\r\n\r\nAdmin\r\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Dear Member,</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>Pleasure to meet you and welcome to the {site_name}.</p>\r\n<p>Below we have mentioned your account details.</p>\r\n<p>Here we go,</p>\r\n<p>Email_id : {email}</p>\r\n<p>Password : {password}</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\r\n<p style=\"margin: 0px;\">{site_name}</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(65, 'reset_password', 'Reset Password', 'Reset Password', 'Dear Member,\n\nBelow we have mentioned your new account details.\n\nHere we go,\n\nPassword : {password}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Dear Member,</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>Below we have mentioned your new account details.</p>\r\n<p>Here we go,</p>\r\n<p>Password : {password}</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\r\n<p style=\"margin: 0px;\">{site_name}</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(66, 'admin_payment', 'Admin payment to Host', 'Admin payed your fees for {list_title}', 'Hello {username},\n\nWe have payed your fees for {list_title}.\n\nDetails as follows,\n\nPlace Name : {list_title}\nCheck in : {checkin}\nCheck out : {checkout}\nPrice : {payed_price}\nPayment Through : {payment_type}\nPay Id: {pay_id}\nPayed Date : {payed_date}\n\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>\n<p>Hi {username} ,</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>We have payed your fees for {list_title}.</p>\n<br />\n<p>Details as follows,</p>\n<p>Place Name : {list_title}</p>\n<p>Check in : {checkin}</p>\n<p>Check out : {checkout}</p>\n<p>Price : {payed_price}</p>\n<p>Payment Through : {payment_type}</p>\n<p>Pay Id : {pay_id}</p>\n<p>Payed Date : {payed_date}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0px;\">{site_name} Team</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(67, 'contact_form', 'Contact Form', 'Message received from contact form', 'Hi Admin,\n\nA message received from contact us page on {date} at {time}.\n\nDetails as follows,\n\nName : {name}\n\nEmail : {email}\n\nMessage : {message}\n\n--\nThanks and Regards,\n\n{site_name} Team', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<p>A message received from contact us page on {date} at {time}.</p>\n<p>Details as follows,</p>\n<p>Name : {name}</p>\n<p>Email : {email}</p>\n<p>Message : {message}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0px;\">{site_name} Team</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(68, 'invite_friend', 'Invite My Friends', '{username} invite You.', 'Hi Friend\'s,\n\n{username} wants you to invite {site_name}\n\n{dynamic_content}\n\n{click_here}\n\n--\nRegards,\n{username}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Friends,</td>\n</tr>\n<tr>\n<td>\n<p>{username} wants you to invite</p>\n<p>{site_name}</p>\n<p>{dynamic_content}</p>\n<p>&nbsp;{click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Regards,</p>\n<p style=\"margin: 0px;\">{username}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(69, 'email_verification', 'Email Verification Link', '{site_name} Email Verification Link', 'Hi {user_name},\n\nPlease Click the below link for your {site_name} email verification.\n\n{click_here}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {user_name},</td>\n</tr>\n<tr>\n<td>\n<p>Please Click the below link for your {site_name} email verification.</p>\n<p>&nbsp;{click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(70, 'referral_credit', 'Referral Credit', 'You are earn {amount} from Referrals', 'Hi {username},\n\nYou are earn the {amount} by {friend_name}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {user_name},</td>\n</tr>\n<tr>\n<td><p>\nYou are earn the {amount} by {friend_name}</p>\n</td>\n</tr><tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(72, 'User_join_to_Referal_mail', 'User join to Referal mail', 'Your Friend Signup', 'Dear {refer_name},\n\nYour friend {friend_name} is now join in {site_name}.Now, $100 is credit in your Travel Credit Possible account.\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Dear {refer_name},</td>\n</tr>\n<tr>\n<td><br />Your friend {friend_name} is now join in {site_name}.Now, $100 is credit in your Travel Credit Possible account.<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(73, 'contact_request_to_host', 'Contact Request to Host', 'Contact Request', 'Hi {host_username},\n		\nPlease click on the following link to contact the user : {link}	\n\nList : {title}\n		\nCheckin Date : {checkin}\n		\nCheckout Date : {checkout}\n		\nGuests : {guest}\n		\nGuest Message  : {message}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_username},</td>\n</tr>\n<tr>\n<td>\nPlease click on the following link to contact the user : {link}<br /><br />\n			\nList : {title}<br /><br />\n		\nCheckin Date : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n		\nGuests : {guest}<br /><br />\n		\nGuest Message  : {message}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(74, 'contact_request_to_guest', 'Contact Request to Guest', 'Contact Request', 'Hi {traveller_username},\n		\nYou have sent contact request to {host_username}.\n\nList : {title}\n		\nCheckin Date : {checkin}\n		\nCheckout Date : {checkout}\n		\nGuests : {guest}\n		\nYour Message  : {message}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveller_username},</td>\n</tr>\n<tr>\n<td>\nYou have sent contact request to {host_username}.<br /><br />\n			\nList : {title}<br /><br />\n		\nCheckin Date : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n		\nGuests : {guest}<br /><br />\n		\nYour Message  : {message}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(75, 'request_granted_to_guest', 'Contact Request Granted to Guest', 'Contact Request Granted', 'Hi {traveller_username},\n\nYour contact request is granted by {host_username}.\n		\nHost Email : {host_email}\n\nList : {title}\n		\nCheckin Date : {checkin}\n		\nCheckout Date : {checkout}\n			\nHost Message  : {message}\n\nURL for Booking  : {link}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveller_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nYour contact request is granted by {host_username}.\n<br /><br />\n\nHost Email : {host_email}<br /><br /> \n\nList : {title}<br /><br /> \n\nCheckin Date : {checkin}<br /><br /> \n\nCheckout Date : {checkout}<br /><br /> \n\nPrice : {price}<br /><br />\n\nHost Message : {message}<br /><br /> \n\nURL for Booking : {link}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(76, 'request_granted_to_host', 'Contact Request Granted to Host', 'Contact Request Granted', 'Hi {host_username},\n\nYou have accept the contact request for {traveller_username}.\n		\nGuest Email : {guest_email}\n\nList : {title}\n		\nCheckin Date : {checkin}\n		\nCheckout Date : {checkout}\n\nPrice : {price}\n				\nYour Message  : {message}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nYou have accept the contact request for {traveller_username}. <br /><br />\n\nGuest Email : {guest_email}<br /><br />	\n		\nList : {title}<br /><br />\n		\nCheckin Date : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n\nPrice : {price}<br /><br />\n	\nYour Message  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(77, 'contact_request_to_admin', 'Contact Request to Admin', 'Contact Request', 'Hi Admin,\n		\n{traveller_username} sent contact request to {host_username}.\n\nList : {title}\n		\nCheckin Date : {checkin}\n		\nCheckout Date : {checkout}\n		\nGuests : {guest}\n\nPrice : {price}\n		\nGuest Message  : {message}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td><br />\n{traveller_username} sent contact request to {host_username}. <br /><br />		\nList : {title}<br /><br />	\nCheckin Date : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n	\nPrice : {price}<br /><br />\n		\nGuest Message  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(78, 'request_granted_to_admin', 'Contact Request Granted to Admin', 'Contact Request Granted', 'Hi Admin,\n\n{host_username} accept the contact request for {traveller_username}.\n		\nGuest Email : {guest_email}\n\nHost Email : {host_email}\n\nList : {title}\n		\nCheckin Date : {checkin}\n		\nCheckout Date : {checkout}\n\nPrice : {price}\n				\nHost Message  : {message}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td><br />\n{host_username} accept the contact request for {traveller_username}.<br /><br />\n\nGuest Email : {guest_email}<br /><br />	\n\nHost Email : {host_email}<br /><br />	\n\nList : {title}<br /><br />	\n\nCheckin Date : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n\nPrice : {price}<br /><br />	\n			\nHost Message  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(79, 'special_request_granted_to_guest', 'Contact Request Granted with Special Offer to Guest', 'Contact Request Granted with Special Offer', 'Hi {traveller_username},\n\n{host_username} accept your contact request with special offer.\n		\nHost Email : {host_email}\n\nList : {title}\n		\nCheckin Date : {checkin}\n		\nCheckout Date : {checkout}\n\nPrice : {price}\n				\nMessage  : {message}\n\nURL for Booking  : {link}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveller_username},</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} accept your contact request with special offer.<br /><br />\n\nHost Email : {host_email}<br /><br />	\n\nList : {title}<br /><br />	\n\nCheckin Date : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n\nPrice : {price}<br /><br />\n			\nMessage  : {message}<br /><br />\n\nURL for Booking  : {link}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(80, 'special_request_granted_to_host', 'Contact Request Granted with Special Offer to Host', 'Contact Request Granted with Special Offer', 'Hi {host_username},\n\nYou have accept the {traveller_username} contact request with special offer.\n		\nGuest Email : {guest_email}\n\nList : {title}\n		\nCheckin Date : {checkin}\n		\nCheckout Date : {checkout}\n\nPrice : {price}\n			\nYour Message  : {message}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_username},</td>\n</tr>\n<tr>\n<td><br />\n\nYou have accept the {traveller_username} contact request with special offer.<br /><br />\n\nGuest Email : {guest_email}<br /><br />\n\nList : {title}<br /><br />	\n\nCheckin Date : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n\nPrice : {price}<br /><br />\n			\nYour Message  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(81, 'special_request_granted_to_admin', 'Contact Request Granted with Special Offer to Admin', 'Contact Request Granted with Special Offer', 'Hi Admin,\n\n{host_username} have accept the {traveller_name} contact request with special offer.\n		\nGuest Email : {guest_email}\n\nHost Email : {host_email}\n\nList : {title}\n		\nCheckin Date : {checkin}\n		\nCheckout Date : {checkout}\n\nPrice : {price}\n			\nHost Message  : {message}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} have accept the {traveller_username} contact request with special offer.<br /><br />\n\nGuest Email : {guest_email}<br /><br />\n\nHost Email : {host_email}<br /><br />\n\nList : {title}<br /><br />	\n\nCheckin Date : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n\nPrice : {price}<br /><br />\n			\nHost Message  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(82, 'request_cancel_to_guest', 'Contact Request Cancelled to Guest', 'Sorry! your contact request is cancelled', 'Hi {traveller_username},\n\nSorry, Your contact request is denied by {host_username} for {title}.\n\nHost Message : {message}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveller_username} ,</td>\n</tr>\n<tr>\n<td>\n<p>Sorry, Your contact request is denied by {host_username} for {title}.</p><br /><br />\nHost Message : {message}<br /><br />\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(83, 'request_cancel_to_host', 'Contact Request Cancelled to Host', 'You have cancelled the contact request', 'Hi {host_username},\n\nYou have cancelled the {traveller_username} contact request for {title}.\n\nYour message : {message}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_username} ,</td>\n</tr>\n<tr>\n<td>\n<p>You have cancelled the {traveller_username} contact request for {title}.</p><br /><br />\nYour message : {message}<br /><br />\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(85, 'checkout_admin', 'Admin: Check Out', 'Traveler Check Out', 'Hi Admin,\n\n{traveler_name} is checkouted from {list_title}.\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} is checkouted form {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(84, 'request_cancel_to_admin', 'Contact Request Cancelled to Admin', '{host_username} cancelled the {traveller_username} contact request', 'Hi Admin,\n\n{host_username} cancelled the {traveller_username} contact request for {title}.\n\nHost message : {message}\n\n--\nThanks and Regards,\n\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{host_username} cancelled the {traveller_username} contact request for {title}.</p>\n<br /><br />\nHost message : {message}.\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(86, 'checkout_host', 'Host: Check Out', 'Traveler Check Out', 'Hi {host_name},\n\n{traveler_name} is checkout from {list_title} .\n\nWrite your review and feedback\n{Click_here}\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} is checkout fromÂ Â {list_title} Â .</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>Write your review and feedback</p> </br></br><p>{Click_here} </p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(87, 'checkout_traveler', 'Traveler: Check Out', 'Your Check Out', 'Hi {traveler_name},\n\nThank you,You are checkout from {list_title}.\n\nWrite your review and feedback\n{Click_here}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Thank you,You are sucessfully checkout from {list_title}.</p>\n<p>Â </p>\n</td>\n</tr>\n<tr>\n<td>\n<p>Write your review and feedback</p> </br></br> <p>{Click_here} </p>\n</td>\n</tr>\n<tr>\n<td>\n<p>Thanks and Regards,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(88, 'contact_discuss_more_to_guest', 'Contact Request - Discuss more to Guest', 'Contact Request - Discuss more', 'Hi {traveller_username},\r\n\r\n{host_username} wants to discuss more with you.\r\n\r\nHost Email : {host_email}\r\n\r\nList : {title}\r\n		\r\nCheckin Date : {checkin}\r\n		\r\nCheckout Date : {checkout}\r\n				\r\nMessage  : {message}\r\n\r\n--\r\nThanks and Regards,\r\nAdmin\r\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi {traveller_username},</td>\r\n</tr>\r\n<tr>\r\n<td><br />\r\n\r\n{host_username} wants to discuss more with you.<br /><br />\r\n\r\nHost Email : {host_email}<br /><br />	\r\n\r\nList : {title}<br /><br />	\r\n\r\nCheckin Date : {checkin}<br /><br />\r\n		\r\nCheckout Date : {checkout}<br /><br />\r\n			\r\nMessage  : {message}<br /><br />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\r\n<p style=\"margin: 0px;\">{site_name}</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(89, 'contact_discuss_more_to_host', 'Contact Request - Discuss more to Host', 'Contact Request - Discuss more', 'Hi {host_username},\r\n\r\nYou wants to discuss more with {traveller_username}.\r\n\r\nGuest Email : {guest_email}\r\n\r\nList : {title}\r\n		\r\nCheckin Date : {checkin}\r\n		\r\nCheckout Date : {checkout}\r\n				\r\nYour Message  : {message}\r\n\r\n--\r\nThanks and Regards,\r\nAdmin\r\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi {host_username},</td>\r\n</tr>\r\n<tr>\r\n<td><br />\r\n\r\nYou wants to discuss more with {traveller_username}.<br /><br />\r\n\r\nGuest Email : {guest_email}<br /><br />	\r\n\r\nList : {title}<br /><br />	\r\n\r\nCheckin Date : {checkin}<br /><br />\r\n		\r\nCheckout Date : {checkout}<br /><br />\r\n			\r\nYour Message  : {message}<br /><br />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\r\n<p style=\"margin: 0px;\">{site_name}</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(90, 'contact_discuss_more_to_admin', 'Contact Request - Discuss more to Admin', 'Contact Request - Discuss more', 'Hi Admin,\r\n\r\n{host_username} wants to discuss more with {traveller_username}.\r\n\r\nGuest Email : {guest_email}\r\n\r\nHost Email : {host_email}\r\n\r\nList : {title}\r\n		\r\nCheckin Date : {checkin}\r\n		\r\nCheckout Date : {checkout}\r\n				\r\nHost Message  : {message}\r\n\r\n--\r\nThanks and Regards,\r\nAdmin\r\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi Admin,</td>\r\n</tr>\r\n<tr>\r\n<td><br />\r\n\r\n{host_username} wants to discuss more with {traveller_username}.<br /><br />\r\n\r\nGuest Email : {guest_email}<br /><br />\r\n\r\nHost Email : {host_email}<br /><br />\r\n\r\nList : {title}<br /><br />\r\n\r\nCheckin Date : {checkin}<br /><br />\r\n		\r\nCheckout Date : {checkout}<br /><br />\r\n			\r\nHost Message  : {message}<br /><br />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style=\"margin: 0 10px 0 0;\">--</p>\r\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\r\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\r\n<p style=\"margin: 0px;\">{site_name}</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(92, 'checkin_admin', 'Admin: CheckIn', 'Traveler Check In', 'Hi Admin,\n\n{traveler_name} is checkin to {list_title}.\n\nGuest Name : {traveler_name}\nGuest Email : {traveler_email}\nHost Name : {host_name}\nHost Email : {host_email}\nList Name : {list_title}\nPrice : {currency}{price}\nCheckin Date : {checkin}\nCheckout Date : {checkout}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} is checkin to {list_title}.<br /><br />\nGuest Name : {traveler_name}.<br /><br />\nGuest Email : {traveler_email}.<br /><br />\nHost Name : {host_name}.<br /><br />\nHost Email : {host_email}.<br /><br />\nList Name : {list_title}.<br /><br />\nPrice : {currency}{price}.<br /><br />\nCheckin Date : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(93, 'checkin_traveller', 'Traveler: Check In', 'Your Check In', 'Hi {traveler_name},\n\nThank you,You are checkin to {list_title}.\n\nHost Name : {host_name}\nList Name : {list_title}\nPrice : {currency}{price}\nCheckin Date : {checkin}\nCheckout Date : {checkout}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nThank you,You are sucessfully checkin to {list_title}.<br /><br />\nHost Name : {host_name}.<br /><br />\nList Name : {list_title}.<br /><br />\nPrice : {currency}{price}.<br /><br />\nCheckin Date : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(94, 'refund_admin', 'Admin: Refund', 'Refund from Admin', 'Hi Admin,\n\nYou have refunded the {refund_amt} amount to {name} {account} account.\n\nGuest Name : {traveler_name}\nGuest Email : {traveler_email}\nHost Name : {host_name}\nHost Email : {host_email}\nList Name : {list_title}\nPrice : {currency}{price}\nCheckin Date : {checkin}\nCheckout Date : {checkout}\nRefunded Amount : {refund_amt}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\nYou have refunded the {refund_amt} amount to {name} {account} account.<br /><br />\nGuest Name : {traveler_name}.<br /><br />\nGuest Email : {traveler_email}.<br /><br />\nHost Name : {host_name}.<br /><br />\nHost Email : {host_email}.<br /><br />\nList Name : {list_title}.<br /><br />\nPrice : {currency}{price}.<br /><br />\nCheckin Date : {checkin}.<br /><br />\nCheckout Date : {checkout}.<br /><br />\nRefunded Amount : {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div>&nbsp;</div>\n</td>\n</tr>\n</tbody>\n</table>');
INSERT INTO `email_templates` (`id`, `type`, `title`, `mail_subject`, `email_body_text`, `email_body_html`) VALUES
(95, 'refund_host', 'Host: Refund', 'Refund from Admin', 'Hi {host_name},\r\n\r\nAdmin refunded the {refund_amt} amount to your {account} account.\r\n\r\nGuest Name : {traveler_name}\r\nList Name : {list_title}\r\nPrice : {currency}{price}\r\nCheckin Date : {checkin}\r\nCheckout Date : {checkout}\r\nRefunded Amount : {refund_amt}\r\n\r\n--\r\nThanks and Regards,\r\n\r\nAdmin\r\n{site_name}\r\n', '<table cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi {host_name},</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<br />\r\nAdmin refunded the {refund_amt} amount to your {account} account..<br /><br />\r\nGuest Name : {traveler_name}.<br /><br />\r\nList Name : {list_title}.<br /><br />\r\nPrice : {currency}{price}.<br /><br />\r\nCheckin Date : {checkin}.<br /><br />\r\nCheckout Date : {checkout}.<br /><br />\r\nRefunded Amount : {refund_amt}\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>--</p>\r\n<p>Thanks and Regards,</p>\r\n<p>{site_name} Team</p>\r\n<div>&nbsp;</div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(96, 'refund_guest', 'Traveler: Refund', 'Refund from Admin', 'Hi {traveler_name},\n\nAdmin refunded the {refund_amt} amount to your {account} account.\n\nHost Name : {host_name}\nList Name : {list_title}\nPrice : {currency}{price}\nCheckin Date : {checkin}\nCheckout Date : {checkout}\nRefunded Amount : {refund_amt}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nAdmin refunded the {refund_amt} amount to your {account} account.<br /><br />\nHost Name : {host_name}.<br /><br />\nList Name : {list_title}.<br /><br />\nPrice : {currency}{price}.<br /><br />\nCheckin Date : {checkin}.<br /><br />\nCheckout Date : {checkout}.<br /><br />\nRefunded Amount : {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div>&nbsp;</div>\n</td>\n</tr>\n</tbody>\n</table>'),
(97, 'list_create_host', 'List creation to Host', 'You have created the new list', 'Hi {host_name},\r\n\r\nYou have created the new list.\r\n\r\nList name : {list_title}\r\n\r\nLink : {link}\r\n\r\nPrice : {price}\r\n\r\n\r\n--\r\nThanks and Regards,\r\nAdmin\r\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi {host_name},</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<br />\r\nYou have created the new list.<br /><br />\r\nList Name : {list_title}.<br /><br />\r\nLink : {link}.<br /><br />\r\nPrice : {price}.\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>--</p>\r\n<p>Thanks and Regards,</p>\r\n<p>{site_name} Team</p>\r\n<div>&nbsp;</div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(98, 'list_create_admin', 'List creation to Admin', '{host_name} have created the new list', 'Hi Admin,\r\n\r\n{host_name} have created the new list.\r\n\r\nHost name : {host_name}\r\n\r\nList name : {list_title}\r\n\r\nLink : {link}\r\n\r\nPrice : {price}\r\n\r\n\r\n--\r\nThanks and Regards,\r\nAdmin\r\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi Admin,</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<br />\r\n{host_name} have created the new list.<br /><br />\r\nHost name : {host_name}.<br /><br />\r\nList name : {list_title}.<br /><br />\r\nLink : {link}.<br /><br />\r\nPrice : {price}.\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>--</p>\r\n<p>Thanks and Regards,</p>\r\n<p>{site_name} Team</p>\r\n<div>&nbsp;</div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(99, 'list_delete_host', 'List deletion to Host', 'You have deleted the list', 'Hi {host_name},\r\n\r\nYou have deleted the list.\r\n\r\nList name : {list_title}\r\n\r\n--\r\nThanks and Regards,\r\nAdmin\r\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hi {host_name},</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<br />\r\nYou have deleted the list.<br /><br />\r\nList Name : {list_title}.\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>--</p>\r\n<p>Thanks and Regards,</p>\r\n<p>{site_name} Team</p>\r\n<div>&nbsp;</div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(100, 'list_delete_admin', 'List deletion to Admin', '{host_name} have deleted the list', 'Hi Admin,\r\n\r\n{host_name} have deleted the list.\r\n\r\nHost name : {host_name}\r\n\r\nList name : {list_title}\r\n\r\n\r\n--\r\nThanks and Regards,\r\nAdmin\r\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{host_name} have deleted the list.<br /><br />\nHost name : {host_name}.<br /><br />\nList name : {list_title}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div>&nbsp;</div>\n</td>\n</tr>\n</tbody>\n</table>'),
(102, 'payment_issue_to_admin', 'Payment issue mail to Admin', 'Payment misconfigured', 'Hi Admin,\n\n{payment_type} API credentials are misconfigured.\n\n{username} tried to make the payment.\n\nEmail ID : {email_id}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{payment_type} API credentials are misconfigured.</p>\n<p>{username} tried to make the payment.</p>\n<p>Email ID : {email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(104, 'refund_host_commission', 'Host: Accept Commission Refund', 'Refunding the Host Accept Commission from Admin', 'Hi {host_name},\n\nAdmin refunded the {refund_amt} amount to your {account} account.\n\nList Name : {list_title}\nRefunded Amount : {refund_amt}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>Admin refunded the {refund_amt} amount to your {account} account.</p>\n<p>List Name : {list_title}</p>\n<p>Refunded Amount : {refund_amt}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(105, 'refund_host_commission_admin', 'Admin: Accept Commission Refund', 'You have Refunding the Host Accept Commission', 'Hi Admin,\n\nYou have refunded the {refund_amt} amount to your {account} account.\n\nList Name : {list_title}\nHost Name : {host_name}\nRefunded Amount : {refund_amt}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<p>You have refunded the {refund_amt} amount to your {account} account.</p>\n<p>List Name : {list_title}</p>\n<p>Host Name : {host_name}</p>\n<p>Refunded Amount : {refund_amt}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(111, 'passport_verification', 'Passport verification for administrator', '{user_name} sent the passport verification  request ,userid-{user_id}', 'Hello Admin,\\n\\n{user_name}sent the passport verification request,user id {user_id}\\n\\nDetails as follows,\\n\\nUser Name : {user_name}\\nUser Id : {user_id}\\n\\n\\n\\n--\\nThanks and Regards,\\n\\n{user_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\\n<tbody>\\n<tr>\\n<td>Hi Admin,</td>\\n</tr>\\n<tr>\\n<td>\\n<p>{user_name} sent the passport verification request ,user id {user_id}.</p>\\n<p>Details as follows,</p>\\n<p>User Name : {user_name}</p>\\n<p>User Id : {user_id}</p>\\n\\n</td>\\n</tr>\\n<tr>\\n<td>\\n<p style=\"margin: 0 10px 0 0;\">--</p>\\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\\n<p style=\"margin: 0px;\">{user_name} </p>\\n</td>\\n</tr>\\n</tbody>\\n</table>'),
(112, 'change password to user', 'admin change to the password', 'Welcome to  {site_name}', 'admin change the passwordOld password:{new_password}Conform password:{conform_password}Dear Member,Old password:{new_password}Conform password:{conform_password}--Thanks and Regards,Admin{site_name} ', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Dear Member,</td>\n</tr>\n<tr>\n<td>\n<p>&nbsp;You have successfully changed your password</p>\n<p>Please use the below password details for login.</p>\n<p>Old password:{new_password}</p>\n<p><span>New password:{conform_password}</span></p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n<div>&nbsp;</div>\n</td>\n</tr>\n</tbody>\n</table>'),
(113, 'spam', 'Spam List', 'Admin Message - Listing reported as Spam!', '****ADMIN MESSAGE****\n\n\n\nThe below listing has been reported as Spam:\n\nURL for Spam Listing : {list_url}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>****ADMIN MESSAGE****</td>\n</tr>\n<tr>\n<td>\n<p>&nbsp;FLEXIFLAT.COM SPAM REPORT!</p>\n<p>The below listing has been reported as Spam:<br /><br />{list_url}</p>\n</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n</tr>\n</tbody>\n</table>\n<p>&nbsp;</p>'),
(114, 'host_notify_review', 'Host Notification for Review', '{guest_name} has a review from {host_name}', 'Hi {guest_name},\n\n{host_name} has added a review about you in {list_name} on {site_name}\n\nAnd he is waiting for your review.\nHost name : {host_name}\nList name : {list_name}\n\n\n--\nThanks and Regards,\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {guest_name},</td>\n</tr>\n<tr>\n<td><br/ > {host_name} has added a review about you in {list_name} on {site_name} <br />And he is waiting for your review.\n<br/>\nHost name : {host_name}<br/>\nList name : {list_name}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div>&nbsp;</div>\n</td>\n</tr>\n</tbody>\n</table>'),
(115, 'guest_notify_review', 'Guest Notification for Review', '{guest_name} has checked out.', 'Hi {host_name},\n\n{guest_name} has checkouted from your list {list_name} on {site_name}\n\nAnd he is waiting for your review.\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_name},</td>\n</tr>\n<tr>\n<td><br /> {guest_name} has checkouted from your list {list_name} on {site_name} <br />And he is waiting for your review.</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div>&nbsp;</div>\n</td>\n</tr>\n</tbody>\n</table>'),
(117, 'list_notification', 'Notification for calendar update', 'The list {list_name} needs an update', 'Hello {host_name},\n\nThe {list_name} you listed in {site_name} don\'t have any updates in last month.\n\nTo improve your ranking in the search results please update your list \n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hello {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>\nThe {list_name} you listed in {site_name} don\'t have any updates in last month.</p>\n<p>\nTo improve your ranking in the search results please update your list \n</p>\n\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>\n'),
(116, 'host_notification', 'Upcoming notification for host', 'The {traveler_name} is having a reservation on your {list_title} on tomorrow', 'Hello {host_name},\r\n\r\n{traveler_name} booked the {list_title} place on tomorrow.\r\n\r\nDetails as follows,\r\n\r\nTraveler Name : {traveler_name}\r\nContact Email Id : {traveler_email_id}\r\nPlace Name : {list_title}\r\nCheck in : {checkin}\r\nCheck out : {checkout}\r\n\r\n\r\n--\r\nThanks and Regards,\r\n\r\nAdmin\r\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Hello {host_name},</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>{traveler_name} booked the {list_title} place on tomorrow.</p>\r\n<p>Details as follows,</p>\r\n\r\n<p>Traveler Name : {traveler_name}</p>\r\n<p>Contact Email Id : {traveler_email_id}</p>\r\n<p>Place Name : {list_title}</p>\r\n<p>Check in : {checkin}</p>\r\n<p>Check out : {checkout}</p>\r\n\r\n\r\n</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>--</p>\r\n<p>Thanks and Regards,</p>\r\n<p>Admin</p>\r\n<p>{site_name}</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n'),
(121, 'tc_book_to_admin_fr', 'Administrative notification for booking travel booking', '{traveler_name} EnvoyÃ© la demande de rÃ©servation en utilisant son Travel Cretids', 'Bonjour Admin,\n\n{traveler_name}EnvoyÃ© la demande de rÃ©servation pour rÃ©server la{list_title} placer sur {book_date} Ã  {book_time} En utilisant ses crÃ©dits de voyage.\n\nDÃ©tails comme suit,\n\nNom du voyageur : {traveler_name}\nIdentifiant du courrier Ã©lectronique de contact : traveler_email_id}\nNom du lieu : {list_title}\nEnregistrement : {checkin}\nCheck-out : {checkout}\nPrix â€‹â€‹du marchÃ© : {market_price}\nMontant payÃ© : {payed_amount}\nCrÃ©dits de voyage : {travel_credits} \nNom d\'hÃ´te : {host_name}\nIdentifiant du courrier hÃ´te : {host_email_id} \n\n--\nMerci et salutations,\n\n{site_name} Ã‰quipe', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name}EnvoyÃ© la demande de rÃ©servation pour rÃ©server la {list_title} placer sur {book_date} a {book_time}En utilisant ses crÃ©dits de voyage.</p>\n<p>DÃ©tails comme suit,</p>\n<p>Nom du voyageur {traveler_name}</p>\n<p>Identifiant du courrier Ã©lectronique de contact : {traveler_email_id}</p>\n<p>Place Name : {list_title}</p>\n<p>Nom du lieu : {checkin}</p>\n<p>Check-out : {checkout}</p>\n<p>Prix â€‹â€‹du marchÃ© : {market_price}</p>\n<p>Montant payÃ©: {payed_amount}</p>\n<p>CrÃ©dits de voyage : {travel_credits}</p>\n<p>Nom d\'hÃ´te : {host_name}</p>\n<p>Identifiant du courrier hÃ´te: {host_email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0px;\">{site_name} Ã‰quipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(122, 'tc_book_to_host_fr', 'Host notification for  Travel cretid booking', '{traveler_name} EnvoyÃ© la demande de rÃ©servation en utilisant son Travel Cretids', 'Bonjour {username},\n\n{traveler_name}EnvoyÃ© la demande de rÃ©servation pour rÃ©server votre {list_title} placer sur {book_date} Ã  {book_time} en utilisant ses crÃ©dits de voyage.\n\nNous vous contacterons avec le paiement appropriÃ©.\n\nDÃ©tails comme suit,\n\nNom du voyageur : {traveler_name}\nIdentifiant du courrier Ã©lectronique de contact : traveler_email_id}\nNom du lieu : {list_title}\nEnregistrement : {checkin}\nCheck-out : {checkout}\nPrix : {market_price}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {username} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name}EnvoyÃ© la demande de rÃ©servation pour rÃ©server votre {list_title} placer sur{book_date} Ã  {book_time} en utilisant ses crÃ©dits de voyage.</p>\n<p>DÃ©tails comme suit,</p>\n<p>Nom du voyageur : {traveler_name}</p>\n<p>Identifiant du courrier Ã©lectronique de contact : {traveler_email_id}</p>\n<p>Nom du lieu : {list_title}</p>\n<p>Enregistrement : {checkin}</p>\n<p>Check-out : {checkout}</p>\n<p>Prix : {market_price}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(123, 'admin_mass_email_fr', 'Admin mass email', '{subject}', 'Bonjour,\n\n{dynamic_content}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Bonjour,</td>\n</tr>\n<tr>\n<td>\n<p>{dynamic_content}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(124, 'user_reference_fr', 'Reference', 'Ã‰crire {username} Une rÃ©fÃ©rence sur {site_name}', 'Sault,\n\n{username} Vous demande de fournir une rÃ©fÃ©rence qu\'ils peuvent afficher publiquement sur leur {site_name} Profil. {site_name} est un marchÃ© communautaire pour les logements qui est construit sur la confiance et la rÃ©putation. Une rÃ©fÃ©rence de vous serait vraiment aider Ã  construire leur rÃ©putation avec la communautÃ©.\nUne rÃ©fÃ©rence sur {site_name} peut aider {username} Trouver un endroit frais pour rester ou accueillir des voyageurs intÃ©ressants.\n\n{click_here} Avoir une rÃ©fÃ©rence pour {username}.\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut,</td>\n</tr>\n<tr>\n<td>\n<p>{username} Vous demande de fournir une rÃ©fÃ©rence qu\'ils peuvent afficher publiquement sur leur {site_name} profil. {site_name} iest un marchÃ© communautaire pour les logements qui est construit sur la confiance et la rÃ©putation. Une rÃ©fÃ©rence de vous serait vraiment aider Ã  construire leur rÃ©putation avec la communautÃ©.\nUne rÃ©fÃ©rence sur {site_name} peut aider {username} Trouver un endroit frais pour rester ou accueillir des voyageurs intÃ©ressants.\n</p>\n<p>{click_here} Avoir une rÃ©fÃ©rence pour {username}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Merci et salutations,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(125, 'reference_receive_fr', 'Reference Receive', 'Vous avez reÃ§u une nouvelle rÃ©fÃ©rence sur {site_name}!', 'Salut {username},\n\n{other_username} A Ã©crit une rÃ©fÃ©rence pour vous. Avant de passer Ã  votre profil public, vous pouvez le consulter et l\'accepter ou l\'ignorer. {click_here} Pour afficher la rÃ©fÃ©rence.\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {username},</td>\n</tr>\n<tr>\n<td>\n<p>{other_username} A Ã©crit une rÃ©fÃ©rence pour vous. Avant de passer Ã  votre profil public, vous pouvez le consulter et l\'accepter ou l\'ignorer. {click_here}  Pour afficher la rÃ©fÃ©rence.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Merci et salutations,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(126, 'host_reservation_notification_fr', 'Reservation notification for host', 'La RÃ©servation a Ã©tÃ© demandÃ©e par {traveler_name}', 'Bonjour {username},\n\n{traveler_name} A rÃ©servÃ© le {list_title} placer sur {book_date} a {book_time}.\n\nDÃ©tails comme suit,\n\nNom du voyageur : {traveler_name}\nIdentifiant du courrier Ã©lectronique de contact : traveler_email_id}\nNom du lieu : {list_title}\nEnregistrement : {checkin}\nCheck-out : {checkout}\nPrix : {market_price}\n\nVeuillez donner la confirmation en cliquant sur l\'action ci-dessous.\n{action_url}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>\n<p>Salut {username} ,</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} A rÃ©servÃ© le {list_title} placer sur {book_date} a {book_time}.</p>\n<br />\n<p>DÃ©tails comme suit,</p>\n<p>Nom du voyageur : {traveler_name}</p>\n<p>Identifiant du courrier Ã©lectronique de contact : {traveler_email_id}</p>\n<p>Nom du lieu : {list_title}</p>\n<p>Enregistrement : {checkin}</p>\n<p>Check-out : {checkout}</p>\n<p>Prix : {market_price}</p>\n<br />\n<p>Veuillez donner la confirmation en cliquant sur l\'action ci-dessous.</p>\n<p>{action_url}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0px;\">{site_name} Ã‰quipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(127, 'traveller_reservation_notification_fr', 'Reservation notification for  traveller', 'Votre demande de rÃ©servation est envoyÃ©e avec succÃ¨s', 'Bonjour {traveler_name},\n\nVotre demande de rÃ©servation est envoyÃ©e avec succÃ¨s Ã  l\'hÃ´te appropriÃ©.\n\nVeuillez attendre sa confirmation.\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Votre demande de rÃ©servation est envoyÃ©e avec succÃ¨s Ã  l\'hÃ´te appropriÃ©.</p>\n<p>Veuillez attendre sa confirmation.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(128, 'admin_reservation_notification_fr', 'Reservation notification for  administrator', '{traveler_name} EnvoyÃ© la demande de rÃ©servation {host_name}', 'Bonjour Admin,\n\n{traveler_name} EnvoyÃ© la demande de rÃ©servation {list_title} placer sut {book_date} a {book_time}.\n\nDÃ©tails comme suit,\n\nNom du voyageur : {traveler_name}\nIdentifiant du courrier Ã©lectronique de contact : {traveler_email_id}\nNom du lieu: {list_title}\nEnregistrement : {checkin}\nCheck-out : {checkout}\nPrix : {market_price}\nNom d\'hÃ´te : {host_name}\nIdentifiant du courrier hÃ´te : {host_email_id} \n\n--\nMerci et salutations,\n\n{site_name}\nÃ‰quipe', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} EnvoyÃ© la demande de rÃ©servation  {list_title} placer sur {book_date} a {book_time}.</p>\n<p>\nDÃ©tails comme suit,</p>\n<p>\nNom du voyageur  : {traveler_name}</p>\n<p>Identifiant du courrier Ã©lectronique de contact : {traveler_email_id}</p>\n<p>Nom du lieu : {list_title}</p>\n<p>Enregistrement : {checkin}</p>\n<p>Check-out : {checkout}</p>\n<p>Prix : {market_price}</p>\n<p>Nom d\'hÃ´te : {host_name}</p>\n<p>Identifiant du courrier hÃ´te: {host_email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0px;\">{site_name} Ã‰quipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(129, 'traveler_reservation_granted_fr', 'Traveler : After Reservation granted', 'FÃ©licitations! Votre demande de rÃ©servation est accordÃ©e.', 'Salut {traveler_name},\n\nFÃ©licitations! Votre demande de rÃ©servation est accordÃ©e par.{host_name} pour {list_name}.\n\nCi-dessous nous avons mentionnÃ© ses coordonnÃ©es,\n\nPrÃ©nom : {Fname}\nNom de famille: {Lname}\nVivre dans: {livein}\nPas de tÃ©lÃ©phone : {phnum}\n\nAdresse exacte est,\n\nAdresse de rue : {street_address}\nAdresse facultative : {optional_address}\nVille : {city}\nEtat: {state}\nPays : {country}\nCode postal : {zipcode}\n\nMessage d\'hÃ´te : {comment} \n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>FÃ©licitations! Votre demande de rÃ©servation est accordÃ©e par. {host_name} pour {list_name}.</p>\n<p>Ci-dessous nous avons mentionnÃ© ses coordonnÃ©es,</p>\n<p>PrÃ©nom : {Fname}</p>\n<p>Nom de famille: {Lname}</p>\n<p>Vivre dans : {livein}</p>\n<p>Pas de tÃ©lÃ©phone: {phnum}</p>\n<p>Adresse exacte est,</p>\n<p>Adresse de rue : {street_address},</p>\n<p>Adresse facultative :{optional_address},</p>\n<p>Ville : {city},</p>\n<p>Etat : {state},</p>\n<p>Pays : {country},</p>\n<p>Code postal : {zipcode}</p>\n<p>Message d\'hÃ´te : {comment}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(130, 'traveler_reservation_declined_fr', 'Traveler : After reservation declined', 'Pardon! Votre demande de rÃ©servation est refusÃ©e', 'Salut {traveler_name},\n\nDÃ©solÃ©, votre demande de rÃ©servation est dinee par {host_name} pour {list_title}.\n\nMessage d\'hÃ´te : {comment} \n\nBientÃ´t, nous vous contacterons avec le paiement appropriÃ©.\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>DÃ©solÃ©, votre demande de rÃ©servation est dinee par {host_name} pour {list_title}.</p>\n<p>Message d\'hÃ´te : {comment}</p>\n<p>BientÃ´t, nous vous contacterons avec le paiement appropriÃ©.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(131, 'traveler_reservation_cancel_fr', 'Traveler : After reservation canceled', '{traveler_name} AnnulÃ© votre liste de rÃ©servation', 'Salut {host_name},\n\nDÃ©solÃ©, votre ({status}) La liste de rÃ©servation est annulÃ©e par{traveler_name} pour {list_title}.\n\n{user_type} Message : {comment} \n\nBien sÃ»r, nous vous contacterons bientÃ´t, s\'il ya un solde de paiement.\n\nEt aussi, si vous avez d\'autres questions, s\'il vous plaÃ®t n\'hÃ©sitez pas Ã  nous contacter.\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>DÃ©solÃ©, votre ({status}) La liste de rÃ©servation est annulÃ©e par {traveler_name} pour {list_title}.</p>\n<p>{user_type} Message : {comment}</p>\n<p>Bien sÃ»r, nous vous contacterons bientÃ´t, s\'il ya un solde de paiement.</p>\n<p>Et aussi, si vous avez d\'autres questions, s\'il vous plaÃ®t n\'hÃ©sitez pas Ã  nous contacter.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(132, 'traveler_reservation_expire_fr', 'Traveler : Reservation Expire', 'Pardon! Votre {type} La demande est expirÃ©e', 'Salut {traveler_name},\n\nVotre {type} Demande est expirÃ©e.\n\nNom d\'hÃ´te : {host_name}\nListe de noms : {list_title}\nPrix : {currency}{price}\nDate d\'arrivÃ©e : {checkin}\nDate de paiement : {checkout}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nVotre {type}Demande est expirÃ©e..<br /><br />\nNom d\'hÃ´te : {host_name}.<br /><br />\nListe de noms : {list_title}.<br /><br />\nPrix : {currency}{price}.<br /><br />\nDate d\'arrivÃ©e : {checkin}.<br /><br />\nDate de paiement : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Merci et salutations,</p>\n<p>{site_name} Ã‰quipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(133, 'host_reservation_expire_fr', 'Host : Reservation Expire', '{type} Demande expirÃ©e pour votre liste', 'Salut {host_name},\n\n{type} Demande expirÃ©e pour {list_title} rÃ©servÃ© par{traveler_name}.\n\nNom de l\'invitÃ© : {traveler_name}\nListe de noms : {list_title}\nPrix : {currency}{price}\nDate d\'arrivÃ©e : {checkin}\nDate de paiement : {checkout}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{type} Demande expirÃ©e pour {list_title} rÃ©servÃ© par{traveler_name}.\n<br /><br />\nNom de l\'invitÃ© : {traveler_name}.<br /><br />\nListe de noms : {list_title}.<br /><br />\nPrix : {currency}{price}.<br /><br />\nDate d\'arrivÃ©e : {checkin}.<br /><br />\nDate de paiement : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Merci et salutations,</p>\n<p>{site_name} Ã‰quipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(134, 'admin_reservation_expire_fr', 'Admin : Reservation Expire', '{type} La demande est expirÃ©e', 'Salut Admin,\n\n{traveler_name} {type} La demande est expirÃ©e {list_title}.\n\nNom de l\'invitÃ© : {traveler_name}\nCourriel des invitÃ©s : {traveler_email}\nNom d\'hÃ´te : {host_name}\nCourriel d\'hÃ´te : {host_email}\nListe de noms : {list_title}\nPrix : {currency}{price}\nDate d\'arrivÃ©e  : {checkin}\nDate de paiement : {checkout}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}\n\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} {type} La demande est expirÃ©e {list_title}.<br /><br />\n\nNom de l\'invitÃ©  : {traveler_name}.<br /><br />\nCourriel des invitÃ©s : {traveler_email}.<br /><br />\nNom d\'hÃ´te : {host_name}.<br /><br />\nCourriel d\'hÃ´te : {host_email}.<br /><br />\nListe de noms : {list_title}.<br /><br />\nPrix : {currency}{price}.<br /><br />\nDate d\'arrivÃ©e  : {checkin}.<br /><br />\nDate de paiement : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Merci et salutations,</p>\n<p>{site_name} Ã‰quipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(135, 'host_reservation_granted_fr', 'Host : After Reservation Granted', 'Vous avez acceptÃ© le {traveler_name} demande de rÃ©servation', 'Salut {host_name},\n\nVous avez acceptÃ© le {traveler_name} Demande de rÃ©servation pour{list_title}.\n\nCi-dessous nous avons mentionnÃ© ses coordonnÃ©es,\n\nPrÃ©nom : {Fname}\nNom de famille : {Lname}\nVivre dans: {livein}\nPas de tÃ©lÃ©phone : {phnum}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Vous avez acceptÃ© le {traveler_name} Demande de rÃ©servation pour {list_title}.</p>\n<p>Ci-dessous nous avons mentionnÃ© ses coordonnÃ©es,</p>\n<p>PrÃ©nom : {Fname}</p>\n<p>Nom de famille : {Lname}</p>\n<p>Vivre dans : {livein}</p>\n<p>Pas de tÃ©lÃ©phone : {phnum}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(136, 'admin_reservation_granted_fr', 'Admin : After Reservation granted', '{host_name} AcceptÃ© le {traveler_name} demande de rÃ©servation', 'Salut Admin,\n\n{host_name} AcceptÃ© le {traveler_name} demande de rÃ©servation {list_title}.\n\n--\nMerci et salutations,\n\n{site_name}\nEquipe', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} AcceptÃ© le {traveler_name} demande de rÃ©servation {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0px;\">{site_name} Ã‰quipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(137, 'host_reservation_declined_fr', 'Host : After Reservation Declined', 'Vous avez refusÃ© {traveler_name} demande de rÃ©servation', 'Salut {host_name},\n\nVous avez refusÃ© {traveler_name} demande de rÃ©servation{list_title}.\n\n--\nMerci et salutations\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Vous avez refusÃ© {traveler_name} demande de rÃ©servation {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(138, 'admin_reservation_declined_fr', 'Admin : After Reservation Declined', '{host_name} RefusÃ© le {traveler_name} Demande de rÃ©servation ', 'Salut Admin,\n\n{host_name} RefusÃ© le {traveler_name} Demande de rÃ©servation pour {list_title}.\n\n--\nMerci et salutations,\n\n{site_name} Equipe', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} RefusÃ© le {traveler_name} Demande de rÃ©servation pour {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0px;\">{site_name} Equipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(139, 'admin_reservation_cancel_fr', 'Admin : After reservation canceled', '{traveler_name} AnnulÃ© le {host_name} Liste de rÃ©servation', 'Salut Admin,\n\n{traveler_name} AnnulÃ© le {host_name} Liste de rÃ©servation ({status}) pour {list_title}.\n\n{penalty}\n\n--\nMerci et salutations\n{site_name} Team', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Sault Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} AnnulÃ© le {host_name}Â  Liste de rÃ©servation ({status}) pour {list_title}.\n<br/><br/>{penalty}\n</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0px;\">{site_name} Ã‰quipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(140, 'host_reservation_cancel_fr', 'Host : After reservation canceled', '{host_name} AnnulÃ© Votre liste de rÃ©servation', 'Salut {traveler_name},\n\n{host_name}  AnnulÃ© Votre liste de rÃ©servation ({status}) pour {list_title}.\n\n\nISi vous avez d\'autres questions, s\'il vous plaÃ®t n\'hÃ©sitez pas Ã  nous contacter.\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name}  AnnulÃ© Votre liste de rÃ©servation ({status}) pour {list_title}.<br /></p>\n<p>Si vous avez d\'autres questions, s\'il vous plaÃ®t n\'hÃ©sitez pas Ã  nous contacter.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Merci et salutations,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(141, 'forgot_password_fr', 'Forgot Password', 'Mot de passe oubliÃ©', 'Cher membre,\n\nCi-dessous nous avons mentionnÃ© vos dÃ©tails de compte.\n\nEt c\'est parti,\n\nEmail_id : {email}\n\nMot de passe : {password}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Cher membre,</td>\n</tr>\n<tr>\n<td>\n<p>Ci-dessous nous avons mentionnÃ© vos dÃ©tails de compte.</p>\n<p>Et c\'est parti,</p>\n<p>Email_id : {email}</p>\n<p>Mot de passe  : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Merci et salutations,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(142, 'checkin_host_fr', 'Host: Check In', 'ArrivÃ©e du voyageur', 'Salut {host_name},\n\n{traveler_name} Est checkin Ã  {list_title}.\n\nNom de l\'invitÃ© : {guest_name}\nListe de noms : {list_title}\nPrix : {currency}{price}\nDate d\'arrivÃ©e : {checkin}\nDate de paiement : {checkout}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} Est checkin Ã Â Â {list_title} Â .<br /><br />\nNom de l\'invitÃ© : {traveler_name}.<br /><br />\nListe de noms : {list_title}.<br /><br />\nPrix : {currency}{price}.<br /><br />\nDate d\'arrivÃ©e : {checkin}.<br /><br />\nDate de paiement : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Merci et salutations,</p>\n<p>{site_name} Ã‰quipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(143, 'users_signin_fr', 'Users Signin', 'Bienvenue Ã   {site_name}', 'Cher membre,\n\nPlaisir de vous rencontrer et bienvenue sur {site_name}.\n\nCi-dessous nous avons mentionnÃ© vos dÃ©tails de compte.\n\nEt c\'est parti,\n\nEmail_id : {email}\n\nMot de passe : {password}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Cher membre,</td>\n</tr>\n<tr>\n<td>\n<p>\nPlaisir de vous rencontrer et bienvenue sur{site_name}.</p>\n<p>\nCi-dessous nous avons mentionnÃ© vos dÃ©tails de compte.</p>\n<p>Et c\'est parti,</p>\n<p>Email_id : {email}</p>\n<p>Mot de passe : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(144, 'reset_password_fr', 'Reset Password', 'rÃ©initialiser le mot de passe', 'Cher membre,\n\nCi-dessous nous avons mentionnÃ© vos nouveaux dÃ©tails de compte.\n\nEt c\'est parti,\n\nMot de passe: {password}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Cher membre,</td>\n</tr>\n<tr>\n<td>\n<p>\nCi-dessous nous avons mentionnÃ© vos nouveaux dÃ©tails de compte.</p>\n<p>\nEt c\'est parti,</p>\n<p>Mot de passe : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(145, 'admin_payment_fr', 'Admin payment to Host', 'Admin a payÃ© vos frais pour {list_title}', 'Bonjour {username},\n\nNous avons payÃ© vos frais pour {list_title}.\n\nDÃ©tails comme suit,\n\nNom du lieu : {list_title}\nEnregistrement : {checkin}\nCheck out : {checkout}\nPrix : {payed_price}\nPaiement par : {payment_type}\nNumÃ©ro de paiement: {pay_id}\nDate de paiement : {payed_date}\n\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>\n<p>Salut {username} ,</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>Nous avons payÃ© vos frais pour {list_title}.</p>\n<br />\n<p>DÃ©tails comme suit,</p>\n<p>Nom du lieu : {list_title}</p>\n<p>Enregistrement: {checkin}</p>\n<p>Check out : {checkout}</p>\n<p>Prix : {payed_price}</p>\n<p>Paiement par : {payment_type}</p>\n<p>NumÃ©ro de paiement: {pay_id}</p>\n<p>Date de paiement : {payed_date}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0px;\">{site_name} Ã‰quipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(146, 'contact_form_fr', 'Contact Form', 'Message reÃ§u du formulaire de contact', 'Salut Admin,\n\nUn message reÃ§u de la page contact nous sur {date} at {time}.\n\nDÃ©tails comme suit,\n\nprÃ©nom : {name}\n\nEmail : {email}\n\nMessage : {message}\n\n--\nMerci et salutations,\n\n{site_name} Equipe', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut Admin,</td>\n</tr>\n<tr>\n<td>\n<p>Un message reÃ§u de la page contact nous sur {date} at {time}.</p>\n<p>DÃ©tails comme suit,</p>\n<p>prÃ©nom : {name}</p>\n<p>Email : {email}</p>\n<p>Message : {message}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0px;\">{site_name} Ã‰quipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(147, 'invite_friend_fr', 'Invite My Friends', '{username} vous inviter.', 'Salut Friend\'s,\n\n{username} Veut que vous invitiez {site_name}\n\n{dynamic_content}\n\n{click_here}\n\n--\nCordialement,\n{username}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut Friends,</td>\n</tr>\n<tr>\n<td>\n<p>{username} Veut que vous invitiez</p>\n<p>{site_name}</p>\n<p>{dynamic_content}</p>\n<p>Â {click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">salutations,</p>\n<p style=\"margin: 0px;\">{username}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(148, 'email_verification_fr', 'Email Verification Link', '{site_name} Lien de vÃ©rification d\'email', 'Salut {user_name},\n\nVeuillez cliquer sur le lien ci-dessous pour {site_name} vÃ©rification de l\'E-mail.\n\n{click_here}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {user_name},</td>\n</tr>\n<tr>\n<td>\n<p>Veuillez cliquer sur le lien ci-dessous pour {site_name} vÃ©rification de l\'E-mail.</p>\n<p>Â {click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(149, 'referral_credit_fr', 'Referral Credit', 'Vous Ãªtes gagnant {amount} de Referrals', 'Salut {username},\n\nVous Ãªtes gagnant {amount} de {friend_name}\n\n--\nMerci et salutations,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {user_name},</td>\n</tr>\n<tr>\n<td><p>\nVous Ãªtes gagnant {amount} de {friend_name}</p>\n</td>\n</tr><tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(150, 'User_join_to_Referal_mail_fr', 'User join to Referal mail', 'Votre ami S\'inscrire', 'cher {refer_name},\n\nVotre ami {friend_name} est maintenant membre de {site_name}. Maintenant, 100 $ est crÃ©ditÃ© dans votre compte Travel Credit Possible.\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Cher {refer_name},</td>\n</tr>\n<tr>\n<td><br />Votre ami {friend_name} est maintenant membre de {site_name}. Maintenant, 100 $ est crÃ©ditÃ© dans votre compte Travel Credit Possible.<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(151, 'contact_request_to_host_fr', 'Contact Request to Host', 'Demande de contact', 'Bonjour {host_username},\n\nVeuillez cliquer sur le lien suivant pour contacter l\'utilisateur: {link}\n\nListe: {titre}\n\nDate d\'arrivÃ©e: {checkin}\n\nDate de paiement: {checkout}\n\nInvitÃ©s: {guest}\n\nMessage de l\'invitÃ©: {message}\n\n-\nMerci et salutations,\nAdmin\n{site_name} ', '<Table style = \"width: 100%;\" Cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour {host_username}, </ td>\n</ Tr>\n<Tr>\n<Td>\nVeuillez cliquer sur le lien suivant pour contacter l\'utilisateur: {link} <br /> <br />\n\nListe: {title} <br /> <br />\n\nDate d\'arrivÃ©e: {checkin} <br /> <br />\n\nDate de paiement: {checkout} <br /> <br />\n\nInvitÃ©s: {guest} <br /> <br />\n\nMessage de l\'invitÃ©: {message} <br /> <br /> </ td>\n</ Tr>\n<Tr>\n<Td>\n<P style = \"margin: 0 10px 0 0;\"> - </ p>\n<P style = \"margin: 0 0 10px 0;\"> Merci et salutations, </ p>\n<P style = \"margin: 0 10px 0 0;\"> Admin </ p>\n<P style = \"margin: 0px;\"> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(152, 'contact_request_to_guest_fr', 'Contact Request to Guest', 'Demande de contact', 'Bonjour {traveller_username},\n\nVous avez envoyÃ© une demande de contact Ã  {host_username}.\n\nListe: {title}\n\nDate d\'arrivÃ©e: {checkin}\n\nDate de paiement: {checkout}\n\nInvitÃ©s: {guest}\n\nVotre message: {message}\n\n-\nMerci et salutations,\nAdmin', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Bonjour {traveller_username},</td>\n</tr>\n<tr>\n<td>\nVous avez envoyÃ© une demande de contact Ã  {host_username}.<br /><br />\n			\nListe : {title}<br /><br />\n		\nDate d\'arrivÃ©e: {checkin}<br /><br />\n		\nDate de paiement : {checkout}<br /><br />\n		\nInvitÃ©s : {guest}<br /><br />\n		\nVotre message : {message}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(153, 'request_granted_to_guest_fr', 'Contact Request Granted to Guest', 'Demande de contact accordÃ©e', 'Salut {traveller_username},\n\nVotre demande de contact est accordÃ©e par {host_username}.\n		\nCourriel d\'hÃ´te: {host_email}\n\nListe : {title}\n		\nDate d\'arrivÃ©e : {checkin}\n		\nDate de paiement : {checkout}\n			\nMessage d\'hÃ´te  : {message}\n\nURL de rÃ©servation  : {link}\n\n--\nMerci et salutations,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut {traveller_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nVotre demande de contact est accordÃ©e par {host_username}.\n<br /><br />\n\nCourriel d\'hÃ´te: {host_email}<br /><br /> \n\nListe : {title}<br /><br /> \n\nDate d\'arrivÃ©e : {checkin}<br /><br /> \n\nDate de paiement : {checkout}<br /><br /> \n\nPrix : {price}<br /><br />\n\nMessage d\'hÃ´te : {message}<br /><br /> \n\nURL de rÃ©servation : {link}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(154, 'request_granted_to_host_fr', 'Contact Request Granted to Host', 'Demande de contact accordÃ©e', 'Bonjour {host_username},\n\nVous avez acceptÃ© la demande de contact de {traveller_username}.\n\nEmail invitÃ©: {guest_email}\n\nListe: {titre}\n\nDate d\'arrivÃ©e: {checkin}\n\nDate de paiement: {checkout}\n\nPrix: {price}\n\nVotre message: {message}\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Bonjour {host_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nVous avez acceptÃ© la demande de contact de {traveller_username}. <br /><br />\n\nEmail invitÃ©: {guest_email}<br /><br />	\n		\nListe: {titre}<br /><br />\n		\nDate d\'arrivÃ©e: {checkin}<br /><br />\n		\nDate de paiement: {checkout}<br /><br />\n\nPrix: {price}<br /><br />\n	\nVotre message: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(155, 'contact_request_to_admin_fr', 'Contact Request to Admin', 'Demande de contact', 'Salut Admin,\n\n{Traveller_username} a envoyÃ© une demande de contact Ã  {host_username}.\n\nListe: {title}\n\nDate d\'arrivÃ©e: {checkin}\n\nDate de paiement: {checkout}\n\nInvitÃ©s: {guest}\n\nPrix: {price}\n\nMessage de l\'invitÃ©: {message}\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut Admin,</td>\n</tr>\n<tr>\n<td><br />\n{Traveller_username} a envoyÃ© une demande de contact Ã  {host_username}. <br /><br />		\nListe: {title}<br /><br />	\nDate d\'arrivÃ©e: {checkin}<br /><br />\n		\nInvitÃ©s: {guest}<br /><br />\n	\nPrix: {price}<br /><br />\n		\nMessage de l\'invitÃ©: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(156, 'request_granted_to_admin_fr', 'Contact Request Granted to Admin', 'Demande de contact accordÃ©e', 'Salut Admin,\n\n{host_username} accepte la demande de contact pour {traveller_username}.\n\nEmail invitÃ©: {guest_email}\n\nCourriel de l\'hÃ´te: {host_email}\n\nListe: {title}\n\nDate d\'arrivÃ©e: {checkin}\n\nDate de paiement: {checkout}\n\nPrix: {price}\n\nMessage d\'hÃ´te: {message}\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut Admin,</td>\n</tr>\n<tr>\n<td><br />\n{Host_username} accepte la demande de contact pour {traveller_username}.\n<br /><br />\n\nEmail invitÃ©: {guest_email}<br /><br />	\n\nCourriel de l\'hÃ´te: {host_email}<br /><br />	\n\nListe: {title}<br /><br />	\n\nDate d\'arrivÃ©e: {checkin}<br /><br />\n		\nDate de paiement: {checkout}<br /><br />\n\nPrix: {price}<br /><br />	\n			\nMessage d\'hÃ´te: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Merci et salutations,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>');
INSERT INTO `email_templates` (`id`, `type`, `title`, `mail_subject`, `email_body_text`, `email_body_html`) VALUES
(157, 'special_request_granted_to_guest_fr', 'Contact Request Granted with Special Offer to Guest', 'Demande de contact accordÃ©e avec offre spÃ©ciale', 'Bonjour {traveller_username},\n\n{host_username} accepte votre demande de contact avec une offre spÃ©ciale.\n\nCourriel de l\'hÃ´te: {host_email}\n\nListe: {title}\n\nDate d\'arrivÃ©e: {checkin}\n\nDate de paiement: {checkout}\n\nPrix: {price}\n\nMessage: {message}\n\nURL de rÃ©servation: {link}\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<Ttble style = \"width: 100%;\" Cellspacing = \"10\" cellpadding = \"0\">\n<tbody>\n<Tr>\n<td> Bonjour {traveller_username}, </ td>\n</tTr>\n</tr>\n<td> <br />\n\n{host_username} accepte votre demande de contact avec une offre spÃ©ciale. <br /> <br />\n\nCourriel de l\'hÃ´te: {host_email} <br /> <br />\n\nListe: {title} <br /> <br />\n\nDate d\'arrivÃ©e: {checkin} <br /> <br />\n\nDate de paiement: {checkout} <br /> <br />\n\nPrix: {price} <br /> <br />\n\nMessage: {message} <br /> <br />\n\nURL de rÃ©servation: {link} <br /> <br />\n\n</ td>\n</ tr>\n<tr>\n<td>\n<p style = \"margin: 0 10px 0 0;\"> - </ p>\n<p style = \"margin: 0 0 10px 0;\"> Merci et salutations, </ p>\n<p style = \"margin: 0 10px 0 0;\"> Admin </ p>\n<p style = \"margin: 0px;\"> {site_name} </ p>\n</ td>\n</ tr>\n</ tbody>\n</ table>'),
(158, 'special_request_granted_to_host_fr', 'Contact Request Granted with Special Offer to Host', 'Demande de contact accordÃ©e avec offre spÃ©ciale', 'Bonjour {host_username},\n\nVous avez acceptÃ© la demande de contact {traveller_username} avec une offre spÃ©ciale.\n\nEmail invitÃ©: {guest_email}\n\nListe: {title}\n\nDate d\'arrivÃ©e: {checkin}\n\nDate de paiement: {checkout}\n\nPrix: {price}\n\nVotre message: {message}\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<Table style = \"width: 100%;\" Cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour {host_username}, </ td>\n</ Tr>\n<Tr>\n<Td> <br />\n\nVous avez accepter la demande de contact {traveller_username} avec une offre spÃ©ciale.\n\nCourriel de l\'invitÃ©: {guest_email} <br /> <br />\n\nListe: {title} <br /> <br />\n\nDate d\'arrivÃ©e: {checkin} <br /> <br />\n\nDate de paiement: {checkout} <br /> <br />\n\nPrix: {price} <br /> <br />\n\nVotre message: {message} <br /> <br />\n\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P style = \"margin: 0 10px 0 0;\"> - </ p>\n<P style = \"margin: 0 0 10px 0;\"> Merci et salutations, </ p>\n<P style = \"margin: 0 10px 0 0;\"> Admin </ p>\n<P style = \"margin: 0px;\"> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(159, 'special_request_granted_to_admin_fr', 'Contact Request Granted with Special Offer to Admin', 'Demande de contact accordÃ©e avec offre spÃ©ciale', 'Salut Admin,\n\n{Host_username} ont accepter la demande de contact {traveller_name} avec une offre spÃ©ciale.\n\nEmail invitÃ©: {guest_email}\n\nCourriel de l\'hÃ´te: {host_email}\n\nListe: {title}\n\nDate d\'arrivÃ©e: {checkin}\n\nDate de paiement: {checkout}\n\nPrix: {price}\n\nMessage d\'hÃ´te: {message}\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<Table style = \"width: 100%;\" Cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour administrateur, </ td>\n</ Tr>\n<Tr>\n<Td> <br />\n\n{Host_username} ont accepter la demande de contact {traveller_username} avec une offre spÃ©ciale.\n\nCourriel de l\'invitÃ©: {guest_email} <br /> <br />\n\nCourriel de l\'hÃ´te: {host_email} <br /> <br />\n\nListe: {title} <br /> <br />\n\nDate d\'arrivÃ©e: {checkin} <br /> <br />\n\nDate de paiement: {checkout} <br /> <br />\n\nPrix: {price} <br /> <br />\n\nMessage d\'hÃ´te: {message} <br /> <br />\n\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P style = \"margin: 0 10px 0 0;\"> - </ p>\n<P style = \"margin: 0 0 10px 0;\"> Merci et salutations, </ p>\n<P style = \"margin: 0 10px 0 0;\"> Admin </ p>\n<P style = \"margin: 0px;\"> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(160, 'request_cancel_to_guest_fr', 'Contact Request Cancelled to Guest', 'Pardon! Votre demande de contact est annulÃ©e', 'Bonjour {traveller_username},\n\nDÃ©solÃ©, votre demande de contact est refusÃ©e par {host_username} pour {title}.\n\nMessage d\'hÃ´te: {message}\n\n-\nMerci et salutations,\n\nAdmin\n{site_name}', '<Table style = \"width: 100%;\" Cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour {traveller_username}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> DÃ©solÃ©, votre demande de contact est refusÃ©e par {host_username} pour {title}. </ P> <br /> <br />\nMessage d\'hÃ´te: {message} <br /> <br />\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P style = \"margin: 0 10px 0 0;\"> - </ p>\n<P style = \"margin: 0 0 10px 0;\"> Merci et salutations, </ p>\n<P style = \"margin: 0 0 10px 0;\"> Admin, </ p>\n<P style = \"margin: 0px;\"> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(161, 'request_cancel_to_host_fr', 'Contact Request Cancelled to Host', 'Vous avez annulÃ© la demande de contact', 'Bonjour {host_username},\n\nVous avez annulÃ© la demande de contact {traveller_username} pour {title}.\n\nVotre message: {message}\n\n-\nMerci et salutations,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<Td> Bonjour {host_username}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> Vous avez annulÃ© la demande de contact {traveller_username} pour {title}. </ P> <br /> <br />\nVotre message: {message} <br /> <br />\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P style = \"margin: 0 10px 0 0;\"> - </ p>\n<P style = \"margin: 0 0 10px 0;\"> Merci et salutations, </ p>\n<P style = \"margin: 0 0 10px 0;\"> Admin, </ p>\n<P style = \"margin: 0px;\"> {site_name} </ p>\n</ Td>\n</tr>\n</tbody>\n</table>'),
(162, 'checkout_admin_fr', 'Admin: Check Out', 'DÃ©part pour le voyageur', 'Salut Admin,\n\n{traveler_name} est lancÃ© Ã  partir de {list_title}.\n\n-\nMerci et salutations,\nAdmin\n{site_name}\n', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour administrateur, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {traveler_name} est un formulaire checkouted {list_title}. </ P>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(163, 'request_cancel_to_admin_fr', 'Contact Request Cancelled to Admin', '{host_username} a annulÃ© la demande de contact {traveller_username}', 'Salut Admin,\n\n{host_username} a annulÃ© la demande de contact {traveller_username} pour {title}.\n\nMessage d\'hÃ´te: {message}\n\n-\nMerci et salutations,\n\n{site_name}', '<Table style = \"width: 100%;\" Cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour administrateur, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {host_username} a annulÃ© la demande de contact {traveller_username} pour {title}. </ P>\n<br /> <br />\nMessage d\'hÃ´te: {message}.\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P style = \"margin: 0 10px 0 0;\"> - </ p>\n<P style = \"margin: 0 0 10px 0;\"> Merci et salutations, </ p>\n<P style = \"margin: 0px;\"> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(164, 'checkout_host_fr', 'Host: Check Out', 'DÃ©part pour le voyageur', 'Bonjour {host_name},\n\n{traveler_name} est lancÃ© Ã  partir de {list_title}.\n\n-\nMerci et salutations,\n\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {host_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {traveler_name} est lancÃ© Ã  partir de {list_title}. </ P>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin, </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(165, 'checkout_traveler_fr', 'Traveler: Check Out', 'Votre DÃ©part', 'Salut {traveler_name},\n\nMerci, Vous Ãªtes checkouted Ã  partir de {list_title}.\n\n\n-\nMerci et salutations,\n\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {traveler_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> Je vous remercie, Vous avez rÃ©ussi checkouted Ã  partir de {list_title}. </ P>\n<P> </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin, </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(166, 'contact_discuss_more_to_guest_fr', 'Contact Request - Discuss more to Guest', 'Demande de contact - Discuter plus', 'Bonjour {traveller_username},\n\n{host_username} souhaite discuter plus avec vous.\n\nCourriel de l\'hÃ´te: {host_email}\n\nListe: {title}\n\nDate d\'arrivÃ©e: {checkin}\n\nDate de paiement: {checkout}\n\nMessage: {message}\n\n--\nMerci et salutations,\nAdmin\n{site_name}', '<Table style = \"width: 100%;\" Cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour {traveller_username}, </ td>\n</ Tr>\n<Tr>\n<Td> <br />\n\n{host_username} souhaite discuter plus avec vous. <br /> <br />\n\nCourriel de l\'hÃ´te: {host_email} <br /> <br />\n\nListe: {title} <br /> <br />\n\nDate d\'arrivÃ©e: {checkin} <br /> <br />\n\nDate de paiement: {checkout} <br /> <br />\n\nMessage: {message} <br /> <br />\n\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P style = \"margin: 0 10px 0 0;\"> - </ p>\n<P style = \"margin: 0 0 10px 0;\"> Merci et salutations, </ p>\n<P style = \"margin: 0 10px 0 0;\"> Admin </ p>\n<P style = \"margin: 0px;\"> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(167, 'contact_discuss_more_to_host_fr', 'Contact Request - Discuss more to Host', 'Demande de contact - Discuter plus', 'Bonjour {host_username},\n\nVous souhaitez en discuter davantage avec {traveller_username}.\n\nEmail invitÃ©: {guest_email}\n\nListe: {title}\n\nDate d\'arrivÃ©e: {checkin}\n\nDate de paiement: {checkout}\n\nVotre message: {message}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<Table style = \"width: 100%;\" Cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour {host_username}, </ td>\n</ Tr>\n<Tr>\n<Td> <br />\n\nVous voulez discuter plus avec {traveller_username}. <br /> <br />\n\nCourriel de l\'invitÃ©: {guest_email} <br /> <br />\n\nListe: {title} <br /> <br />\n\nDate d\'arrivÃ©e: {checkin} <br /> <br />\n\nDate de paiement: {checkout} <br /> <br />\n\nVotre message: {message} <br /> <br />\n\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P style = \"margin: 0 10px 0 0;\"> - </ p>\n<P style = \"margin: 0 0 10px 0;\"> Merci et salutations, </ p>\n<P style = \"margin: 0 10px 0 0;\"> Admin </ p>\n<P style = \"margin: 0px;\"> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</Table>'),
(168, 'contact_discuss_more_to_admin_fr', 'Contact Request - Discuss more to Admin', 'Demande de contact - Discuter plus', 'Salut Admin,\n\n{host_username} Veut discuter plus avec {traveller_username}.\n\nCourriel des invitÃ©s: {guest_email}\n\nCourriel de l\'hÃ´te: {host_email}\n\nListe: {title}\n\nDate d\'arrivÃ©e: {checkin}\n\nDate de paiement: {checkout}\n\nMessage d\'hÃ´te: {message}\n\n--\nMerci et salutations,\nAdmin\n{site_name}', '<table style = \"width: 100%;\" cellspacing = \"10\" cellpadding = \"0\">\n<tbody>\n<tr>\n<td> Bonjour administrateur, </td>\n</tr>\n<tr>\n<td> <br />\n\n{host_username} souhaite discuter plus avec {traveller_username}. <br /> <br />\n\nCourriel de l\'invitÃ©: {guest_email} <br /> <br />\n\nCourriel de l\'hÃ´te: {host_email} <br /> <br />\n\nListe: {title} <br /> <br />\n\nDate d\'arrivÃ©e: {checkin} <br /> <br />\n\nDate de paiement: {checkout} <br /> <br />\n\nMessage d\'hÃ´te: {message} <br /> <br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style = \"margin: 0 10px 0 0;\"> - </p>\n<p style = \"margin: 0 0 10px 0;\"> Merci et salutations, </p>\n<p style = \"margin: 0 10px 0 0;\"> Admin </p>\n<p style = \"margin: 0px;\"> {site_name} </p>\n</td>\n</tr>\n</tbody>\n</table>'),
(169, 'checkin_admin_fr', 'Admin: CheckIn', 'ArrivÃ©e du voyageur', 'Salut administrateur,\n\n{traveler_name} est cochÃ© Ã  {list_title}.\n\nNom de l\'invitÃ©: {traveler_name}\nCourriel des invitÃ©s: {traveler_email}\nNom d\'hÃ´te: {host_name}\nCourriel de l\'hÃ´te: {host_email}\nNom de la liste: {list_title}\nPrix: {currency} {price}\nDate d\'arrivÃ©e: {checkin}\nDate de paiement: {checkout}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}\n', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour administrateur, </ td>\n</ Tr>\n<Tr>\n<Td>\n<br />\n{traveler_name} est cochÃ© Ã  {list_title}. <br /> <br />\nNom de l\'invitÃ©: {traveler_name}. <br /> <br />\nCourriel de l\'invitÃ©: {traveler_email}. <br /> <br />\nNom d\'hÃ´te: {host_name}. <br /> <br />\nCourriel de l\'hÃ´te: {host_email}. <br /> <br />\nNom de la liste: {list_title}. <br /> <br />\nPrix: {currency} {price}. <br /> <br />\nDate d\'arrivÃ©e: {checkin}. <br /> <br />\nDate de paiement: {checkout}.\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> -- </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(170, 'checkin_traveller_fr', 'Traveler: Check In', 'Votre arrivÃ©e', 'Salut {traveler_name},\n\nMerci, Vous Ãªtes en ligne Ã  {list_title}.\n\nNom d\'hÃ´te: {host_name}\nNom de la liste: {list_title}\nPrix: {currency} {price}\nDate d\'arrivÃ©e: {checkin}\nDate de paiement: {checkout}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}\n', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {traveler_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<br />\nMerci, Vous avez rÃ©ussi le check-in Ã  {list_title}. <br /> <br />\nNom d\'hÃ´te: {host_name}. <br /> <br />\nNom de la liste: {list_title}. <br /> <br />\nPrix: {currency} {price}. <br /> <br />\nDate d\'arrivÃ©e: {checkin}. <br /> <br />\nDate de paiement: {checkout}.\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> -- </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(171, 'refund_admin_fr', 'Admin: Refund', 'Remboursement d\'Admin', 'Salut Admin,\n\nVous avez remboursÃ© le montant {refund_amt} au compte {name} {account}.\n\nNom de l\'invitÃ©: {traveler_name}\nCourriel des invitÃ©s: {traveler_email}\nNom d\'hÃ´te: {host_name}\nCourriel de l\'hÃ´te: {host_email}\nNom de la liste: {list_title}\nPrix: {currency} {price}\nDate d\'arrivÃ©e: {checkin}\nDate de paiement: {checkout}\nMontant remboursÃ©: {refund_amt}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}\n', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour administrateur, </ td>\n</ Tr>\n<Tr>\n<Td>\n<br />\nVous avez remboursÃ© le montant {refund_amt} au compte {name} {account}. <br /> <br />\nNom de l\'invitÃ©: {traveler_name}. <br /> <br />\nCourriel de l\'invitÃ©: {traveler_email}. <br /> <br />\nNom d\'hÃ´te: {host_name}. <br /> <br />\nCourriel de l\'hÃ´te: {host_email}. <br /> <br />\nNom de la liste: {list_title}. <br /> <br />\nPrix: {currency} {price}. <br /> <br />\nDate d\'arrivÃ©e: {checkin}. <br /> <br />\nDate de paiement: {checkout}. <br /> <br />\nMontant remboursÃ©: {refund_amt}\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> -- </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(172, 'refund_host_fr', 'Host: Refund', 'Remboursement d\'Admin', 'Bonjour {host_name},\n\nAdmin a remboursÃ© le montant {refund_amt} Ã  votre compte {account}.\n\nNom de l\'invitÃ©: {traveler_name}\nNom de la liste: {list_title}\nPrix: {currency} {price}\nDate d\'arrivÃ©e: {checkin}\nDate de paiement: {checkout}\nMontant remboursÃ©: {refund_amt}\n\n--\nMerci et salutations,\nAdmin\n{site_name}\n', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {host_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<br />\nAdmin a remboursÃ© le montant {refund_amt} Ã  votre compte {account}. <br /> <br />\nNom de l\'invitÃ©: {traveler_name}. <br /> <br />\nNom de la liste: {list_title}. <br /> <br />\nPrix: {currency} {price}. <br /> <br />\nDate d\'arrivÃ©e: {checkin}. <br /> <br />\nDate de paiement: {checkout}. <br /> <br />\nMontant remboursÃ©: {refund_amt}\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> -- </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(173, 'refund_guest_fr', 'Traveler: Refund', 'Remboursement d\'Admin', 'Salut {traveler_name},\n\nAdmin a remboursÃ© le montant {refund_amt} Ã  votre compte {account}.\n\nNom d\'hÃ´te: {host_name}\nNom de la liste: {list_title}\nPrix: {currency} {price}\nDate d\'arrivÃ©e: {checkin}\nDate de paiement: {checkout}\nMontant remboursÃ©: {refund_amt}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}\n', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {traveler_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<br />\nAdmin a remboursÃ© le montant {refund_amt} Ã  votre compte {account}. <br /> <br />\nNom d\'hÃ´te: {host_name}. <br /> <br />\nNom de la liste: {list_title}. <br /> <br />\nPrix: {currency} {price}. <br /> <br />\nDate d\'arrivÃ©e: {checkin}. <br /> <br />\nDate de paiement: {checkout}. <br /> <br />\nMontant remboursÃ©: {refund_amt}\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> -- </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(174, 'list_create_host_fr', 'List creation to Host', 'Vous avez crÃ©Ã© la nouvelle liste', 'Bonjour {host_name},\n\nVous avez crÃ©Ã© la nouvelle liste.\n\nNom de la liste: {list_title}\n\nLien: {link}\n\nPrix: {price}\n\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {host_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<br />\nVous avez crÃ©Ã© la nouvelle liste. <br /> <br />\nNom de la liste: {list_title}. <br /> <br />\nLien: {link}. <br /> <br />\nPrix: {prix}.\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(175, 'list_create_admin_fr', 'List creation to Admin', '{host_name} Ont crÃ©Ã© la nouvelle liste', 'Salut Admin,\n\n{host_name} ont crÃ©Ã© la nouvelle liste.\n\nNom d\'hÃ´te: {host_name}\n\nNom de la liste: {list_title}\n\nLien: {link}\n\nPrix: {price}\n\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour administrateur, </ td>\n</ Tr>\n<Tr>\n<Td>\n<br />\n{host_name} ont crÃ©Ã© la nouvelle liste. <br /> <br />\nNom d\'hÃ´te: {host_name}. <br /> <br />\nNom de la liste: {list_title}. <br /> <br />\nLien: {link}. <br /> <br />\nPrix: {prix}.\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(176, 'list_delete_host_fr', 'List deletion to Host', 'Vous avez supprimÃ© la liste', 'Bonjour {host_name},\n\nVous avez supprimÃ© la liste.\n\nNom de la liste: {list_title}\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {host_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<br />\nVous avez supprimÃ© la liste. <br /> <br />\nNom de la liste: {list_title}.\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(177, 'list_delete_admin_fr', 'List deletion to Admin', '{host_name} Ont supprimÃ© la liste', 'Salut Admin,\n\n{host_name} ont supprimÃ© la liste.\n\nNom d\'hÃ´te: {host_name}\n\nNom de la liste: {list_title}\n\n\n--\nMerci et salutations,\nAdmin\n{site_name}', '<table cellspacing = \"10\" cellpadding = \"0\">\n<tbody>\n<tr>\n<td> Bonjour administrateur, </ td>\n</tr>\n<tr>\n<td>\n<br />\n{host_name} ont supprimÃ© la liste. <br /> <br />\nNom d\'hÃ´te: {host_name}. <br /> <br />\nNom de la liste: {list_title}.\n</td>\n</tr>\n<tr>\n<td>\n<p> -- </p>\n<p> Merci et salutations, </p>\n<p> {site_name} Ã‰quipe </p>\n<Div> </ div>\n</td>\n</tr>\n</tbody>\n</table>'),
(178, 'payment_issue_to_admin_fr', 'Payment issue mail to Admin', 'Paiement mal configurÃ©', 'Salut Admin,\n\n{payment_type} Les informations d\'identification de l\'API sont mal configurÃ©es.\n\n{username} a essayÃ© de faire le paiement.\n\nID de courriel: {email_id}\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour administrateur, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {payment_type} Les informations d\'identification de l\'API sont mal configurÃ©es. </ P>\n<P> {username} a essayÃ© de faire le paiement. </ P>\n<P> ID de courrier Ã©lectronique: {email_id} </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(179, 'refund_host_commission_fr', 'Host: Accept Commission Refund', 'Remboursement de l\'hÃ´te Accepter la Commission de l\'Admin', 'Bonjour {host_name},\n\nAdmin a remboursÃ© le montant {refund_amt} Ã  votre compte {account}.\n\nNom de la liste: {list_title}\nMontant remboursÃ©: {refund_amt}\n\n-\nMerci et salutations,\n\nAdmin\n{site_name}\n', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {host_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> L\'administrateur a remboursÃ© le montant {refund_amt} Ã  votre compte {account}. </ P>\n<P> Nom de la liste: {list_title} </ p>\n<P> Montant remboursÃ©: {refund_amt} </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(180, 'refund_host_commission_admin_fr', 'Admin: Accept Commission Refund', 'Vous avez Remboursement de la Commission Accepter l\'hÃ´te', 'Salut Admin,\n\nVous avez remboursÃ© le montant {refund_amt} Ã  votre compte {account}.\n\nNom de la liste: {list_title}\nNom d\'hÃ´te: {host_name}\nMontant remboursÃ©: {refund_amt}\n\n-\nMerci et salutations,\n\nAdmin\n{site_name}\n', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour administrateur, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> Vous avez remboursÃ© le montant {refund_amt} dans votre compte {account}. </ P>\n<P> Nom de la liste: {list_title} </ p>\n<P> Nom d\'hÃ´te: {host_name} </ p>\n<P> Montant remboursÃ©: {refund_amt} </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(181, 'passport_verification_fr', 'Passport verification for administrator', '{user_name} EnvoyÃ© la demande de vÃ©rification du passeport, userid-{user_id}', 'Bonjour Admin,\\n\\n{user_name}EnvoyÃ© la demande de vÃ©rification de passeport, l\'identifiant de l\'utilisateur{user_id}\\n\\nDÃ©tails comme suit,\\n\\nNom d\'utilisateur: {user_name}\\nIdentifiant d\'utilisateur : {user_id}\\n\\n\\n\\n--\\nMerci et salutations,\\n\\n{user_name}', '<table style = \"width: 100%;\" \\n <td> \\ n <td> \\n <td> \\n <trd> \\n <trd> \\n <td> <p> {user_name} a envoyÃ© la demande de vÃ©rification de passeport, user id {user_id}. </p> \\ n <p> DÃ©tails comme suit: <user name> </ p> \\ N <p> Id utilisateur: {user_id} </p> \\ n \\ n </ td> \\ n </ tr> \\ n <tr> \\ n <td> \\ n <p style = \"margin: 0 10px 0 0; \"> - </p> \\ n <p style =\" margin: 0 0 10px 0; \"> Merci et salutations, <p style =\" margin: 0px; \"> User_name} </ p> \\ n </ td> \\ n </ tr> \\ n </ tbody> \\ n </ table>'),
(182, 'change password to user_fr', 'admin change to the password', 'Bienvenue sur {site_name}', 'Mot de passe ancien: {new_password} Conform mot de passe: {conform_password} - Merci et salutations, Admin {site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Cher Membre, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> Vous avez rÃ©ussi Ã  changer votre mot de passe </ p>\n<P> Veuillez utiliser les dÃ©tails du mot de passe ci-dessous pour vous connecter. </ P>\n<P> Ancien mot de passe: {new_password} </ p>\n<P> <span> Nouveau mot de passe: {conform_password} </ span> </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(183, 'spam_fr', 'Spam List', 'Admin Message - Listing dÃ©clarÃ© comme Spam!', '**** MESSAGE D\'ADMINISTRATION ****\n\n\n\nLa liste ci-dessous a Ã©tÃ© signalÃ©e comme Spam:\n\nURL de la liste des spams: {list_url}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> **** MESSAGE D\'ADMINISTRATION **** </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> RAPPORT SPAM FLEXIFLAT.COM! </ P>\n<P> La liste ci-dessous a Ã©tÃ© signalÃ©e comme Spam: <br /> <br /> {list_url} </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td> </ td>\n</ Tr>\n</ Tbody>\n</ Table>\n<p>Â </p>'),
(184, 'host_notify_review_fr', 'Host Notification for Review', '{guest_name} a une rÃ©vision de {host_name}', 'Salut {guest_name},\n\n{host_name} a ajoutÃ© un avis sur vous dans {list_name} sur {site_name}\n\nEt il attend votre avis.\nNom d\'hÃ´te: {host_name}\nNom de liste: {list_name}\n\n\n-\nMerci et salutations,\nAdmin\n{site_name}\n', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour {guest_name}, </ td>\n</ Tr>\n<Tr>\n<Td> <br/> {host_name} a ajoutÃ© un avis sur vous dans {list_name} sur {site_name} <br /> Et il attend votre avis.\n<br/>\nNom d\'hÃ´te: {host_name} <br/>\nNom de liste: {list_name}\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(185, 'guest_notify_review_fr', 'Guest Notification for Review', '{guest_name} a vÃ©rifiÃ©.', 'Bonjour {host_name},\n\n{guest_name} a cochÃ© Ã  partir de votre liste {list_name} sur {site_name}\n\nEt il attend votre avis.\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {host_name}, </ td>\n</ Tr>\n<Tr>\n<Td> <br /> {guest_name} a checkÃ© depuis votre liste {list_name} sur {site_name} <br /> Et il attend votre avis. </ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(186, 'list_notification_fr', 'Notification for calendar update', 'La liste {list_name} nÃ©cessite une mise Ã  jour', 'Bonjour {host_name},\n\nLe {list_name} que vous avez indiquÃ© dans {site_name} n\'a aucune mise Ã  jour le mois dernier.\n\nPour amÃ©liorer votre classement dans les rÃ©sultats de recherche, veuillez mettre Ã  jour votre liste\n-\nMerci et salutations,\n\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour {host_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P>\nLe {list_name} que vous avez indiquÃ© dans {site_name} n\'a aucune mise Ã  jour le mois dernier. </ P>\n<P>\nPour amÃ©liorer votre classement dans les rÃ©sultats de recherche, veuillez mettre Ã  jour votre liste\n</ P>\n\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>\n'),
(187, 'host_notification_fr', 'Upcoming notification for host', 'Le {traveler_name} a une rÃ©servation sur votre {list_title} demain', 'Bonjour {host_name},\n\n{traveler_name} a rÃ©servÃ© la place {list_title} demain.\n\nDÃ©tails comme suit,\n\nNom du voyageur: {traveler_name}\nIdentifiant du courriel de contact: {traveler_email_id}\nNom de la localitÃ©: {list_title}\nDate d\'arrivÃ©e: {checkin}\nVÃ©rifiez: {checkout}\n\n\n-\nMerci et salutations,\n\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour {host_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {traveler_name} a rÃ©servÃ© la place {list_title} demain. </ P>\n<P> DÃ©tails comme suit, </ p>\n\n<P> Nom du voyageur: {traveler_name} </ p>\n<P> Identifiant du courriel de contact: {traveler_email_id} </ p>\n<P> Nom de lieu: {list_title} </ p>\n<P> ArrivÃ©e: {checkin} </ p>\n<P> Consultez: {checkout} </ p>\n\n\n</ P>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>\n'),
(188, 'tc_book_to_admin_gr', 'Admin notification for  Travel cretid booking', '{traveler_name} a envoyÃ© la demande de rÃ©servation en utilisant ses Travel Cretids', 'Bonjour Administrator,\n\n{traveler_name} a envoyÃ© la demande de rÃ©servation pour rÃ©server la place {list_title} le {book_date} Ã  {book_time} en utilisant ses CrÃ©dits de voyage.\n\nDÃ©tails comme suit,\n\nNom du voyageur: {traveler_name}\nIdentifiant du courriel de contact: {traveler_email_id}\nNom de la localitÃ©: {list_title}\nDate d\'arrivÃ©e: {checkin}\nVÃ©rifiez: {checkout}\nPrix â€‹â€‹du marchÃ©: {market_price}\nMontant payÃ©: {payed_amount}\nCrÃ©dits de voyage: {travel_credits}\nNom d\'hÃ´te: {host_name}\nIdentifiant du courrier hÃ´te: {host_email_id}\n\n-\nMerci et salutations,\n\n{site_name}\nMannschaft', '<Table style = \"width: 100%;\" Cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour administrateur, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {traveler_name} a envoyÃ© la demande de rÃ©servation pour rÃ©server la place {list_title} le {book_date} Ã  {book_time} en utilisant ses Travel Credits. </ P>\n<P> DÃ©tails comme suit, </ p>\n<P> Nom du voyageur: {traveler_name} </ p>\n<P> Identifiant du courriel de contact: {traveler_email_id} </ p>\n<P> Nom de lieu: {list_title} </ p>\n<P> ArrivÃ©e: {checkin} </ p>\n<P> Consultez: {checkout} </ p>\n<P> Prix du marchÃ©: {market_price} </ p>\n<P> Montant payÃ©: {payed_amount} </ p>\n<P> CrÃ©dits de voyage: {travel_credits} </ p>\n<P> Nom d\'hÃ´te: {host_name} </ p>\n<P> Identifiant du courrier hÃ´te: {host_email_id} </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P style = \"margin: 0 10px 0 0;\"> - </ p>\n<P style = \"margin: 0 0 10px 0;\"> Merci et salutations, </ p>\n<P style = \"margin: 0px;\"> {site_name} Ã‰quipe </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(189, 'tc_book_to_host_gr', 'Host notification for  Travel cretid booking', '{traveler_name} hat die Reservierungsanfrage unter Verwendung seiner Travel Cretids gesendet', 'Hallo {username},\n\n{traveler_name}hat die Reservierungsanfrage unter Verwendung seiner Travel Cretids gesendet {list_title} place on {book_date} at {book_time} Unter Verwendung seiner Reisekredite.\n\nWir setzen uns mit Ihnen in Verbindung.\n\nDetails wie folgt,\n\nName des Reisenden: {traveler_name}\nKontakt E-Mail-ID : {traveler_email_id}\nOrtsname : {list_title}\nCheck-in : {checkin}\nCheck-out : {checkout}\nPreis : {market_price}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {username} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name}hat die Reservierungsanfrage unter Verwendung seiner Travel Cretids gesendet {list_title} place on {book_date} at {book_time} Unter Verwendung seiner Reisekredite.</p>\n<p>Wir setzen uns mit Ihnen in Verbindung,</p>\n<p>Name des Reisenden : {traveler_name}</p>\n<p>Kontakt E-Mail-ID: {traveler_email_id}</p>\n<p>Ortsname: {list_title}</p>\n<p>Check-in : {checkin}</p>\n<p>Check-out : {checkout}</p>\n<p>Presis : {market_price}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 0 10px 0;\">Administrator,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(190, 'admin_mass_email_gr', 'Admin mass email', '{subject}', 'Hallo Benutzer,\n\n{dynamic_content}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Benutzer,</td>\n</tr>\n<tr>\n<td>\n<p>{dynamic_content}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(191, 'user_reference_gr', 'Reference', 'Schreiben {username} Ein Verweis auf {site_name}', 'Hallo,\n\n{username} Bitten Sie, einen Verweis, dass sie Ã¶ffentlich auf ihre {site_name} profile. {site_name} Ist ein Community-Marktplatz fÃ¼r UnterkÃ¼nfte, die auf Vertrauen und Reputation aufgebaut sind. Ein Hinweis von Ihnen wÃ¼rde wirklich helfen, ihren Ruf mit der Gemeinde aufzubauen.\nEin Verweis auf {site_name} kann helfen {username} Finden Sie einen kÃ¼hlen Ort zu bleiben oder Host interessante Reisende.\n\n{click_here} Eine Referenz haben{username}.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo,</td>\n</tr>\n<tr>\n<td>\n<p>{username} Bitten Sie, einen Verweis, dass sie Ã¶ffentlich auf ihre {site_name} profile. {site_name} Ist ein Community-Marktplatz fÃ¼r UnterkÃ¼nfte, die auf Vertrauen und Reputation aufgebaut sind. Ein Hinweis von Ihnen wÃ¼rde wirklich helfen, ihren Ruf mit der Gemeinde aufzubauen. Finden Sie einen kÃ¼hlen Ort zu bleiben oder Host interessante Reisende.</p>\n<p>{click_here} Eine Referenz haben {username}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(192, 'reference_receive_gr', 'Reference Receive', 'Sie haben eine neue Referenz erhalten {site_name}!', 'Hallo,\n\n{username} Bitten Sie, einen Verweis, dass sie Ã¶ffentlich auf ihre {site_name} profile. {site_name} Ist ein Community-Marktplatz fÃ¼r UnterkÃ¼nfte, die auf Vertrauen und Reputation aufgebaut sind. Ein Hinweis von Ihnen wÃ¼rde wirklich helfen, ihren Ruf mit der Gemeinde aufzubauen.\nEin Verweis auf {site_name} kann helfen {username} Finden Sie einen kÃ¼hlen Ort zu bleiben oder Host interessante Reisende.\n\n{click_here} Eine Referenz haben{username}.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {username},</td>\n</tr>\n<tr>\n<td>\n<p>{other_username} has written a reference for you. Before it goes to your public profile, you can review it and either accept or ignore it. {click_here} to view the reference.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(193, 'host_reservation_notification_gr', 'Reservation notification for host', 'Die Reservierung wurde beantragt  {traveler_name}', 'Hallo {username},\n\n{traveler_name} Buchte das {list_title} place on {book_date} beim {book_time}.\n\nDetails wie folgt,\n\nName des Reisenden : {traveler_name}\nKontakt E-Mail-ID : {traveler_email_id}\nOrtsname: {list_title}\nCheck-In : {checkin}\nCheck-out : {checkout}\nPreis: {market_price}\n\nBitte geben Sie die BestÃ¤tigung an, indem Sie auf die untenstehende Aktion klicken.\n{action_url}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>\n<p>Hallo {username} ,</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Buchte das {list_title} place on {book_date} beim {book_time}.</p>\n<br />\n<p>Details wie folgt,</p>\n<p>Name des Reisenden  : {traveler_name}</p>\n<p>Kontakt E-Mail-ID: {traveler_email_id}</p>\n<p>Ortsname: {list_title}</p>\n<p>Check-In  : {checkin}</p>\n<p>Check-out : {checkout}</p>\n<p>Preis : {market_price}</p>\n<br />\n<p>Bitte geben Sie die BestÃ¤tigung an, indem Sie auf die untenstehende Aktion klicken.</p>\n<p>{action_url}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0px;\">{site_name} Mannschaft</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(194, 'traveller_reservation_notification_gr', 'Reservation notification for  traveller', 'Ihre Buchungsanfrage wird erfolgreich gesendet', 'Hallo {traveler_name},\n\nIhre Reservierungsanfrage wurde erfolgreich an den entsprechenden Host gesendet.\n\nBitte warten Sie auf seine BestÃ¤tigung.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Ihre Reservierungsanfrage wurde erfolgreich an den entsprechenden Host gesendet.</p>\n<p>Bitte warten Sie auf seine BestÃ¤tigung.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 0 10px 0;\">Administrator,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(195, 'admin_reservation_notification_gr', 'Reservation notification for  administrator', '{traveler_name} Schickte die Reservierungsanfrage an {host_name}', 'Halo Administrator,\n\n{traveler_name} Schickte die Reservierungsanfrage an {list_title} place on {book_date} at {book_time}.\n\nDetails wie folgt,\n\nName des Reisenden : {traveler_name}\nKontakt E-Mail-ID : {traveler_email_id}\nOrtsname : {list_title}\nCheck-in : {checkin}\nCheck-out : {checkout}\nPreis : {market_price}\nHostname : {host_name}\nHost-Email-Id : {host_email_id} \n\n--\nDanke und GrÃ¼ÃŸe,\n\n{site_name} Mannschaft', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo Administrator,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Schickte die Reservierungsanfrage an {list_title} place on {book_date} at {book_time}.\n</p>\n<p>Details wie folgt,</p>\n<p>Name des Reisenden : {traveler_name}</p>\n<p>Contact Email Id : {traveler_email_id}</p>\n<p>Ortsname : {list_title}</p>\n<p>Check-in : {checkin}</p>\n<p>Check-out : {checkout}</p>\n<p>Preis : {market_price}</p>\n<p>Hostname : {host_name}</p>\n<p>Host-Email-Id : {host_email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0px;\">{site_name} Mannschaft</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(196, 'traveler_reservation_granted_gr', 'Traveler : After Reservation granted', 'GlÃ¼ckwunsch! Ihre Reservierungsanfrage wird erteilt.', 'Hallo {traveler_name},\n\nGlÃ¼ckwunsch! Ihre Reservierungsanfrage wird erteilt.{host_name} fÃ¼r {list_name}.\n\nUnten erwÃ¤hnten wir seine Kontaktdaten,\n\nVorname : {Fname}\nNachname : {Lname}\nLebe in : {livein}\nPhone No : {phnum}\n\nGenaue Adresse ist,\n\nAdresse : {street_address}\nOptionale Anschrift : {optional_address}\nStadt : {city}\nBundesland : {state}\nLand : {country}\nPostleitzahl : {zipcode}\n\nHost-Nachricht : {comment} \n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>GlÃ¼ckwunsch! Ihre Reservierungsanfrage wird erteilt. {host_name} fÃ¼r {list_name}.</p>\n<p>Unten erwÃ¤hnten wir seine Kontaktdaten,</p>\n<p>Vorname : {Fname}</p>\n<p>Nachname : {Lname}</p>\n<p>Lebe in: {livein}</p>\n<p>Phone No : {phnum}</p>\n<p>Genaue Adresse ist,</p>\n<p>Adresse: {street_address},</p>\n<p>Optionale Anschrift :{optional_address},</p>\n<p>Stadt : {city},</p>\n<p>Bundesland : {state},</p>\n<p>Land : {country},</p>\n<p>Postleitzahl : {zipcode}</p>\n<p>Host-Nachricht : {comment}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 0 10px 0;\">Administrator,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(197, 'traveler_reservation_declined_gr', 'Traveler : After reservation declined', 'Es tut uns leid! Ihre Reservierungsanfrage wird verweigert', 'Hallo {traveler_name},\n\nEs tut uns leid! Ihre Reservierungsanfrage wird verweigert {host_name} fÃ¼r {list_title}.\n\nHost-Nachricht : {comment} \n\nBald werden wir Sie mit der entsprechenden Zahlung in Verbindung setzen.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>\nEs tut uns leid! Ihre Reservierungsanfrage wird verweigert {host_name} fÃ¼r {list_title}.</p>\n<p>Host-Nachricht : {comment}</p>\n<p>Bald werden wir Sie mit der entsprechenden Zahlung in Verbindung setzen.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 0 10px 0;\">Administrator,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(198, 'traveler_reservation_cancel_gr', 'Traveler : After reservation canceled', '{traveler_name} Hat Ihre Reservierungsliste storniert', 'Hallo {host_name},\n\nEntschuldige, Deine ({status}) Hat Ihre Reservierungsliste storniert{traveler_name} fÃ¼r {list_title}.\n\n{user_type} Nachricht : {comment} \n\nSicher, dass wir mit Ihnen bald in Verbindung treten, wenn es irgendein Zahlungsausgleich gibt.\n\nUnd auch wenn Sie irgendwelche anderen Fragen haben, fÃ¼hlen Sie bitte sich frei, mit uns in Verbindung zu treten.\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Entschuldige, Deine ({status}) Hat Ihre Reservierungsliste storniert{traveler_name} fÃ¼r {list_title}.</p>\n<p>{user_type} Nachricht : {comment} </p>\n<p>Sicher, dass wir mit Ihnen bald in Verbindung treten, wenn es irgendein Zahlungsausgleich gibt.\n\n</p>\n<p>Und auch wenn Sie irgendwelche anderen Fragen haben, fÃ¼hlen Sie bitte sich frei, mit uns in Verbindung zu treten.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 0 10px 0;\">Administrator,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(199, 'traveler_reservation_expire_gr', 'Traveler : Reservation Expire', 'Es tut uns leid! Ihre{type} Ist abgelaufen', 'Hallo {traveler_name},\n\nIhre {type} Ist abgelaufen.\n\nHostname : {host_name}\nListennamen : {list_title}\nPreis : {currency}{price}\nCheck-in Datum : {checkin}\nCheckout Datum : {checkout}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nIhre {type} Ist abgelaufen.<br /><br />\nHostname : {host_name}.<br /><br />\nListennamen : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin Datum : {checkin}.<br /><br />\nCheckout Datum : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(200, 'host_reservation_expire_gr', 'Host : Reservation Expire', '{type} Anforderung fÃ¼r Ihre Liste abgelaufen', 'Hallo {host_name},\n\n{type} Anforderung fÃ¼r Ihre Liste abgelaufen {list_title} Gebucht von {traveler_name}.\n\nBenutzername : {traveler_name}\nListennamen : {list_title}\nPreis : {currency}{price}\nCheckin Datum : {checkin}\nCheckout Datum : {checkout}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{type} Anforderung fÃ¼r Ihre Liste abgelaufen {list_title} Gebucht von {traveler_name}.<br /><br />\nBenutzername :  : {traveler_name}.<br /><br />\nListennamen : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin Datum : {checkin}.<br /><br />\nCheckout Datum : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(201, 'admin_reservation_expire_gr', 'Admin : Reservation Expire', '{type} Ist abgelaufen', 'Hallo Administrator,\n\n{traveler_name} {type} Anforderung ist abgelaufen {list_title}.\n\nBenutzername : {traveler_name}\nGÃ¤ste-E-Mail : {traveler_email}\nHostname : {host_name}\nHost-Email : {host_email}\nListtenname : {list_title}\nPreis : {currency}{price}\nCheckin Datum : {checkin}\nCheckout Datum : {checkout}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} {type} Anforderung ist abgelaufen {list_title}.<br /><br />\nBenutzername : {traveler_name}<br /><br />\nGÃ¤ste-E-Mail : {traveler_email}.<br /><br />\nHostname : {host_name}.<br /><br />\nHost-email : {host_email}.<br /><br />\nListtenname : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin Datum : {checkin}.<br /><br />\nCheckout Datum : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(202, 'host_reservation_granted_gr', 'Host : After Reservation Granted', 'Sie haben die Reservierungsanfrage {traveler_name} akzeptiert', 'Hallo {hostname},\n\nSie haben die Reservierungsanfrage {traveler_name} fÃ¼r {list_title} akzeptiert.\n\nUnten erwÃ¤hnten wir seine Kontaktdaten,\n\nVorname: {Fname}\nNachname: {Lname}\nLeben in: {livein}\nTelefonnummer: {phnum}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style = \"width: 100%;\" cellspacing = \"10\" cellpadding = \"0\">\n<tbody>\n<tr>\n<td> Hallo {Hostname}, </td>\n</tr>\n<tr>\n<td>\n<p> Sie haben die Reservierungsanfrage {traveler_name} fÃ¼r {list_title} akzeptiert. </p>\n<p> Im Folgenden haben wir seine Kontaktdaten erwÃ¤hnt, </p>\n<p> Vorname: {Fname} </p>\n<p> Nachname: {Lname} </p>\n<p> Leben in: {livein} </p>\n<p> Telefonnummer: {phnum} </p>\n</td>\n</tr>\n<tr>\n<td>\n<p style = \"margin: 0 10px 0 0;\"> - </p>\n<p style = \"margin: 0 0 10px 0;\"> Danke und GrÃ¼ÃŸe, </p>\n<p style = \"margin: 0 0 10px 0;\"> Administrator, </p>\n<p style = \"margin: 0px;\"> {site_name} </p>\n</td>\n</tr>\n</tbody>\n</table>'),
(203, 'admin_reservation_granted_gr', 'Admin : After Reservation granted', '{host_name} Akzeptiert {traveler_name} Reservierungsanfrage', 'Hallo Administrator,\n\n{host_name} Akzeptiert {traveler_name} Reservierungsanfrage\n{list_title}.\n\n--\nDanke und GrÃ¼ÃŸe,\n\n{site_name} Mannschaft', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} Akzeptiert {traveler_name} Reservierungsanfrage\n{list_title}.\n</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0px;\">{site_name} Mannschaft</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(204, 'host_reservation_declined_gr', 'Host : After Reservation Declined', 'Sie haben die {traveler_name} Reservierungsanfrage', 'Hallo {host_name},\n\nSie haben die {traveler_name} Reservierungsanfrage pour {list_title}.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Sie haben die {traveler_name} Reservierungsanfrage pour {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 0 10px 0;\">Administrator,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(205, 'admin_reservation_declined_gr', 'Admin : After Reservation Declined', '{host_name} Lehnte die {traveler_name} Buchungsanfrage fÃ¼r', 'Hallo Administrator,\n\n{host_name} Lehnte die {traveler_name} Buchungsanfrage fÃ¼r{list_title}.\n\n--\nDanke und GrÃ¼ÃŸe,\n\n{site_name} Mannschaft', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} Lehnte die{traveler_name} Buchungsanfrage fÃ¼r {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0px;\">{site_name} Mannschaft</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(206, 'admin_reservation_cancel_gr', 'Admin : After reservation canceled', '{traveler_name} Storniert die {host_name} Reservierungsliste', 'Hallo Administrator,\n\n{traveler_name} Storniert die {host_name} Reservierungsliste ({status}) fur {list_title}.\n\n{penalty}\n\n--\nDanke und GrÃ¼ÃŸe,\n\n{site_name} Mannschaft', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Storniert die {host_name}Â  Reservierungsliste ({status}) fur {list_title}.\n<br/><br/>{penalty}\n</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0px;\">{site_name} Mannschaft</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(207, 'host_reservation_cancel_gr', 'Host : After reservation canceled', '{host_name} Annulliert Ihre Reservierungsliste', 'Hallo {traveler_name},\n\n{host_name}  Annulliert Ihre Reservierungsliste ({status}) fur {list_title}.\n\n\nWenn Sie irgendwelche anderen Fragen haben, fÃ¼hlen Sie bitte sich frei, mit uns in Verbindung zu treten.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name}  Annulliert Ihre Reservierungsliste ({status}) fur {list_title}.<br /></p>\n<p>Wenn Sie irgendwelche anderen Fragen haben, fÃ¼hlen Sie bitte sich frei, mit uns in Verbindung zu treten.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>');
INSERT INTO `email_templates` (`id`, `type`, `title`, `mail_subject`, `email_body_text`, `email_body_html`) VALUES
(208, 'forgot_password_gr', 'Forgot Password', 'Passwort vergessen', 'Liebes Mitglied,\n\nIm Folgenden haben wir Ihre Kontodaten erwÃ¤hnt.\n\nAuf geht\'s,\n\nEmail_id : {email}\n\nPasswort : {password}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Liebes Mitglied,</td>\n</tr>\n<tr>\n<td>\n<p>Im Folgenden haben wir Ihre Kontodaten erwÃ¤hnt.</p>\n<p>Auf geht\'s,</p>\n<p>Email_id : {email}</p>\n<p>Passwort : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(209, 'checkin_host_gr', 'Host: Check In', 'Anreise', 'Hallo {host_name},\n\n{traveler_name} Ist checkin zu {list_title}.\n\nBenutzername: {guest_name}\nListennamen : {list_title}\nPreis : {currency}{price}\nCheckin-Datum : {checkin}\nCheckout-Datum : {checkout}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} Ist checkin zuÂ {list_title} .<br /><br />\nBenutzername : {traveler_name}.<br /><br />\nListennamen : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin-Datum : {checkin}.<br /><br />\nCheckout-Datum : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(210, 'users_signin_gr', 'Users Signin', 'Willkommen zu {site_name}', 'Liebes Mitglied,\n\nWir freuen uns auf Ihren Besuch {site_name}.\n\nIm Folgenden haben wir Ihre Kontodaten erwÃ¤hnt.\n\nAuf geht\'s,\n\nEmail_id : {email}\n\nPasswort: {password}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Liebes Mitglied,</td>\n</tr>\n<tr>\n<td>\n<p>Wir freuen uns auf Ihren Besuch {site_name}.</p>\n<p>Im Folgenden haben wir Ihre Kontodaten erwÃ¤hnt.</p>\n<p>Auf geht\'s,</p>\n<p>Email_id : {email}</p>\n<p>Passwort: {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(211, 'reset_password_gr', 'Reset Password', 'Passwort zurÃ¼cksetzen', 'Liebes Mitglied,\n\nIm Folgenden haben wir Ihre neuen Kontoinformationen erwÃ¤hnt.\n\nAuf geht\'s,\n\nPasswort : {password}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Liebes Mitglied,</td>\n</tr>\n<tr>\n<td>\n<p>Im Folgenden haben wir Ihre neuen Kontoinformationen erwÃ¤hnt.</p>\n<p>Auf geht\'s,</p>\n<p>Passwort : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(212, 'admin_payment_gr', 'Admin payment to Host', 'Admin bezahlt Ihre GebÃ¼hren fÃ¼r {list_title}', 'Hallo {username},\n\nAdmin bezahlt Ihre GebÃ¼hren fÃ¼r {list_title}.\n\nDetails wie folgt,\n\nOrtsname : {list_title}\nCheck-in : {checkin}\nCheck-out : {checkout}\nPreis : {payed_price}\nZahlung durch : {payment_type}\nZahl-ID: {pay_id}\nGezahltes Datum: {payed_date}\n\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>\n<p>hallo {username} ,</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>Admin bezahlt Ihre GebÃ¼hren fÃ¼r {list_title}.</p>\n<br />\n<p>Details wie folgt,</p>\n<p>Ortsname : {list_title}</p>\n<p>Check-in : {checkin}</p>\n<p>Check-out : {checkout}</p>\n<p>Preis : {payed_price}</p>\n<p>Zahlung durch : {payment_type}</p>\n<p>Zahl-ID : {pay_id}</p>\n<p>Gezahltes Datum : {payed_date}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0px;\">{site_name} Mannschaft</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(213, 'contact_form_gr', 'Contact Form', 'Nachricht vom Kontaktformular erhalten', 'Hallo Administrator,\n\nEine Nachricht erhalten Sie von uns {date} at {time}.\n\nDetails wie folgt,\n\nName : {name}\n\nEmail : {email}\n\nNachricht : {message}\n\n--\nDanke und GrÃ¼ÃŸe,\n\n{site_name} Mannschaft', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Admin,</td>\n</tr>\n<tr>\n<td>\n<p>Eine Nachricht erhalten Sie von uns {date} at {time}.</p>\n<p>Details wie folgt,</p>\n<p>Name : {name}</p>\n<p>Email : {email}</p>\n<p>Nachricht : {message}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0px;\">{site_name} Mannschaft</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(214, 'invite_friend_gr', 'Invite My Friends', '{username} laden Sie ein.', 'Hallo Friend\'s,\n\n{username} MÃ¶chte, dass Sie einladen {site_name}\n\n{dynamic_content}\n\n{click_here}\n\n--\nGrÃ¼ÃŸe,\n{username}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Friends,</td>\n</tr>\n<tr>\n<td>\n<p>{username} MÃ¶chte, dass Sie einladen</p>\n<p>{site_name}</p>\n<p>{dynamic_content}</p>\n<p>Â {click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0px;\">{username}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(215, 'email_verification_gr', 'Email Verification Link', '{site_name} E-Mail-BestÃ¤tigungslink', 'Hallo {user_name},\n\nBitte klicken Sie auf den untenstehenden Link fÃ¼r Ihre E-Mail-BestÃ¤tigung {site_name}.\n\n{click_here}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {user_name},</td>\n</tr>\n<tr>\n<td>\n<p>Bitte klicken Sie auf den untenstehenden Link fÃ¼r Ihre E-Mail-BestÃ¤tigung {site_name}.</p>\n<p>Â {click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(216, 'referral_credit_gr', 'Referral Credit', 'Sie verdienen {amount} Von Verweisen', 'Hallo {username},\n\nSie verdienen die {amount} by {friend_name}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {user_name},</td>\n</tr>\n<tr>\n<td><p>\nSie verdienen die {amount} by {friend_name}</p>\n</td>\n</tr><tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(217, 'User_join_to_Referal_mail_gr', 'User join to Referal mail', 'Deine Freund-Anmeldung', 'sehr geehrter {refer_name},\n\nIhr Freund {friend_name} ist jetzt Mitglied in {site_name} .Now, $ 100 ist Kredit in Ihrem Reisekredit mÃ¶gliches Konto.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>sehr geehrter {refer_name},</td>\n</tr>\n<tr>\n<td><br />Ihr Freund {friend_name} ist jetzt Mitglied in {site_name} .Now, $ 100 ist Kredit in Ihrem Reisekredit mÃ¶gliches Konto.<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(218, 'contact_request_to_host_gr', 'Contact Request to Host', 'Kontaktanfrage', 'Hallo {host_username},\n		\nBitte klicken Sie auf den folgenden Link, um den Benutzer zu kontaktieren : {link}	\n\nListe : {title}\n		\nCheckin-Datum : {checkin}\n		\nCheckout-Datum : {checkout}\n		\nGÃ¤ste : {guest}\n		\nGast-Mitteilung  : {message}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_username},</td>\n</tr>\n<tr>\n<td>\nBitte klicken Sie auf den folgenden Link, um den Benutzer zu kontaktieren : {link}<br /><br />\n			\nListe : {title}<br /><br />\n		\nCheckin-Datum : {checkin}<br /><br />\n		\nCheckout-Datum : {checkout}<br /><br />\n		\nGÃ¤ste : {guest}<br /><br />\n		\nGast-Mitteilung  : {message}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(219, 'contact_request_to_guest_gr', 'Contact Request to Guest', 'Kontaktanfrage', 'Hallo {traveller_username},\n\nSie haben Ihre Anfrage an {host_username} gesendet.\n\nListe: {title}\n\nCheckin Datum: {checkin}\n\nKasse Datum: {checkout}\n\nGÃ¤ste: {guest}\n\nIhre Nachricht: {message}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<Table style = \"width: 100%;\" Cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Hallo {traveller_username}, </ td>\n</ Tr>\n<Tr>\n<Td>\nSie haben Kontaktanfrage an {host_username} gesendet. <br /> <br />\n\nListe: {title} <br /> <br />\n\nEinchecken Datum: {checkin} <br /> <br />\n\nKasse Datum: {checkout} <br /> <br />\n\nGÃ¤ste: {guest} <br /> <br />\n\nIhre Nachricht: {message} <br /> <br /> </ td>\n</ Tr>\n<Tr>\n<Td>\n<P style = \"margin: 0 10px 0 0;\"> - </ p>\n<P style = \"margin: 0 0 10px 0;\"> Danke und GrÃ¼ÃŸe, </ p>\n<P style = \"margin: 0 10px 0 0;\"> Administrator </ p>\n<P style = \"margin: 0px;\"> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(220, 'request_granted_to_guest_gr', 'Contact Request Granted to Guest', 'Kontaktanfrage erwÃ¼nscht', 'Hallo {traveller_username},\n\nIhre Kontaktanfrage wird von {host_username}.\n		\nHoste-Email : {host_email}\n\nListe : {title}\n		\nCheckin-Datum : {checkin}\n		\nCheckout-Datum : {checkout}\n			\nHostMessage  : {message}\n\nURL fÃ¼r die Buchung  : {link}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveller_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nIhre Kontaktanfrage wird von {host_username}.\n<br /><br />\n\nHoste-Email : {host_email}<br /><br /> \n\nListe : {title}<br /><br /> \n\nCheckin-Datum : {checkin}<br /><br /> \n\nCheckout-Datum : {checkout}<br /><br /> \n\nPreis : {price}<br /><br />\n\nHostMessage : {message}<br /><br /> \n\nURL fÃ¼r die Buchung : {link}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(221, 'request_granted_to_host_gr', 'Contact Request Granted to Host', 'Kontaktanfrage erwÃ¼nscht', 'Hallo {host_username},\n\nSie haben die Kontaktanfrage akzeptiert {traveller_username}.\n		\nGueste-Email : {guest_email}\n\nListe : {title}\n		\nCheckin-Datum : {checkin}\n		\nCheckout-Datum : {checkout}\n\nPreis : {price}\n				\nDeine Nachricht  : {message}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nSie haben die Kontaktanfrage akzeptiert{traveller_username}. <br /><br />\n\nGuesteEmail : {guest_email}<br /><br />	\n		\nListe : {title}<br /><br />\n		\nCheckin-Datum : {checkin}<br /><br />\n		\nCheckout-Datum : {checkout}<br /><br />\n\nPreis : {price}<br /><br />\n	\nDeine Nachricht  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(222, 'contact_request_to_admin_gr', 'Contact Request to Admin', 'Kontaktanfrage', 'Hallo Admin,\n		\n{traveller_username} Gesendet Kontaktanfrage an {host_username}.\n\nListe : {title}\n		\nCheckin-Datum : {checkin}\n		\nCheckout-Datum : {checkout}\n		\nGueste : {guest}\n\nPreis : {price}\n		\nGast-Mitteilung  : {message}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Admin,</td>\n</tr>\n<tr>\n<td><br />\n{traveller_username} Gesendet Kontaktanfrage an {host_username}. <br /><br />		\nListe : {title}<br /><br />	\nCheckin-Datum : {checkin}<br /><br />\n		\nCheckout-Datum : {checkout}<br /><br />\n	\nPreis : {price}<br /><br />\n		\nGast-Mitteilung  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(223, 'request_granted_to_admin_gr', 'Contact Request Granted to Admin', 'Kontaktanfrage erwÃ¼nscht', 'Hallo Administrator,\n\n{host_username} Akzeptieren Sie die Kontaktanfrage fÃ¼r{traveller_username}.\n		\nGÃ¤ste-E-Mail : {guest_email}\n\nHost-E-Mail : {host_email}\n\nListe : {title}\n		\nCheckin-Datum : {checkin}\n		\nCheckout-Datum : {checkout}\n\nPreis : {price}\n				\nHost-Nachricht  : {message}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td><br />\n{host_username} Akzeptieren Sie die Kontaktanfrage fÃ¼r {traveller_username}.<br /><br />\n\nGÃ¤ste-E-Mail : {guest_email}<br /><br />	\n\nHost-E-Mail : {host_email}<br /><br />	\n\nListe : {title}<br /><br />	\n\nCheckin-Datum : {checkin}<br /><br />\n		\nCheckout-Datum : {checkout}<br /><br />\n\nPreis : {price}<br /><br />	\n			\nHost-Nachricht  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(224, 'special_request_granted_to_guest_gr', 'Contact Request Granted with Special Offer to Guest', 'Kontaktanfrage mit Sonderangebot', 'Hallo {traveller_username},\n\n{host_username} Nehmen Sie mit uns Kontakt auf.\n		\nHost-E-mail : {host_email}\n\nListe : {title}\n		\nCheckin-Datum : {checkin}\n		\nCheckout-Datum : {checkout}\n\nPreis : {price}\n				\nMessage  : {message}\n\nURL fÃ¼r die Buchung  : {link}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveller_username},</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} Nehmen Sie mit uns Kontakt auf.<br /><br />\n\nHost-E-mail : {host_email}<br /><br />	\n\nListe : {title}<br /><br />	\n\nCheckin-Datum : {checkin}<br /><br />\n		\nCheckout-Datum : {checkout}<br /><br />\n\nPreis : {price}<br /><br />\n			\nMessage  : {message}<br /><br />\n\nURL fÃ¼r die Buchung  : {link}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(225, 'special_request_granted_to_host_gr', 'Contact Request Granted with Special Offer to Host', 'Kontaktanfrage mit Sonderangebot', 'Hallo {host_username},\n\nSie haben die {traveller_username} Kontaktanfrage mit Sonderangebot.\n		\nGuste-E-mail : {guest_email}\n\nListe : {title}\n		\nCheckin-Datum : {checkin}\n		\nCheckout-Datum : {checkout}\n\nPreis : {price}\n			\nDeine Nachricht  : {message}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_username},</td>\n</tr>\n<tr>\n<td><br />\n\nSie haben die {traveller_username} Kontaktanfrage mit Sonderangebot.<br /><br />\n\nGuste-E-mail : {guest_email}<br /><br />\n\nListe : {title}<br /><br />	\n\nCheckin-Datum : {checkin}<br /><br />\n		\nCheckout-Datum : {checkout}<br /><br />\n\nPreis : {price}<br /><br />\n			\nDeine Nachricht  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(226, 'special_request_granted_to_admin_gr', 'Contact Request Granted with Special Offer to Admin', 'Kontaktanfrage mit Sonderangebot', 'Hallo Administrator,\n\n{host_username} die Anfrage {traveller_name} mit einem Sonderangebot akzeptieren.		\n\nGuste-E-mail : {guest_email}\n\nHost-E-mail : {host_email}\n\nListe : {title}\n		\nCheckin-Datum : {checkin}\n		\nCheckout-Datum : {checkout}\n\nPreis : {price}\n			\nHost-Nachricht  : {message}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} die Anfrage {traveller_name} mit einem Sonderangebot akzeptieren.<br /><br />\n\nGuste-E-mail : {guest_email}<br /><br />\n\nHost-E-mail : {host_email}<br /><br />\n\nListe : {title}<br /><br />	\n\nCheckin-Datum : {checkin}<br /><br />\n		\nCheckout-Datum : {checkout}<br /><br />\n\nPreis : {price}<br /><br />\n			\nHost-Nachricht  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(227, 'request_cancel_to_guest_gr', 'Contact Request Cancelled to Guest', 'Es tut uns leid! Wird Ihre Kontaktanfrage storniert', 'Hallo {traveller_username},\n\nIhre Kontaktanfrage wird durch {host_username} fÃ¼r {title} verweigert.\n\nHost-Nachricht: {message}\n\n-\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveller_username} ,</td>\n</tr>\n<tr>\n<td>\n<p>Ihre Kontaktanfrage wird durch {host_username} fÃ¼r {title} verweigert.</p><br /><br />\nHost-Nachricht: {message}<br /><br />\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 0 10px 0;\">Administrator,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(228, 'request_cancel_to_host_gr', 'Contact Request Cancelled to Host', 'Sie haben die Kontaktanfrage storniert', 'Hallo {host_username},\n\nSie haben die {traveller_username} Kontaktanfrage fÃ¼r {title} storniert.\n\nIhre Nachricht: {message}\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_username} ,</td>\n</tr>\n<tr>\n<td>\n<p>Sie haben die {traveller_username} Kontaktanfrage fÃ¼r {title} storniert.</p><br /><br />\nIhre Nachricht: {message}<br /><br />\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 0 10px 0;\">Administrator,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(229, 'checkout_admin_gr', 'Admin: Check Out', 'Reisescheck', 'Hallo Administrator,\n\n{traveler_name} wird aus {list_title} Ã¼berprÃ¼ft.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} wird aus {list_title} Ã¼berprÃ¼ft.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(230, 'request_cancel_to_admin_gr', 'Contact Request Cancelled to Admin', '{host_username} Hat die Kontaktanforderung {traveller_username} abgebrochen', 'Hallo Administrator,\n\n{host_username} Stornierte die {traveller_username} Kontaktanfrage fÃ¼r {title}.\n\nHost-Nachricht: {message}\n\n--\nDanke und GrÃ¼ÃŸe,\n\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<p>{host_username} Stornierte die {traveller_username} Kontaktanfrage fÃ¼r {title}.</p>\n<br /><br />\nHost-Nachricht: {message}\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(231, 'checkout_host_gr', 'Host: Check Out', 'Reisescheck', 'Hallo {host_name},\n\n{traveler_name} Wird von {list_title} Ã¼berprÃ¼ft.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Wird von {list_title} Ã¼berprÃ¼ft.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(232, 'checkout_traveler_gr', 'Traveler: Check Out', 'Ihre Abreise', 'Hallo {traveler_name},\n\nVielen Dank, Sie werden von {list_title} Ã¼berprÃ¼ft.\n\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Vielen Dank, Sie werden von {list_title} Ã¼berprÃ¼ft.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator,</p>\n<p>{site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(233, 'contact_discuss_more_to_guest_gr', 'Contact Request - Discuss more to Guest', 'Contact Request - Discuss more', 'Hallo {traveller_username},\n\n{host_username} MÃ¶chte mehr mit Ihnen besprechen.\n\nHost-E-mail : {host_email}\n\nListe : {title}\n		\nCheckin-Datum : {checkin}\n		\nCheckout-Datum : {checkout}\n				\nNachricht  : {message}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveller_username},</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} MÃ¶chte mehr mit Ihnen besprechen.<br /><br />\n\nHost-E-mail : {host_email}<br /><br />	\n\nListe : {title}<br /><br />	\n\nCheckin-Datum : {checkin}<br /><br />\n		\nCheckout-Datum : {checkout}<br /><br />\n			\nNachricht  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(234, 'contact_discuss_more_to_host_gr', 'Contact Request - Discuss more to Host', 'Kontakt Anfrage - Besprechen Sie mehr', 'Hallo {host_username},\n\nSie wollen mehr mit diskutieren {traveller_username}.\n\nGuste-E-mail : {guest_email}\n\nListe : {title}\n		\nCheckin-Datum : {checkin}\n		\nCheckout-Datum : {checkout}\n				\nDeine Nachricht  : {message}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_username},</td>\n</tr>\n<tr>\n<td><br />\n\nSie wollen mehr mit diskutieren{traveller_username}.<br /><br />\n\nGuste-E-mail : {guest_email}<br /><br />	\n\nListe : {title}<br /><br />	\n\nCheckin-Datum : {checkin}<br /><br />\n		\nCheckout-Datum : {checkout}<br /><br />\n			\nDeine Nachricht : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(235, 'contact_discuss_more_to_admin_gr', 'Contact Request - Discuss more to Admin', 'Kontakt Anfrage - Besprechen Sie mehr', 'Hallo Administrator,\n\n{host_username} mÃ¶chte mehr mit {traveller_username} diskutieren.\n\nGuste-E-mail : {guest_email}\n\nHost-E-mail : {host_email}\n\nListe : {title}\n		\nCheckin-Datum : {checkin}\n		\nCheckout-Datum : {checkout}\n				\nHost-Nachricht  : {message}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} mÃ¶chte mehr mit {traveller_username} diskutieren.<br /><br />\n\nGuste-E-mail : {guest_email}<br /><br />\n\nHost-E-mail : {host_email}<br /><br />\n\nListe : {title}<br /><br />\n\nCheckin-Datum : {checkin}<br /><br />\n		\nCheckout-Datum : {checkout}<br /><br />\n			\nHost-Nachricht  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Danke und GrÃ¼ÃŸe,</p>\n<p style=\"margin: 0 10px 0 0;\">Administrator</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(236, 'checkin_admin_gr', 'Admin: CheckIn', 'Anreise', 'Hallo Administrator,\n\n{traveler_name} wird in {list_title} geprÃ¼ft.\n\nGusteName : {traveler_name}\nGuste-E-mail : {traveler_email}\nHostname : {host_name}\nHost-E-mail : {host_email}\nListename : {list_title}\nPreis : {currency}{price}\nCheckin-Datum : {checkin}\nCheckout-Datum : {checkout}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} wird in {list_title} geprÃ¼ft.<br /><br />\nGustename : {traveler_name}.<br /><br />\nGuste-E-mail : {traveler_email}.<br /><br />\nHostname : {host_name}.<br /><br />\nHost-E-mail : {host_email}.<br /><br />\nListename : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin-Datum : {checkin}.<br /><br />\nCheckout-Datum : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(237, 'checkin_traveller_gr', 'Traveler: Check In', 'Ihr Check In', 'Hallo {traveler_name},\n\nVielen Dank, Sie sind checkin to {list_title}.\n\nHostname : {host_name}\nListename : {list_title}\nPreis : {currency}{price}\nCheckin-Datum : {checkin}\nCheckout-Datum : {checkout}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nVielen Dank, Sie sind checkin to {list_title}.<br /><br />\nHostname : {host_name}.<br /><br />\nListename : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin-Datum : {checkin}.<br /><br />\nCheckout-Datum : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Administrator</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(238, 'refund_admin_gr', 'Admin: Refund', 'Erstattung von Admin', 'Hallo Administrator,\n\nSie haben das Konto {refund_amt} auf {name} {account} zurÃ¼ckerstattet.\n\nGustename : {traveler_name}\nGuste-E-mail : {traveler_email}\nHostname : {host_name}\nHost-E-mail : {host_email}\nListename : {list_title}\nPreis : {currency}{price}\nCheckin-Datum : {checkin}\nCheckout-Datum : {checkout}\nRÃ¼ckerstattungsbetrag : {refund_amt}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<br />\nSie haben das Konto {refund_amt} auf {name} {account} zurÃ¼ckerstattet.<br /><br />\nGustename : {traveler_name}.<br /><br />\nGuste-E-mail : {traveler_email}.<br /><br />\nHostname : {host_name}.<br /><br />\nHost-E-mail : {host_email}.<br /><br />\nListename : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin-Datum : {checkin}.<br /><br />\nCheckout-Datum : {checkout}.<br /><br />\nRÃ¼ckerstattungsbetrag : {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator {site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(239, 'refund_host_gr', 'Host: Refund', 'Erstattung von Admin', 'Hallo {host_name},\n\nAdmin hat den {refund_amt} Betrag auf Ihr Konto zurÃ¼ckerstattet. {account}. \n\nGustename : {traveler_name}\nListename : {list_title}\nPreis : {currency}{price}\nCheckin-Datum : {checkin}\nCheckout-Datum : {checkout}\nRÃ¼ckerstattungsbetrag : {refund_amt}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nAdmin hat den {refund_amt} Betrag auf Ihr Konto zurÃ¼ckerstattet. {account}. <br /><br />\nGustename : {traveler_name}.<br /><br />\nListename : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin-Datum : {checkin}.<br /><br />\nCheckout-Datum : {checkout}.<br /><br />\nRÃ¼ckerstattungsbetrag: {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(240, 'refund_guest_gr', 'Traveler: Refund', 'Erstattung von Admin', 'Hallo {traveler_name},\n\nAdmin hat den {refund_amt} Betrag auf Ihr Konto zurÃ¼ckerstattet. {account}.\n\nHostname : {host_name}\nListename : {list_title}\nPreis : {currency}{price}\nCheckin-Datum : {checkin}\nCheckout-Datum : {checkout}\nRÃ¼ckerstattungsbetrag : {refund_amt}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nAdmin hat den {refund_amt} Betrag auf Ihr Konto zurÃ¼ckerstattet. {account}.<br /><br />\nHostname : {host_name}.<br /><br />\nListename : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin-Datum : {checkin}.<br /><br />\nCheckout-Datum : {checkout}.<br /><br />\nRÃ¼ckerstattungsbetrag  : {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator {site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(241, 'list_create_host_gr', 'List creation to Host', 'Sie haben die neue Liste angelegt.', 'Hallo {host_name},\n\nSie haben die neue Liste angelegt.\n\nListename : {list_title}\n\nLink : {link}\n\nPreis : {price}\n\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nSie haben die neue Liste angelegt.<br /><br />\nListename : {list_title}.<br /><br />\nLink : {link}.<br /><br />\nPreis : {price}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(242, 'list_create_admin_gr', 'List creation to Admin', '{host_name} Haben die neue Liste erstellt', 'Hello Administrator,\n\n{host_name} Haben die neue Liste erstellt\n\nHostname : {host_name}\n\nListename : {list_title}\n\nLink : {link}\n\nPreis : {price}\n\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<br />\n{host_name} Haben die neue Liste erstellt<br /><br />\nHostname : {host_name}.<br /><br />\nListename : {list_title}.<br /><br />\nLink : {link}.<br /><br />\nPreis : {price}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(243, 'list_delete_host_gr', 'List deletion to Host', 'Sie haben die Liste gelÃ¶scht.', 'Hallo {host_name},\n\nSie haben die Liste gelÃ¶scht.\n\nListename : {list_title}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nSie haben die Liste gelÃ¶scht.<br /><br />\nListename : {list_title}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(244, 'list_delete_admin_gr', 'List deletion to Admin', '{host_name} Haben die Liste gelÃ¶scht', 'Hallo Administrator,\n\n{host_name} Haben die Liste gelÃ¶scht\n\nHostname : {host_name}\n\nListename : {list_title}\n\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<br />\n{host_name} Haben die Liste gelÃ¶scht<br /><br />\nHostname : {host_name}.<br /><br />\nListename : {list_title}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(245, 'payment_issue_to_admin_gr', 'Payment issue mail to Admin', 'Zahlung falsch konfiguriert', 'Hallo Administrator,\n\n{payment_type} API-Anmeldeinformationen sind falsch konfiguriert.\n\n{username} Versuchte, die Zahlung zu leisten.\n\nE-mail-ID : {email_id}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<p>{payment_type} API-Anmeldeinformationen sind falsch konfiguriert.</p>\n<p>{username} Versuchte, die Zahlung zu leisten.</p>\n<p>E-mail-ID : {email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(246, 'refund_host_commission_gr', 'Host: Accept Commission Refund', 'RÃ¼ckerstattung des Hosts Accept Commission von Admin', 'Hallo {host_name},\n\nAdmin hat den {refund_amt} Betrag auf Ihr Konto zurÃ¼ckerstattet{account}.\n\nListename : {list_title}\nRefunded Amount : {refund_amt}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>Admin hat den {refund_amt} Betrag auf Ihr Konto zurÃ¼ckerstattet{account}.</p>\n<p>Listename : {list_title}</p>\n<p>RÃ¼ckerstattungsbetrag : {refund_amt}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(247, 'refund_host_commission_admin_gr', 'Admin: Accept Commission Refund', 'Sie haben RÃ¼ckerstattung der Host Accept Commission', 'Hallo Administrator,\n\nSie haben den Betrag {refund_amt} auf Ihr Konto Ã¼berwiesen{account}. \n\nListename : {list_title}\nHostname : {host_name}\nRÃ¼ckerstattungsbetrag : {refund_amt}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<p>Sie haben den Betrag {refund_amt} auf Ihr Konto Ã¼berwiesen{account} .\n</p>\n<p>Listename : {list_title}</p>\n<p>Hostname : {host_name}</p>\n<p>RÃ¼ckerstattungsbetrag : {refund_amt}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(248, 'passport_verification_gr', 'Passport verification for administrator', '{user_name} Hat die Pass-ÃœberprÃ¼fung Antrag, userid-{user_id}', '\\ N \\ n {user_name} hat die PassÃ¼berprÃ¼fung gesendet, Benutzer-ID {user_id} \\ n \\ nDetails wie folgt: \\ n \\ nBenutzername: {user_name} \\ nUser-ID: {user_id} \\ n \\ n \\ n \\ N - \\ nThanks und Respekt, \\ n \\ n {userrname}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\\n<tbody>\\n<tr>\\n<td>Hallo Administrator,</td>\\n</tr>\\n<tr>\\n<td>\\n<p>{user_name} hat die PassÃ¼berprÃ¼fung gesendet, Benutzer-ID-{user_id}.</p>\\n<p>Details wie folgt: </p>\\n<p>User Name : {user_name}</p>\\n<p>User Id : {user_id}</p>\\n\\n</td>\\n</tr>\\n<tr>\\n<td>\\n<p style=\"margin: 0 10px 0 0;\">--</p>\\n<p style=\"margin: 0 0 10px 0;\">Thanks und Respekt,</p>\\n<p style=\"margin: 0px;\">{user_name} </p>\\n</td>\\n</tr>\\n</tbody>\\n</table>'),
(249, 'change password to user_gr', 'admin change to the password', 'Willkommen zu  {site_name}', 'Administrator Passwort Ã¤ndern: {new_password} Konformes Passwort: {conform_password} Liebes Mitglied, altes Passwort: {new_password} Konformes Passwort: {conform_password} - Danke und GrÃ¼ÃŸe, Administrator {site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Liebes Mitglied,</td>\n</tr>\n<tr>\n<td>\n<p>Â Sie haben Ihr Passwort erfolgreich geÃ¤ndert</p>\n<p>Bitte verwenden Sie die unten angegebenen Passwortdaten fÃ¼r die Anmeldung.</p>\n<p>Altes Passwort:{new_password}</p>\n<p><span>Neues Kennwort:{conform_password}</span></p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator</p>\n<p>{site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(250, 'spam_gr', 'Spam List', 'Administrator Message - Listing gemeldet als Spam!', '**** ADMINISTRATOR MELDUNG ****\n\n\n\nDie folgende Liste wurde als Spam gemeldet:\n\nURL fÃ¼r Spam-Auflistung: {list_url}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>****ADMINISTRATOR MELDUNG****</td>\n</tr>\n<tr>\n<td>\n<p>Â FLEXIFLAT.COM SPAMBERICHT!</p>\n<p>Die folgende Liste wurde als Spam gemeldet:<br /><br />{list_url}</p>\n</td>\n</tr>\n<tr>\n<td>Â </td>\n</tr>\n</tbody>\n</table>\n<p>Â </p>'),
(251, 'host_notify_review_gr', 'Host Notification for Review', '{guest_name} Hat eine Bewertung von {host_name}', 'Hallo {guest_name},\n\n{host_name} Hat eine Rezension Ã¼ber Sie in {list_name} auf {site_name} hinzugefÃ¼gt\n\nUnd er wartet auf Ihre Bewertung.\nHostname : {host_name}\nListename : {list_name}\n\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {guest_name},</td>\n</tr>\n<tr>\n<td><br/ > {host_name} Hat eine Rezension Ã¼ber Sie in {list_name} auf {site_name} hinzugefÃ¼gt <br />Und er wartet auf Ihre Bewertung.\n<br/>\nHostname : {host_name}<br/>\nListename : {list_name}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(252, 'guest_notify_review_gr', 'Guest Notification for Review', '{guest_name} Hat ausgecheckt.', 'Hallo {host_name},\n\n{guest_name} Hat Ã¼berprÃ¼ft von Ihrer Liste {list_name} auf {site_name}\n\nUnd er wartet auf Ihre Bewertung.\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name},</td>\n</tr>\n<tr>\n<td><br /> {guest_name} Hat Ã¼berprÃ¼ft von Ihrer Liste {list_name} auf {site_name}<br />Und er wartet auf Ihre Bewertung.</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(253, 'list_notification_gr', 'Notification for calendar update', 'Die Liste {list_name} benÃ¶tigt ein Update', 'Hallo {host_name},\n\nDie {list_name}, die Sie in {site_name} aufgefÃ¼hrt haben, haben keine Updates im letzten Monat.\n\nUm Ihr Ranking in den Suchergebnissen zu verbessern, aktualisieren Sie bitte Ihre Liste\n-\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>\nDie {list_name}, die Sie in {site_name} aufgefÃ¼hrt haben, haben keine Updates im letzten Monat.</p>\n<p>\nUm Ihr Ranking in den Suchergebnissen zu verbessern, aktualisieren Sie bitte Ihre Liste. \n</p>\n\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>\n'),
(254, 'host_notification_gr', 'Upcoming notification for host', '{traveler_name} Hat eine Reservierung auf Ihrem {list_title} auf morgen', 'Hallo {host_name},\n\n{traveler_name} Hat eine Reservierung auf Ihrem {list_title} auf morgen\n\nDetails wie folgt,\n\nName des Reisenden: {traveler_name}\nKontakt E-Mail-ID: {traveler_email_id}\nOrtsname: {list_title}\nEinchecken: {checkin}\nAbreise: {checkout}\n\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Hat eine Reservierung auf Ihrem {list_title} auf morgen</p>\n<p>Details wie folgt,\n</p>\n\n<p>Name des Reisenden : {traveler_name}</p>\n<p>Kontakt E-Mail-ID : {traveler_email_id}</p>\n<p>Ortsname : {list_title}</p>\n<p>Einchecken : {checkin}</p>\n<p>Abreise: {checkout}</p>\n\n\n</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>\n'),
(255, 'tc_book_to_admin_it', 'Admin notification for  Travel cretid booking', ' {traveler_name} ha inviato la richiesta di prenotazione utilizzando i suoi Cretids di viaggio', 'Ciao Admin,\n\n{traveler_name} inviato la richiesta di prenotazione per prenotare la{list_title} metti su {book_date} at {book_time} utilizzando i suoi crediti di viaggio.\n\nDettagli come segue,\n\nNome Traveler : {traveler_name}\nContatto Email Id: {traveler_email_id}\nNome del luogo : {list_title}\nCheck in : {checkin}\nCheck out : {checkout}\nPrezzo di mercato : {market_price}\nsomma pagata: {payed_amount}\nCrediti di viaggio : {travel_credits} \nNome Host : {host_name}\nHost Email Id : {host_email_id} \n\n--\nGrazie e saluti,\n\n{site_name} Squadra', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} inviato la richiesta di prenotazione per prenotare la{list_title} metti su {book_date} at {book_time} utilizzando i suoi crediti di viaggio.</p>\n<p>\nDettagli come segue,</p>\n<p>\nNome Traveler : {traveler_name}</p>\n<p>Contatto Email Id: {traveler_email_id}</p>\n<p>Nome del luogo : {list_title}</p>\n<p>Check in : {checkin}</p>\n<p>Check out : {checkout}</p>\n<p>Prezzo di mercato : {market_price}</p>\n<p>somma pagata : {payed_amount}</p>\n<p>Crediti di viaggio : {travel_credits}</p>\n<p>Nome Host: {host_name}</p>\n<p>Host Email Id : {host_email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0px;\">{site_name} Squadra</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(256, 'tc_book_to_host_it', 'Host notification for  Travel cretid booking', ' {traveler_name}inviato la richiesta di prenotazione utilizzando i suoi Cretids di viaggio', 'Ciao {username},\n\n{traveler_name} ha inviato la richiesta di prenotazione per prenotare il tuo {list_title} luogo il {book_date} a {book_time} utilizzando i suoi crediti di viaggio.\n\n\nCi metteremo in contatto con il pagamento appropriata.\n\nDettagli come segue,\n\nNome Traveler : {traveler_name}\nContatto Email Id : {traveler_email_id}\nNome del luogo : {list_title}\nCheck in : {checkin}\nCheck out : {checkout}\nPrezzo : {market_price}\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {username} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} ha inviato la richiesta di prenotazione per prenotare il tuo {list_title} luogo il {book_date} a {book_time} utilizzando i suoi crediti di viaggio.</p>\n<p>Dettagli come segue,</p>\n<p>Nome Traveler : {traveler_name}</p>\n<p>Contatto Email Id: {traveler_email_id}</p>\n<p>Nome del luogo : {list_title}</p>\n<p>Check in : {checkin}</p>\n<p>Check out : {checkout}</p>\n<p>Prezzo : {market_price}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(257, 'admin_mass_email_it', 'Admin mass email', '{subject}', 'Ciao utente,\n\n{dynamic_content}\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao utente,</td>\n</tr>\n<tr>\n<td>\n<p>{dynamic_content}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(258, 'user_reference_it', 'Reference', 'Scrivi {username} un riferimento {site_name}', 'Ciao,\n\n{username} si chiede di fornire un punto di riferimento che possano mostrare pubblicamente il loro {site_name} profilo. {site_name} Ã¨ un mercato comunitario per le sistemazioni che si basa su fiducia e la reputazione. Un riferimento da voi sarebbe davvero contribuire a costruire la loro reputazione con la comunitÃ .\nUn riferimento su {site_name} puÃ² aiutare {username} trovare un luogo fresco per rimanere o ospitare i viaggiatori interessanti.\n\n{click_here} di avere un riferimento per {username}.\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao,</td>\n</tr>\n<tr>\n<td>\n<p>{username} si chiede di fornire un punto di riferimento che possano mostrare pubblicamente il loro {site_name} profilo. {site_name} Ã¨ un mercato comunitario per le sistemazioni che si basa su fiducia e la reputazione. Un riferimento da voi sarebbe davvero contribuire a costruire la loro reputazione con la comunitÃ .\nUn riferimento su {site_name} puÃ² aiutare {username} trovare un luogo fresco per rimanere o ospitare i viaggiatori interessanti.{click_here} di avere un riferimento per {username}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(259, 'reference_receive_it', 'Reference Receive', 'Hai ricevuto un nuovo riferimento in {site_name}!', 'Ciao {username},\n\n{other_username} ha scritto un riferimento per voi. Prima si va al tuo profilo pubblico, Ã¨ possibile rivedere e accettare o ignorarlo. {click_here} per visualizzare il riferimento.\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {username},</td>\n</tr>\n<tr>\n<td>\n<p>{other_username} ha scritto un riferimento per voi. Prima si va al tuo profilo pubblico, Ã¨ possibile rivedere e accettare o ignorarlo. {click_here} per visualizzare il riferimento.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(260, 'host_reservation_notification_it', 'Reservation notification for host', 'La prenotazione Ã¨ stata richiesta da {traveler_name}', 'Ciao {username},\n\n{traveler_name} prenotato il {list_title} luogo il {book_date} a \n{book_time}.\n\nDettagli come segue,\n\nNome dei viaggiatori: {traveler_name}\nContatto e-mail Id: {traveler_email_id}\nLuogo Nome: {list_title}\nIl check-in: {checkin}\nPartenza: {checkout}\nPrezzo: {market_price}\n\nSi prega di dare conferma cliccando l\'azione di seguito.\n{action_url}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>\n<p>Ciao {username} ,</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} prenotato il {list_title} luogo il {book_date} a \n{book_time}.</p>\n<br />\n<p>Dettagli come segue,\n</p>\n<p>Nome dei viaggiatori: {traveler_name}</p>\n<p>Contatto e-mail Id: {traveler_email_id}</p>\n<p>Luogo Nome: {list_title}</p>\n<p>Check in : {checkin}</p>\n<p>Partenza: {checkout}</p>\n<p>Prezzo: {market_price}</p>\n<br />\n<p>Si prega di dare conferma cliccando l\'azione di seguito.</p>\n<p>{action_url}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0px;\">{site_name} Squadra</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(261, 'traveller_reservation_notification_it', 'Reservation notification for  traveller', 'La vostra richiesta di prenotazione viene inviata con successo', 'Ciao {traveler_name},\n\nLa vostra richiesta di prenotazione viene inviata con successo per l\'host appropriato.\n\nSi prega di attendere la sua conferma.\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>La vostra richiesta di prenotazione viene inviata con successo per l\'host appropriato.\n</p>\n<p>Si prega di attendere la sua conferma.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>');
INSERT INTO `email_templates` (`id`, `type`, `title`, `mail_subject`, `email_body_text`, `email_body_html`) VALUES
(262, 'admin_reservation_notification_it', 'Reservation notification for  administrator', '{traveler_name} ha inviato la richiesta di prenotazione a {host_name}', 'Ciao Admin,\n\n{traveler_name} ha inviato la richiesta di prenotazione per {list_title} luogo il {book_date} a {book_time}.\n\nDettagli come segue,\n\nNome dei viaggiatori: {traveler_name}\nContatto e-mail Id: {traveler_email_id}\nLuogo Nome: {list_title}\nIl check-in: {checkin}\nPartenza: {checkout}\nPrezzo: {market_price}\nNome Host: {host_name}\nHost Email Id: {host_email_id}\n\n--\nGrazie e saluti,\n\n{site_name} Squadra', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} ha inviato la richiesta di prenotazione per {list_title} luogo il {book_date} a {book_time}.</p>\n<p>Dettagli come segue,</p>\n<p>Nome dei viaggiatori: {traveler_name}</p>\n<p>Contatto e-mail Id: {traveler_email_id}</p>\n<p>Luogo Nome : {list_title}</p>\n<p>Il check-in: {checkin}</p>\n<p>Partenza : {checkout}</p>\n<p>Prezzo : {market_price}</p>\n<p>Nome Host: {host_name}</p>\n<p>Host Email Id : {host_email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0px;\">{site_name} Squadra</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(263, 'traveler_reservation_granted_it', 'Traveler : After Reservation granted', 'Congratulazioni! La vostra richiesta di prenotazione Ã¨ concesso.', 'Ciao {traveler_name},\n\nComplimenti, la tua richiesta di prenotazione Ã¨ garantita da {host_name} per {list_name}.\n\nQui di seguito abbiamo accennato i suoi dati di contatto,\n\nNome: {Fname}\nCognome: {Lname}\nLive In: {livein}\nTelefono No: {Phnum}\n\nIndirizzo esatto Ã¨,\n\nIndirizzo: {street_address}\nOpzionale Indirizzo: {optional_address}\nCittÃ : {city}\nStato: {state}\nPaese: {country}\nCAP: {zipcode}\n\nHost Messaggio: {comment}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Complimenti, la tua richiesta di prenotazione Ã¨ garantita da {host_name} per {list_name}.</p>\n<p>Qui di seguito abbiamo accennato i suoi dati di contatto,</p>\n<p>Nome : {Fname}</p>\n<p>Cognome : {Lname}</p>\n<p>Live In : {livein}</p>\n<p>Telefone No : {phnum}</p>\n<p>Indirizzo esatto Ã¨,</p>\n<p>Indirizzo: {street_address},</p>\n<p>Opzionale Indirizzo:{optional_address},</p>\n<p>CittÃ  : {city},</p>\n<p>Stato : {state},</p>\n<p>Paese : {country},</p>\n<p>CAP : {zipcode}</p>\n<p>Host Messaggio : {comment}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(264, 'traveler_reservation_declined_it', 'Traveler : After reservation declined', 'Scusate! La vostra richiesta di prenotazione viene negata', 'Ciao {traveler_name},\n\nSpiacente, la tua richiesta di prenotazione Ã¨ cenato da {host_name} per {list_title}.\n\nHost Messaggio: {comment}\n\nPresto, Ci metteremo in contatto con il pagamento appropriata.\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Spiacente, la tua richiesta di prenotazione Ã¨ cenato da {host_name} per {list_title}.</p>\n<p>Host Messaggio: {comment}</p>\n<p>Presto, Ci metteremo in contatto con il pagamento appropriata.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(265, 'traveler_reservation_cancel_it', 'Traveler : After reservation canceled', '{raveler_name} annullato la tua lista di prenotazione', 'Ciao {host_name},\n\nSiamo spiacenti, il tuo ({status}) lista di prenotazione viene annullata da {traveler_name} per {list_title}.\n\n{user_type} Messaggio: {comment}\n\nCerto ti contatteremo presto, se non vi Ã¨ alcuna bilancia dei pagamenti.\n\nE anche, se avete altre domande, non esitate a contattarci.\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Siamo spiacenti, il tuo ({status}) lista di prenotazione viene annullata da {traveler_name} per {list_title}.</p>\n<p>{user_type} Messaggio: {comment}</p>\n<p>Certo ti contatteremo presto, se non vi Ã¨ alcuna bilancia dei pagamenti.</p>\n<p>E anche, se avete altre domande, non esitate a contattarci.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(266, 'traveler_reservation_expire_it', 'Traveler : Reservation Expire', 'Scusate! Il tuo tipo {type} richiesta Ã¨ scaduto', 'Ciao {traveler_name},\n\nIl tuo tipo {type} richiesta Ã¨ scaduta.\n\nNome Host: {host_name}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nIl tuo tipo {type} richiesta Ã¨ scaduta.<br /><br />\nNome Host: {host_name}<br /><br />\nNome elenco: {list_title}.<br /><br />\nPrezzo : {currency}{price}.<br /><br />\nCheckin Data : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(267, 'host_reservation_expire_it', 'Host : Reservation Expire', '{type} richiesta scaduto per la vostra lista', 'Ciao {host_name},\n\n{type} Tipo di richiesta scaduto per {list_title} prenotato da  {traveler_name}.\n\nNome Cliente: {traveler_name}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{type} Tipo di richiesta scaduto per {list_title} prenotato da  {traveler_name}.<br /><br />\nNome Cliente:{traveler_name}.<br /><br />\nNome elenco: : {list_title}.<br /><br />\nPrezzo:  {currency}{price}.<br /><br />\nCheckin Data: {checkin}.<br /><br />\nCheckout Date:  {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(268, 'admin_reservation_expire_it', 'Admin : Reservation Expire', '{type} richiesta Ã¨ scaduto', 'Ciao Admin,\n\n{traveler_name} {type} richiesta Ã¨ scaduto per {list_title}\n\nNome Cliente: {traveler_name}\nOspite Email: {traveler_email}\nNome Host: {host_name}\nHost E-mail: {host_email}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}\n\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} {type} richiesta Ã¨ scaduto per {list_title}.<br /><br />\nNome Cliente: {traveler_name}.<br /><br />\nOspite Email: {traveler_email}.<br /><br />\nNome Host:  {host_name}.<br /><br />\nHost E-mail:  {host_email}.<br /><br />\nNome elenco:  {list_title}.<br /><br />\nPrezzo : {currency}{price}.<br /><br />\nCheckin Data : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Team</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(269, 'host_reservation_granted_it', 'Host : After Reservation Granted', 'Hai accettato il {traveler_name} richiesta di prenotazione', 'Ciao {host_name},\n\nHai accettato il {traveler_name} richiesta di prenotazione per \n{list_title}.\n\nQui di seguito abbiamo accennato i suoi dati di contatto,\n\nNome: {Fname}\nCognome: {Lname}\nLive In: {livein}\nTelefono No: {Phnum}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Hai accettato il {traveler_name} richiesta di prenotazione per \n{list_title}.</p>\n<p>Qui di seguito abbiamo accennato i suoi dati di contatto,</p>\n<p>Nome : {Fname}</p>\n<p>Cogname : {Lname}</p>\n<p>Live In : {livein}</p>\n<p>Telefone No : {phnum}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(270, 'admin_reservation_granted_it', 'Admin : After Reservation granted', '{host_name} accettato il {traveler_name} richiesta di prenotazione', 'Ciao Admin,\n\n{host_name} accettato il {traveler_name} richiesta di prenotazione per {list_title}.\n\n-\nGrazie e saluti,\n\n{site_name} Squadra', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} accettato il {traveler_name} richiesta di prenotazione per {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0px;\">{site_name} Squadra</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(271, 'host_reservation_declined_it', 'Host : After Reservation Declined', 'Hai rifiutato il {traveler_name} richiesta di prenotazione', 'Ciao {host_name},\n\nHai rifiutato il {traveler_name} richiesta di prenotazione per \n{list_title}.\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Hai rifiutato il {traveler_name} richiesta di prenotazione per \n{list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(272, 'admin_reservation_declined_it', 'Admin : After Reservation Declined', '{host_name} rifiutato l\'{traveler_name} richiesta di prenotazione', 'Ciao Admin,\n\n{host_name} rifiutato l\'{traveler_name} richiesta di prenotazione per {list_title}.\n\n-\nGrazie e saluti,\n\n{site_name} Squadra', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{{host_name} rifiutato l\'{traveler_name} richiesta di prenotazione per {list_title}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0px;\">{site_name} Squadra</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(273, 'admin_reservation_cancel_it', 'Admin : After reservation canceled', '{traveler_name} annullato il {host_name} lista di prenotazione', 'Ciao Admin,\n\n{traveler_name} annullato il {host_name} lista di prenotazione ({status}) per {list_title}.\n\n{penalty}\n\n--\nGrazie e saluti,\n\n{site_name} Squadra', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} annullato il {host_name} lista di prenotazione ({status}) per {list_title}.\n<br/><br/>{penalty}\n</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0px;\">{site_name} Squadra</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(274, 'host_reservation_cancel_it', 'Host : After reservation canceled', '{host_name} cancellato la lista di prenotazione', 'Ciao {traveler_name},\n\n{host_name} cancellare la vostra lista di prenotazione ({status}) per {list_title}.\n\n\nSe avete altre domande, non esitate a contattarci.\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} cancellare la vostra lista di prenotazione ({status}) per {list_title}.<br /></p>\n<p>Se avete altre domande, non esitate a contattarci.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(275, 'forgot_password_it', 'Forgot Password', 'Ha dimenticato la password', 'Caro membro,\n\nQui di seguito abbiamo accennato i dettagli del conto.\n\nEccoci qui,\n\nEMAIL_ID: {email}\n\nPassword: {password}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Caro membro,\n</td>\n</tr>\n<tr>\n<td>\n<p>Qui di seguito abbiamo accennato i dettagli del conto.</p>\n<p>Eccoci qui,</p>\n<p>EMAIL_ID : {email}</p>\n<p>Password : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(276, 'checkin_host_it', 'Host: Check In', 'Controllare viaggiatore nel', 'Ciao {host_name},\n\n{traveler_name} Ã¨ checkin a {list_title}.\n\nNome Cliente: {guest_name}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>CIao {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} Ã¨ checkin a {list_title}.<br /><br />\nNome Cliente:{traveler_name}.<br /><br />\nNome elenco:  {list_title}.<br /><br />\nPrezzo: {currency}{price}.<br /><br />\nCheckin Data: {checkin}.<br /><br />\nCheckout Date: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(277, 'users_signin_it', 'Users Signin', 'Benvenuto a {site_name}', 'Caro membro,\n\nPiacere di conoscerla e il benvenuto alla {site_name}.\n\nQui di seguito abbiamo accennato i dettagli del conto.\n\nEccoci qui,\n\nEMAIL_ID: {email}\n\nPassword: {password}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Caro membro,</td>\n</tr>\n<tr>\n<td>\n<p>Piacere di conoscerla e il benvenuto alla {site_name}.</p>\n<p>Qui di seguito abbiamo accennato i dettagli del conto.</p>\n<p>Eccoci qui,</p>\n<p>EMAIL_ID : {email}</p>\n<p>Password : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(278, 'reset_password_it', 'Reset Password', 'Resetta la password', 'Caro membro,\n\nQui di seguito abbiamo accennato vostri nuovi dettagli dell\'account.\n\nEccoci qui,\n\nPassword: {password}\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Caro membro,</td>\n</tr>\n<tr>\n<td>\n<p>Qui di seguito abbiamo accennato vostri nuovi dettagli dell\'account.</p>\n<p>Eccoci qui,</p>\n<p>Password : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(279, 'admin_payment_it', 'Admin payment to Host', 'Admin pagato le vostre tasse per {list_title}', 'Ciao {username},\n\nAbbiamo pagato le tue spese per {list_title}.\n\nDettagli come segue,\n\nLuogo Nome: {list_title}\nIl check-in: {checkin}\nPartenza: {checkout}\nPrezzo: {payed_price}\nPagamento tramite: {payment_type}\nPay Id: {pay_id}\nPagato Data: {payed_date}\n\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>\n<p>Ciao {username} ,</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>Abbiamo pagato le tue spese per {list_title}.</p>\n<br />\n<p>Dettagli come segue,\n</p>\n<p>Luogo Nome: {list_title}</p>\n<p>II check-in : {checkin}</p>\n<p>Partenza : {checkout}</p>\n<p>Prezzo : {payed_price}</p>\n<p>Pagamento tramite: {payment_type}</p>\n<p>Pay Id : {pay_id}</p>\n<p>Pagato Data : {payed_date}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0px;\">{site_name} Squadra</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(280, 'contact_form_it', 'Contact Form', 'Messaggio ricevuto dal modulo di contatto', 'Ciao Admin,\n\nUn messaggio ricevuto dalla pagina contatti il â€‹â€‹{date} a {time}.\n\nDettagli come segue,\n\nNome: {name}\n\nE-mail: {email}\n\nMessaggio: {message}\n\n-\nGrazie e saluti,\n\n{site_name} Squadra', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<p>Un messaggio ricevuto dalla pagina contatti il â€‹â€‹{date} a {time}.</p>\n<p>Dettagli come segue,</p>\n<p>Nome : {name}</p>\n<p>E-mail : {email}</p>\n<p>Messaggio : {message}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0px;\">{site_name} Squadra</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(281, 'invite_friend_it', 'Invite My Friends', '{username} invito te.', 'Ciao amici,\n\n{username} vi vuole invitare {site_name}\n\n{dynamic_content}\n\n{click_here}\n\n--\nSaluti,\n{username}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao amici,</td>\n</tr>\n<tr>\n<td>\n<p>{username} vi vuole invitare</p>\n<p>{site_name}</p>\n<p>{dynamic_content}</p>\n<p>Â {click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Saluti,,</p>\n<p style=\"margin: 0px;\">{username}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(282, 'email_verification_it', 'Email Verification Link', '{site_name} E-mail di verifica link', 'Ciao {user_name},\n\nSi prega, clicca sul link qui sotto per il vostro {site_name} verifica e-mail.\n\n{click here}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {user_name},</td>\n</tr>\n<tr>\n<td>\n<p>Si prega, clicca sul link qui sotto per il vostro {site_name} verifica e-mail.</p>\n<p>Â {click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(283, 'referral_credit_it', 'Referral Credit', 'Sei guadagnare {amount} da Referenti', 'Ciao {username},\n\nSei guadagnare il {amount} da {friend_name}\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {user_name},</td>\n</tr>\n<tr>\n<td><p>\nSei guadagnare il {amount} da {friend_name}</p>\n</td>\n</tr><tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(284, 'User_join_to_Referal_mail_it', 'User join to Referal mail', 'Il tuo amico Iscrizione', 'Caro {refer_name},\n\nIl tuo amico {friedn_name} Ã¨ ora unirsi {site_name} .Ora, $ 100 Ã¨ di credito nel tuo account possibile viaggiare credito.\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Caro {refer_name},</td>\n</tr>\n<tr>\n<td><br />Il tuo amico {friedn_name} Ã¨ ora unirsi {site_name} .Ora, $ 100 Ã¨ di credito nel tuo account possibile viaggiare credito.<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(285, 'contact_request_to_host_it', 'Contact Request to Host', 'Richiesta di contatto', 'Ciao {host_username},\n\nCliccate sul seguente link per contattare l\'utente: {link}\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nOspiti: {guest}\n\nOspite Messaggio: {message}\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_username},</td>\n</tr>\n<tr>\n<td>\nCliccate sul seguente link per contattare l\'utente: {link}\n<br /><br />\n			\nLista : {title}<br /><br />\n		\nCheckin Data : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n		\nOspiti : {guest}<br /><br />\n		\nOspite Messaggio : {message}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(286, 'contact_request_to_guest_it', 'Contact Request to Guest', 'Richiesta di contatto', 'Ciao {traveller_username},\n\nHai inviato richiesta di contatto a {host_username}.\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nOspiti: {guest}\n\nIl tuo messaggio: {message}\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveller_username},</td>\n</tr>\n<tr>\n<td>\nHai inviato richiesta di contatto a {host_username}.<br /><br />\n			\nLista : {title}<br /><br />\n		\nCheckin Data : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n		\nOspiti : {guest}<br /><br />\n		\nIl tuo messaggio : {message}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(287, 'request_granted_to_guest_it', 'Contact Request Granted to Guest', 'Contatti Richiesta Concesso', 'Ciao {traveller_username},\n\nLa vostra richiesta di contatto Ã¨ garantita da {host_username}.\n\nHost E-mail: {host_email}\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nHost Messaggio: {message}\n\nURL per la prenotazione: {link}\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveller_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nLa vostra richiesta di contatto Ã¨ garantita da {host_username}.\n<br /><br />\n\nHost E-mail : {host_email}<br /><br /> \n\nLista : {title}<br /><br /> \n\nCheckin Data : {checkin}<br /><br /> \n\nCheckout Date : {checkout}<br /><br /> \n\nPrice : {price}<br /><br />\n\nHost Messaggio : {message}<br /><br /> \n\nURL per la prenotazione: {link}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(288, 'request_granted_to_host_it', 'Contact Request Granted to Host', 'Contatti Richiesta Concesso', 'Ciao {host_username},\n\nHai accettare la richiesta di contatto per {traveller_username}.\n\nOspite Email: {guest_email}\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nPrezzo: {price}\n\nIl tuo messaggio: {message}\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nHai accettare la richiesta di contatto per {traveller_username}. <br /><br />\n\nOspite Email : {guest_email}<br /><br />	\n		\nLista : {title}<br /><br />\n		\nCheckin Data : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n\nPrezzo : {price}<br /><br />\n	\nIl tuo messaggio: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(289, 'contact_request_to_admin_it', 'Contact Request to Admin', 'Richiesta di contatto', 'Ciao Admin,\n\n{traveller_username} inviato richiesta di contatto a {host_username}.\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nOspiti: {guest}\n\nPrezzo: {price}\n\nOspite Messaggio: {message}\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td><br />\n{traveller_username} inviato richiesta di contatto a {host_username}. <br /><br />		\nLista : {title}<br /><br />	\nCheckin Data : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n	\nPrezzo : {price}<br /><br />\n		\nOspite Messaggio: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(290, 'request_granted_to_admin_it', 'Contact Request Granted to Admin', 'Contatti Richiesta Concesso', 'Ciao Admin,\n\n{host_username} accettare la richiesta di contatto per \n{traveller_username}.\n\nOspite Email: {guest_email}\n\nHost E-mail: {host_email}\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nPrezzo: {price}\n\nHost Messaggio: {message}\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td><br />\n{host_username} accettare la richiesta di contatto per \n{traveller_username}.<br /><br />\n\nOspite Email : {guest_email}<br /><br />	\n\nHost E-mail : {host_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nCheckin Data : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n\nPrezzo : {price}<br /><br />	\n			\nHost Messaggio : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(291, 'special_request_granted_to_guest_it', 'Contact Request Granted with Special Offer to Guest', 'Contatti Richiesta Concesso con l\'offerta speciale', 'Ciao {traveller_username},\n\n{host_username} accettare la tua richiesta di contatto con l\'offerta speciale.\n\nHost E-mail: {host_email}\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nPrezzo: {price}\n\nMessaggio: {message}\n\nURL per la prenotazione: {link}\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveller_username},</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} accettare la tua richiesta di contatto con l\'offerta speciale.\n<br /><br />\n\nHost E-mail : {host_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nCheckin Data : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n\nPrezzo : {price}<br /><br />\n			\nMessaggio  : {message}<br /><br />\n\nURL per la prenotazione  : {link}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(292, 'special_request_granted_to_host_it', 'Contact Request Granted with Special Offer to Host', 'Contatti Richiesta Concesso con l\'offerta speciale', 'Ciao {host_username},\n\nHai accettare il {traveller_username} richiesta di contatto con l\'offerta speciale.\n\nOspite Email: {guest_email}\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nPrezzo: {price}\n\nIl tuo messaggio: {message}\n\n--\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_username},</td>\n</tr>\n<tr>\n<td><br />\n\nHai accettare il {traveller_username} richiesta di contatto con l\'offerta speciale.<br /><br />\n\nOspite Email : {guest_email}<br /><br />\n\nLista : {title}<br /><br />	\n\nCheckin Data : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n\nPrezzo : {price}<br /><br />\n			\nIl tuo messaggio : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(293, 'special_request_granted_to_admin_it', 'Contact Request Granted with Special Offer to Admin', 'Contatti Richiesta Concesso con l\'offerta speciale', 'Ciao Admin,\n\n{host_username} avere accettare il {traveller_name} richiesta di contatto con l\'offerta speciale.\n\nOspite Email: {guest_email}\n\nHost E-mail: {host_email}\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nPrezzo: {price}\n\nHost Messaggio: {message}\n\n--\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} avere accettare il {traveller_name} richiesta di contatto con l\'offerta speciale.<br /><br />\n\nOspite Email : {guest_email}<br /><br />\n\nHost E-mail : {host_email}<br /><br />\n\nLista : {title}<br /><br />	\n\nCheckin Data : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n\nPrezzo : {price}<br /><br />\n			\nHost Messaggio  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(294, 'request_cancel_to_guest_it', 'Contact Request Cancelled to Guest', 'Scusate! la tua richiesta di contatto viene annullata', 'Ciao {traveller_username},\n\nSpiacente, la tua richiesta di contatto Ã¨ negato da {host_username} di {title}.\n\nHost Messaggio: {message}\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveller_username} ,</td>\n</tr>\n<tr>\n<td>\n<p>Spiacente, la tua richiesta di contatto Ã¨ negato da {host_username} di {title}.</p><br /><br />\nHost Messaggio : {message}<br /><br />\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,\n</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(295, 'request_cancel_to_host_it', 'Contact Request Cancelled to Host', 'Hai annullato la richiesta di contatto', 'Ciao {host_username},\n\nHai annullato il {traveller_username} richiesta di contatto per {title}.\n\nIl tuo messaggio: {message}\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_username} ,</td>\n</tr>\n<tr>\n<td>\n<p>Hai annullato il {traveller_username} richiesta di contatto per {title}.</p><br /><br />\nIl tuo messaggio: {message}<br /><br />\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(296, 'checkout_admin_it', 'Admin: Check Out', 'Controllare Traveler Out', 'Ciao Admin,\n\n{traveler_name} Ã¨ checkouted da {list_title}.\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Ã¨ checkouted da {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(297, 'request_cancel_to_admin_it', 'Contact Request Cancelled to Admin', '{host_username} annullato il {traveller_username} richiesta di contatto', 'Ciao Admin,\n\n{host_username} annullato il {traveller_username} richiesta di contatto per {title}.\n\nmessaggio Host: {message}\n\n-\nGrazie e saluti,\n\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{host_username} annullato il {traveller_username} richiesta di contatto per {title}.</p>\n<br /><br />\nHost messaggio : {message}.\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(298, 'checkout_host_it', 'Host: Check Out', 'Controllare Traveler Out', 'Ciao {host_name},\n\n{traveler_name} Ã¨ checkouted da {list_title}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Ã¨ checkouted da {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(299, 'checkout_traveler_it', 'Traveler: Check Out', 'Il check-out', 'Ciao {traveler_name},\n\nGrazie, Stai checkouted da {list_title}.\n\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Grazie, Stai checkouted da {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(300, 'contact_discuss_more_to_guest_it', 'Contact Request - Discuss more to Guest', 'Contatti Richiesta - Discutere di piÃ¹', 'Ciao {traveller_username},\n\n{host_username} vuole discutere di piÃ¹ con voi.\n\nHost E-mail: {host_email}\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nMessaggio: {message}\n\n--\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveller_username},</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} vuole discutere di piÃ¹ con voi<br /><br />\n\nHost E-mail : {host_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nCheckin Data : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n			\nMessaggio  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(301, 'contact_discuss_more_to_host_it', 'Contact Request - Discuss more to Host', 'Contatti Richiesta - Discutere di piÃ¹', 'Ciao {host_username},\n\nSi vuole discutere di piÃ¹ con {traveller_username}.\n\nOspite Email: {guest_email}\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nIl tuo messaggio: {message}\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_username},</td>\n</tr>\n<tr>\n<td><br />\n\nSi vuole discutere di piÃ¹ con {traveller_username}.<br /><br />\n\nOspite Email : {guest_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nCheckin Data : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n			\nIl tuo messaggio: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(302, 'contact_discuss_more_to_admin_it', 'Contact Request - Discuss more to Admin', 'Contatti Richiesta - Discutere di piÃ¹', 'Ciao Admin,\n\n{host_username} vuole discutere di piÃ¹ con {traveller_username}.\n\nOspite Email: {guest_email}\n\nHost E-mail: {host_email}\n\nLista: {title}\n\nCheckin Data: {checkin}\n\nCheckout Date: {checkout}\n\nHost Messaggio: {message}\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} wants to discuss more with {traveller_username}.<br /><br />\n\nOspite Email : {guest_email}<br /><br />\n\nHost E-mail : {host_email}<br /><br />\n\nLista : {title}<br /><br />\n\nCheckin Data : {checkin}<br /><br />\n		\nCheckout Date : {checkout}<br /><br />\n			\nHost Messaggio  : {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(303, 'checkin_admin_it', 'Admin: CheckIn', 'Controllare viaggiatore nel', 'Ciao Admin,\n\n{traveler_name} Ã¨ checkin a {list_title}\n\nNome Cliente: {traveler_name}\nOspite Email: {traveler_email}\nNome Host: {host_name}\nHost E-mail: {host_email}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} Ã¨ checkin a {list_title}.<br /><br />\nNome Cliente: : {traveler_name}.<br /><br />\nOspite Email : {traveler_email}.<br /><br />\nHost Nome : {host_name}.<br /><br />\nHost E-mail : {host_email}.<br /><br />\nNome elenco : {list_title}.<br /><br />\nPrezzo : {currency}{price}.<br /><br />\nCheckin Data : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(304, 'checkin_traveller_it', 'Traveler: Check In', 'Il check-in', 'Ciao {traveler_name},\n\nGrazie, Stai checkin a {list_title}.\n\nNome Host: {host_name}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nGrazie, Stai checkin a {list_title}.<br /><br />\nNome Host  : {host_name}.<br /><br />\nNome elenco: {list_title}.<br /><br />\nPrezzo : {currency}{price}.<br /><br />\nCheckin Data : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(305, 'refund_admin_it', 'Admin: Refund', 'Rimborso da Admin', 'Ciao Admin,\n\nHai rimborsato il {refund_amt} ammontano a {name} account {account}.\n\nNome Cliente: { traveler_name}\nOspite Email: {traveler_email}\nNome Host: {host_name}\nHost E-mail: {host_email}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\nImporto rimborsato: {refund_amt}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\nHai rimborsato il {refund_amt} ammontano a {name} account {account}.<br /><br />\nNome Cliente: : {traveler_name}.<br /><br />\nOspite Email : {traveler_email}.<br /><br />\nNome Host: {host_name}.<br /><br />\nHost E-mail : {host_email}.<br /><br />\nNome elenco : {list_title}.<br /><br />\nPrezzo : {currency}{price}.<br /><br />\nCheckin Data : {checkin}.<br /><br />\nCheckout Date : {checkout}.<br /><br />\nImporto rimborsato: {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(306, 'refund_host_it', 'Host: Refund', 'Rimborso da Admin', 'Ciao {host_name},\n\nAdmin rimborsato il {refund_amt} importo al tuo account {account}.\n\nNome Cliente: {traveler_name}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\nImporto rimborsato: {refund_amt}\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao{host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nAdmin rimborsato il {refund_amt} importo al tuo account {account}.<br /><br />\nNome Cliente : {traveler_name}.<br /><br />\nNome elenco : {list_title}.<br /><br />\nPrezzo : {currency}{price}.<br /><br />\nCheckin Data : {checkin}.<br /><br />\nCheckout Date : {checkout}.<br /><br />\nImporto rimborsato : {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(307, 'refund_guest_it', 'Traveler: Refund', 'Rimborso da Admin', 'Ciao {traveler_name},\n\nAdmin rimborsato il {refund_amt} importo al tuo account {account}.\n\nNome Host: {host_name}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\nImporto rimborsato: {refund_amt}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nAdmin rimborsato il {refund_amt} importo al tuo account {account}.<br /><br />\nNome Host: {host_name}.<br /><br />\nNome elenco : {list_title}.<br /><br />\nPrezzo : {currency}{price}.<br /><br />\nCheckin Data : {checkin}.<br /><br />\nCheckout Date : {checkout}.<br /><br />\nImporto rimborsato: {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(308, 'list_create_host_it', 'List creation to Host', 'Ãˆ stato creato il nuovo elenco', 'Ciao {host_name},\n\nÃˆ stato creato il nuovo elenco.\n\nNome elenco: {list_title}\n\nLink: {link}\n\nPrezzo: {price}\n\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nÃˆ stato creato il nuovo elenco.<br /><br />\nNome elenco:  {list_title}.<br /><br />\nLink : {link}.<br /><br />\nPrezzo : {price}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(309, 'list_create_admin_it', 'List creation to Admin', '{host_name} hanno creato il nuovo elenco', 'Ciao Admin,\n\n{host_name} hanno creato il nuovo elenco.\n\nNome host: {host_name}\n\nNome elenco: {list_title}\n\nLink: {link}\n\nPrezzo: {price}\n\n\n--\nGrazie e saluti,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{host_name} hanno creato il nuovo elenco.<br /><br />\nHost nome : {host_name}.<br /><br />\nNome elenco : {list_title}.<br /><br />\nLink : {link}.<br /><br />\nprezzo : {price}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(310, 'list_delete_host_it', 'List deletion to Host', 'Ãˆ stato eliminato l\'elenco', 'Ciao {host_name},\n\nÃˆ stato eliminato l\'elenco.\n\nNome elenco: {list_title}\n\n--\nGrazie e saluti,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nÃˆ stato eliminato l\'elenco.<br /><br />\nNome elenco: {list_title}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(311, 'list_delete_admin_it', 'List deletion to Admin', '{host_name} hanno cancellato la lista', 'Ciao Admin,\n\n{host_name} hanno cancellato la lista\n\nNome Host : {host_name}\n\nNome elenno : {list_title}\n\n\n--\nGrazie e saluti,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{host_name} hanno cancellato la lista<br /><br />\nNome Host : {host_name}.<br /><br />\nNome elenno : {list_title}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(312, 'payment_issue_to_admin_it', 'Payment issue mail to Admin', 'pagamento configurato male', 'Ciao Admin,\n\n{payment_type} credenziali API sono mal configurato.\n\n{username} cercato di effettuare il pagamento.\n\nID e-mail: {email_id}\n\n--\nGrazie e saluti,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{payment_type} credenziali API sono mal configurato.</p>\n<p>{username} cercato di effettuare il pagamento.</p>\n<p>ID e-mail : {email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(313, 'refund_host_commission_it', 'Host: Accept Commission Refund', 'Rimborso del Host Accetta Commissione da Admin', 'Ciao {host_name},\n\nAdmin rimborsato il {refund_amt} importo al tuo account {account}.\n\nNome elenco: {list_title}\nImporto rimborsato: {refund_amt}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>Admin rimborsato il {refund_amt} importo al tuo account {account}.</p>\n<p>Nome elenco: {list_title}</p>\n<p>Importo rimborsato: {refund_amt}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(314, 'refund_host_commission_admin_it', 'Admin: Accept Commission Refund', 'Hai rimborso del Host Accetta Commissione', 'Ciao Admin,\n\nHai rimborsato il {refund_amt} importo al tuo account {account}.\n\nNome elenco: {list_title}\nNome Host: {host_name}\nImporto rimborsato: {refund_amt}\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<p>Hai rimborsato il {refund_amt} importo al tuo account {account}.</p>\n<p>Nome elenco:{list_title}</p>\n<p>Nome Host: {host_name}</p>\n<p>Importo rimborsato: {refund_amt}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(315, 'passport_verification_it', 'Passport verification for administrator', '{user_name} inviato la richiesta di verifica del passaporto, userid-{user_id}', 'Ciao Admin, \\ n \\ n {user_name} inviato la richiesta di verifica del passaporto, ID utente {user id} \\ n \\ nDetails come segue, \\ n \\ nuser Nome: {user_name} \\ nuser Id: {user_id} \\ n \\ n \\ n \\ n - \\ nGrazie e saluti, \\ n \\ n {user_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\\n<tbody>\\n<tr>\\n<td>Ciao Admin,</td>\\n</tr>\\n<tr>\\n<td>\\n<p>{user_name} inviato la richiesta di verifica del passaporto, ID utente {user_id}.</p>\\n<p>Details come segue,</p>\\n<p>User Nome : {user_name}</p>\\n<p>User Id : {user_id}</p>\\n\\n</td>\\n</tr>\\n<tr>\\n<td>\\n<p style=\"margin: 0 10px 0 0;\">--</p>\\n<p style=\"margin: 0 0 10px 0;\">Grazie e saluti,</p>\\n<p style=\"margin: 0px;\">{user_name} </p>\\n</td>\\n</tr>\\n</tbody>\\n</table>'),
(316, 'change password to user_it', 'admin change to the password', 'Welcome to  {site_name}', 'Admin modificare la password passwordOld: {new_password} Conform la password: {conform_password} Caro membro, Vecchia password: {new_password} Conform la password: {conform_password} - Grazie e saluti, Amministratore {site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Caro membro,</td>\n</tr>\n<tr>\n<td>\n<p>Â Ãˆ stata modificata con successo la password</p>\n<p>Si prega di utilizzare i dettagli Password qui sotto per il login.</p>\n<p>Vecchia password:{new_password}</p>\n<p><span>Nuova password:{conform_password}</span></p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(317, 'spam_it', 'Spam List', 'Admin Message - Annuncio segnalato come Spam!', '**** ADMIN MESSAGGIO****\n\n\n\nL\'elenco qui sotto Ã¨ stato segnalato come spam:\n\nURL Spam Listing: {list_url}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>****ADMIN MESSAGGIO****</td>\n</tr>\n<tr>\n<td>\n<p>Â FLEXIFLAT.COM RELAZIONE SPAM!</p>\n<p>L\'elenco qui sotto Ã¨ stato segnalato come spam:<br /><br />{list_url}</p>\n</td>\n</tr>\n<tr>\n<td>Â </td>\n</tr>\n</tbody>\n</table>\n<p>Â </p>');
INSERT INTO `email_templates` (`id`, `type`, `title`, `mail_subject`, `email_body_text`, `email_body_html`) VALUES
(318, 'host_notify_review_it', 'Host Notification for Review', '{guest_name} ha una revisione da {host_name}', 'Ciao {guest_name},\n\n{host_name} ha aggiunto un commento su di te in {list_name} sopra {site_name}\n\nE lui Ã¨ in attesa per la tua opinione.\nNome host: {host_name}\nNome elenco: {list_name}\n\n\n--\nGrazie e saluti,\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {guest_name},</td>\n</tr>\n<tr>\n<td><br/ > {host_name} ha aggiunto un commento su di te in {list_name} sopra {site_name} <br />E lui Ã¨ in attesa per la tua opinione.\n<br/>\nNome host : {host_name}<br/>\nNome elenco : {list_name}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(319, 'guest_notify_review_it', 'Guest Notification for Review', '{guest_name} ha estratto.', 'Ciao {host_name},\n\n{guest_name} ha checkouted dalla lista {list_name} a {site_name}\n\nE lui Ã¨ in attesa per la tua opinione.\n\n--\nGrazie e saluti,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name},</td>\n</tr>\n<tr>\n<td><br /> {guest_name} ha checkouted dalla lista {list_name} a {site_name}<br />E lui Ã¨ in attesa per la tua opinione.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(320, 'list_notification_it', 'Notification for calendar update', 'L\'elenco {list_name} ha bisogno di un aggiornamento', 'Ciao {host_name},\n\nIl {list_name} avete elencato in {site_name} non hanno alcun aggiornamento nell\'ultimo mese.\n\nPer migliorare il posizionamento nei risultati di ricerca si prega di aggiornare l\'elenco\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>\nIl {list_name} avete elencato in {site_name} non hanno alcun aggiornamento nell\'ultimo mese.</p>\n<p>\nPer migliorare il posizionamento nei risultati di ricerca si prega di aggiornare l\'elenco\n</p>\n\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>\n'),
(321, 'host_notification_it', 'Upcoming notification for host', 'Il {traveler_name} sta avendo una prenotazione sul tuo {list_title} domani', 'Ciao {host_name},\n\n{traveler_name} prenotato il {list_title} luogo domani.\n\nDettagli come segue,\n\nNome dei viaggiatori: {traveler_name}\nContatto e-mail Id: {traveler_email_id}\nLuogo Nome: {list_title}\nIl check-in: {checkin}\nPartenza: {checkout}\n\n\n--\nGrazie e saluti,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} prenotato il  {list_title} luogo domani.</p>\n<p>Dettagli come segue,</p>\n\n<p>Nome dei viaggiatori: {traveler_name}</p>\n<p>Contatto e-mail Id: {traveler_email_id}</p>\n<p>Luogo Nome: {list_title}</p>\n<p>Il check-in:{checkin}</p>\n<p>Partenza : {checkout}</p>\n\n\n</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>\n'),
(322, 'tc_book_to_admin_po', 'Admin notification for  Travel cretid booking', '{traveler_name} enviou o pedido de reserva usando seu Travel Cretids', 'OlÃ¡, Admin,\n\n{traveler_name} enviou o pedido de reserva para reservar o {list_title} lugar em {book_date} em {book_time} usando seus crÃ©ditos de viagem.\n\nDetalhes como segue,\n\nNome do viajante: {traveler_name}\nId. Do e-mail de contato: {traveler_email_id}\nNome do local: {list_title}\nCheck-in: {checkin}\nConfira: {checkout}\nPreÃ§o de mercado: {market_price}\nMontante pago: {payed_amount}\nCrÃ©ditos de Viagem: {travel_credits}\nNome do host: {host_name}\nID do host de e-mail: {host_email_id}\n\n--\nObrigado e cumprimentos,\n\n{site_name}\nEquipe', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} enviou o pedido de reserva para reservar o {list_title} lugar em {book_date} em {book_time} usando seus crÃ©ditos de viagem.</p>\n<p>Detalhes como segue,</p>\n<p>Nome do viajante: {traveler_name}</p><P> ID do e-mail de contato: {traveler_email_id} </ p>\n<P> Nome do local: {list_title} </ p>\n<P> Check-in: {checkin} </ p>\n<P> Confira: {checkout} </ p>\n<P> PreÃ§o de mercado: {market_price} </ p>\n<P> Montante pago: {payed_amount} </ p>\n<P> CrÃ©ditos de viagem: {travel_credits} </ p>\n<P> Nome do host: {host_name} </ p>\n<P> ID do host de e-mail: {host_email_id} </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P style = \"margin: 0 10px 0 0;\"> - </ p>\n<P style = \"margin: 0 0 10px 0;\"> Obrigado e Atenciosamente, </ p>\n<p style = \"margin: 0px;\"> {site_name} Equipe </ p>\n</td>\n</tr>\n</tbody>\n</table>'),
(323, 'tc_book_to_host_po', 'Host notification for  Travel cretid booking', ' {traveler_name} Enviou o pedido de reserva utilizando o seu Travel Cretids', 'OlÃ¡ {username},\n\n{traveler_name} enviou o pedido de reserva para reservar o seu {list_title} lugar em {book_date} em {book_time} usando seus crÃ©ditos de viagem.\n\nEntraremos em contato com vocÃª com o pagamento apropriado.\n\nDetalhes como segue,\n\nNome do viajante: {traveler_name}\nId. Do e-mail de contato: {traveler_email_id}\nNome do local: {list_title}\nCheck-in: {checkin}\nConfira: {checkout}\nPreÃ§o: {market_price}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {username} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} enviou o pedido de reserva para reservar o seu {list_title} lugar em {book_date} em {book_time} usando seus crÃ©ditos de viagem.</p>\n<p>Detalhes como segue,</p>\n<p>Nome do viajante: {traveler_name}</p>\n<p>Id. Do e-mail de contato: {traveler_email_id}</p>\n<p>Nome do local:{list_title}</p>\n<p>Check in : {checkin}</p>\n<p>Confira : {checkout}</p>\n<p>Preco : {market_price}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(324, 'admin_mass_email_po', 'Admin mass email', '{subject}', 'Oi UsuÃ¡rio,\n\n{dynamic_content}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi UsuÃ¡rio,</td>\n</tr>\n<tr>\n<td>\n<p>{dynamic_content}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(325, 'user_reference_po', 'Reference', 'Escreva {username} uma referÃªncia em {site_name}', 'Oi,\n\n{username} estÃ¡ pedindo que vocÃª forneÃ§a uma referÃªncia que eles possam exibir publicamente em seu perfil {site_name}. {site_name} Ã© um mercado comunitÃ¡rio para acomodaÃ§Ãµes construÃ­do com base na confianÃ§a e reputaÃ§Ã£o. Uma referÃªncia de vocÃª realmente ajudaria a construir sua reputaÃ§Ã£o com a comunidade.\nUma referÃªncia em {site_name} pode ajudar {username} a encontrar um lugar legal para hospedar ou hospedar viajantes interessantes.\n\n{click_here} para ter uma referÃªncia para {username}.\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi,</td>\n</tr>\n<tr>\n<td>\n<p>{username} estÃ¡ pedindo que vocÃª forneÃ§a uma referÃªncia que eles possam exibir publicamente em seu perfil {site_name}. {site_name} Ã© um mercado comunitÃ¡rio para acomodaÃ§Ãµes construÃ­do com base na confianÃ§a e reputaÃ§Ã£o. Uma referÃªncia de vocÃª realmente ajudaria a construir sua reputaÃ§Ã£o com a comunidade.</p>\n<p>{click_here} para ter uma referÃªncia para {username}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(326, 'reference_receive_po', 'Reference Receive', 'VocÃª recebeu uma nova referÃªncia em {site_name}!', 'OlÃ¡ {username},\n\n{other_username} escreveu uma referÃªncia para vocÃª. Antes de ir para o seu perfil pÃºblico, vocÃª pode revÃª-lo e aceitÃ¡-lo ou ignorÃ¡-lo. {click_here} para ver a referÃªncia.\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {username},</td>\n</tr>\n<tr>\n<td>\n<p>{other_username} escreveu uma referÃªncia para vocÃª. Antes de ir para o seu perfil pÃºblico, vocÃª pode revÃª-lo e aceitÃ¡-lo ou ignorÃ¡-lo. {click_here} para ver a referÃªncia.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(327, 'host_reservation_notification_po', 'Reservation notification for host', 'A reserva foi solicitada por {traveler_name}', 'OlÃ¡ {username},\n\n{traveler_name} reservou o {list_title} lugar em {book_date} em {book_time}.\n\nDetalhes como segue,\n\nNome do viajante: {traveler_name}\nId. Do e-mail de contato: {traveler_email_id}\nNome do local: {list_title}\nCheck-in: {checkin}\nConfira: {checkout}\nPreÃ§o: {market_price}\n\nPor favor, dÃª a confirmaÃ§Ã£o clicando na aÃ§Ã£o abaixo.\n{action_url}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>\n<p>Ola {username} ,</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} reservou o {list_title} lugar em {book_date} em {book_time}.</p>\n<br />\n<p>Detalhes como segue,</p>\n<p>Nome do viajante: {traveler_name}</p>\n<p>Id. Do e-mail de contato:  {traveler_email_id}</p>\n<p>Nome do local : {list_title}</p>\n<p>Check-in : {checkin}</p>\n<p>Confira : {checkout}</p>\n<p>PreÃ§o : {market_price}</p>\n<br />\n<p>Por favor, dÃª a confirmaÃ§Ã£o clicando na aÃ§Ã£o abaixo.\n{action_url}</p>\n<p>{action_url}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">--\nObrigado e cumprimentos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(328, 'traveller_reservation_notification_po', 'Reservation notification for  traveller', 'Seu pedido de reserva Ã© enviado com Ãªxito', 'OlÃ¡ {traveler_name},\n\nSua solicitaÃ§Ã£o de reserva Ã© enviada com Ãªxito para o host apropriado.\n\nAguarde a sua confirmaÃ§Ã£o.\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Sua solicitaÃ§Ã£o de reserva Ã© enviada com Ãªxito para o host apropriado.</p>\n<p>Aguarde a sua confirmaÃ§Ã£o.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(329, 'admin_reservation_notification_po', 'Reservation notification for  administrator', '{traveler_name} enviou o pedido de reserva para {host_name}', 'OlÃ¡, Admin,\n\n{traveler_name} enviou o pedido de reserva para {list_title} colocar em {book_date} em {book_time}.\n\nDetalhes como segue,\n\nNome do viajante: {traveler_name}\nId. Do e-mail de contato: {traveler_email_id}\nNome do local: {list_title}\nCheck-in: {checkin}\nConfira: {checkout}\nPreÃ§o: {market_price}\nNome do host: {host_name}\nID do host de e-mail: {host_email_id}\n\n--\nObrigado e cumprimentos,\n\n{site_name} \nEquipe', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} enviou o pedido de reserva para {list_title} colocar em {book_date} em {book_time}.</p>\n<p>Detalhes como segue,</p>\n<p>Nome do viajante: {traveler_name}</p>\n<p>Id. Do e-mail de contato:  {traveler_email_id}</p>\n<p>Nome do local: {list_title}</p>\n<p>Check-in : {checkin}</p>\n<p>Confira: {checkout}</p>\n<p>PreÃ§o : {market_price}</p>\n<p>Nome do host: {host_name}</p>\n<p>ID do host de e-mail: {host_email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(330, 'traveler_reservation_granted_po', 'Traveler : After Reservation granted', 'ParabÃ©ns! Seu pedido de reserva Ã© concedido.', 'OlÃ¡ {traveler_name},\n\nParabÃ©ns, A sua solicitaÃ§Ã£o de reserva Ã© concedida por {host_name} para {list_name}.\n\nAbaixo mencionamos seus detalhes de contato,\n\nPrimeiro nome: {Fname}\nSobrenome: {Lname}\nViver em: {livein}\nTelefone: {phnum}\n\nO endereÃ§o exato Ã©,\n\nEndereÃ§o: {street_address}\nEndereÃ§o opcional: {optional_address}\nCidade: {city}\nEstado: {state}\nPaÃ­s: {country}\nCÃ³digo postal: {zipcode}\n\nMensagem do Host: {comment}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>ParabÃ©ns, A sua solicitaÃ§Ã£o de reserva Ã© concedida por {host_name} para {list_name}.</p>\n<p>Abaixo mencionamos seus detalhes de contato,</p>\n<p>Primeiro nome:  {Fname}</p>\n<p>Sobrenome : {Lname}</p>\n<p>Viver em : {livein}</p>\n<p>Telefone: {phnum}</p>\n<p>O endereÃ§o exato Ã©,</p>\n<p>EndereÃ§o: {street_address},</p>\n<p>EndereÃ§o opcional: {optional_address},</p>\n<p>Cidade : {city},</p>\n<p>Estado : {state},</p>\n<p>PaÃ­s : {country},</p>\n<p>CÃ³digo postal : {zipcode}</p>\n<p>Mensagem do Host: {comment}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(331, 'traveler_reservation_declined_po', 'Traveler : After reservation declined', 'Desculpa! Seu pedido de reserva Ã© negado', 'OlÃ¡ {traveler_name},\n\nDesculpe, sua solicitaÃ§Ã£o de reserva Ã© jantada por {host_name} para {list_title}.\n\nMensagem do Host: {comment}\n\nEm breve, entraremos em contato com vocÃª com o pagamento apropriado.\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Desculpe, sua solicitaÃ§Ã£o de reserva Ã© jantada por {host_name} para {list_title}.</p>\n<p>Mensagem do Host: {comment}</p>\n<p>Em breve, entraremos em contato com vocÃª com o pagamento apropriado.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">--\nObrigado e cumprimentos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(332, 'traveler_reservation_cancel_po', 'Traveler : After reservation canceled', '{traveler_name} cancelou a sua lista de reservas', 'OlÃ¡ {host_name},\n\nSua lista de reservas ({status}) Ã© cancelada por {traveler_name} para {list_title}.\n\n{user_type} Mensagem: {comment}\n\nClaro que entraremos em contato com vocÃª em breve, se houver algum saldo de pagamento.\n\nE tambÃ©m, se vocÃª tiver quaisquer outras dÃºvidas, nÃ£o hesite em contactar-nos.\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Sua lista de reservas ({status}) Ã© cancelada por {traveler_name} para {list_title}.</p>\n<p>{user_type} Mensagem: {comment}</p>\n<p>Claro que entraremos em contato com vocÃª em breve, se houver algum saldo de pagamento.</p>\n<p>E tambÃ©m, se vocÃª tiver quaisquer outras dÃºvidas, nÃ£o hesite em contactar-nos.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(333, 'traveler_reservation_expire_po', 'Traveler : Reservation Expire', 'Desculpa! O seu pedido de {type} expirou', 'OlÃ¡ {traveler_name},\n\nSua solicitaÃ§Ã£o {type} expirou.\n\nNome do host: {host_name}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nSua solicitaÃ§Ã£o {type} expirou.<br /><br />\nNome do host: {host_name}.<br /><br />\nNome da lista: {list_title}.<br /><br />\nPreÃ§o:  {currency}{price}.<br /><br />\nData de Checkin: {checkin}.<br /><br />\nData de SaÃ­da: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(334, 'host_reservation_expire_po', 'Host : Reservation Expire', '{type} Pedido expirado para a sua lista', 'OlÃ¡ {host_name},\n\n{type} pedido expirado para {list_title} reservado por {traveler_name}.\n\nNome do hÃ³spede: {traveler_name}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{type} pedido expirado para {list_title} reservado por {traveler_name}.<br /><br />\nNome do hÃ³spede : {traveler_name}.<br /><br />\nNome da lista : {list_title}.<br /><br />\nPreÃ§o : {currency}{price}.<br /><br />\nData de Checkin:  {checkin}.<br /><br />\nData de SaÃ­da: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(335, 'admin_reservation_expire_po', 'Admin : Reservation Expire', '{type} O pedido expirou', 'OlÃ¡, Admin,\n\n{traveler_name} A solicitaÃ§Ã£o {type} expirou para {list_title}.\n\nNome do hÃ³spede: {traveler_name}\nEmail do hÃ³spede: {traveler_email}\nNome do host: {host_name}\nHost Email: {host_email}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} A solicitaÃ§Ã£o {type} expirou para {list_title}.<br /><br />\nNome do hÃ³spede: {traveler_name}.<br /><br />\nEmail do hÃ³spede: {traveler_email}.<br /><br />\nNome do host: {host_name}.<br /><br />\nHost Email: {host_email}.<br /><br />\nNome da lista : {list_title}.<br /><br />\nPreÃ§o : {currency}{price}.<br /><br />\nData de Checkin: {checkin}.<br /><br />\nData de SaÃ­da:  {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(336, 'host_reservation_granted_po', 'Host : After Reservation Granted', 'Aceitou o pedido de reserva {traveler_name}', 'OlÃ¡ {host_name},\n\nVocÃª aceitou a solicitaÃ§Ã£o de reserva {traveler_name} para {list_title}.\n\nAbaixo mencionamos seus detalhes de contato,\n\nPrimeiro nome: {Fname}\nSobrenome: {Lname}\nViver em: {livein}\nTelefone: {phnum}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>VocÃª aceitou a solicitaÃ§Ã£o de reserva {traveler_name} para {list_title}.</p>\n<p>Abaixo mencionamos seus detalhes de contato,</p>\n<p>Primeiro nome : {Fname}</p>\n<p>Sobrenome : {Lname}</p>\n<p>Viver em: {livein}</p>\n<p>Telefone : {phnum}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(337, 'admin_reservation_granted_po', 'Admin : After Reservation granted', '{Host_name} aceitou o pedido de reserva {traveler_name}', 'OlÃ¡, Admin,\n\n{host_name} aceitou a solicitaÃ§Ã£o de reserva {traveler_name} para {list_title}.\n\n--\nObrigado e cumprimentos,\n\n{site_name} \nEquipe', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} aceitou a solicitaÃ§Ã£o de reserva {traveler_name} para {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(338, 'host_reservation_declined_po', 'Host : After Reservation Declined', 'VocÃª recusou o pedido de reserva {traveler_name}', 'OlÃ¡ {host_name},\n\nVocÃª recusou a solicitaÃ§Ã£o de reserva {traveler_name} para {list_title}.\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>VocÃª recusou a solicitaÃ§Ã£o de reserva {traveler_name} para {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(339, 'admin_reservation_declined_po', 'Admin : After Reservation Declined', '{host_name} recusou o pedido de reserva {traveler_name}', 'OlÃ¡, Admin,\n\n{host_name} recusou a solicitaÃ§Ã£o de reserva {traveler_name} para {list_title}.\n\n--\nObrigado e cumprimentos,\n\n{site_name} \nEquipe', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} recusou a solicitaÃ§Ã£o de reserva {traveler_name} para {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(340, 'admin_reservation_cancel_po', 'Admin : After reservation canceled', '{traveler_name} cancelou a lista de reservas {host_name}', 'OlÃ¡, Admin,\n\n{traveler_name} cancelou a lista de reservas {host_name} ({status}) para {list_title}.\n\n{penalty}\n\n-\nObrigado e cumprimentos,\n\n{site_name} \nEquipe', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} cancelou a lista de reservas {host_name} ({status}) para {list_title}.\n<br/><br/>{penalty}\n</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,\n</p>\n<p style=\"margin: 0px;\">{site_name} Equipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(341, 'host_reservation_cancel_po', 'Host : After reservation canceled', '{host_name} cancelado A sua lista de reservas', 'OlÃ¡ {traveler_name},\n\n{host_name} cancelar a sua lista de reservas ({status}) para {list_title}.\n\n\nSe vocÃª tiver quaisquer outras dÃºvidas, nÃ£o hesite em contactar-nos.\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} cancelar a sua lista de reservas ({status}) para {list_title}.<br /></p>\n<p>Se vocÃª tiver quaisquer outras dÃºvidas, nÃ£o hesite em contactar-nos.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(342, 'forgot_password_po', 'Forgot Password', 'Esqueceu a senha', 'Caro Membro,\n\nAbaixo, mencionamos os detalhes da sua conta.\n\nAqui vamos nÃ³s,\n\nEmail_id: {email}\n\nSenha: {password}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Caro Membro,</td>\n</tr>\n<tr>\n<td>\n<p>Abaixo, mencionamos os detalhes da sua conta.</p>\n<p>Aqui vamos nÃ³s,</p>\n<p>Email_id : {email}</p>\n<p>Senha : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(343, 'checkin_host_po', 'Host: Check In', 'Check-in do viajante', 'OlÃ¡ {host_name},\n\n{traveler_name} estÃ¡ em checkin para {list_title}.\n\nNome do hÃ³spede: {guest_name}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} estÃ¡ em checkin para {list_title}.<br /><br />\nNome do hÃ³spede:  {traveler_name}.<br /><br />\nNome da lista: {list_title}.<br /><br />\nPreÃ§o : {currency}{price}.<br /><br />\nData de Checkin: {checkin}.<br /><br />\nData de SaÃ­da: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div></div>\n</td>\n</tr>\n</tbody>\n</table>'),
(344, 'users_signin_po', 'Users Signin', 'Bem-vindo ao {site_name}', 'Caro Membro,\n\nPrazer em conhecÃª-lo e bem-vindo ao {site_name}.\n\nAbaixo, mencionamos os detalhes da sua conta.\n\nAqui vamos nÃ³s,\n\nEmail_id: {email}\n\nSenha: {password}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Caro Membro,</td>\n</tr>\n<tr>\n<td>\n<p>Prazer em conhecÃª-lo e bem-vindo ao {site_name}.</p>\n<p>Abaixo, mencionamos os detalhes da sua conta.</p>\n<p>Aqui vamos nÃ³s,</p>\n<p>Email_id : {email}</p>\n<p>Senha : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,\n</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(345, 'reset_password_po', 'Reset Password', 'Trocar a senha', 'Caro Membro,\n\nAbaixo, mencionamos os detalhes da sua nova conta.\n\nAqui vamos nÃ³s,\n\nSenha: {password}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Caro Membro,</td>\n</tr>\n<tr>\n<td>\n<p>Abaixo, mencionamos os detalhes da sua nova conta.</p>\n<p>Aqui vamos nÃ³s,</p>\n<p>Senha : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(346, 'admin_payment_po', 'Admin payment to Host', 'O administrador pagou suas taxas para {list_title}', 'OlÃ¡ {username},\n\nPagamos suas taxas para {list_title}.\n\nDetalhes como segue,\n\nNome do local: {list_title}\nCheck-in: {checkin}\nConfira: {checkout}\nPreÃ§o: {payed_price}\nPagamento atravÃ©s de: {payment_type}\nID de pagamento: {pay_id}\nData de pagamento: {payed_date}\n\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>\n<p>Ola {username} ,</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>Pagamos suas taxas para {list_title}.</p>\n<br />\n<p>Detalhes como segue,</p>\n<p>Nome do local: {list_title}</p>\n<p>Check in : {checkin}</p>\n<p>Confira : {checkout}</p>\n<p>PreÃ§o : {payed_price}</p>\n<p>Pagamento atravÃ©s de: {payment_type}</p>\n<p>ID de pagamento: {pay_id}</p>\n<p>Data de pagamento: {payed_date}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(347, 'contact_form_po', 'Contact Form', 'Mensagem recebida do formulÃ¡rio de contato', 'OlÃ¡, Admin,\n\nUma mensagem recebida da pÃ¡gina de contato em {date} em {time}.\n\nDetalhes como segue,\n\nNome: {name}\n\nEmail: {email}\n\nMensagem: {message}\n\n--\nObrigado e cumprimentos,\n\n{site_name} \nEquipe', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td>\n<p>Uma mensagem recebida da pÃ¡gina de contato em {date} em {time}.</p>\n<p>Detalhes como segue,</p>\n<p>Nome : {name}</p>\n<p>Email : {email}</p>\n<p>Mensagem : {message}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipe</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(348, 'invite_friend_po', 'Invite My Friends', '{username} convidam vocÃª.', 'OlÃ¡ amigos,\n\n{username} deseja que vocÃª invite {site_name}\n\n{dynamic_content}\n\n{Click_here}\n\n--\nSaudaÃ§Ãµes,\n{username}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OlÃ¡ amigos,</td>\n</tr>\n<tr>\n<td>\n<p>{username} deseja que vocÃª invite</p>\n<p>{site_name}</p>\n<p>{dynamic_content}</p>\n<p>Â {click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">SaudaÃ§Ãµes,</p>\n<p style=\"margin: 0px;\">{username}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(349, 'email_verification_po', 'Email Verification Link', '{site_name} Link de verificaÃ§Ã£o de e-mail', 'OlÃ¡ {user_name},\n\nClique no link abaixo para a sua verificaÃ§Ã£o de e-mail {site_name}.\n\n{click_here}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OlÃ¡ {user_name},</td>\n</tr>\n<tr>\n<td>\n<p>Clique no link abaixo para a sua verificaÃ§Ã£o de e-mail {site_name}.</p>\n<p>Â {click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(350, 'referral_credit_po', 'Referral Credit', 'VocÃª ganha {amount} de referÃªncias', 'OlÃ¡ {username},\n\nVocÃª ganha o {amount} por {friend_name}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {user_name},</td>\n</tr>\n<tr>\n<td><p>\nVocÃª ganha o {amount} por {friend_name}</p>\n</td>\n</tr><tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(351, 'User_join_to_Referal_mail_po', 'User join to Referal mail', 'Seu Amigo', 'Prezado {refer_name},\n\nSeu amigo {friend_name} agora Ã© membro de {site_name}. Agora, $ 100 Ã© creditado em sua conta Travel Credit Possible.\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Prezado {refer_name},</td>\n</tr>\n<tr>\n<td><br />Seu amigo {friend_name} agora Ã© membro de {site_name}. Agora, $ 100 Ã© creditado em sua conta Travel Credit Possible.<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(352, 'contact_request_to_host_po', 'Contact Request to Host', 'RequisiÃ§Ã£o de contato', 'Oi {host_username},\n\nClique no link a seguir para entrar em contato com o usuÃ¡rio: {link}\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nConvidados: {guest}\n\nMensagem do hÃ³spede: {message}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi {host_username},</td>\n</tr>\n<tr>\n<td>\nClique no link a seguir para entrar em contato com o usuÃ¡rio: {link}<br /><br />\n			\nLista : {title}<br /><br />\n		\nData de Checkin: {checkin}<br /><br />\n		\nData de SaÃ­da: {checkout}<br /><br />\n		\nConvidados : {guest}<br /><br />\n		\nMensagem do hÃ³spede: {message}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(353, 'contact_request_to_guest_po', 'Contact Request to Guest', 'RequisiÃ§Ã£o de contato', 'OlÃ¡ {traveller_username},\n\nVocÃª enviou a solicitaÃ§Ã£o de contato para {host_username}.\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nConvidados: {guest}\n\nSua mensagem: {message}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OlÃ¡ {traveller_username},</td>\n</tr>\n<tr>\n<td>\nVocÃª enviou a solicitaÃ§Ã£o de contato para {host_username}.<br /><br />\n			\nLista : {title}<br /><br />\n		\nData de Checkin:  {checkin}<br /><br />\n		\nData de SaÃ­da: {checkout}<br /><br />\n		\nConvidados : {guest}<br /><br />\n		\nSua mensagem: {message}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(354, 'request_granted_to_guest_po', 'Contact Request Granted to Guest', 'SolicitaÃ§Ã£o Concedida', 'OlÃ¡ {traveller_username},\n\nSua solicitaÃ§Ã£o de contato Ã© concedida por {host_username}.\n\nHost Email: {host_email}\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nMensagem do Host: {message}\n\nURL para reservas: {link}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OlÃ¡ {traveller_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nSua solicitaÃ§Ã£o de contato Ã© concedida por {host_username}.\n<br /><br />\n\nHost Email : {host_email}<br /><br /> \n\nLista : {title}<br /><br /> \n\nData de Checkin:  {checkin}<br /><br /> \n\nData de SaÃ­da {checkout}<br /><br /> \n\nPreis : {price}<br /><br />\n\nMensagem do Host: {message}<br /><br /> \n\nURL para reservas: {link}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(355, 'request_granted_to_host_po', 'Contact Request Granted to Host', 'SolicitaÃ§Ã£o Concedida', 'Oi {host_username},\n\nVocÃª aceita a solicitaÃ§Ã£o de contato para {traveller_username}.\n\nEmail do Convidado: {guest_email}\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nPreÃ§o: {price}\n\nSua mensagem: {message}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi {host_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nVocÃª aceita a solicitaÃ§Ã£o de contato para {traveller_username}.<br /><br />\n\nEmail do Convidado: {guest_email}<br /><br />	\n		\nLista : {title}<br /><br />\n		\nData de Checkin: {checkin}<br /><br />\n		\nData de SaÃ­da : {checkout}<br /><br />\n\nPreÃ§o : {price}<br /><br />\n	\nSua mensagem: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(356, 'contact_request_to_admin_po', 'Contact Request to Admin', 'RequisiÃ§Ã£o de contato', 'OlÃ¡, Admin,\n\n{traveller_username} enviou a solicitaÃ§Ã£o de contato para {host_username}.\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nConvidados: {guest}\n\nPreÃ§o: {price}\n\nMensagem do hÃ³spede: {message}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi Admin,</td>\n</tr>\n<tr>\n<td><br />\n{traveller_username} enviou a solicitaÃ§Ã£o de contato para {host_username}. <br /><br />		\nLista : {title}<br /><br />	\nData de Checkin:  {checkin}<br /><br />\n		\nData de SaÃ­da: {checkout}<br /><br />\n	\nPreÃ§o : {price}<br /><br />\n		\nMensagem do hÃ³spede:  {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(357, 'request_granted_to_admin_po', 'Contact Request Granted to Admin', 'SolicitaÃ§Ã£o Concedida', 'OlÃ¡, Admin,\n\n{host_username} aceitar a solicitaÃ§Ã£o de contato para {traveller_username}.\n\nEmail do Convidado: {guest_email}\n\nHost Email: {host_email}\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nPreÃ§o: {price}\n\nMensagem do Host: {message}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi Admin,</td>\n</tr>\n<tr>\n<td><br />\n{host_username} aceitar a solicitaÃ§Ã£o de contato para {traveller_username}.<br /><br />\n\nEmail do Convidado: {guest_email}<br /><br />	\n\nHost Email : {host_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nData de Checkin: {checkin}<br /><br />\n		\nData de SaÃ­da: {checkout}<br /><br />\n\nPreÃ§o : {price}<br /><br />	\n			\nMensagem do Host: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(358, 'special_request_granted_to_guest_po', 'Contact Request Granted with Special Offer to Guest', 'Pedido de contato concedido com oferta especial', 'OlÃ¡ {traveller_username},\n\n{host_username} aceitar a sua solicitaÃ§Ã£o de contato com oferta especial.\n\nHost Email: {host_email}\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nPreÃ§o: {price}\n\nMensagem: {message}\n\nURL para reservas: {link}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OlÃ¡ {traveller_username},</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} aceitar a sua solicitaÃ§Ã£o de contato com oferta especial.<br /><br />\n\nHost Email : {host_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nData de Checkin: {checkin}<br /><br />\n		\nData de SaÃ­da : {checkout}<br /><br />\n\nPreÃ§o : {price}<br /><br />\n			\nMensagem  : {message}<br /><br />\n\nURL para reservas:  {link}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(359, 'special_request_granted_to_host_po', 'Contact Request Granted with Special Offer to Host', 'Pedido de contato concedido com oferta especial', 'Oi {host_username},\n\nVocÃª aceita a solicitaÃ§Ã£o de contato {traveller_username} com oferta especial.\n\nEmail do Convidado: {guest_email}\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nPreÃ§o: {price}\n\nSua mensagem: {message}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OI {host_username},</td>\n</tr>\n<tr>\n<td><br />\n\nVocÃª aceita a solicitaÃ§Ã£o de contato {traveller_username} com oferta especial.<br /><br />\n\nEmail do Convidado: {guest_email}<br /><br />\n\nLista : {title}<br /><br />	\n\nData de Checkin: {checkin}<br /><br />\n		\nData de SaÃ­da: {checkout}<br /><br />\n\nPreÃ§o : {price}<br /><br />\n			\nSua mensagem: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(360, 'special_request_granted_to_admin_po', 'Contact Request Granted with Special Offer to Admin', 'Pedido de contato concedido com oferta especial', 'OlÃ¡, Admin,\n\n{host_username} tem que aceitar a solicitaÃ§Ã£o de contato {traveller_name} com oferta especial.\n\nEmail do Convidado: {guest_email}\n\nHost Email: {host_email}\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nPreÃ§o: {price}\n\nMensagem do Host: {message}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} tem que aceitar a solicitaÃ§Ã£o de contato {traveller_name} com oferta especial.<br /><br />\n\nEmail do Convidado:  {guest_email}<br /><br />\n\nHost Email : {host_email}<br /><br />\n\nLista : {title}<br /><br />	\n\nData de Checkin:  {checkin}<br /><br />\n		\nData de SaÃ­da: {checkout}<br /><br />\n\nPreÃ§o : {price}<br /><br />\n			\nMensagem do Host: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(361, 'request_cancel_to_guest_po', 'Contact Request Cancelled to Guest', 'Desculpa! Seu pedido de contato Ã© cancelado', 'OlÃ¡ {traveller_username},\n\nDesculpe, Sua solicitaÃ§Ã£o de contato foi negada por {host_username} para {title}.\n\nMensagem do Host: {message}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {traveller_username} ,</td>\n</tr>\n<tr>\n<td>\n<p>Desculpe, Sua solicitaÃ§Ã£o de contato foi negada por {host_username} para {title}.</p><br /><br />\nMensagem do Host: {message}<br /><br />\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,\n,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(362, 'request_cancel_to_host_po', 'Contact Request Cancelled to Host', 'VocÃª cancelou o pedido de contato', 'Oi {host_username},\n\nVocÃª cancelou a solicitaÃ§Ã£o de contato {traveller_username} para {title}.\n\nSua mensagem: {message}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi {host_username} ,</td>\n</tr>\n<tr>\n<td>\n<p>VocÃª cancelou a solicitaÃ§Ã£o de contato {traveller_username} para {title}.</p><br /><br />\nSua mensagem: {message}<br /><br />\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(363, 'checkout_admin_po', 'Admin: Check Out', 'Traveler Check Out', 'OlÃ¡, Admin,\n\n{traveler_name} Ã© checkouted de {list_title}.\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OI Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Ã© checkouted de {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(364, 'request_cancel_to_admin_po', 'Contact Request Cancelled to Admin', '{host_username} cancelou o pedido de contacto {traveller_username}', 'OlÃ¡, Admin,\n\n{host_username} cancelou a solicitaÃ§Ã£o de contato {traveller_username} para {title}.\n\nMensagem de host: {message}\n\n--\nObrigado e cumprimentos,\n\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{host_username} cancelou a solicitaÃ§Ã£o de contato {traveller_username} para {title}.</p>\n<br /><br />\nMensagem de host: {message}\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(365, 'checkout_host_po', 'Host: Check Out', 'Traveler Check Out', 'OlÃ¡ {host_name},\n\n{traveler_name} Ã© checkouted de {list_title}.\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Ã© checkouted de {list_title}..</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(366, 'checkout_traveler_po', 'Traveler: Check Out', 'Seu SaÃ­da', 'OlÃ¡ {traveler_name},\n\nObrigado, VocÃª estÃ¡ checkouted de {list_title}.\n\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Obrigado, VocÃª estÃ¡ checkouted de {list_title}.</p>\n<p> </p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(367, 'contact_discuss_more_to_guest_po', 'Contact Request - Discuss more to Guest', 'SolicitaÃ§Ã£o de contato - Discuta mais', 'OlÃ¡ {traveller_username},\n\n{host_username} quer discutir mais com vocÃª.\n\nHost Email: {host_email}\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nMensagem: {message}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {traveller_username},</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} quer discutir mais com vocÃª.<br /><br />\n\nHost Email : {host_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nData de Checkin: {checkin}<br /><br />\n		\nData de SaÃ­da: {checkout}<br /><br />\n			\nMensagem: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(368, 'contact_discuss_more_to_host_po', 'Contact Request - Discuss more to Host', 'SolicitaÃ§Ã£o de contato - Discuta mais', 'Oi {host_username},\n\nVocÃª quer discutir mais com {traveller_username}.\n\nEmail do Convidado: {guest_email}\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nSua mensagem: {message}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi {host_username},</td>\n</tr>\n<tr>\n<td><br />\n\nVocÃª quer discutir mais com {traveller_username}.<br /><br />\n\nEmail do Convidado:  {guest_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nData de Checkin: {checkin}<br /><br />\n		\nData de SaÃ­da: {checkout}<br /><br />\n			\nSua mensagem:  {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e cumprimentos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(369, 'contact_discuss_more_to_admin_po', 'Contact Request - Discuss more to Admin', 'SolicitaÃ§Ã£o de contato - Discuta mais', 'Contate o Pedido - olÃ¡! Admin,\n\n{host_username} quer discutir mais com {traveller_username}.\n\nEmail do Convidado: {guest_email}\n\nHost Email: {host_email}\n\nLista: {title}\n\nData de Checkin: {checkin}\n\nData de SaÃ­da: {checkout}\n\nMensagem do Host: {message}\n\n--\nObrigado e SaudaÃ§Ãµes,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi Admin,</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} quer discutir mais com {traveller_username}.\n<br /><br />\n\nEmail do Convidado: {guest_email}<br /><br />\n\nHost Email : {host_email}<br /><br />\n\nLista : {title}<br /><br />\n\nData de SaÃ­da:  {checkin}<br /><br />\n		\nData de SaÃ­da : {checkout}<br /><br />\n			\nMensagem do Host: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Obrigado e SaudaÃ§Ãµes,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>');
INSERT INTO `email_templates` (`id`, `type`, `title`, `mail_subject`, `email_body_text`, `email_body_html`) VALUES
(370, 'checkin_admin_po', 'Admin: CheckIn', 'Check-in do viajante', 'OlÃ¡, Admin,\n\n{traveler_name} estÃ¡ em checkin para {list_title}.\n\nNome do hÃ³spede: {traveler_name}\nEmail do hÃ³spede: {traveler_email}\nNome do host: {host_name}\nHost Email: {host_email}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OlÃ¡ Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} estÃ¡ em checkin para {list_title}.<br /><br />\nEmail do hÃ³spede: {traveler_name}.<br /><br />\nGuest Email : {traveler_email}.<br /><br />\nNome do host: {host_name}.<br /><br />\nHost Email: {host_email}.<br /><br />\nNome da lista: {list_title}.<br /><br />\nPreÃ§o : {currency}{price}.<br /><br />\nData de Checkin: {checkin}.<br /><br />\nData de SaÃ­da: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div></div>\n</td>\n</tr>\n</tbody>\n</table>'),
(371, 'checkin_traveller_po', 'Traveler: Check In', 'Seu Check In', 'OlÃ¡ {traveler_name},\n\nObrigado, VocÃª estÃ¡ check-in para {list_title}.\n\nNome do host: {host_name}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OlÃ¡ {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nObrigado, VocÃª estÃ¡ check-in para {list_title}.<br /><br />\nNome do host: {host_name}.<br /><br />\nNome da lista: {list_title}.<br /><br />\nPreÃ§o : {currency}{price}.<br /><br />\nData de Checkin: {checkin}.<br /><br />\nData de SaÃ­da: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div></div>\n</td>\n</tr>\n</tbody>\n</table>'),
(372, 'refund_admin_po', 'Admin: Refund', 'Reembolso de Admin', 'OlÃ¡, Admin,\n\nVocÃª reembolsou o valor {refund_amt} na conta {name} {account}.\n\nNome do hÃ³spede: {traveler_name}\nEmail do hÃ³spede: {traveler_email}\nNome do host: {host_name}\nHost Email: {host_email}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\nValor reembolsado: {refund_amt}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\nVocÃª reembolsou o valor {refund_amt} na conta {name} {account}.<br /><br />\nNome do hÃ³spede:  {traveler_name}.<br /><br />\nEmail do hÃ³spede:  {traveler_email}.<br /><br />\nNome do host: {host_name}.<br /><br />\nHost Email : {host_email}.<br /><br />\nNome da lista: {list_title}.<br /><br />\nPreÃ§o : {currency}{price}.<br /><br />\nData de Checkin: {checkin}.<br /><br />\nData de SaÃ­da: {checkout}.<br /><br />\nValor reembolsado: {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(373, 'refund_host_po', 'Host: Refund', 'Reembolso de Admin', 'OlÃ¡ {host_name},\n\nO Admin reembolsou o valor {refund_amt} na sua conta {account}.\n\nNome do hÃ³spede: {traveler_name}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\nValor reembolsado: {refund_amt}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OlÃ¡ {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nO Admin reembolsou o valor {refund_amt} na sua conta {account}.<br /><br />\nNome do hÃ³spede:  {traveler_name}.<br /><br />\nNome da lista: {list_title}.<br /><br />\nPreÃ§o : {currency}{price}.<br /><br />\nData de Checkin: {checkin}.<br /><br />\nData de SaÃ­da: {checkout}.<br /><br />\nValor reembolsado:{refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(374, 'refund_guest_po', 'Traveler: Refund', 'Reembolso de Admin', 'OlÃ¡ {traveler_name},\n\nO Admin reembolsou o valor {refund_amt} na sua conta {account}.\n\nNome do host: {host_name}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\nValor reembolsado: {refund_amt}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nO Admin reembolsou o valor {refund_amt} na sua conta {account}.<br /><br />\nNome do host: {host_name}.<br /><br />\nNome da lista: {list_title}.<br /><br />\nPreÃ§o : {currency}{price}.<br /><br />\nData de Checkin: {checkin}.<br /><br />\nData de SaÃ­da:  {checkout}.<br /><br />\nValor reembolsado: {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(375, 'list_create_host_po', 'List creation to Host', 'VocÃª criou a nova lista', 'OlÃ¡ {host_name},\n\nVocÃª criou a nova lista.\n\nNome da lista: {list_title}\n\nLink: {link}\n\nPreÃ§o: {price}\n\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nVocÃª criou a nova lista.<br /><br />\nNome da lista: {list_title}.<br /><br />\nLink : {link}.<br /><br />\nPreÃ§o : {price}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(376, 'list_create_admin_po', 'List creation to Admin', '{host_name} criaram a nova lista', 'OlÃ¡, Admin,\n\n{host_name} criaram a nova lista.\n\nNome do host: {host_name}\n\nNome da lista: {list_title}\n\nLink: {link}\n\nPreÃ§o: {price}\n\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{host_name} criaram a nova lista.<br /><br />\nNome do host:  {host_name}.<br /><br />\nNome da lista:{list_title}.<br /><br />\nLink : {link}.<br /><br />\nPreÃ§o : {price}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(377, 'list_delete_host_po', 'List deletion to Host', 'VocÃª excluiu a lista.', 'OlÃ¡ {host_name},\n\nVocÃª excluiu a lista.\n\nNome da lista: {list_title}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nVocÃª excluiu a lista.<br /><br />\nNome da lista: {list_title}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(378, 'list_delete_admin_po', 'List deletion to Admin', '{host_name} apagaram a lista.', 'OlÃ¡, Admin,\n\n{host_name} apagaram a lista.\n\nNome do host: {host_name}\n\nNome da lista: {list_title}\n\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{host_name} apagaram a lista.<br /><br />\nNome do host:  {host_name}.<br /><br />\nNome da lista: {list_title}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(379, 'payment_issue_to_admin_po', 'Payment issue mail to Admin', 'Pagamento mal configurado', 'Ola Admin,\n\n{payment_type} As credenciais da API estÃ£o mal configuradas.\n\n{username} tentou efetuar o pagamento.\n\nID do e-mail: {email_id}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{payment_type} As credenciais da API estÃ£o mal configuradas.</p>\n<p>{username} tentou efetuar o pagamento.</p>\n<p>ID do e-mail: {email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(380, 'refund_host_commission_po', 'Host: Accept Commission Refund', 'Reembolsando a Host Accept Commission do Admin', 'OlÃ¡ {host_name},\n\nO Admin reembolsou o valor {refund_amt} na sua conta {account}.\n\nNome da lista: {list_title}\nValor reembolsado: {refund_amt}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>O Admin reembolsou o valor {refund_amt} na sua conta {account}.</p>\n<p>Nome da lista: {list_title}</p>\n<p>Valor reembolsado:  {refund_amt}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(381, 'refund_host_commission_admin_po', 'Admin: Accept Commission Refund', 'VocÃª tem o reembolso da Host Accept Commission', 'OlÃ¡, Admin,\n\nVocÃª reembolsou o valor {refund_amt} na sua conta {account}.\n\nNome da lista: {list_title}\nNome do host: {host_name}\nValor reembolsado: {refund_amt}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi Admin,</td>\n</tr>\n<tr>\n<td>\n<p>VocÃª reembolsou o valor {refund_amt} na sua conta {account}.</p>\n<p>Nome da lista: {list_title}</p>\n<p>Nome do host: {host_name}</p>\n<p>Valor reembolsado: {refund_amt}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(382, 'passport_verification_po', 'Passport verification for administrator', '{user_name} Enviou o pedido de verificaÃ§Ã£o de passaporte, userid-{user_id}', 'OlÃ¡ Admin, \\ n \\ n {user_name} enviou a solicitaÃ§Ã£o de verificaÃ§Ã£o de passaporte, id do usuÃ¡rio {user_id} \\ n \\ nDetalhes como se segue, \\ n \\ n Nome do usuÃ¡rio: {user_name} \\ nUsuÃ¡rio: {user_id} \\ n \\ n \\ n \\ N - \\ nObrigado e Atenciosamente, \\ n \\ n {user_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\\n<tbody>\\n<tr>\\n<td>Oi Admin,</td>\\n</tr>\\n<tr>\\n<td>\\n<p>{user_name} {user_name} enviou a solicitaÃ§Ã£o de verificaÃ§Ã£o de passaporte, id do usuÃ¡rio {user_id}.</p>\\n<p>Detalhes como se segue,</p>\\n<p>Nome do usuÃ¡rio: {user_name}</p>\\n<p>UsuÃ¡rio: {user_id}</p>\\n\\n</td>\\n</tr>\\n<tr>\\n<td>\\n<p style=\"margin: 0 10px 0 0;\">--</p>\\n<p style=\"margin: 0 0 10px 0;\">Obrigado e Atenciosamente,</p>\\n<p style=\"margin: 0px;\">{user_name} </p>\\n</td>\\n</tr>\\n</tbody>\\n</table>'),
(383, 'change password to user_po', 'admin change to the password', 'Bem-vindo ao  {site_name}', 'Senha antiga: {new_password} Conforme a senha: {conform_password} - Obrigado e Atenciosamente, Admin {site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Caro Membro,</td>\n</tr>\n<tr>\n<td>\n<p>VocÃª mudou sua senha com sucesso</p>\n<p>Use os detalhes da senha abaixo para login.</p>\n<p>Senha antiga: {new_password}</p>\n<p><span>New password:{conform_password}</span></p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(384, 'spam_po', 'Spam List', 'Admin Message - Listagem reportada como Spam!', '**** ADMIN MESSAGE ****\n\n\n\nA lista abaixo foi relatada como Spam:\n\nURL para listagem de spam: {list_url}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>****ADMIN MESSAGE****</td>\n</tr>\n<tr>\n<td>\n<p>Â FLEXIFLAT.COM SPAM REPORT!</p>\n<p>Ele abaixo da lista foi relatado como Spam:<br /><br />{list_url}</p>\n</td>\n</tr>\n<tr>\n<td>Â </td>\n</tr>\n</tbody>\n</table>\n<p>Â </p>'),
(385, 'host_notify_review_po', 'Host Notification for Review', '{guest_name} Tem uma revisÃ£o de {host_name}', 'OlÃ¡ {guest_name},\n\n{host_name} adicionou um comentÃ¡rio sobre vocÃª em {list_name} em {site_name}\n\nE ele estÃ¡ esperando sua revisÃ£o.\nNome do host: {host_name}\nNome da lista: {list_name}\n\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi {guest_name},</td>\n</tr>\n<tr>\n<td><br/ >{host_name} adicionou um comentÃ¡rio sobre vocÃª em {list_name} em {site_name}<br />E ele estÃ¡ esperando sua revisÃ£o.\n<br/>\nNome do host: {host_name}<br/>\nNome da lista:{list_name}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(386, 'guest_notify_review_po', 'Guest Notification for Review', '{guest_name} Check-out.', 'OlÃ¡ {host_name},\n\n{guest_name} tem checkouted da sua lista {list_name} em {site_name}\n\nE ele estÃ¡ esperando sua revisÃ£o.\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OI {host_name},</td>\n</tr>\n<tr>\n<td><br /> {guest_name} tem checkouted da sua lista {list_name} em {site_name} <br />E ele estÃ¡ esperando sua revisÃ£o.</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(387, 'list_notification_po', 'Notification for calendar update', 'A lista {list_name} precisa de uma atualizaÃ§Ã£o', 'OlÃ¡ {host_name},\n\nO {list_name} listado em {site_name} nÃ£o tem atualizaÃ§Ãµes no mÃªs passado.\n\nPara melhorar sua classificaÃ§Ã£o nos resultados da pesquisa, atualize sua lista\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>\nO {list_name} listado em {site_name} nÃ£o tem atualizaÃ§Ãµes no mÃªs passado.</p>\n<p>\nPara melhorar sua classificaÃ§Ã£o nos resultados da pesquisa, atualize sua lista\n</p>\n\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>\n'),
(388, 'host_notification_po', 'Upcoming notification for host', 'O {traveler_name} tem uma reserva no seu {list_title} amanhÃ£', 'OlÃ¡ {host_name},\n\n{traveler_name} reservou o {list_title} lugar em amanhÃ£.\n\nDetalhes como segue,\n\nNome do viajante: {traveler_name}\nId. Do e-mail de contato: {traveler_email_id}\nNome do local: {list_title}\nCheck-in: {checkin}\nConfira: {checkout}\n\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} reservou o {list_title} lugar em amanhÃ£.</p>\n<p>Detalhes como segue,</p>\n\n<p>Nome do viajante: {traveler_name}</p>\n<p>Id. Do e-mail de contato: {traveler_email_id}</p>\n<p>Nome do local: {list_title}</p>\n<p>Check-in: {checkin}</p>\n<p>Confira: {checkout}</p>\n\n\n</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>\n'),
(389, 'tc_book_to_admin_sp', 'Admin notification for  Travel cretid booking', '{traveler_name} enviÃ³ la solicitud de reserva usando sus Travel Cretids', 'Hola administrador\n\n{traveler_name} enviÃ³ la solicitud de reserva para reservar el lugar {list_title} el {book_date} en {book_time} utilizando sus Travel Credits.\n\nDetalles como sigue,\n\nNombre del viajero: {traveler_name}\nId. De correo electrÃ³nico de contacto: {traveler_email_id}\nNombre del lugar: {list_title}\nLlegada: {checkin}\nEcha un vistazo a: {checkout}\nPrecio de mercado: {price_price}\nCantidad pagada: {payed_amount}\nCrÃ©ditos de viaje: {travel_credits}\nNombre de host: {host_name}\nID de correo electrÃ³nico del host: {host_email_id}\n\n--\nGracias y saludos,\n\n{site_name} Equipo', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} enviÃ³ la solicitud de reserva para reservar el lugar {list_title} el {book_date} en {book_time} utilizando sus Travel Credits.</p>\n<p>Detalles como sigue,</p>\n<p>Nombre del viajero: : {traveler_name}</p>\n<p>Id. De correo electrÃ³nico de contacto {traveler_email_id}</p>\n<p>Nombre del lugar: {list_title}</p>\n<p>Llegada: {checkin}</p>\n<p>Echa un vistazo a: {checkout}</p>\n<p>Cantidad pagada:  {market_price}</p>\n<p>Cantidad pagada: {payed_amount}</p>\n<p>CrÃ©ditos de viaje: {travel_credits}</p>\n<p>Nombre de host:  {host_name}</p>\n<p>ID de correo electrÃ³nico del host:  {host_email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipo</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(390, 'tc_book_to_host_sp', 'Host notification for  Travel cretid booking', '{traveler_name} enviÃ³ la solicitud de reserva usando sus Travel Cretids', 'Hola {username},\n\n{traveler_name} enviÃ³ la solicitud de reserva para reservar su {list_title} lugar el {book_date} en {book_time} utilizando sus Travel Credits.\n\nNos pondremos en contacto con usted con el pago correspondiente.\n\nDetalles como sigue,\n\nNombre del viajero: {traveler_name}\nId. De correo electrÃ³nico de contacto: {traveler_email_id}\nNombre del lugar: {list_title}\nLlegada: {checkin}\nEcha un vistazo a: {checkout}\nPrecio: {price_price}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {username} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} enviÃ³ la solicitud de reserva para reservar su {list_title} lugar el {book_date} en {book_time} utilizando sus Travel Credits.</p>\n<p>Detalles como sigue,</p>\n<p>Nombre del viajero:  {traveler_name}</p>\n<p>Id. De correo electrÃ³nico de contacto: {traveler_email_id}</p>\n<p>Nombre del lugar:  {list_title}</p>\n<p>Llegada : {checkin}</p>\n<p>Echa un vistazo a:  {checkout}</p>\n<p>Precio : {market_price}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(391, 'admin_mass_email_sp', 'Admin mass email', '{subject}', 'Hola Usuario,\n\n{dynamic_content}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola Usuario,</td>\n</tr>\n<tr>\n<td>\n<p>{dynamic_content}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(392, 'user_reference_sp', 'Reference', 'Escribe {username} una referencia en {site_name}', 'Hola,\n\n{username} le pide que proporcione una referencia que pueda mostrar pÃºblicamente en su perfil {site_name}. {site_name} es un mercado comunitario para alojamientos que se basa en la confianza y la reputaciÃ³n. Una referencia de usted realmente ayudarÃ­a a construir su reputaciÃ³n con la comunidad.\nUna referencia en {site_name} puede ayudar a {username} a encontrar un lugar fresco para alojarse o alojar viajeros interesantes.\n\n{click_here} para tener una referencia para {username}.\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola,</td>\n</tr>\n<tr>\n<td>\n<p>{username} le pide que proporcione una referencia que pueda mostrar pÃºblicamente en su perfil {site_name}. {site_name} es un mercado comunitario para alojamientos que se basa en la confianza y la reputaciÃ³n. Una referencia de usted realmente ayudarÃ­a a construir su reputaciÃ³n con la comunidad.\nUna referencia en {site_name} puede ayudar a {username} a encontrar un lugar fresco para alojarse o alojar viajeros interesantes.</p>\n<p>{click_here} para tener una referencia para {username}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(393, 'reference_receive_sp', 'Reference Receive', 'Â¡Recibiste una nueva referencia en {site_name}!', 'Hola {username},\n\n{other_username} ha escrito una referencia para usted. Antes de que vaya a tu perfil pÃºblico, puedes revisarlo y aceptarlo o ignorarlo. {click_here} para ver la referencia.\n\n--\nGracias y saludos,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {username},</td>\n</tr>\n<tr>\n<td>\n<p>{other_username} ha escrito una referencia para usted. Antes de que vaya a tu perfil pÃºblico, puedes revisarlo y aceptarlo o ignorarlo. {click_here} para ver la referencia.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(394, 'host_reservation_notification_sp', 'Reservation notification for host', 'La reserva fue solicitada por {traveler_name}', 'Hola {username},\n\n{traveler_name} reservÃ³ el {list_title} lugar el {book_date} en {book_time}.\n\nDetalles como sigue,\n\nNombre del viajero: {traveler_name}\nId. De correo electrÃ³nico de contacto: {traveler_email_id}\nNombre del lugar: {list_title}\nLlegada: {checkin}\nEcha un vistazo a: {checkout}\nPrecio: {price_price}\n\nPor favor, dÃ© la confirmaciÃ³n haciendo clic en la acciÃ³n de abajo.\n{cction_url}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>\n<p>Hola {username} ,</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} reservÃ³ el {list_title} lugar el {book_date} en {book_time}.</p>\n<br />\n<p>Detalles como sigue,</p>\n<p>Nombre del viajero: {traveler_name}</p>\n<p>Id. De correo electrÃ³nico de contacto: {traveler_email_id}</p>\n<pNombre del lugar:  {list_title}</p>\n<p>Llegada {checkin}</p>\n<p>Echa un vistazo a: {checkout}</p>\n<p>Precio : {market_price}</p>\n<br />\n<p>Por favor, dÃ© la confirmaciÃ³n haciendo clic en la acciÃ³n de abajo.\n</p>\n<p>{action_url}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipo</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(395, 'traveller_reservation_notification_sp', 'Reservation notification for  traveller', 'Su solicitud de reserva se envÃ­a con Ã©xito', 'Hola {traveler_name},\n\nSu solicitud de reserva se envÃ­a correctamente al host correspondiente.\n\nPor favor espere su confirmaciÃ³n.\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Su solicitud de reserva se envÃ­a correctamente al host correspondiente.</p>\n<p>Por favor espere su confirmaciÃ³n.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(396, 'admin_reservation_notification_sp', 'Reservation notification for  administrator', '{traveler_name} enviÃ³ la solicitud de reserva a {host_name}', 'Hola administrador\n\n{traveler_name} enviÃ³ la solicitud de reserva de {list_title} lugar el {book_date} en {book_time}.\n\nDetalles como sigue,\n\nNombre del viajero: {traveler_name}\nId. De correo electrÃ³nico de contacto: {traveler_email_id}\nNombre del lugar: {list_title}\nLlegada: {checkin}\nEcha un vistazo a: {checkout}\nPrecio: {price_price}\nNombre de host: {host_name}\nID de correo electrÃ³nico del host: {host_email_id}\n\n--\nGracias y saludos,\n\n{site_name} Equipo', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} enviÃ³ la solicitud de reserva de {list_title} lugar el {book_date} en {book_time}.</p>\n<p>Detalles como sigue,</p>\n<p>Nombre del viajero:  {traveler_name}</p>\n<p>Id. De correo electrÃ³nico de contacto: {traveler_email_id}</p>\n<p>Nombre del lugar: {list_title}</p>\n<p>Llegada : {checkin}</p>\n<p>Echa un vistazo a: {checkout}</p>\n<p>Precio : {market_price}</p>\n<p>Nombre de host: {host_name}</p>\n<p>ID de correo electrÃ³nico del host:  {host_email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipo</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(397, 'traveler_reservation_granted_sp', 'Traveler : After Reservation granted', 'Felicitaciones! Su solicitud de reserva se concede.', 'Hola {traveler_name},\n\nEnhorabuena, su solicitud de reserva es otorgada por {host_name} para {list_name}.\n\nA continuaciÃ³n mencionamos sus datos de contacto,\n\nNombre: {Fname}\nApellido: {Lname}\nEn vivo: {livein}\nTelÃ©fono: {phnum}\n\nLa direcciÃ³n exacta es,\n\nDirecciÃ³n de la calle: {street_address}\nDirecciÃ³n opcional: {optional_address}\nCiudad: {city}\nEstado: {state}\nPaÃ­s: {country}\nCÃ³digo postal: {zipcode}\n\nMensaje del anfitriÃ³n: {comment}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Enhorabuena, su solicitud de reserva es otorgada por {host_name} para {list_name}.</p>\n<p>A continuaciÃ³n mencionamos sus datos de contacto,</p>\n<p>Nombre : {Fname}</p>\n<p>Apellido : {Lname}</p>\n<p>En vivo: {livein}</p>\n<p>TelÃ©fono: {phnum}</p>\n<p>La direcciÃ³n exacta es,</p>\n<p>DirecciÃ³n de la calle: {street_address},</p>\n<p>DirecciÃ³n opcional: {optional_address},</p>\n<p>Ciudad : {city},</p>\n<p>Estado : {state},</p>\n<p>PaÃ­s : {country},</p>\n<p>CÃ³digo postal: {zipcode}</p>\n<p>Mensaje del anfitriÃ³n: {comment}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(398, 'traveler_reservation_declined_sp', 'Traveler : After reservation declined', 'Â¡Lo siento! Se rechaza su solicitud de reserva', 'Hola {traveler_name},\n\nLo sentimos, Su solicitud de reserva es cenada por {host_name} para {list_title}.\n\nMensaje del anfitriÃ³n: {comment}\n\nPronto, nos pondremos en contacto con usted con el pago correspondiente.\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Lo sentimos, Su solicitud de reserva es cenada por {host_name} para {list_title}.</p>\n<p>Mensaje del anfitriÃ³n: {comment}</p>\n<p>Pronto, nos pondremos en contacto con usted con el pago correspondiente.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(399, 'traveler_reservation_cancel_sp', 'Traveler : After reservation canceled', '{traveler_name} cancelÃ³ su lista de reservas', 'Hola {host_name},\n\nLo sentimos, su lista de reservas ({status}) es cancelada por {traveler_name} para {list_title}.\n\n{user_type} Mensaje: {comment}\n\nSeguro que nos pondremos en contacto con usted en breve, si hay algÃºn saldo de pago.\n\nY tambiÃ©n, si usted tiene cualesquiera otras preguntas, sienta por favor libre de entrarnos en contacto con.\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Lo sentimos, su lista de reservas ({status}) es cancelada por {traveler_name} para {list_title}.</p>\n<p>{user_type} Mensaje : {comment}</p>\n<p>Seguro que nos pondremos en contacto con usted en breve, si hay algÃºn saldo de pago.</p>\n<p>Y tambiÃ©n, si usted tiene cualesquiera otras preguntas, sienta por favor libre de entrarnos en contacto con.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(400, 'traveler_reservation_expire_sp', 'Traveler : Reservation Expire', 'Â¡Lo siento! Su solicitud {type} ha caducado.', 'Hola {traveler_name},\n\nSu solicitud {type} ha caducado.\n\nNombre de host: {host_name}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nSu solicitud {type} ha caducado.<br /><br />\nNombre de host:  {host_name}.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nPrecio : {currency}{price}.<br /><br />\nFecha de llegada: {checkin}.<br /><br />\nFecha de pago: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(401, 'host_reservation_expire_sp', 'Host : Reservation Expire', '{type} solicitud caducada para su lista', 'Hola {host_name},\n\n{type} solicitud caducada para {list_title} reservada por {traveler_name}.\n\nNombre de invitado: {traveler_name}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{type} solicitud caducada para {list_title} reservada por {traveler_name}.<br /><br />\nNombre de invitado:  {traveler_name}.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nPrecio : {currency}{price}.<br /><br />\nFecha de llegada: {checkin}.<br /><br />\nFecha de pago: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(402, 'admin_reservation_expire_sp', 'Admin : Reservation Expire', '{type} La solicitud ha caducado', 'Hola administrador\n\n{traveler_name} La solicitud {type} ha caducado para {list_title}.\n\nNombre de invitado: {traveler_name}\nCorreo electrÃ³nico de invitado: {traveler_email}\nNombre de host: {host_name}\nCorreo de host: {host_email}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}\n\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} La solicitud {type} ha caducado para {list_title}.<br /><br />\nNombre de invitado: {traveler_name}.<br /><br />\nCorreo electrÃ³nico de invitado: {traveler_email}.<br /><br />\nNombre de host: {host_name}.<br /><br />\nCorreo de host:  {host_email}.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nPrecio: : {currency}{price}.<br /><br />\nFecha de llegada:  {checkin}.<br /><br />\nFecha de pago:  {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Team</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(403, 'host_reservation_granted_sp', 'Host : After Reservation Granted', 'Ha aceptado la solicitud de reserva {traveler_name}', 'Hola {host_name},\n\nHa aceptado la solicitud de reserva {traveler_name} de {list_title}.\n\nA continuaciÃ³n mencionamos sus datos de contacto,\n\nNombre: {Fname}\nApellido: {Lname}\nEn vivo: {livein}\nTelÃ©fono: {phnum}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Ha aceptado la solicitud de reserva {traveler_name} de {list_title}.</p>\n<p>A continuaciÃ³n mencionamos sus datos de contacto,</p>\n<p>Nombre: {Fname}</p>\n<p>Apellido : {Lname}</p>\n<p>En vivo : {livein}</p>\n<p>TelÃ©fono : {phnum}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(404, 'admin_reservation_granted_sp', 'Admin : After Reservation granted', '{host_name} aceptÃ³ la solicitud de reserva de {traveler_name}', 'Hola administrador\n\n{host_name} aceptÃ³ la solicitud de reserva {traveler_name} para {list_title}.\n\n--\nGracias y saludos,\n{site_name} Equipo', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} aceptÃ³ la solicitud de reserva {traveler_name} para {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipo</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(405, 'host_reservation_declined_sp', 'Host : After Reservation Declined', 'Ha rechazado la solicitud de reserva de {traveler_name}', 'Hola {host_name},\n\nHa rechazado la solicitud de reserva {traveler_name} de {list_title}.\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Ha rechazado la solicitud de reserva {traveler_name} de {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(406, 'admin_reservation_declined_sp', 'Admin : After Reservation Declined', '{host_name} rechazÃ³ la solicitud de reserva de {traveler_name}', 'Hola administrador\n\n{host_name} rechazÃ³ la solicitud de reserva {traveler_name} de {list_title}.\n\n--\nGracias y saludos,\n\n{site_name} Equipo', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} rechazÃ³ la solicitud de reserva {traveler_name} de {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipo</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(407, 'admin_reservation_cancel_sp', 'Admin : After reservation canceled', '{traveler_name} cancelÃ³ la lista de reservas {host_name}', 'Hola administrador\n\n{traveler_name} cancelÃ³ la lista de reservas {host_name} ({status}) para {list_title}.\n\n{penalty}\n\n--\nGracias y saludos,\n\n{site_name} Equipo', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} cancelÃ³ la lista de reservas {host_name} ({status}) para {list_title}.\n<br/><br/>{penalty}\n</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipo</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(408, 'host_reservation_cancel_sp', 'Host : After reservation canceled', '{host_name} cancelado Su lista de reservas', 'Hola {traveler_name},\n\n{host_name} cancela la lista de reservas ({status}) de {list_title}.\n\n\nSi tiene alguna otra pregunta, no dude en contactar con nosotros.\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{host_name} cancela la lista de reservas ({status}) de {list_title}.<br /></p>\n<p>Si tiene alguna otra pregunta, no dude en contactar con nosotros.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(409, 'forgot_password_sp', 'Forgot Password', 'Se te olvidÃ³ tu contraseÃ±a', 'Querido miembro,\n\nA continuaciÃ³n hemos mencionado los detalles de su cuenta.\n\nAquÃ­ vamos,\n\nEmail_id: {email}\n\nContraseÃ±a: {contraseÃ±a}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Querido miembro,</td>\n</tr>\n<tr>\n<td>\n<p>A continuaciÃ³n hemos mencionado los detalles de su cuenta.</p>\n<p>AquÃ­ vamos,</p>\n<p>Email_id : {email}</p>\n<p>ContraseÃ±a : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(410, 'checkin_host_sp', 'Host: Check In', 'Cheque de viajero', 'Hola {host_name},\n\n{traveler_name} se registra en {list_title}.\n\nNombre de invitado: {guest_name}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} se registra en {list_title}.<br /><br />\nNombre de invitado: {traveler_name}.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nPrecio : {currency}{price}.<br /><br />\nFecha de llegada: {checkin}.<br /><br />\nFecha de pago: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(411, 'users_signin_sp', 'Users Signin', 'Bienvenido a {site_name}', 'Querido miembro,\n\nEs un placer conocerlo y darle la bienvenida a {site_name}.\n\nA continuaciÃ³n hemos mencionado los detalles de su cuenta.\n\nAquÃ­ vamos,\n\nEmail_id: {email}\n\nContraseÃ±a: {contraseÃ±a}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Querido miembro,</td>\n</tr>\n<tr>\n<td>\n<p>Es un placer conocerlo y darle la bienvenida a {site_name}.</p>\n<p>A continuaciÃ³n hemos mencionado los detalles de su cuenta.</p>\n<p>AquÃ­ vamos,</p>\n<p>Email_id : {email}</p>\n<p>ContraseÃ±a : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(412, 'reset_password_sp', 'Reset Password', 'Restablecer la contraseÃ±a', 'Querido miembro,\n\nA continuaciÃ³n, hemos mencionado los detalles de su nueva cuenta.\n\nAquÃ­ vamos,\n\nContraseÃ±a: {password}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Querido miembro,</td>\n</tr>\n<tr>\n<td>\n<p>A continuaciÃ³n, hemos mencionado los detalles de su nueva cuenta.\n</p>\n<p>AquÃ­ vamos,</p>\n<p>ContraseÃ±a : {password}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(413, 'admin_payment_sp', 'Admin payment to Host', 'El administrador pagÃ³ sus honorarios por {list_title}', 'Hola {username},\n\nHemos pagado sus honorarios por {list_title}.\n\nDetalles como sigue,\n\nNombre del lugar: {list_title}\nLlegada: {checkin}\nEcha un vistazo a: {checkout}\nPrecio: {payed_price}\nPago a travÃ©s de: {payment_type}\nID de pago: {pay_id}\nFecha de pago: {payed_date}\n\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>\n<p>Hola {username} ,</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>Hemos pagado sus honorarios por {list_title}.</p>\n<br />\n<p>Detalles como sigue,</p>\n<p>Nombre del lugar: {list_title}</p>\n<p>Llegada: {checkin}</p>\n<p>Echa un vistazo a: {checkout}</p>\n<p>Precio : {payed_price}</p>\n<p>Pago a travÃ©s de: {payment_type}</p>\n<p>ID de pago: {pay_id}</p>\n<p>Fecha de pago : {payed_date}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipo</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(414, 'contact_form_sp', 'Contact Form', 'Mensaje recibido del formulario de contacto', 'Hola administrador\n\nUn mensaje recibido de la pÃ¡gina de contacto de {date} en {time}.\n\nDetalles como sigue,\n\nNombre: {name}\n\nCorreo electrÃ³nico: {email}\n\nMensaje: {message}\n\n--\nGracias y saludos,\n\n{site_name} Equipo', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola Admin,</td>\n</tr>\n<tr>\n<td>\n<p>Un mensaje recibido de la pÃ¡gina de contacto de {date} en {time}.</p>\n<p>Detalles como sigue,</p>\n<p>Nombre : {name}</p>\n<p>Correo electrÃ³nico : {email}</p>\n<p>Mensaje : {message}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0px;\">{site_name} Equipo</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(415, 'invite_friend_sp', 'Invite My Friends', '{username} invitarte.', 'Hola amigos,\n\n{username} Quiere que invites {site_name}\n\n{dynamic_content}\n\n{click_here}\n\n--\nSaludos,\n{username}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola amigos,</td>\n</tr>\n<tr>\n<td>\n<p>{username} Quiere que invites</p>\n<p>{site_name}</p>\n<p>{dynamic_content}</p>\n<p>Â {click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Saludos,</p>\n<p style=\"margin: 0px;\">{username}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(416, 'email_verification_sp', 'Email Verification Link', '{site_name} Enlace de verificaciÃ³n de correo electrÃ³nico', 'Hola {user_name},\n\nPor favor, haga clic en el enlace de abajo para su verificaciÃ³n de correo electrÃ³nico de {site_name}.\n\n{click_here}\n\n-\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {user_name},</td>\n</tr>\n<tr>\n<td>\n<p>Por favor, haga clic en el enlace de abajo para su verificaciÃ³n de correo electrÃ³nico de {site_name}.</p>\n<p>Â {click_here}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(417, 'referral_credit_sp', 'Referral Credit', 'Usted gana {amount} de referencias', 'Hola {username},\n\nSe obtiene {amount} por {friend_name}\n\n--\nGracias y saludos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo{user_name},</td>\n</tr>\n<tr>\n<td><p>\nSe obtiene {amount} por {friend_name}</p>\n</td>\n</tr><tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(418, 'User_join_to_Referal_mail_sp', 'User join to Referal mail', 'Tu Amigo RegÃ­strate', 'Estimado {refer_name},\n\nSu amigo {friend_name} ahora es unirse a {site_name}. Ahora, $ 100 es un crÃ©dito en su cuenta de Travel Credit Possible.\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Estimado {refer_name},</td>\n</tr>\n<tr>\n<td><br />Su amigo {friend_name} ahora es unirse a {site_name}. Ahora, $ 100 es un crÃ©dito en su cuenta de Travel Credit Possible.<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(419, 'contact_request_to_host_sp', 'Contact Request to Host', 'Solicitud de contacto', 'Hola {host_username},\n\nHaga clic en el siguiente enlace para ponerse en contacto con el usuario: {link}\n\nLista: {tÃ­tle}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nInvitados: {guest}\n\nMensaje del huÃ©sped: {message}\n\n--\nGracias y saludos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_username},</td>\n</tr>\n<tr>\n<td>\nHaga clic en el siguiente enlace para ponerse en contacto con el usuario: {link}<br /><br />\n			\nLista : {title}<br /><br />\n		\nFecha de llegada: {checkin}<br /><br />\n		\nFecha de pago: {checkout}<br /><br />\n		\nInvitados : {guest}<br /><br />\n		\nMensaje del huÃ©sped: {message}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(420, 'contact_request_to_guest_sp', 'Contact Request to Guest', 'Solicitud de contacto', 'Hola {traveller_username},\n\nHa enviado la solicitud de contacto a {host_username}.\n\nLista: {tÃ­tle}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nInvitados: {guest}\n\nSu mensaje: {message}\n\n--\nGracias y saludos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {traveller_username},</td>\n</tr>\n<tr>\n<td>\nHa enviado la solicitud de contacto a {host_username}.<br /><br />\n			\nLista : {title}<br /><br />\n		\nFecha de llegada:  {checkin}<br /><br />\n		\nFecha de pago: {checkout}<br /><br />\n		\nInvitados : {guest}<br /><br />\n		\nSu mensaje: {message}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(421, 'request_granted_to_guest_sp', 'Contact Request Granted to Guest', 'Solicitud de contacto concedida', 'Hola {traveller_username},\n\nSu solicitud de contacto es otorgada por {host_username}.\n\nCorreo de host: {host_email}\n\nLista: {tÃ­tle}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nMensaje del anfitriÃ³n: {message}\n\nURL de reserva: {link}\n\n--\nGracias y saludos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {traveller_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nSu solicitud de contacto es otorgada por {host_username}.\n<br /><br />\n\nCorreo de host:  {host_email}<br /><br /> \n\nLista : {title}<br /><br /> \n\nFecha de llegada: {checkin}<br /><br /> \n\nFecha de pago: {checkout}<br /><br /> \n\nPreeze : {price}<br /><br />\n\nMensaje del anfitriÃ³n: {message}<br /><br /> \n\nURL de reserva: {link}<br /><br /></td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(422, 'request_granted_to_host_sp', 'Contact Request Granted to Host', 'Solicitud de contacto concedida', 'Hola {host_username},\n\nDebe aceptar la solicitud de contacto de {traveller_username}.\n\nCorreo electrÃ³nico de invitado: {guest_email}\n\nLista: {tÃ­tle}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nPrecio: {price}\n\nSu mensaje: {message}\n\n--\nGracias y saludos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_username},</td>\n</tr>\n<tr>\n<td><br /><br />\nDebe aceptar la solicitud de contacto de {traveller_username}. <br /><br />\n\nCorreo electrÃ³nico de invitado : {guest_email}<br /><br />	\n		\nLista : {title}<br /><br />\n		\nFecha de llegada:  {checkin}<br /><br />\n		\nFecha de pago: {checkout}<br /><br />\n\nPrecio : {price}<br /><br />\n	\nSu mensaje: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(423, 'contact_request_to_admin_sp', 'Contact Request to Admin', 'Solicitud de contacto', 'Hola administrador\n\n{traveller_username} enviÃ³ la solicitud de contacto a {host_username}.\n\nLista: {tÃ­tle}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nInvitados: {guest}\n\nPrecio: {price}\n\nMensaje del huÃ©sped: {message}\n\n--\nGracias y saludos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo administrador,</td>\n</tr>\n<tr>\n<td><br />\n{traveller_username} enviÃ³ la solicitud de contacto a {host_username}. <br /><br />		\nLista : {title}<br /><br />	\nFecha de llegada: {checkin}<br /><br />\n		\nFecha de pago: {checkout}<br /><br />\n	\nPrecio : {price}<br /><br />\n		\nMensaje del huÃ©sped: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>');
INSERT INTO `email_templates` (`id`, `type`, `title`, `mail_subject`, `email_body_text`, `email_body_html`) VALUES
(424, 'request_granted_to_admin_sp', 'Contact Request Granted to Admin', 'Solicitud de contacto concedida', 'Hola administrador\n\n{host_username} acepta la solicitud de contacto de {traveller_username}.\n\nCorreo electrÃ³nico de invitado: {guest_email}\n\nCorreo de host: {host_email}\n\nLista: {tÃ­tle}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nPrecio: {price}\n\nMensaje del anfitriÃ³n: {message}\n\n--\nGracias y saludos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td><br />\n{host_username} acepta la solicitud de contacto de {traveller_username}.<br /><br />\n\nCorreo electrÃ³nico de invitado:  {guest_email}<br /><br />	\n\nCorreo de host: {host_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nFecha de llegada: {checkin}<br /><br />\n		\nFecha de pago: {checkout}<br /><br />\n\nPrecio : {price}<br /><br />	\n			\nMensaje del anfitriÃ³n: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(425, 'special_request_granted_to_guest_sp', 'Contact Request Granted with Special Offer to Guest', 'Solicitud de contacto otorgada con oferta especial', 'Hola {traveller_username},\n\n{host_username} acepta su solicitud de contacto con una oferta especial.\n\nCorreo de host: {host_email}\n\nLista: {tÃ­tle}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nPrecio: {price}\n\nMensaje: {message}\n\nURL de reserva: {link}\n\n--\nGracias y saludos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {traveller_username},</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} acepta su solicitud de contacto con una oferta especial.<br /><br />\n\nCorreo de host:  {host_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nFecha de llegada: {checkin}<br /><br />\n		\nFecha de pago: {checkout}<br /><br />\n\nPrecio : {price}<br /><br />\n			\nMensaje  : {message}<br /><br />\n\nURL de reserva: {link}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(426, 'special_request_granted_to_host_sp', 'Contact Request Granted with Special Offer to Host', 'Solicitud de contacto otorgada con oferta especial', 'Hola {host_username},\n\nTienes que aceptar la solicitud de contacto {traveller_username} con oferta especial.\n\nCorreo electrÃ³nico de invitado: {guest_email}\n\nLista: {tÃ­tle}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nPrecio: {price}\n\nSu mensaje: {message}\n\n--\nGracias y saludos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_username},</td>\n</tr>\n<tr>\n<td><br />\n\nTienes que aceptar la solicitud de contacto {traveller_username} con oferta especial.<br /><br />\n\nCorreo electrÃ³nico de invitado : {guest_email}<br /><br />\n\nLista : {title}<br /><br />	\n\nFecha de llegada: {checkin}<br /><br />\n		\nFecha de pago: {checkout}<br /><br />\n\nPrecio : {price}<br /><br />\n			\nSu mensaje: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(427, 'special_request_granted_to_admin_sp', 'Contact Request Granted with Special Offer to Admin', 'Solicitud de contacto otorgada con oferta especial', 'Hola administrador\n\n{host_username} tiene que aceptar la solicitud de contacto {traveller_name} con oferta especial.\n\nCorreo electrÃ³nico de invitado: {guest_email}\n\nCorreo de host: {host_email}\n\nLista: {tÃ­tleo}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nPrecio: {price}\n\nMensaje del anfitriÃ³n: {message}\n\n--\nGracias y saludos,\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} tiene que aceptar la solicitud de contacto {traveller_name} con oferta especial.<br /><br />\n\nCorreo electrÃ³nico de invitado: {guest_email}<br /><br />\n\nCorreo de host: {host_email}<br /><br />\n\nLista : {title}<br /><br />	\n\nFecha de llegada: {checkin}<br /><br />\n		\nFecha de pago: {checkout}<br /><br />\n\nPrecio: {price}<br /><br />\n			\nMensaje del anfitriÃ³n: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">Admin</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(428, 'request_cancel_to_guest_sp', 'Contact Request Cancelled to Guest', 'Â¡Lo siento! Su solicitud de contacto se cancela', 'Hola {traveller_username},\n\nLo sentimos, su solicitud de contacto es denegada por {host_username} para {title}.\n\nMensaje del anfitriÃ³n: {message}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {traveller_username},</td>\n</tr>\n<tr>\n<td>\n<p>Lo sentimos, su solicitud de contacto es denegada por {host_username} para {title}.</p><br /><br />\nMensaje del anfitriÃ³n: {message}<br /><br />\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(429, 'request_cancel_to_host_sp', 'Contact Request Cancelled to Host', 'Ha cancelado la solicitud de contacto', 'Hola {host_username},\n\nHas cancelado la solicitud de contacto {traveller_username} para {title}.\n\nSu mensaje: {message}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_username} ,</td>\n</tr>\n<tr>\n<td>\n<p>Has cancelado la solicitud de contacto {traveller_username} para {title}.</p><br /><br />\nSu mensaje: {message}<br /><br />\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(430, 'checkout_admin_sp', 'Admin: Check Out', 'Salida del viajero', 'Hola administrador\n\n{traveler_name} se registra desde {list_title}.\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} se registra desde {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(431, 'request_cancel_to_admin_sp', 'Contact Request Cancelled to Admin', '{host_username} cancelÃ³ la solicitud de contacto {traveller_username}', 'Hola administrador,\n\n{host_username} cancelÃ³ la solicitud de contacto {traveller_username} para {title}.\n\nMensaje de host: {message}\n\n--\nGracias y saludos,\n\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td>\n<p>{host_username} cancelÃ³ la solicitud de contacto {traveller_username} para {title}..</p>\n<br /><br />\nMensaje de host: {message}\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(432, 'checkout_host_sp', 'Host: Check Out', 'Salida del viajero', 'Hola {host_name},\n\n{traveler_name} se registra desde {list_title}.\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} se registra desde {list_title}..</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(433, 'checkout_traveler_sp', 'Traveler: Check Out', 'Su salida', 'Hola {traveler_name},\n\nGracias, Recibido de {list_title}.\n\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<p>Gracias, Recibido de {list_title}.</p>\n<p> </p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(434, 'contact_discuss_more_to_guest_sp', 'Contact Request - Discuss more to Guest', 'Solicitud de contacto - Discutir mÃ¡s', 'Hola {traveller_username},\n\n{host_username} quiere discutir mÃ¡s contigo.\n\nCorreo de host: {host_email}\n\nLista: {tÃ­tle}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nMensaje: {message}\n\n--\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {traveller_username},</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} quiere discutir mÃ¡s contigo.<br /><br />\n\nCorreo de host:  {host_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nFecha de llegada: {checkin}<br /><br />\n		\nFecha de pago:  {checkout}<br /><br />\n			\nMensaje: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">AdministraciÃ³n</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(435, 'contact_discuss_more_to_host_sp', 'Contact Request - Discuss more to Host', 'Solicitud de contacto - Discutir mÃ¡s', 'Hola {host_username},\n\nDesea discutir mÃ¡s con {traveller_username}.\n\nCorreo electrÃ³nico de invitado: {guest_email}\n\nLista: {tÃ­tle}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nSu mensaje: {message}\n\n--\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_username},</td>\n</tr>\n<tr>\n<td><br />\n\nDesea discutir mÃ¡s con {traveller_username}.<br /><br />\n\nCorreo electrÃ³nico de invitado: {guest_email}<br /><br />	\n\nLista : {title}<br /><br />	\n\nFecha de llegada: {checkin}<br /><br />\n		\nFecha de pago : {checkout}<br /><br />\n			\nSu mensaje:  {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">AdministraciÃ³n</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(436, 'contact_discuss_more_to_admin_sp', 'Contact Request - Discuss more to Admin', 'Solicitud de contacto - Discutir mÃ¡s', 'Hola administrador,\n\n{host_username} quiere discutir mÃ¡s con {traveller_username}.\n\nCorreo electrÃ³nico de invitado: {guest_email}\n\nCorreo de host: {host_email}\n\nLista: {tÃ­tle}\n\nFecha de llegada: {checkin}\n\nFecha de pago: {checkout}\n\nMensaje del anfitriÃ³n: {message}\n\n--\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td><br />\n\n{host_username} quiere discutir mÃ¡s con {traveller_username}.<br /><br />\n\nCorreo electrÃ³nico de invitado: {guest_email}<br /><br />\n\nCorreo de host: {host_email}<br /><br />\n\nLista : {title}<br /><br />\n\nFecha de llegada: {checkin}<br /><br />\n		\nFecha de pago: {checkout}<br /><br />\n			\nMensaje del anfitriÃ³n: {message}<br /><br />\n\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\n<p style=\"margin: 0 10px 0 0;\">AdministraciÃ³n</p>\n<p style=\"margin: 0px;\">{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(437, 'checkin_admin_sp', 'Admin: CheckIn', 'Traveler Check In', 'Hola administrador,\n\n{traveler_name} se registra en {list_title}.\n\nNombre de invitado: {traveler_name}\nCorreo electrÃ³nico de invitado: {traveler_email}\nNombre de host: {host_name}\nCorreo de host: {host_email}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\n\n--\nGracias y saludos,\n\nAdministraciÃ³n\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} se registra en {list_title}.<br /><br />\nNombre de invitado: {traveler_name}.<br /><br />\nCorreo electrÃ³nico de invitado: {traveler_email}.<br /><br />\nNombre de host: {host_name}.<br /><br />\nCorreo de host: {host_email}.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nPrecio : {currency}{price}.<br /><br />\nFecha de llegada: {checkin}.<br /><br />\nFecha de pago: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(438, 'checkin_traveller_sp', 'Traveler: Check In', 'Su Check In', 'Hola {traveler_name},\n\nGracias, EstÃ¡s registrando {list_title}.\n\nNombre de host: {host_name}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\n\n--\nGracias y saludos,\n\nAdministraciÃ³n\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nGracias, EstÃ¡s registrando {list_title}.<br /><br />\nNombre de host: {host_name}.<br /><br />\nNombre de la lista:  {list_title}.<br /><br />\nPrecio : {currency}{price}.<br /><br />\nFecha de llegada: {checkin}.<br /><br />\nFecha de pago: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(439, 'refund_admin_sp', 'Admin: Refund', 'Reembolso del administrador', 'Hola administrador,\n\nHa devuelto el importe de {refund_amt} a la cuenta {name} {account}.\n\nNombre de invitado: {traveler_name}\nCorreo electrÃ³nico de invitado: {traveler_email}\nNombre de host: {host_name}\nCorreo de host: {host_email}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\nCantidad reembolsada: {refund_amt}\n\n-\nGracias y saludos,\n\nadministrador,\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td>\n<br />\nHa devuelto el importe de {refund_amt} a la cuenta {name} {account}.<br /><br />\nNombre de invitado: {traveler_name}.<br /><br />\nCorreo electrÃ³nico de invitado: {traveler_email}.<br /><br />\nNombre de host: {host_name}.<br /><br />\nCorreo de host: {host_email}.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nPrecio : {currency}{price}.<br /><br />\nFecha de llegada: {checkin}.<br /><br />\nFecha de pago: {checkout}.<br /><br />\nCantidad reembolsada: {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(440, 'refund_host_sp', 'Host: Refund', 'Reembolso del administrador', 'Hola {host_name},\n\nAdmin devolviÃ³ el importe de {refund_amt} a su cuenta de {account}.\n\nNombre de invitado: {traveler_name}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\nCantidad reembolsada: {refund_amt}\n\n--\nGracias y saludos,\n\nAdministraciÃ³n\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nAdmin devolviÃ³ el importe de {refund_amt} a su cuenta de {account}.<br /><br />\nNombre de invitado: {traveler_name}.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nPrecio : {currency}{price}.<br /><br />\nFecha de llegada: {checkin}.<br /><br />\nFecha de pago: {checkout}.<br /><br />\nCantidad reembolsada:  {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(441, 'refund_guest_sp', 'Traveler: Refund', 'Reembolso del administrador', 'Hola {traveler_name},\n\nAdmin devolviÃ³ el importe de {refund_amt} a su cuenta de {account}.\n\nNombre de host: {host_name}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\nCantidad reembolsada: {refund_amt}\n\n--\nGracias y saludos,\n\nAdministraciÃ³n\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nAdmin devolviÃ³ el importe de {refund_amt} a su cuenta de {account}.<br /><br />\nNombre de host: {host_name}.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nPrecio : {currency}{price}.<br /><br />\nFecha de llegada:  {checkin}.<br /><br />\nFecha de pago: {checkout}.<br /><br />\nCantidad reembolsada: {refund_amt}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(442, 'list_create_host_sp', 'List creation to Host', 'Ha creado la nueva lista', 'Hola {host_name},\n\nHa creado la nueva lista.\n\nNombre de la lista: {list_title}\n\nEnlace: {link}\n\nPrecio: {price}\n\n\n--\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nHa creado la nueva lista.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nEnlace : {link}.<br /><br />\nPrecio : {price}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(443, 'list_create_admin_sp', 'List creation to Admin', '{host_name} han creado la nueva lista', 'Hola administrador,\n\n{host_name} han creado la nueva lista.\n\nNombre de host: {host_name}\n\nNombre de la lista: {list_title}\n\nEnlace: {link}\n\nPrecio: {price}\n\n\n--\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td>\n<br />\n{host_name} han creado la nueva lista.<br /><br />\nNombre de host: {host_name}.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nEnlace : {link}.<br /><br />\nPrecio : {price}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(444, 'list_delete_host_sp', 'List deletion to Host', 'Has eliminado la lista', 'Hola {host_name},\n\nHa eliminado la lista.\n\nNombre de la lista: {list_title}\n\n--\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nHa eliminado la lista.<br /><br />\nNombre de la lista: {list_title}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(445, 'list_delete_admin_sp', 'List deletion to Admin', '{host_name} Han borrado la lista', 'Hola administrador,\n\n{host_name} Han borrado la lista\n\nNombre de host : {host_name}\n\nLista de nombres: {list_title}\n\n\n--\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td>\n<br />\n{host_name} Han borrado la lista<br /><br />\nNombre de host : {host_name}.<br /><br />\nLista de nombres: {list_title}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(446, 'payment_issue_to_admin_sp', 'Payment issue mail to Admin', 'Pago mal configurado', 'Hola administrador,\n\n{payment_type} Las credenciales de la API estÃ¡n mal configuradas.\n\n{username} intentÃ³ realizar el pago.\n\nID de correo electrÃ³nico: {email_id}\n\n--\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td>\n<p>{payment_type} Las credenciales de la API estÃ¡n mal configuradas.</p>\n<p>{username} intentÃ³ realizar el pago.</p>\n<p>ID de correo electrÃ³nico: {email_id}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(447, 'refund_host_commission_sp', 'Host: Accept Commission Refund', 'Reembolso del anfitriÃ³n Acepte la comisiÃ³n del Admin', 'Hola {host_name},\n\nAdmin devolviÃ³ el importe de {refund_amt} a su cuenta de {account}.\n\nNombre de la lista: {list_title}\nCantidad reembolsada: {refund_amt}\n\n--\nGracias y saludos,\n\nAdministraciÃ³n\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>Admin devolviÃ³ el importe de {refund_amt} a su cuenta de {account}.</p>\n<p>Nombre de la lista: {list_title}</p>\n<p>Cantidad reembolsada: {refund_amt}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>AdministraciÃ³n</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(448, 'refund_host_commission_admin_sp', 'Admin: Accept Commission Refund', 'Tiene reembolso de la ComisiÃ³n de aceptaciÃ³n del anfitriÃ³n', 'Hola administrador,\n\nHas devuelto el importe de {refund_amt} a tu cuenta de {account}.\n\nNombre de la lista: {list_title}\nNombre de host: {host_name}\nCantidad reembolsada: {refund_amt}\n\n--\nGracias y saludos,\n\nAdministraciÃ³n\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td>\n<p>Has devuelto el importe de {refund_amt} a tu cuenta de {account}.</p>\n<p>Nombre de la lista:  {list_title}</p>\n<p>Nombre de host: {host_name}</p>\n<p>Cantidad reembolsada: {refund_amt}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>AdministraciÃ³n</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(449, 'passport_verification_sp', 'Passport verification for administrator', '{user_name} enviÃ³ la solicitud de verificaciÃ³n de pasaporte, userid- {user_id}', 'Hola Administrador, \\ n \\ n {user_name} enviÃ³ la solicitud de verificaciÃ³n de pasaporte, id de usuario {user_id} \\ n \\ nDetalles como sigue, \\ n \\ n Nombre de usuario: {user_name} \\ nIdentificaciÃ³n de usuario: {user_id} \\ n \\ n \\ n \\ N - \\ nGracias y saludos, \\ n \\ n {user_name}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\\n<tbody>\\n<tr>\\n<td>Hola Administrador, </td>\\n</tr>\\n<tr>\\n<td>\\n<p>{user_name} enviÃ³ la solicitud de verificaciÃ³n de pasaporte, id de usuario {user_id}.</p>\\n<p>Detalles como sigue,</p>\\n<p>User Name : {user_name}</p>\\n<p>User Id : {user_id}</p>\\n\\n</td>\\n</tr>\\n<tr>\\n<td>\\n<p style=\"margin: 0 10px 0 0;\">--</p>\\n<p style=\"margin: 0 0 10px 0;\">Gracias y saludos,</p>\\n<p style=\"margin: 0px;\">{user_name} </p>\\n</td>\\n</tr>\\n</tbody>\\n</table>'),
(450, 'change password to user_sp', 'admin change to the password', 'Bienvenido a {site_name}', 'ContraseÃ±a de usuario: {new_password} ContraseÃ±a de conformidad: {conform_password} Estimado miembro, ContraseÃ±a anterior: {new_password} Conform contraseÃ±a: {conform_password} - Gracias y Saludos, Admin {site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Querido miembro,</td>\n</tr>\n<tr>\n<td>\n<p>Â Ha cambiado correctamente su contraseÃ±a</p>\n<p>Por favor, utilice los detalles de la contraseÃ±a a continuaciÃ³n para iniciar sesiÃ³n.</p>\n<p>ContraseÃ±a anterior:{new_password}</p>\n<p><span>Nueva contraseÃ±a:{conform_password}</span></p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(451, 'spam_sp', 'Spam List', 'Admin Message - Listado como spam!', '**** MENSAJE DE ADMINISTRACIÃ“N ****\n\n\n\nEl siguiente listado ha sido reportado como Spam:\n\nURL para la lista de anuncios no deseados: {list_url}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>**** MENSAJE DE ADMINISTRACIÃ“N ****</td>\n</tr>\n<tr>\n<td>\n<p>Â FLEXIFLAT.COM Â¡INFORME DE SPAM!</p>\n<p>El siguiente listado ha sido reportado como Spam:<br /><br />{list_url}</p>\n</td>\n</tr>\n<tr>\n<td>Â </td>\n</tr>\n</tbody>\n</table>\n<p>Â </p>'),
(452, 'host_notify_review_sp', 'Host Notification for Review', '{guest_name} tiene una revisiÃ³n de {host_name}', 'Hola {guest_name},\n\n{host_name} ha aÃ±adido una opiniÃ³n sobre usted en {list_name} en {site_name}\n\nY Ã©l estÃ¡ esperando su revisiÃ³n.\nNombre de host: {host_name}\nNombre de la lista: {list_name}\n\n\n--\nGracias y saludos,\nAdministraciÃ³n\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {guest_name},</td>\n</tr>\n<tr>\n<td><br/ > {host_name} ha aÃ±adido una opiniÃ³n sobre usted en {list_name} en {site_name} <br />Y Ã©l estÃ¡ esperando su revisiÃ³n.\n<br/>\nNombre de host:  {host_name}<br/>\nNombre de la lista: {list_name}\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} equipo</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(453, 'guest_notify_review_sp', 'Guest Notification for Review', '{guest_name} ha salido.', 'Hola {host_name},\n\n{guest_name} ha salido de su lista {list_name} en {site_name}\n\nY Ã©l estÃ¡ esperando su revisiÃ³n.\n\n--\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_name},</td>\n</tr>\n<tr>\n<td><br /> {guest_name} ha salido de su lista {list_name} en {site_name}<br />Y Ã©l estÃ¡ esperando su revisiÃ³n.</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} equipo</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(454, 'list_notification_sp', 'Notification for calendar update', 'La lista {list_name} necesita una actualizaciÃ³n', 'Hola {host_name},\n\nEl {list_name} que has incluido en {site_name} no tiene actualizaciones el mes pasado.\n\nPara mejorar tu clasificaciÃ³n en los resultados de la bÃºsqueda, actualiza tu lista\n--\nGracias y saludos,\n\nAdministraciÃ³n\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>\nEl {list_name} que has incluido en {site_name} no tiene actualizaciones el mes pasado.</p>\n<p>\nPara mejorar tu clasificaciÃ³n en los resultados de la bÃºsqueda, actualiza tu lista\n</p>\n\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>AdministraciÃ³n</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>\n'),
(455, 'host_notification_sp', 'Upcoming notification for host', 'El {traveler_name} tiene una reserva en tu {list_title} de maÃ±ana', 'Hola {host_name},\n\n{traveler_name} reservÃ³ el lugar {list_title} maÃ±ana.\n\nDetalles como sigue,\n\nNombre del viajero: {traveler_name}\nId. De correo electrÃ³nico de contacto: {traveler_email_id}\nNombre del lugar: {list_title}\nLlegada: {checkin}\nEcha un vistazo a: {checkout}\n\n\n--\nGracias y saludos,\n\nAdministraciÃ³n\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {host_name},</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} reservÃ³ el lugar {list_title} maÃ±ana.</p>\n<p>Detalles como sigue,</p>\n\n<p>Nombre del viajero:  {traveler_name}</p>\n<p>Id. De correo electrÃ³nico de contacto: {traveler_email_id}</p>\n<p>Nombre del lugar: {list_title}</p>\n<p>Llegada {checkin}</p>\n<p>Echa un vistazo a: {checkout}</p>\n\n\n</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>AdministraciÃ³n</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>\n'),
(456, 'user_inactive_days', 'User Inactive Days', 'Its been a while...We\'ve Missed You!', 'Notification for inactive user,\n\nHi { user_name },\n\nWe noticed that you haven\'t visited your account these days.\nIs everything okay? Have you noticed anything different about us? We have been working hard to make some positive changes. \nSummary of your Account:\n\nNumber of lists you have created\n\nNumber of your lists has been reserved by others\nMake your business more successful with great communication.\n\n{click_here} to have a look your profile.\n\n--\nThanks and Regards,\nAdmin\n{site_name} ', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Dear {user_name},</td>\n</tr>\n<tr>\n<td>\n <p>We noticed that you haven\'t visited your account these ( {days} ) days.</p>\n<p>Is everything okay? Have you noticed anything different about us? We have been working hard to make some positive changes.</p>\n<p>Make your business more successful with great communication.</p>\n</br>\n<p>Summary of your Account:</p>\n<p>Number of lists you have created - {num_list}</p>\n<p>Number of your lists has been reserved by others - {num_reserved_list}</p>\n</br>\n\n<p> <b>{click_here}</b> to have a look your profile</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(457, 'user_inactive_months', 'User Inactive Months', 'Its been a while...We\'ve Missed You!', 'Notification for inactive user,\n\nHi { user_name },\n\nWe noticed that you haven\'t visited your account these months.\nIs everything okay? Have you noticed anything different about us? We have been working hard to make some positive changes. \nSummary of your Account:\n\nNumber of lists you have created\n\nNumber of your lists has been reserved by others\nMake your business more successful with great communication.\n\n{click_here} to have a look your profile.\n\n--\nThanks and Regards,\nAdmin\n{site_name} ', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Dear {user_name},</td>\n</tr>\n<tr>\n<td>\n <p>We noticed that you haven\'t visited your account these ( {months} ) months.</p>\n<p>Is everything okay? Have you noticed anything different about us? We have been working hard to make some positive changes.</p>\n<p>Make your business more successful with great communication.</p>\n</br>\n<p>Summary of your Account:</p>\n<p>Number of lists you have created - {num_list}</p>\n<p>Number of your lists has been reserved by others - {num_reserved_list}</p>\n</br>\n\n<p> <b>{click_here}</b> to have a look your profile</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(458, 'user_inactive_year', 'User Inactive Year', 'Its been a while...We\'ve Missed You!', 'Notification for inactive user,\n\nHi { user_name },\n\nWe noticed that you haven\'t visited your account these year.\nIs everything okay? Have you noticed anything different about us? We have been working hard to make some positive changes. \nSummary of your Account:\n\nNumber of lists you have created\n\nNumber of your lists has been reserved by others\nMake your business more successful with great communication.\n\n{click_here} to have a look your profile.\n\n--\nThanks and Regards,\nAdmin\n{site_name} ', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Dear {user_name},</td>\n</tr>\n<tr>\n<td>\n <p>We noticed that you haven\'t visited your account these ( {year} ) year.</p>\n<p>Is everything okay? Have you noticed anything different about us? We have been working hard to make some positive changes.</p>\n<p>Make your business more successful with great communication.</p>\n</br>\n<p>Summary of your Account:</p>\n<p>Number of lists you have created - {num_list}</p>\n<p>Number of your lists has been reserved by others - {num_reserved_list}</p>\n</br>\n\n<p> <b>{click_here}</b> to have a look your profile</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(459, 'auto_checkin_host', 'Host: Auto Check In', 'Traveler Auto Check In', 'Hi {host_name},\n\n{traveler_name} is checkin to {list_title}.\n\nGuest Name : {guest_name}\nList Name : {list_title}\nPrice : {currency}{price}\nCheckin Date : {checkin}\nCheckout Date : {checkout}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} is checkin to  {list_title}  .<br /><br />\nGuest Name : {traveler_name}.<br /><br />\nList Name : {list_title}.<br /><br />\nPrice : {currency}{price}.<br /><br />\nCheckin Date : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(460, 'auto_checkin_admin', 'Admin: Auto CheckIn', 'Traveler Auto Check In', 'Hi Admin,\n\n{traveler_name} is checkin to {list_title}.\n\nGuest Name : {traveler_name}\nGuest Email : {traveler_email}\nHost Name : {host_name}\nHost Email : {host_email}\nList Name : {list_title}\nPrice : {currency}{price}\nCheckin Date : {checkin}\nCheckout Date : {checkout}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} is checkin to {list_title}.<br /><br />\nGuest Name : {traveler_name}.<br /><br />\nGuest Email : {traveler_email}.<br /><br />\nHost Name : {host_name}.<br /><br />\nHost Email : {host_email}.<br /><br />\nList Name : {list_title}.<br /><br />\nPrice : {currency}{price}.<br /><br />\nCheckin Date : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(461, 'auto_checkin_traveller', 'Traveler: Auto Check In', 'Your Auto Check In', 'Hi {traveler_name},\n\nThank you,You are checkin to {list_title}.\n\nHost Name : {host_name}\nList Name : {list_title}\nPrice : {currency}{price}\nCheckin Date : {checkin}\nCheckout Date : {checkout}\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nThank you,You are sucessfully checkin to {list_title}.<br /><br />\nHost Name : {host_name}.<br /><br />\nList Name : {list_title}.<br /><br />\nPrice : {currency}{price}.<br /><br />\nCheckin Date : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(462, 'auto_checkout_admin', 'Admin: Auto Check Out', 'Traveler Auto Check Out', 'Hi Admin,\n\n{traveler_name} is checkouted from {list_title}.\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} is checkouted form {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Team</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(463, 'auto_checkout_host', 'Host: Auto Check Out', 'Traveler Auto Check Out', 'Hi {host_name},\n\n{traveler_name} is check out from {list_title} .\n\nWrite your review and feedback\nLink:{host_review}\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} is checkout from  {list_title}  .</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>Write your review and feedback</p> </br></br><p>Link:{host_review} </p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(464, 'auto_checkout_traveler', 'Traveler: Auto Check Out', 'Your Auto Check Out', 'Hi {traveler_name},\n\nThank you,You are check-outed from {list_title}.\n\nWrite your review and feedback\nLink:{review}\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Thank you,You are sucessfully checkouted from {list_title}.</p>\n<p> </p>\n</td>\n</tr>\n<tr>\n<td>\n<p>Write your review and feedback</p> </br></br> <p>Link:{review} </p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(467, 'referral_invite_fr', 'Refferal Invitation', '{username} vous invite Ã  {site_name}', 'Bonjour,\n\n{username} veut que vous Ã©conomisiez de l\'argent avec {site_name}\n\n{dynamic_content}\n\n{click_here}\n\n--\nMerci et salutations,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour Utilisateur, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {username} veut que vous Ã©conomisiez de l\'argent avec {site_name} </ p>\n<P> {dynamic_content} </ p>\n<P> {click_here} Pour commencer maintenant! </ P>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(466, 'referral_invite', 'Refferal Invitation', '{username} has invited you to {site_name}', 'Hi user,\n\n{username} wants you to save money with {site_name}\n\n{dynamic_content}\n\n{click_here}\n\n--\nThanks and Regards,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi User,</td>\n</tr>\n<tr>\n<td>\n<p>{username} wants you to save money with {site_name}</p>\n<p>{dynamic_content}</p>\n<p>{click_here} To Started Now!</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(468, 'referral_invite_gr', 'Refferal Invitation', '{username} hat Sie zu {site_name} eingeladen', 'Hallo Benutzer,\n\n{username} mÃ¶chte, dass Sie mit {site_name}\n\n{dynamic_content}\n\n{click_here}\n\n--\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Hallo Benutzer, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {username} mÃ¶chte, dass Sie mit {site_name} </ p> Geld sparen\n<P> {dynamic_content} </ p>\n<P> {click_here} Jetzt gestartet! </ P>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P>--</ p>\n<P> Danke und GrÃ¼ÃŸe, </ p>\n<P> Administrator </ p>\n<P> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(469, 'referral_invite_it', 'Refferal Invitation', '{username} ti ha invitato a {site_name}', 'Ciao utente,\n\n{username} vi vuole risparmiare denaro con {site_name}\n\n{dynamic_content}\n\n{click_here}\n\n--\nGrazie e saluti,\nAdmin\n{site_name}', '<Table cellspacing = cellpadding \"10\" = \"0\">\n<Tbody>\n<Tr>\n<Td> Ciao utente, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {username} vi vuole risparmiare denaro con {site_name} </ p>\n<P> {dynamic_content} </ p>\n<P> {click_here} Per Iniziare Ora! </ P>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> -- </ p>\n<P> Grazie e saluti, </ p>\n<P> Amministrazione </ p>\n<P> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(470, 'referral_invite_po', 'Refferal Invitation', '{username} convidou vocÃª para {site_name}', 'Oi pessoal,\n\n{username} deseja que vocÃª economize dinheiro com {site_name}\n\n{dynamic_content}\n\n{click_here}\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> OlÃ¡ UsuÃ¡rio, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {username} deseja que vocÃª economize dinheiro com {site_name} </ p>\n<P> {dynamic_content} </ p>\n<P> {click_here} Para comeÃ§ar agora! </ P>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Obrigado e cumprimentos, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(471, 'referral_invite_sp', 'Refferal Invitation', '{username} te ha invitado a {site_name}', 'Hola usuario,\n\n{username} quiere que ahorres dinero con {site_name}\n\n{dynamic_content}\n\n{click_here}\n\n--\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Hola Usuario, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {username} quiere ahorrar dinero con {site_name} </ p>\n<P> {dynamic_content} </ p>\n<P> {click_here} Para empezar ahora! </ P>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Gracias y Saludos, </ p>\n<P> Admin </ p>\n<P> {nombre_sitio} </ p>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(472, 'user_inactive_days_fr', 'Inactive user days', 'Son Ã©tÃ© un moment ... Nous vous avons manquÃ©!', 'Notification pour utilisateur inactif,\n\nBonjour {user_name},\n\nNous avons remarquÃ© que vous n\'avez pas visitÃ© votre compte de nos jours.\nTout va bien? Avez-vous remarquÃ© quelque chose de diffÃ©rent chez nous? Nous avons travaillÃ© dur pour apporter des changements positifs.\nRÃ©sumÃ© de votre compte:\n\nNombre de listes que vous avez crÃ©Ã©es\n\nLe nombre de vos listes a Ã©tÃ© rÃ©servÃ© par d\'autres personnes\nFaites de votre entreprise plus de succÃ¨s avec une grande communication.\n\n{click_here} pour voir votre profil.\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Cher {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n <P> Nous avons remarquÃ© que vous n\'avez pas visitÃ© votre compte ces ({days}) jours. </ P>\n<P> Est-ce que tout va bien? Avez-vous remarquÃ© quelque chose de diffÃ©rent chez nous? Nous avons travaillÃ© dur pour faire quelques changements positifs. </ P>\n<P> Faites de votre entreprise plus de succÃ¨s avec une grande communication. </ P>\n</ Br>\n<P> RÃ©sumÃ© de votre compte: </ p>\n<P> Nombre de listes que vous avez crÃ©Ã©es - {num_list} </ p>\n<P> Le nombre de vos listes a Ã©tÃ© rÃ©servÃ© par d\'autres - {num_reserved_list} </ p>\n</ Br>\n<P> <b> {click_here} </ b> pour consulter votre profil </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(473, 'user_inactive_days_gr', 'Inactive user days', 'Es war eine Weile ... Wir haben Sie vermisst!', 'Lieber {user_name},\n\nWir haben festgestellt, dass Sie Ihr Konto in diesen Tagen nicht besucht haben.\nIst alles in Ordnung? Haben Sie etwas anderes von uns bemerkt? Wir haben hart gearbeitet, um einige positive VerÃ¤nderungen zu machen.\nZusammenfassung Ihres Kontos:\n\nAnzahl der von Ihnen erstellten Listen\n\nDie Nummer Ihrer Listen wurde von anderen reserviert\nMachen Sie Ihr Unternehmen erfolgreicher mit groÃŸer Kommunikation.\n\n{click_here}, um sich Ihr Profil anzusehen.\n\n-\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Lieber {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n <P> Wir haben festgestellt, dass Sie Ihr Konto ({days}) Tage nicht besucht haben. </ P>\n<P> Ist alles in Ordnung? Haben Sie etwas anderes von uns bemerkt? Wir haben hart gearbeitet, um einige positive VerÃ¤nderungen zu machen. </ P>\n<P> Machen Sie Ihr Unternehmen erfolgreicher mit groÃŸer Kommunikation. </ P>\n</ Br>\n<P> Zusammenfassung Ihres Kontos: </ p>\n<P> Anzahl der von Ihnen erstellten Listen - {num_list} </ p>\n<P> Die Nummer Ihrer Listen wurde von anderen - {num_reserved_list} </ p> reserviert\n</ Br>\n<P> <b> {click_here} </ b> um Ihr Profil anzusehen </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Danke und GrÃ¼ÃŸe, </ p>\n<P> Administrator </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(474, 'user_inactive_days_it', 'Inactive user days', 'Il suo stato un po \'... Ci sei mancato!', 'La notifica per l\'utente inattivo,\n\nCiao {user_name},\n\nAbbiamo notato che non avete visitato il tuo account in questi giorni.\nVa tutto bene? Avete notato qualcosa di diverso su di noi? Abbiamo lavorato duramente per rendere alcuni cambiamenti positivi.\nSintesi del tuo account:\n\nNumero di liste create\n\nNumero degli elenchi Ã¨ stato riservato da altri\nRendere il vostro business piÃ¹ successo con grande comunicazione.\n\n{click_here} per avere uno sguardo tuo profilo.\n\n--\nGrazie e saluti,\nAdmin\n{site_name}', '<Table cellspacing = cellpadding \"10\" = \"0\">\n<Tbody>\n<Tr>\n<Td> Caro {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n <P> Abbiamo notato che non avete visitato il vostro conto di queste ({days}) giorni. </ P>\n<P> Va tutto bene? Avete notato qualcosa di diverso su di noi? Abbiamo lavorato duramente per rendere alcuni cambiamenti positivi. </ P>\n<P> rendere il business piÃ¹ successo con la grande comunicazione. </ P>\n</ Br>\n<P> Sintesi del suo account: </ p>\n<P> Numero di liste di aver creato - {num_list} </ p>\n<P> Numero vostre liste Ã¨ stato riservato da altri - {num_reserved_list} </ p>\n</ Br>\n<P> <b> {click_here} </ b> per avere uno sguardo tuo profilo </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Grazie e saluti, </ p>\n<P> Amministrazione </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(475, 'user_inactive_days_po', 'Inactive user days', 'Tem sido um tempo ... We\'ve Missed You!', 'NotificaÃ§Ã£o para usuÃ¡rio inativo,\n\nOlÃ¡ {user_name},\n\nPercebemos que vocÃª nÃ£o visitou sua conta nos dias de hoje.\nEstÃ¡ tudo bem? VocÃª notou algo diferente sobre nÃ³s? Temos vindo a trabalhar arduamente para fazer algumas mudanÃ§as positivas.\nResumo de sua conta:\n\nNÃºmero de listas criadas\n\nO nÃºmero de suas listas foi reservado por outros\nFaÃ§a o seu negÃ³cio mais bem sucedido com grande comunicaÃ§Ã£o.\n\n{click_here} para ver o seu perfil.\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table cellspacing = \"10\" cellpadding = \"0\">\n<tbody>\n<tr>\n<td> Prezado {user_name}, </ td>\n</tr>\n<tr>\n<td>\n <p> Percebemos que vocÃª nÃ£o visitou sua conta nestes ({days}) dias. </ P>\n<p> EstÃ¡ tudo bem? VocÃª notou algo diferente sobre nÃ³s? Temos vindo a trabalhar arduamente para fazer algumas mudanÃ§as positivas. </ P>\n<p> FaÃ§a o seu negÃ³cio mais bem sucedido com grande comunicaÃ§Ã£o. </ P>\n</br>\n<p> Resumo da sua conta: </ p>\n<p> NÃºmero de listas que criou - {num_list} </ p>\n<p> O nÃºmero de suas listas foi reservado por outros - {num_reserved_list} </ p>\n</br>\n<p> <b> {click_here} </ b> para ver o seu perfil </ p>\n</td>\n</tr>\n<tr>\n<td>\n<p> - </p>\n<p> Obrigado e cumprimentos, </p>\n<p> Admin </p>\n<p> {site_name} </p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(476, 'user_inactive_days_sp', 'Inactive user days', 'Ha pasado un tiempo ... Â¡Te hemos faltado!', 'NotificaciÃ³n para usuarios inactivos,\n\nHola {user_name},\n\nNos dimos cuenta de que no has visitado tu cuenta en estos dÃ­as.\nÂ¿EstÃ¡ todo bien? Â¿Has notado algo diferente acerca de nosotros? Hemos estado trabajando duro para hacer algunos cambios positivos.\nResumen de su cuenta:\n\nNÃºmero de listas que ha creado\n\nEl nÃºmero de sus listas ha sido reservado por otros\nHaga su negocio mÃ¡s acertado con gran comunicaciÃ³n.\n\n{click_here} para echar un vistazo a su perfil.\n\n-\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Estimado {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n <P> Hemos notado que no has visitado tu cuenta en estos dÃ­as ({dÃ­as}). </ P>\n<P> Â¿EstÃ¡ todo bien? Â¿Has notado algo diferente acerca de nosotros? Hemos estado trabajando duro para hacer algunos cambios positivos. </ P>\n<P> Haga que su empresa tenga mÃ¡s Ã©xito con una gran comunicaciÃ³n. </ P>\n</ Br>\n<P> Resumen de su cuenta: </ p>\n<P> NÃºmero de listas que ha creado - {num_list} </ p>\n<P> El nÃºmero de sus listas ha sido reservado por otros - {num_reserved_list} </ p>\n</ Br>\n<P> <b> {click_here} </ b> para echar un vistazo a tu perfil </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Gracias y Saludos, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>');
INSERT INTO `email_templates` (`id`, `type`, `title`, `mail_subject`, `email_body_text`, `email_body_html`) VALUES
(477, 'user_inactive_months_fr', 'User Inactive Months', 'Son Ã©tÃ© un moment ... Nous vous avons manquÃ©!', 'Notification pour utilisateur inactif,\n\nBonjour {user_name},\n\nNous avons remarquÃ© que vous n\'avez pas visitÃ© votre compte ces derniers mois.\nTout va bien? Avez-vous remarquÃ© quelque chose de diffÃ©rent chez nous? Nous avons travaillÃ© dur pour apporter des changements positifs.\nRÃ©sumÃ© de votre compte:\n\nNombre de listes que vous avez crÃ©Ã©es\n\nLe nombre de vos listes a Ã©tÃ© rÃ©servÃ© par d\'autres personnes\nFaites de votre entreprise plus de succÃ¨s avec une grande communication.\n\n{click_here} pour voir votre profil.\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Cher {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n <P> Nous avons remarquÃ© que vous n\'avez pas visitÃ© votre compte ces ({months}) mois. </ P>\n<P> Est-ce que tout va bien? Avez-vous remarquÃ© quelque chose de diffÃ©rent chez nous? Nous avons travaillÃ© dur pour faire quelques changements positifs. </ P>\n<P> Faites de votre entreprise plus de succÃ¨s avec une grande communication. </ P>\n</ Br>\n<P> RÃ©sumÃ© de votre compte: </ p>\n<P> Nombre de listes que vous avez crÃ©Ã©es - {num_list} </ p>\n<P> Le nombre de vos listes a Ã©tÃ© rÃ©servÃ© par d\'autres - {num_reserved_list} </ p>\n</ Br>\n\n<P> <b> {click_here} </ b> pour consulter votre profil </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(478, 'user_inactive_months_gr', 'User Inactive Months', 'Es war eine Weile ... Wir haben Sie vermisst!', 'Lieber {user_name},\n\nWir haben festgestellt, dass Sie Ihr Konto in diesen Monaten nicht besucht haben.\nIst alles in Ordnung? Haben Sie etwas anderes von uns bemerkt? Wir haben hart gearbeitet, um einige positive VerÃ¤nderungen zu machen.\nZusammenfassung Ihres Kontos:\n\nAnzahl der von Ihnen erstellten Listen\n\nDie Nummer Ihrer Listen wurde von anderen reserviert\nMachen Sie Ihr Unternehmen erfolgreicher mit groÃŸer Kommunikation.\n\n{click_here}, um sich Ihr Profil anzusehen.\n\n-\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Lieber {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n <P> Wir haben festgestellt, dass Sie diese Monate {(months)} nicht besucht haben. </ P>\n<P> Ist alles in Ordnung? Haben Sie etwas anderes von uns bemerkt? Wir haben hart gearbeitet, um einige positive VerÃ¤nderungen zu machen. </ P>\n<P> Machen Sie Ihr Unternehmen erfolgreicher mit groÃŸer Kommunikation. </ P>\n</ Br>\n<P> Zusammenfassung Ihres Kontos: </ p>\n<P> Anzahl der von Ihnen erstellten Listen - {num_list} </ p>\n<P> Die Nummer Ihrer Listen wurde von anderen - {num_reserved_list} </ p> reserviert\n</ Br>\n\n<P> <b> {click_here} </ b> um Ihr Profil anzusehen </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Danke und GrÃ¼ÃŸe, </ p>\n<P> Administrator </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(479, 'user_inactive_months_it', 'User Inactive Months', 'Il suo stato un po \'... Ci sei mancato!', 'La notifica per l\'utente inattivo,\n\nCiao {user_name},\n\nAbbiamo notato che non avete visitato il tuo account di questi mesi.\nVa tutto bene? Avete notato qualcosa di diverso su di noi? Abbiamo lavorato duramente per rendere alcuni cambiamenti positivi.\nSintesi del tuo account:\n\nNumero di liste create\n\nNumero degli elenchi Ã¨ stato riservato da altri\nRendere il vostro business piÃ¹ successo con grande comunicazione.\n\n{click_here} per avere uno sguardo tuo profilo.\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<Table cellspacing = cellpadding \"10\" = \"0\">\n<Tbody>\n<Tr>\n<Td> Caro {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n <P> Abbiamo notato che non avete visitato il vostro conto di queste ({months}) mesi. </ P>\n<P> Va tutto bene? Avete notato qualcosa di diverso su di noi? Abbiamo lavorato duramente per rendere alcuni cambiamenti positivi. </ P>\n<P> rendere il business piÃ¹ successo con la grande comunicazione. </ P>\n</ Br>\n<P> Sintesi del suo account: </ p>\n<P> Numero di liste di aver creato - {num_list} </ p>\n<P> Numero vostre liste Ã¨ stato riservato da altri - {num_reserved_list} </ p>\n</ Br>\n\n<P> <b> {click_here} </ b> per avere uno sguardo tuo profilo </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Grazie e saluti, </ p>\n<P> Amministrazione </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(480, 'user_inactive_months_po', 'User Inactive Months', 'Tem sido um tempo ... We\'ve Missed You!', 'NotificaÃ§Ã£o para usuÃ¡rio inativo,\n\nOlÃ¡ {user_name},\n\nPercebemos que vocÃª nÃ£o visitou sua conta nesses meses.\nEstÃ¡ tudo bem? VocÃª notou algo diferente sobre nÃ³s? Temos vindo a trabalhar arduamente para fazer algumas mudanÃ§as positivas.\nResumo de sua conta:\n\nNÃºmero de listas criadas\n\nO nÃºmero de suas listas foi reservado por outros\nFaÃ§a o seu negÃ³cio mais bem sucedido com grande comunicaÃ§Ã£o.\n\n{click_here} para ver o seu perfil.\n\n-\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Prezado {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n <P> Percebemos que vocÃª nÃ£o visitou sua conta nesses ({months}) meses. </ P>\n<P> EstÃ¡ tudo bem? VocÃª notou algo diferente sobre nÃ³s? Temos vindo a trabalhar arduamente para fazer algumas mudanÃ§as positivas. </ P>\n<P> FaÃ§a o seu negÃ³cio mais bem sucedido com grande comunicaÃ§Ã£o. </ P>\n</ Br>\n<P> Resumo da sua conta: </ p>\n<P> NÃºmero de listas que criou - {num_list} </ p>\n<P> O nÃºmero de suas listas foi reservado por outros - {num_reserved_list} </ p>\n</ Br>\n\n<P> <b> {click_here} </ b> para ver o seu perfil </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Obrigado e cumprimentos, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(481, 'user_inactive_months_sp', 'User Inactive Months', 'Ha pasado un tiempo ... Â¡Te hemos faltado!', 'NotificaciÃ³n para usuarios inactivos,\n\nHola {user_name},\n\nNos hemos dado cuenta de que no has visitado tu cuenta en estos meses.\nÂ¿EstÃ¡ todo bien? Â¿Has notado algo diferente acerca de nosotros? Hemos estado trabajando duro para hacer algunos cambios positivos.\nResumen de su cuenta:\n\nNÃºmero de listas que ha creado\n\nEl nÃºmero de sus listas ha sido reservado por otros\nHaga su negocio mÃ¡s acertado con gran comunicaciÃ³n.\n\n{click_here} para echar un vistazo a su perfil.\n\n-\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Estimado {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n <P> Hemos observado que no has visitado tu cuenta estos ({months}) meses. </ P>\n<P> Â¿EstÃ¡ todo bien? Â¿Has notado algo diferente acerca de nosotros? Hemos estado trabajando duro para hacer algunos cambios positivos. </ P>\n<P> Haga que su empresa tenga mÃ¡s Ã©xito con una gran comunicaciÃ³n. </ P>\n</ Br>\n<P> Resumen de su cuenta: </ p>\n<P> NÃºmero de listas que ha creado - {num_list} </ p>\n<P> El nÃºmero de sus listas ha sido reservado por otros - {num_reserved_list} </ p>\n</ Br>\n\n<P> <b> {click_here} </ b> para echar un vistazo a tu perfil </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Gracias y Saludos, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(482, 'user_inactive_year_fr', 'User Inactive Year', 'Son Ã©tÃ© un moment ... Nous vous avons manquÃ©!', 'Notification pour utilisateur inactif,\n\nBonjour {user_name},\n\nNous avons remarquÃ© que vous n\'avez pas visitÃ© votre compte cette annÃ©e.\nTout va bien? Avez-vous remarquÃ© quelque chose de diffÃ©rent chez nous? Nous avons travaillÃ© dur pour apporter des changements positifs.\nRÃ©sumÃ© de votre compte:\n\nNombre de listes que vous avez crÃ©Ã©es\n\nLe nombre de vos listes a Ã©tÃ© rÃ©servÃ© par d\'autres personnes\nFaites de votre entreprise plus de succÃ¨s avec une grande communication.\n\n{Click_here} pour voir votre profil.\n\n-\nMerci et salutations,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Cher {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n  <P> Nous avons remarquÃ© que vous n\'avez pas visitÃ© votre compte ces ({year}) annÃ©e. </ P>\n<P> Est-ce que tout va bien? Avez-vous remarquÃ© quelque chose de diffÃ©rent chez nous? Nous avons travaillÃ© dur pour faire quelques changements positifs. </ P>\n<P> Faites de votre entreprise plus de succÃ¨s avec une grande communication. </ P>\n</ Br>\n<P> RÃ©sumÃ© de votre compte: </ p>\n<P> Nombre de listes que vous avez crÃ©Ã©es - {num_list} </ p>\n<P> Le nombre de vos listes a Ã©tÃ© rÃ©servÃ© par d\'autres - {num_reserved_list} </ p>\n</ Br>\n\n<P> <b> {click_here} </ b> pour consulter votre profil </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(483, 'user_inactive_year_gr', 'User Inactive Year', 'Es war eine Weile ... Wir haben Sie vermisst!', 'Benachrichtigung fÃ¼r inaktive Benutzer,\n\nLieber {user_name},\n\nWir haben festgestellt, dass Sie Ihr Konto in diesem Jahr nicht besucht haben.\nIst alles in Ordnung? Haben Sie etwas anderes von uns bemerkt? Wir haben hart gearbeitet, um einige positive VerÃ¤nderungen zu machen.\nZusammenfassung Ihres Kontos:\n\nAnzahl der von Ihnen erstellten Listen\n\nDie Nummer Ihrer Listen wurde von anderen reserviert\nMachen Sie Ihr Unternehmen erfolgreicher mit groÃŸer Kommunikation.\n\n{click_here}, um sich Ihr Profil anzusehen.\n\n-\nDanke und GrÃ¼ÃŸe,\nAdministrator\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Lieber {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n  <P> Wir haben festgestellt, dass Sie dieses ({year}) Jahr nicht besucht haben. </ P>\n<P> Ist alles in Ordnung? Haben Sie etwas anderes von uns bemerkt? Wir haben hart gearbeitet, um einige positive VerÃ¤nderungen zu machen. </ P>\n<P> Machen Sie Ihr Unternehmen erfolgreicher mit groÃŸer Kommunikation. </ P>\n</ Br>\n<P> Zusammenfassung Ihres Kontos: </ p>\n<P> Anzahl der von Ihnen erstellten Listen - {num_list} </ p>\n<P> Die Nummer Ihrer Listen wurde von anderen - {num_reserved_list} </ p> reserviert\n</ Br>\n\n<P> <b> {click_here} </ b> um Ihr Profil anzusehen </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Danke und GrÃ¼ÃŸe, </ p>\n<P> Administrator </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(484, 'user_inactive_year_it', 'User Inactive Year', 'Il suo stato un po \'... Ci sei mancato!', 'La notifica per l\'utente inattivo,\n\nCiao {user_name},\n\nAbbiamo notato che non avete visitato il tuo account questi anni.\nVa tutto bene? Avete notato qualcosa di diverso su di noi? Abbiamo lavorato duramente per rendere alcuni cambiamenti positivi.\nSintesi del tuo account:\n\nNumero di liste create\n\nNumero degli elenchi Ã¨ stato riservato da altri\nRendere il vostro business piÃ¹ successo con grande comunicazione.\n\n{click_here} per avere uno sguardo tuo profilo.\n\n-\nGrazie e saluti,\nAdmin\n{site_name}', '<Table cellspacing = cellpadding \"10\" = \"0\">\n<Tbody>\n<Tr>\n<Td> Caro {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n  <P> Abbiamo notato che non avete visitato il tuo account questi ({year}) anno. </ P>\n<P> Va tutto bene? Avete notato qualcosa di diverso su di noi? Abbiamo lavorato duramente per rendere alcuni cambiamenti positivi. </ P>\n<P> rendere il business piÃ¹ successo con la grande comunicazione. </ P>\n</ Br>\n<P> Sintesi del suo account: </ p>\n<P> Numero di liste di aver creato - {num_list} </ p>\n<P> Numero vostre liste Ã¨ stato riservato da altri - {num_reserved_list} </ p>\n</ Br>\n\n<P> <b> {click_here} </ b> per avere uno sguardo tuo profilo </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Grazie e saluti, </ p>\n<P> Amministrazione </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(485, 'user_inactive_year_po', 'User Inactive Year', 'NotificaÃ§Ã£o para usuÃ¡rio inativo,', 'NotificaÃ§Ã£o para usuÃ¡rio inativo,\n\nOlÃ¡ {user_name},\n\nPercebemos que vocÃª nÃ£o visitou sua conta este ano.\nEstÃ¡ tudo bem? VocÃª notou algo diferente sobre nÃ³s? Temos vindo a trabalhar arduamente para fazer algumas mudanÃ§as positivas.\nResumo de sua conta:\n\nNÃºmero de listas criadas\n\nO nÃºmero de suas listas foi reservado por outros\nFaÃ§a o seu negÃ³cio mais bem sucedido com grande comunicaÃ§Ã£o.\n\n{click_here} para ver o seu perfil.\n\n-\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Prezado {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n  <P> Percebemos que vocÃª nÃ£o visitou sua conta nestes ({year}) ano. </ P>\n<P> EstÃ¡ tudo bem? VocÃª notou algo diferente sobre nÃ³s? Temos vindo a trabalhar arduamente para fazer algumas mudanÃ§as positivas. </ P>\n<P> FaÃ§a o seu negÃ³cio mais bem sucedido com grande comunicaÃ§Ã£o. </ P>\n</ Br>\n<P> Resumo da sua conta: </ p>\n<P> NÃºmero de listas que criou - {num_list} </ p>\n<P> O nÃºmero de suas listas foi reservado por outros - {num_reserved_list} </ p>\n</ Br>\n\n<P> <b> {click_here} </ b> para ver o seu perfil </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Obrigado e cumprimentos, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(486, 'user_inactive_year_sp', 'User Inactive Year', 'Ha pasado un tiempo ... Â¡Te hemos faltado!', 'NotificaciÃ³n para usuarios inactivos,\n\nHola {user_name},\n\nHemos notado que no has visitado tu cuenta este aÃ±o.\nÂ¿EstÃ¡ todo bien? Â¿Has notado algo diferente acerca de nosotros? Hemos estado trabajando duro para hacer algunos cambios positivos.\nResumen de su cuenta:\n\nNÃºmero de listas que ha creado\n\nEl nÃºmero de sus listas ha sido reservado por otros\nHaga su negocio mÃ¡s acertado con gran comunicaciÃ³n.\n\n{click_here} para echar un vistazo a su perfil.\n\n-\nGracias y saludos,\nAdministraciÃ³n\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Estimado {user_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n  <P> Nos dimos cuenta de que no has visitado tu cuenta estos ({year}) aÃ±o. </ P>\n<P> Â¿EstÃ¡ todo bien? Â¿Has notado algo diferente acerca de nosotros? Hemos estado trabajando duro para hacer algunos cambios positivos. </ P>\n<P> Haga que su empresa tenga mÃ¡s Ã©xito con una gran comunicaciÃ³n. </ P>\n</ Br>\n<P> Resumen de su cuenta: </ p>\n<P> NÃºmero de listas que ha creado - {num_list} </ p>\n<P> El nÃºmero de sus listas ha sido reservado por otros - {num_reserved_list} </ p>\n</ Br>\n\n<P> <b> {click_here} </ b> para echar un vistazo a tu perfil </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Gracias y Saludos, </ p>\n<P> Admin </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(487, 'auto_checkin_traveller_fr', 'Traveler: Auto Check In', 'Votre arrivÃ©e', 'Salut {traveler_name},\n\nMerci, Vous Ãªtes en ligne Ã  {list_title}.\n\nNom d\'hÃ´te: {host_name}\nNom de la liste: {list_title}\nPrix: {currency} {price}\nDate d\'arrivÃ©e: {checkin}\nDate de paiement: {checkout}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {traveler_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<br />\nMerci, Vous avez rÃ©ussi le check-in Ã  {list_title}. <br /> <br />\nNom d\'hÃ´te: {host_name}. <br /> <br />\nNom de la liste: {list_title}. <br /> <br />\nPrix: {currency} {price}. <br /> <br />\nDate d\'arrivÃ©e: {checkin}. <br /> <br />\nDate de paiement: {checkout}.\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> -- </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(488, 'auto_checkin_traveller_gr', 'Traveler: Check In', 'Ihr Check In', 'Hallo {traveler_name},\n\nVielen Dank, Sie sind checkin to {list_title}.\n\nHostname : {host_name}\nListename : {list_title}\nPreis : {currency}{price}\nCheckin-Datum : {checkin}\nCheckout-Datum : {checkout}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nVielen Dank, Sie sind checkin to {list_title}.<br /><br />\nHostname : {host_name}.<br /><br />\nListename : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin-Datum : {checkin}.<br /><br />\nCheckout-Datum : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(489, 'auto_checkin_traveller_it', 'Traveler: Auto Check In', 'Il check-in', 'Ciao {traveler_name},\n\nGrazie, Stai checkin a {list_title}.\n\nNome Host: {host_name}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nGrazie, Stai checkin a {list_title}.<br /><br />\nNome Host  : {host_name}.<br /><br />\nNome elenco: {list_title}.<br /><br />\nPrezzo : {currency}{price}.<br /><br />\nCheckin Data : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(490, 'auto_checkin_traveller_po', 'Traveler: Auto Check In', 'Seu Check In', 'OlÃ¡ {traveler_name},\n\nObrigado, VocÃª estÃ¡ check-in para {list_title}.\n\nNome do host: {host_name}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OlÃ¡ {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nObrigado, VocÃª estÃ¡ check-in para {list_title}.<br /><br />\nNome do host: {host_name}.<br /><br />\nNome da lista: {list_title}.<br /><br />\nPreÃ§o : {currency}{price}.<br /><br />\nData de Checkin: {checkin}.<br /><br />\nData de SaÃ­da: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{site_name} Equipe</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(491, 'auto_checkin_traveller_sp', 'Traveler: Auto Check In', 'Su Check In', 'Hola {traveler_name},\n\nGracias, EstÃ¡s registrando {list_title}.\n\nNombre de host: {host_name}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\n\n--\nGracias y saludos,\n\nAdministraciÃ³n\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<br />\nGracias, EstÃ¡s registrando {list_title}.<br /><br />\nNombre de host: {host_name}.<br /><br />\nNombre de la lista:  {list_title}.<br /><br />\nPrecio : {currency}{price}.<br /><br />\nFecha de llegada: {checkin}.<br /><br />\nFecha de pago: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(492, 'auto_checkout_traveler_fr', 'Traveler: Auto Check Out', 'Votre DÃ©part', 'Salut {traveler_name},\n\nMerci, Vous Ãªtes checkouted Ã  partir de {list_title}.\n\n\n-\nMerci et salutations,\n\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {traveler_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> Je vous remercie, Vous avez rÃ©ussi checkouted Ã  partir de {list_title}. </ P>\n<P> </ p>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin, </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(493, 'auto_checkout_traveler_gr', 'Traveler: Atuo Check Out', 'Ihre Abreise', 'Hallo {traveler_name},\n\nVielen Dank, Sie werden von {list_title} Ã¼berprÃ¼ft.\n\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Vielen Dank, Sie werden von {list_title} Ã¼berprÃ¼ft.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(494, 'auto_checkout_traveler_it', 'Traveler: Auto Check Out', 'Il check-out', 'Ciao {traveler_name},\n\nGrazie, Stai checkouted da {list_title}.\n\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Grazie, Stai checkouted da {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(495, 'auto_checkout_traveler_po', 'Traveler: Auto Check out', 'Seu SaÃ­da', 'OlÃ¡ {traveler_name},\n\nObrigado, VocÃª estÃ¡ checkouted de {list_title}.\n\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Obrigado, VocÃª estÃ¡ checkouted de {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(496, 'auto_checkout_traveler_sp', 'Traveler: Auto Check Out', 'Su salida', 'Hola {traveler_name},\n\nGracias, Recibido de {list_title}.\n\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {traveler_name},</td>\n</tr>\n<tr>\n<td>\n<p>Gracias, Recibido de {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(497, 'auto_checkin_host_fr', 'Host: Auto Check In', 'ArrivÃ©e du voyageur', 'Hallo {host_name},\n\n{traveler_name} Est checkin Ã  {list_title}.\n\nNom de l\'invitÃ© : {guest_name}\nListe de noms : {list_title}\nPrix : {currency}{price}\nDate d\'arrivÃ©e : {checkin}\nDate de paiement : {checkout}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} Est checkin Ã Â Â {list_title} Â .<br /><br />\nNom de l\'invitÃ© : {traveler_name}.<br /><br />\nListe de noms : {list_title}.<br /><br />\nPrix : {currency}{price}.<br /><br />\nDate d\'arrivÃ©e : {checkin}.<br /><br />\nDate de paiement : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Merci et salutations,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(498, 'auto_checkin_host_gr', 'Host: Auto Check In', 'Anreise', 'Hallo {host_name},\n\n{traveler_name} Ist checkin zu {list_title}.\n\nBenutzername: {guest_name}\nListennamen : {list_title}\nPreis : {currency}{price}\nCheckin-Datum : {checkin}\nCheckout-Datum : {checkout}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} Ist checkin zuÂ {list_title} .<br /><br />\nBenutzername : {traveler_name}.<br /><br />\nListennamen : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin-Datum : {checkin}.<br /><br />\nCheckout-Datum : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(499, 'auto_checkin_host_it', 'Host: Auto Check In', 'Controllare viaggiatore nel', 'Ciao {host_name},\n\n{traveler_name} Ã¨ checkin a {list_title}.\n\nNome Cliente: {guest_name}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>CIao {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} Ã¨ checkin a {list_title}.<br /><br />\nNome Cliente:{traveler_name}.<br /><br />\nNome elenco:  {list_title}.<br /><br />\nPrezzo: {currency}{price}.<br /><br />\nCheckin Data: {checkin}.<br /><br />\nCheckout Date: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(500, 'auto_checkin_host_po', 'Host: Auto Check In', 'Check-in do viajante', 'OlÃ¡ {host_name},\n\n{traveler_name} estÃ¡ em checkin para {list_title}.\n\nNome do hÃ³spede: {guest_name}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} estÃ¡ em checkin para {list_title}.<br /><br />\nNome do hÃ³spede:  {traveler_name}.<br /><br />\nNome da lista: {list_title}.<br /><br />\nPreÃ§o : {currency}{price}.<br /><br />\nData de Checkin: {checkin}.<br /><br />\nData de SaÃ­da: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(501, 'auto_checkin_host_sp', 'Host: Auto Check In', 'Cheque de viajero', 'Hola {host_name},\n\n{traveler_name} se registra en {list_title}.\n\nNombre de invitado: {guest_name}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_name},</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} se registra en {list_title}.<br /><br />\nNombre de invitado: {traveler_name}.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nPrecio : {currency}{price}.<br /><br />\nFecha de llegada: {checkin}.<br /><br />\nFecha de pago: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(502, 'auto_checkout_host_fr', 'Host: Auto Check Out', 'DÃ©part pour le voyageur', 'Bonjour {host_name},\n\n{traveler_name} est lancÃ© Ã  partir de {list_title}.\n\n-\nMerci et salutations,\n\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Salut {host_name}, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {traveler_name} est lancÃ© Ã  partir de {list_title}. </ P>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> Admin, </ p>\n<P> {site_name} </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(503, 'auto_checkout_host_gr', 'Host: Auto Check Out', 'Reisescheck', 'Hallo {host_name},\n\n{traveler_name} Wird von {list_title} Ã¼berprÃ¼ft.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Wird von {list_title} Ã¼berprÃ¼ft.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>Administrator,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(504, 'auto_checkout_host_it', 'Host: Auto Check Out', 'Controllare Traveler Out', 'Ciao {host_name},\n\n{traveler_name} Ã¨ checkouted da {list_title}\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Ã¨ checkouted da {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(505, 'auto_checkout_host_po', 'Host: Auto Check Out', 'Traveler Check Out', 'OlÃ¡ {host_name},\n\n{traveler_name} Ã© checkouted de {list_title}.\n\n--\nObrigado e cumprimentos,\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ola {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Ã© checkouted de {list_title}..</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(506, 'auto_checkout_host_sp', 'Host: Auto Check Out', 'Salida del viajero', 'Hola {host_name},\n\n{traveler_name} se registra desde {list_title}.\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} se registra desde {list_title}..</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(507, 'auto_checkout_admin_fr', 'Admin: Auto Check Out', 'DÃ©part pour le voyageur', 'Salut Admin,\n\n{traveler_name} est lancÃ© Ã  partir de {list_title}.\n\n-\nMerci et salutations,\nAdmin\n{site_name}\n', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour administrateur, </ td>\n</ Tr>\n<Tr>\n<Td>\n<P> {traveler_name} est un formulaire checkouted {list_title}. </ P>\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> - </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(508, 'auto_checkout_admin_gr', 'Admin: Auto Check Out', 'Reisescheck', 'Hallo Administrator,\n\n{traveler_name} wird aus {list_title} Ã¼berprÃ¼ft.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} wird aus {list_title} Ã¼berprÃ¼ft.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(509, 'auto_checkout_admin_it', 'Admin: Auto Check Out', 'Controllare Traveler Out', 'Ciao Admin,\n\n{traveler_name} Ã¨ checkouted da {list_title}.\n\n-\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Ã¨ checkouted da {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(510, 'auto_checkout_admin_po', 'Admin: Auto Check Out', 'Traveler Check Out', 'OlÃ¡, Admin,\n\n{traveler_name} Ã© checkouted de {list_title}.\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OI Admin,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} Ã© checkouted de {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(511, 'auto_checkout_admin_sp', 'Admin: Auto Check Out', 'Salida del viajero', 'Hola administrador\n\n{traveler_name} se registra desde {list_title}.\n\n--\nGracias y saludos,\n\nAdmin\n{site_name}\n', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td>\n<p>{traveler_name} se registra desde {list_title}.</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(512, 'auto_checkin_admin_fr', 'Admin: Auto Check In', 'ArrivÃ©e du voyageur', 'Salut Admin,\n\n{traveler_name} est cochÃ© Ã  {list_title}.\n\nNom de l\'invitÃ©: {traveler_name}\nCourriel des invitÃ©s: {traveler_email}\nNom d\'hÃ´te: {host_name}\nCourriel de l\'hÃ´te: {host_email}\nNom de la liste: {list_title}\nPrix: {currency} {price}\nDate d\'arrivÃ©e: {checkin}\nDate de paiement: {checkout}\n\n--\nMerci et salutations,\n\nAdmin\n{site_name}', '<Table cellspacing = \"10\" cellpadding = \"0\">\n<Tbody>\n<Tr>\n<Td> Bonjour administrateur, </ td>\n</ Tr>\n<Tr>\n<Td>\n<br />\n{traveler_name} est cochÃ© Ã  {list_title}. <br /> <br />\nNom de l\'invitÃ©: {traveler_name}. <br /> <br />\nCourriel de l\'invitÃ©: {traveler_email}. <br /> <br />\nNom d\'hÃ´te: {host_name}. <br /> <br />\nCourriel de l\'hÃ´te: {host_email}. <br /> <br />\nNom de la liste: {list_title}. <br /> <br />\nPrix: {currency} {price}. <br /> <br />\nDate d\'arrivÃ©e: {checkin}. <br /> <br />\nDate de paiement: {checkout}.\n</ Td>\n</ Tr>\n<Tr>\n<Td>\n<P> -- </ p>\n<P> Merci et salutations, </ p>\n<P> {site_name} Ã‰quipe </ p>\n<Div> </ div>\n</ Td>\n</ Tr>\n</ Tbody>\n</ Table>'),
(513, 'auto_checkin_admin_gr', 'Admin: Auto Check In', 'Anreise', 'Hallo Administrator,\n\n{traveler_name} wird in {list_title} geprÃ¼ft.\n\nGusteName : {traveler_name}\nGuste-E-mail : {traveler_email}\nHostname : {host_name}\nHost-E-mail : {host_email}\nListename : {list_title}\nPreis : {currency}{price}\nCheckin-Datum : {checkin}\nCheckout-Datum : {checkout}\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdministrator\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo Administrator,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} wird in {list_title} geprÃ¼ft.<br /><br />\nGustename : {traveler_name}.<br /><br />\nGuste-E-mail : {traveler_email}.<br /><br />\nHostname : {host_name}.<br /><br />\nHost-E-mail : {host_email}.<br /><br />\nListename : {list_title}.<br /><br />\nPreis : {currency}{price}.<br /><br />\nCheckin-Datum : {checkin}.<br /><br />\nCheckout-Datum : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Danke und GrÃ¼ÃŸe,</p>\n<p>{site_name} Mannschaft</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(514, 'auto_checkin_admin_it', 'Admin: Auto Check In', 'Controllare viaggiatore nel', 'Ciao Admin,\n\n{traveler_name} Ã¨ checkin a {list_title}\n\nNome Cliente: {traveler_name}\nOspite Email: {traveler_email}\nNome Host: {host_name}\nHost E-mail: {host_email}\nNome elenco: {list_title}\nPrezzo: {currency} {price}\nCheckin Data: {checkin}\nCheckout Date: {checkout}\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Ciao Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} Ã¨ checkin a {list_title}.<br /><br />\nNome Cliente: : {traveler_name}.<br /><br />\nOspite Email : {traveler_email}.<br /><br />\nHost Nome : {host_name}.<br /><br />\nHost E-mail : {host_email}.<br /><br />\nNome elenco : {list_title}.<br /><br />\nPrezzo : {currency}{price}.<br /><br />\nCheckin Data : {checkin}.<br /><br />\nCheckout Date : {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Grazie e saluti,</p>\n<p>{site_name} Squadra</p>\n<div>Â </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(515, 'auto_checkin_admin_po', 'Admin: Auto Check In', 'Check-in do viajante', 'OlÃ¡, Admin,\n\n{traveler_name} estÃ¡ em checkin para {list_title}.\n\nNome do hÃ³spede: {traveler_name}\nEmail do hÃ³spede: {traveler_email}\nNome do host: {host_name}\nHost Email: {host_email}\nNome da lista: {list_title}\nPreÃ§o: {currency} {price}\nData de Checkin: {checkin}\nData de SaÃ­da: {checkout}\n\n--\nObrigado e cumprimentos,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>OlÃ¡ Admin,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} estÃ¡ em checkin para {list_title}.<br /><br />\nEmail do hÃ³spede: {traveler_name}.<br /><br />\nGuest Email : {traveler_email}.<br /><br />\nNome do host: {host_name}.<br /><br />\nHost Email: {host_email}.<br /><br />\nNome da lista: {list_title}.<br /><br />\nPreÃ§o : {currency}{price}.<br /><br />\nData de Checkin: {checkin}.<br /><br />\nData de SaÃ­da: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Obrigado e cumprimentos,</p>\n<p>{site_name} Equipe</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(516, 'auto_checkin_admin_sp', 'Admin: Auto Check In', 'Auto Traveler Check In', 'Hola administrador,\n\n{traveler_name} se registra en {list_title}.\n\nNombre de invitado: {traveler_name}\nCorreo electrÃ³nico de invitado: {traveler_email}\nNombre de host: {host_name}\nCorreo de host: {host_email}\nNombre de la lista: {list_title}\nPrecio: {currency} {price}\nFecha de llegada: {checkin}\nFecha de pago: {checkout}\n\n--\nGracias y saludos,\n\nAdministraciÃ³n\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hola administrador,</td>\n</tr>\n<tr>\n<td>\n<br />\n{traveler_name} se registra en {list_title}.<br /><br />\nNombre de invitado: {traveler_name}.<br /><br />\nCorreo electrÃ³nico de invitado: {traveler_email}.<br /><br />\nNombre de host: {host_name}.<br /><br />\nCorreo de host: {host_email}.<br /><br />\nNombre de la lista: {list_title}.<br /><br />\nPrecio : {currency}{price}.<br /><br />\nFecha de llegada: {checkin}.<br /><br />\nFecha de pago: {checkout}.\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Gracias y saludos,</p>\n<p>{site_name} Equipo</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(517, 'test_email', 'Test email', 'Testing mail', 'Hi,\n\n\n{site_name}.\n\n--\nThanks and Regards,', 'Hi,\n\n\n{site_name}.\n\n--\nThanks and Regards,'),
(518, 'wish_list_email_share', 'Wish list Email Share', 'Wish list Email Share', '{link}\n{message}\n\n\n--\nThanks and Regards,\n{username}\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi,</td>\n</tr>\n<tr>\n<td>\n<p>{message}</p>\n<p>{link}</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>{username},</p>\n<p>{site_name}</p>\n<div> </div>\n</td>\n</tr>\n</tbody>\n</table>'),
(519, 'Incomplete_listing_host', 'Follow up from {site_title} about your listings', 'Follow up from {site_title} about your listings', 'Hi {host_name},\n\nWe noticed that your some of the listings posted from {site_title} are not published or completed yet.\n\nWe would like to know the trouble that you are facing to publish the listing.\n\nWe are ready to fix your issues if any and support you with {site_title}.\n\n--\nThanks and Regards,\n\nAdmin\n{site_title}\n', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {host_name} ,</td>\n</tr>\n<tr>\n<td>\n<p><br />We noticed that your some of the listings posted from {site_title} are not published or completed yet.<br /><br />We would like to know the trouble that you are facing to publish your listing.<br /><br />We are ready to fix your issues if any and support you with {site_title}.<br /><br /></p>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_title}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(520, 'new_listing_user', 'Newly arrival listing of {site_title}', 'Newly arrival listing of {site_title}', 'Hi  {user_name},\n\nNewly added listings in {site_title}\n\nUsually people from all over the world are hoping to live in these homes or apartment. Are any of them on your Wish List? \n\n\r\nHere you can check these listings.\n\n{content}\n\n--\nThanks and Regards,\n\nAdmin\n{site_title}', '<table style=\"width: 100%;\" cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {user_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>&nbsp;</p>\n<p style=\"text-align: center;\"><span style=\"font-size: 18px;\"><strong>Newly added listings in {site_title}</strong></span></p>\n<p>Usually people from all over the world are hoping to live in these homes or apartment.</p>\n<p>Are any of them on your Wish List?</p>\n<p><br />Here you can check these listings.</p>\n<p><a style=\"font-weight: bold; margin: 5% 4%; margin-bottom: 10px; text-align: left; line-height: 1.3; color: #fff !important; text-decoration: none; font-family: Helvetica,Arial,sans-serif; background-color: #ff5a5f; border-radius: 100px; display: inline-block; padding: 12px 24px 12px 24px;\" href=\"{link}home\" target=\"blank\">See more homes</a></p>\n<div style=\"float: left;\">{img_name}</div>\n</td>\n</tr>\n<tr>\n<td>\n<p style=\"margin: 0 10px 0 0;\">--</p>\n<p style=\"margin: 0 0 10px 0;\">Thanks and Regards,</p>\n<p style=\"margin: 0 0 10px 0;\">Admin,</p>\n<p style=\"margin: 0px;\">{site_title}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(521, 'instant_book_reservation_granted', 'Guest: Instant booking reservation granted', 'Congrats! Your reservation granted.', 'Hi {traveler_name},\n\nCongratulation, Your reservation granted without host approval for {list_title}.\n\n--\nThanks and Regards,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Congratulation, Your reservation granted without host approval for {list_title}.</p>\n<p>&nbsp;</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>--</p>\n<p>Thanks and Regards,</p>\n<p>Admin,</p>\n<p>{site_name}</p>\n</td>\n</tr>\n</tbody>\n</table>'),
(522, 'instant_book_reservation_granted_fr', 'Guest: Instant booking reservation granted', 'Congrats! Votre rÃ©servation accordÃ©e.', 'Salut {traveler_name}\n\nFÃ©licitation, votre rÃ©servation accordÃ©e sans l\'approbation de l\'hÃ´te {list_title}.\n\n--\nMerci et salutations,\n\nAdministrateur\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Salut&nbsp;{traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>F&eacute;licitation, votre r&eacute;servation accord&eacute;e sans l\'approbation de l\'h&ocirc;te {list_title}.</p>\n<p>&nbsp;</p>\n</td>\n</tr>\n<tr>\n<td>\n<p>---</p>\n<pre>Merci et salutations,<br /><br />Administrateur<br />{site_name}</pre>\n</td>\n</tr>\n</tbody>\n</table>'),
(523, 'instant_book_reservation_granted_gr', 'Guest: Instant booking reservation granted', 'Congrats! Zugegeben Ihre Buchung.', 'Hallo {traveler_name},\n\nGlÃ¼ckwunsch, Ihre Buchung ohne Host erteilte Genehmigung fÃ¼r {list_title}.\n\n--\nDanke und GrÃ¼ÃŸe,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hallo {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Gl&uuml;ckwunsch, Ihre Buchung ohne Host erteilte Genehmigung f&uuml;r {list_title}.</p>\n<p>&nbsp;</p>\n</td>\n</tr>\n<tr>\n<td>\n<pre>--<br />Danke und Gr&uuml;&szlig;e,<br /><br />Admin<br />{site_name}</pre>\n</td>\n</tr>\n</tbody>\n</table>'),
(524, 'instant_book_reservation_granted_it', 'Guest: Instant booking reservation granted', 'Complimenti! Certo la prenotazione.', 'Hi {traveler_name},\n\nComplimenti, la prenotazione senza oste concesso l\'approvazione per {list_title}.\n\n--\nGrazie e saluti,\n\nAdmin\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Hi {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Complimenti, la prenotazione senza oste concesso l\'approvazione per {list_title}.</p>\n<p>&nbsp;</p>\n</td>\n</tr>\n<tr>\n<td>\n<pre>--<br />Grazie e saluti,<br /><br /></pre>\n<pre>Admin<br />{site_name}</pre>\n</td>\n</tr>\n</tbody>\n</table>'),
(525, 'instant_book_reservation_granted_sp', 'Guest: Instant booking reservation granted', 'Congrats! Otorgado su reserva.', 'Hola {traveler_name},\n\nEnhorabuena, Su reserva sin anfitriÃ³n homologaciÃ³n concedida a {list_title}.\n\n--\nGracias y Saludos,\n\nadministraciÃ³n\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Halo {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Enhorabuena, Su reserva sin anfitri&oacute;n homologaci&oacute;n concedida a {list_title}.</p>\n<p>&nbsp;</p>\n</td>\n</tr>\n<tr>\n<td>\n<pre>--<br />Gracias y Saludos,<br /><br />administraci&oacute;n<br />{site_name}</pre>\n<div>&nbsp;</div>\n</td>\n</tr>\n</tbody>\n</table>'),
(526, 'instant_book_reservation_granted_po', 'Guest: Instant booking reservation granted', 'ParabÃ©ns! Concedida a sua reserva.', 'Oi {traveler_name},\n\nParabÃ©ns, sua reserva sem acolhimento homologaÃ§Ã£o concedida a {list_title}.\n\n--\nObrigado e cumprimentos,\n\nadministrador\n{site_name}', '<table cellspacing=\"10\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td>Oi {traveler_name} ,</td>\n</tr>\n<tr>\n<td>\n<p>Parab&eacute;ns, sua reserva sem acolhimento homologa&ccedil;&atilde;o concedida a {list_title}.</p>\n<p>&nbsp;</p>\n</td>\n</tr>\n<tr>\n<td>\n<pre>--<br />Obrigado e cumprimentos,<br /><br />administrador<br />{site_name}</pre>\n<div>&nbsp;</div>\n</td>\n</tr>\n</tbody>\n</table>');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(128) CHARACTER SET utf8 NOT NULL,
  `faq_content` text CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` int(31) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question`, `faq_content`, `status`, `created`) VALUES
(1, 'Need help on this page?', '<p>Every one must be know how to work on this product. It is helpful to shows how to work in this product dropinn.</p>', 1, 0),
(2, 'How do i sign up?', '<p>It is helpful to shows how to sign up to access this product.</p>', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `google_analytics`
--

CREATE TABLE `google_analytics` (
  `id` int(255) NOT NULL,
  `transaction_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `google_analytics`
--

INSERT INTO `google_analytics` (`id`, `transaction_id`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE `help` (
  `id` int(11) NOT NULL,
  `question` varchar(125) NOT NULL,
  `description` text NOT NULL,
  `page_refer` varchar(150) NOT NULL,
  `created` varchar(150) NOT NULL,
  `modified_date` varchar(150) NOT NULL,
  `status` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `help`
--

INSERT INTO `help` (`id`, `question`, `description`, `page_refer`, `created`, `modified_date`, `status`) VALUES
(1, 'Need help on this page?', '<p>Every one must be know how to work on this product. It is helpful to shows how to work in this product dropinn.</p>', 'home', '', '1375233569', 0),
(2, 'How do i sign up?', ' It is helpful to shows how to sign up to access this product.', 'home', '', '', 0),
(3, 'How can i create an account?', ' It is helpful to shows how to create an account to access this product.', 'dashboard', '1375203327', '', 0),
(5, 'How can i view my reserved rooms?', ' It is helpful to shows how to view my reserved rooms.', 'dashboard', '1375204538', '', 0),
(6, 'How can i edit my reserved rooms? 	', 'It is helpful to shows how to edit my reserved rooms.', 'hosting', '1375204557', '', 0),
(7, 'How to set my payout method to pay?', 'To click a payout method in dashboard and then select a payout method to pay.', 'account', '1375204597', '', 0),
(8, 'How to view my transaction history?', '&lt;p&gt;How to view my transaction history?&lt;/p&gt;', 'payout', '1375205019', '1375211187', 0),
(9, 'How can i add new rooms?', '&lt;p&gt;Fill the form correctly and then add new rooms&lt;/p&gt;', 'new', '1375211799', '', 0),
(10, 'How can i view my inbox?', '<p>Go to dashboard and then select a link inbox to view your messages</p>', 'inbox', '1375215395', '1375215585', 0),
(11, 'How can i cancel my reserved rooms?', '<p>Go to dashboard and then select a link reservations to view your reserved rooms. In this link has a cancellation button to cancel the reservations.', 'travelling', '1375215747', '', 1),
(12, 'How can i edit my profile?', 'Login to dashboard and then click a link edit profile to edit.', 'edit', '', '', 1),
(13, 'How can i view my reviews?', '<p>view your reviews to click a link in profile.</p>', 'reviews', '1375233515', '', 1),
(14, 'How can i view my current trip?', 'Select a travellin link and then click a current trip tab to view your current trips.', 'current_trip', '', '', 1),
(15, 'Need help on this page?', '<p>Every one must be know how to work on this product. It is helpful to shows how to work in this product dropinn.</p>', 'guide', '', '1377699914', 1),
(16, 'How do I verify my phone number?', '<p>To verify your phone number:</p>\r\n<ol>\r\n<li>Click your name in the top-right corner of site.</li>\r\n<li>Select <strong>Edit Profile</strong></li>\r\n<li>Look for <strong>Phone Number</strong> and click <strong>Add a phone number</strong></li>\r\n<li>Use the drop-down menu to select your country. We&rsquo;ll automatically insert the right country code.</li>\r\n<li>Enter your area code and phone number</li>\r\n<li>Click <strong>Verify via SMS</strong> or <strong>Verify via Call</strong>. We\'ll send you a 4-digit code via an SMS (text) message or automated phone call.</li>\r\n<li>Enter the code we sent you and and click <strong>Verify</strong></li>\r\n<li>If you don&rsquo;t see a confirmation message, try refreshing the page. If the method you chose isn\'t working, try the other one.</li>\r\n</ol>', 'verify', '1375215395', '', 0),
(17, 'How do references work?', '<p>&nbsp;Hosts and guests can receive public references from friends, family members, and colleagues to help build their profile. References help people throughout the Airbnb community get to know you, and feel more comfortable booking a reservation with you.<br /><br />You need an account to request and write references, and a reference will only display on the recipient&rsquo;s profile if the author of the reference has a profile photo of their own.<br /><br />To request a reference:<br /><br />&nbsp;&nbsp;&nbsp; Click your name in the top-right corner of site.<br />&nbsp;&nbsp;&nbsp; Select Edit Profile<br />&nbsp;&nbsp;&nbsp; Select References on the left-hand side and use the Request References section to send emails to the people you want to get a reference from<br /><br />In the References About You section, you&rsquo;ll see references that you&rsquo;ve received. You have to approve each reference before it will appear on your profile.<br /><br />In the References By You section, you&rsquo;ll see reference requests you&rsquo;ve received. The only way you can write a reference for someone else in the community is in response to a reference request&mdash;you can&rsquo;t proactively leave a reference for someone.<br /><br />References are different from reviews, which hosts and guests write after a completed trip.</p>', 'references', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(200) NOT NULL,
  `address` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `host_cancellation_penalty`
--

CREATE TABLE `host_cancellation_penalty` (
  `id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `amount` float NOT NULL,
  `currency` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `host_cancellation_policy`
--

CREATE TABLE `host_cancellation_policy` (
  `id` int(1) NOT NULL,
  `days` int(2) NOT NULL,
  `months` int(2) NOT NULL,
  `before_amount` int(100) NOT NULL,
  `after_amount` int(100) NOT NULL,
  `free_cancellation` int(100) NOT NULL,
  `currency` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `host_cancellation_policy`
--

INSERT INTO `host_cancellation_policy` (`id`, `days`, `months`, `before_amount`, `after_amount`, `free_cancellation`, `currency`) VALUES
(1, 7, 6, 50, 100, 1, 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `ical_import`
--

CREATE TABLE `ical_import` (
  `id` int(50) NOT NULL,
  `list_id` int(50) NOT NULL,
  `url` varchar(500) NOT NULL,
  `last_sync` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Invoice`
--

CREATE TABLE `Invoice` (
  `id` int(10) NOT NULL,
  `invoice_number` int(255) NOT NULL,
  `order_number` int(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `address_1` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `postal` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `vat` varchar(200) NOT NULL,
  `company` varchar(200) NOT NULL,
  `billto` varchar(255) NOT NULL,
  `invoice_date` varchar(255) NOT NULL,
  `shipment_date` varchar(255) NOT NULL,
  `booking_period` varchar(255) NOT NULL,
  `months` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `subtotal` varchar(255) NOT NULL,
  `commission` varchar(255) NOT NULL,
  `total` int(10) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `joinus`
--

CREATE TABLE `joinus` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `joinus`
--

INSERT INTO `joinus` (`id`, `name`, `url`) VALUES
(1, 'Twitter', 'http://twitter.com/cogzidel'),
(2, 'Facebook', 'https://www.facebook.com/cogzidel'),
(3, 'Google', 'https://plus.google.com/116955559424123283004/about'),
(4, 'Youtube', 'http://www.youtube.com/results?search_query=cogzidel');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `code` varchar(7) NOT NULL,
  `name` varchar(30) NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `code`, `name`, `status`) VALUES
(1, 'en', 'English', '1'),
(2, 'fr', 'French', '1'),
(3, 'it', 'Italian', '1'),
(4, 'gr', 'German', '1'),
(5, 'po', 'Portuguese', '1'),
(6, 'sp', 'Spanish', '1');

-- --------------------------------------------------------

--
-- Table structure for table `list`
--

CREATE TABLE `list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `is_created` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `address` text CHARACTER SET utf8,
  `country` varchar(50) CHARACTER SET utf8 NOT NULL,
  `street_address` varchar(100) CHARACTER SET utf8 NOT NULL,
  `optional_address` varchar(100) CHARACTER SET utf8 NOT NULL,
  `city` varchar(25) CHARACTER SET utf8 NOT NULL,
  `state` varchar(25) CHARACTER SET utf8 NOT NULL,
  `zip_code` varchar(25) NOT NULL,
  `exact` int(11) NOT NULL,
  `directions` text CHARACTER SET utf8,
  `lat` decimal(18,14) NOT NULL,
  `long` decimal(18,14) NOT NULL,
  `property_id` int(11) NOT NULL,
  `room_type` varchar(50) NOT NULL,
  `bedrooms` int(11) NOT NULL,
  `beds` int(11) NOT NULL,
  `bed_type` varchar(50) NOT NULL,
  `bathrooms` float DEFAULT NULL,
  `amenities` varchar(111) NOT NULL,
  `title` text CHARACTER SET utf8,
  `desc` text CHARACTER SET utf8,
  `space` varchar(500) NOT NULL,
  `guests_info` varchar(500) NOT NULL,
  `interaction` varchar(500) NOT NULL,
  `overview` varchar(500) NOT NULL,
  `getting_around` varchar(500) NOT NULL,
  `othert_thing` varchar(500) NOT NULL,
  `capacity` int(11) NOT NULL,
  `cancellation_policy` int(2) NOT NULL,
  `street_view` smallint(6) NOT NULL,
  `price` int(11) NOT NULL,
  `sublet_price` int(50) NOT NULL,
  `sublet_status` enum('0','1') NOT NULL,
  `sublet_startdate` varchar(150) NOT NULL,
  `sublet_enddate` varchar(150) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `house_rule` text CHARACTER SET utf8 NOT NULL,
  `calendar_type` int(1) NOT NULL,
  `is_enable` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `list_pay` int(1) NOT NULL DEFAULT '0',
  `payment` int(1) NOT NULL DEFAULT '0',
  `page_viewed` bigint(20) NOT NULL,
  `review` int(11) NOT NULL DEFAULT '0',
  `overall_review` int(11) NOT NULL,
  `created` int(31) NOT NULL,
  `neighbor` text CHARACTER SET utf8,
  `is_featured` int(11) NOT NULL DEFAULT '0',
  `video_code` varchar(255) NOT NULL,
  `step_status` varchar(200) NOT NULL,
  `check_status` tinyint(255) NOT NULL,
  `banned` varchar(255) NOT NULL DEFAULT '0',
  `home_type` varchar(25) NOT NULL,
  `instance_book` varchar(100) NOT NULL,
  `min_stay` int(15) NOT NULL,
  `max_stay` int(15) NOT NULL,
  `room_id` int(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list`
--

INSERT INTO `list` (`id`, `is_created`, `user_id`, `address`, `country`, `street_address`, `optional_address`, `city`, `state`, `zip_code`, `exact`, `directions`, `lat`, `long`, `property_id`, `room_type`, `bedrooms`, `beds`, `bed_type`, `bathrooms`, `amenities`, `title`, `desc`, `space`, `guests_info`, `interaction`, `overview`, `getting_around`, `othert_thing`, `capacity`, `cancellation_policy`, `street_view`, `price`, `sublet_price`, `sublet_status`, `sublet_startdate`, `sublet_enddate`, `currency`, `email`, `phone`, `house_rule`, `calendar_type`, `is_enable`, `status`, `list_pay`, `payment`, `page_viewed`, `review`, `overall_review`, `created`, `neighbor`, `is_featured`, `video_code`, `step_status`, `check_status`, `banned`, `home_type`, `instance_book`, `min_stay`, `max_stay`, `room_id`) VALUES
(1, 0, 1, 'Dunas Altas, El Cipras (Guarnician Militar), Zona 4, 22785 Ensenada Municipality, Baja California, Mexico', 'Mexico', 'Dunas Altas, El Cipras (Guarnician Militar), Zona 4, 22785 Ensenada Municipality, Baja California, M', '', 'Zona', 'Baja California', '264634', 0, NULL, '31.78962130000000', '-116.60434580000003', 6, 'Private', 1, 1, 'Airbed', 1, '4,7,10,11', 'Modern Rustic Beach House with Pool', 'This unique house is a 3 bedrooms beach house with pool designed by architect Jorge Gracia recently featured in in the architectural books \'\'21st Century - 150 of the World\'\'s Best\'\' and \'\'Architecture Now - Houses\'\', published by TASCHEN.', '', '', '', '', '', '', 1, 1, 0, 250, 0, '0', '', '', 'USD', '', '', '', 1, 1, 1, 1, 0, 16, 0, 0, 1366502043, 'nothing select', 1, '', '', 0, '0', '', '', 0, 0, 0),
(2, 0, 1, 'Carrera 1C # 162A-2 a 162A-100, Bogota, Cundinamarca, Colombia', 'Colombia', 'Carrera 1C # 162A-2 a 162A-100, Bogota, Cundinamarca, Colombia', '', 'Bogota', 'Cundinamarca', '87684', 0, NULL, '4.73524780000000', '-74.01826100000000', 5, 'Private', 1, 1, 'None', 0, '2,8,14,18,20', 'New beautiful apartment in Spb', 'New beautiful apartment in the centre of the city in a new house with a balcony overlooking the old area of Sankt Petersburg.', '', '', '', '', '', '', 2, 1, 0, 221, 0, '0', '', '', 'PHP', '', '', '', 1, 1, 1, 1, 0, 6, 0, 0, 1366502043, 'nothing select', 1, '', '', 0, '0', '', '', 0, 0, 0),
(3, 0, 1, '184, 64606 Palva County, Estonia', 'Estonia', '184, 64606 Palva County, Estonia', '', 'Palva', 'Estonia', '235325', 0, 'NULL', '58.21135870000001', '27.16648880000003', 7, 'Shared', 1, 0, '', 0, '', 'Sunny Room in Brooklyn', '10 minutes to Williamsburg, 20 minutes to manhattan!\nA sunny private room with a Queen size futon and big closet in a new renovated apartment (this March), with a SHARED bathroom , has Wi-Fi, it\'\'s on the first floor, so no need to drag your heavy suitcase up down stairs.', '', '', '', '', '', '', 2, 1, 0, 125, 0, '0', '', '', 'EUR', '', '', '', 1, 1, 1, 1, 0, 6, 0, 0, 1366502307, 'nothing select', 1, '', '', 0, '0', '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `list_pay`
--

CREATE TABLE `list_pay` (
  `id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `amount` varchar(25) NOT NULL,
  `currency` varchar(25) NOT NULL,
  `created` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `list_photo`
--

CREATE TABLE `list_photo` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `list_id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` varchar(500) NOT NULL,
  `resize` varchar(500) NOT NULL,
  `resize1` varchar(500) NOT NULL,
  `map` varchar(500) NOT NULL,
  `highlights` text NOT NULL,
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `created` int(31) NOT NULL,
  `item_id` int(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_photo`
--

INSERT INTO `list_photo` (`id`, `user_id`, `list_id`, `name`, `image`, `resize`, `resize1`, `map`, `highlights`, `is_featured`, `created`, `item_id`) VALUES
(33, 0, 1, 'bRaqsah8.jpg', '0', '0', '0', '0', '', 1, 1438892174, 0),
(34, 0, 1, 'TotuyK4x.jpg', '0', '0', '0', '0', '', 0, 1438892174, 0),
(36, 0, 1, 'b6j6BpWF.jpg', '0', '0', '0', '0', '', 0, 1438892302, 0),
(37, 0, 2, '5BMSMxWP.jpg', '0', '0', '0', '0', '', 1, 1438892599, 0),
(38, 0, 3, 'Te2DBmSI.jpg', '0', '0', '0', '0', '', 1, 1438893064, 0);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_history`
--

CREATE TABLE `login_history` (
  `id` bigint(20) NOT NULL,
  `session_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `location` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `logout` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_history`
--

INSERT INTO `login_history` (`id`, `session_id`, `ip_address`, `user_agent`, `last_activity`, `location`, `user_id`, `logout`) VALUES
(1, '22ce146f669e0be9fbc77de305a3b208', '27.250.3.186', 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0', 1503909642, '', '1', 0),
(2, '25ae5d1156da1ff4bee2cfd8b684b525', '66.249.89.137', 'Mediapartners-Google', 1503909347, '', '0', 0),
(3, 'ee0137c963c540eb7eb6617040cb86e6', '66.249.64.73', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1503909354, '', '0', 0),
(4, '511c048a3648e7373d8f4753f1eeae74', '61.0.242.140', 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36', 1503914979, '', '0', 0),
(5, '4f1207790776319c1425b0ba82016e13', '27.250.3.186', 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0', 1503919028, '', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lys_status`
--

CREATE TABLE `lys_status` (
  `id` int(5) NOT NULL,
  `user_id` int(3) NOT NULL,
  `calendar` int(1) NOT NULL DEFAULT '0',
  `price` int(1) NOT NULL DEFAULT '0',
  `overview` int(1) NOT NULL DEFAULT '0',
  `title` int(1) NOT NULL,
  `summary` int(1) NOT NULL,
  `photo` int(1) NOT NULL DEFAULT '0',
  `amenities` int(1) NOT NULL DEFAULT '0',
  `address` int(1) NOT NULL DEFAULT '0',
  `listing` int(1) NOT NULL DEFAULT '0',
  `beds` int(1) NOT NULL,
  `bathrooms` int(1) NOT NULL,
  `bedscount` int(1) NOT NULL,
  `bedtype` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lys_status`
--

INSERT INTO `lys_status` (`id`, `user_id`, `calendar`, `price`, `overview`, `title`, `summary`, `photo`, `amenities`, `address`, `listing`, `beds`, `bathrooms`, `bedscount`, `bedtype`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `map_photo`
--

CREATE TABLE `map_photo` (
  `user_id` int(255) NOT NULL,
  `list_id` bigint(100) NOT NULL,
  `id` bigint(100) NOT NULL,
  `map` varchar(500) NOT NULL,
  `created` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `list_id` bigint(20) UNSIGNED NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `conversation_id` int(11) NOT NULL DEFAULT '0',
  `userby` int(11) NOT NULL,
  `userto` int(11) NOT NULL,
  `subject` varchar(70) NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `created` int(31) NOT NULL,
  `is_read` tinyint(4) NOT NULL DEFAULT '0',
  `is_starred` tinyint(4) NOT NULL,
  `is_respond` int(1) NOT NULL,
  `is_archived` int(1) NOT NULL,
  `message_type` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message_type`
--

CREATE TABLE `message_type` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `url` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_type`
--

INSERT INTO `message_type` (`id`, `name`, `url`) VALUES
(1, 'Reservation Request', 'trips/request'),
(2, 'Conversation', 'trips/conversation'),
(3, 'Message', 'trips/conversation'),
(4, 'Review Request', 'trips/review_by_host'),
(5, 'Review Request', 'trips/review_by_traveller'),
(6, 'Inquiry', 'trips/conversation'),
(7, 'Contacts Request', 'contacts/request'),
(8, 'Contacts Response', 'contacts/response'),
(9, 'Referrals', 'trips/conversation'),
(10, 'List Creation', 'trips/conversation'),
(11, 'List Edited', 'trips/conversation'),
(12, 'Request Sent', 'trips/request_sent');

-- --------------------------------------------------------

--
-- Table structure for table `metas`
--

CREATE TABLE `metas` (
  `id` int(11) NOT NULL,
  `url` varchar(111) NOT NULL,
  `name` varchar(300) NOT NULL,
  `title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metas`
--

INSERT INTO `metas` (`id`, `url`, `name`, `title`, `meta_description`, `meta_keyword`) VALUES
(1, 'account/index', 'Edit_account_details', 'Edit account details', 'Edit account details', 'Edit account details'),
(2, 'account/payout', 'Your_Payment_Method_details', 'Your Payment Method details', 'Your Payment Method details', 'Your Payment Method details'),
(3, 'account/setDefault', 'Set_Default_Payout_Preferences', 'Set Default Payout Preferences', 'Set Default Payout Preferences', 'Set Default Payout Preferences'),
(4, 'account/transaction', 'Your_Transaction_Details', 'Your Transaction Details', 'Your Transaction Details', 'Your Transaction Details'),
(5, 'calendar/single', 'Calendar', 'Calendar', 'Calendar', 'Calendar'),
(6, 'home/dashboard', 'Dashboard', 'Dashboard', 'Dashboard', 'Dashboard'),
(7, 'hosting/index', 'Your_Hosting_data', 'Your Listings', 'Your Listings', 'Your Listings'),
(8, 'hosting/change_status', 'Manage_Listings', 'Manage Listings', 'Manage Listings', 'Manage Listings'),
(9, 'hosting/sort_by_status', 'Manage Listings', 'Manage Listings', 'Manage Listings', 'Manage Listings'),
(10, 'hosting/my_reservation', 'My_Reservations', 'My Reservations', 'My Reservations', 'My Reservations'),
(11, 'hosting/policies', 'Stand_Bys', 'Policies', 'Policies', 'Policies'),
(12, 'info/index', 'Access_Deny', 'Access Deny', 'Access Deny', 'Access Deny'),
(13, 'info/deny', 'Access_Deny', 'Access Deny', 'Access Deny', 'Access Deny'),
(14, 'info/how_it_works', 'How it works', 'How it works', 'How it works', 'How it works'),
(15, 'listpay/index', 'Payment_Option', 'Payment Option', 'Payment Option', 'Payment Option'),
(16, 'message/inbox', 'Inbox', 'Inbox', 'Inbox', 'Inbox'),
(17, 'pages/contact', 'Contact_Us', 'Contact Us', 'Contact Us', 'Contact Us'),
(18, 'pages/faq', 'FAQs', 'FAQs', 'FAQs', 'FAQs'),
(19, 'payments/form', 'Confirm_your_booking', 'Confirm your booking', 'Confirm your booking', 'Confirm your booking'),
(20, 'referrals/index', 'Invite_Your_Friends', 'Invite Your Friends', 'Invite Your Friends', 'Invite Your Friends'),
(21, 'referrals/email', 'Invite_Your_Friends -Email', 'Invite Your Friends - Email', 'Invite Your Friends - Email', 'Invite Your Friends - Email'),
(22, 'referrals/tell_a_friend', 'Tell_A_Friend', 'Tell A Friend', 'Tell A Friend', 'Tell A Friend'),
(23, 'rooms/index', 'Rooms', 'Rooms', 'Rooms', 'Rooms'),
(24, 'rooms/newlist', 'List_Your_property', 'List your property', 'List your property', 'List your property'),
(25, 'rooms/edit', 'Edit_your_Listing', 'Edit your Listing', 'Edit your Listing', 'Edit your Listing'),
(26, 'rooms/edit_photo', 'Add_photo_for_this_listing', 'Add photo for this listing', 'Add photo for this listing', 'Add photo for this listing'),
(27, 'rooms/edit_price', 'Edit_the_price_information_for_your_site', 'Edit price', 'Edit price', 'Edit price'),
(28, 'rooms/change_status', 'Manage_Listings', 'Manage Listings', 'Manage Listings', 'Manage Listings'),
(29, 'search/index', 'Search_Elements', 'Search Elements', 'Search Elements', 'Search Elements'),
(30, 'travelling/current_trip', 'Your_Current_Trips', 'Your Current Trips', 'Your Current Trips', 'Your Current Trips'),
(31, 'travelling/your_trips', 'Your_trips', 'Your Trips', 'Your Trips', 'Your Trips'),
(32, 'travelling/previous_trips', 'Your_Previous_Trips_Trips', 'Your Previous Trips', 'Your Previous Trips', 'Your Previous Trips'),
(33, 'travelling/starred_items', 'List_your_stared_Item', 'List your starred Items', 'List your starred Items', 'List your starred Items'),
(34, 'travelling/host_details', 'Host_Details', 'Host Details', 'Host Details', 'Host Details'),
(35, 'travelling/billing', 'Reservation_Request', 'Reservation Request', 'Reservation Request', 'Reservation Request'),
(36, 'trips/request', 'Reservation_Request', 'Reservation Request', 'Reservation Request', 'Reservation Request'),
(37, 'trips/conversation', 'Conversations', 'Conversations', 'Conversations', 'Conversations'),
(38, 'trips/review_by_host', 'Review', 'Review', 'Review', 'Review'),
(39, 'trips/review_by_traveller', 'Review', 'Review', 'Review', 'Review'),
(40, 'host_review', 'View_Your_Review', 'View Your Review', 'View Your Review', 'View Your Review'),
(41, 'trips/traveler_review', 'View_your_review', 'View your review', 'View your review', 'View your review'),
(42, 'users/edit', 'Edit_your_Profile', 'Edit your Profile', 'Edit your Profile', 'Edit your Profile'),
(43, 'users/references', 'Your_recommendation_details', 'References details', 'References details', 'References details'),
(44, 'users/reviews', 'Your_Reviews_and_Recommendation', 'Your Reviews', 'Your Reviews', 'Your Reviews'),
(45, 'users/vouch', 'Recommend_your_friends', 'Recommend your friends', 'Recommend your friends', 'Recommend your friends'),
(46, 'users/signup', 'Sign_Up_for_the_site', 'Sign Up for the site', 'Sign Up for the site', 'Sign Up for the site'),
(47, 'users/signin', 'Sign_In / Sign_Up', 'Sign In / Sign Up', 'Sign In / Sign up', 'Sign In / Sign up'),
(48, 'uers/login', 'Sign_In / Sign_up', 'Sign In / Sign up', 'Sign In / Sign up', 'Sign In / Sign up'),
(49, 'users/logout', 'Logout_Shortly', 'Logout Shortly', 'Logout Shortly', 'Logout Shortly'),
(50, 'users/change_password', 'Change_Password', 'Change Password', 'Change Password', 'Change Password'),
(51, 'pages/cancellation_policy', 'cancellation_policy', 'Cancellation Policy', 'Cancellation Policy', 'Cancellation Policy'),
(52, 'account/mywishlist', 'My Wishlist', 'My Wishlist', 'My Wishlist', 'My Wishlist'),
(53, 'home/popular', 'Popular', 'Popular', 'Popular', 'Popular'),
(54, 'home/friends', 'Friends', 'Friends', 'Friends', 'Friends'),
(55, 'home/neighborhoods', 'Neighborhoods', 'Neighborhoods', 'Neighborhoods', 'Neighborhoods'),
(56, 'home/help', 'Help', 'Help', 'Help', 'Help'),
(57, 'users/verify', 'Verification', 'Verification', 'Verification', 'Verification'),
(58, 'home/verify', 'Verify', 'Verify', 'Verify', 'Verify'),
(59, 'neighbourhoods/detail_place', 'Neighbourhoods', 'Neighbourhoods', 'Neighbourhoods', 'Neighbourhoods'),
(60, 'neighbourhoods/city_places', 'Neighbourhoods', 'Neighbourhoods', 'Neighbourhoods', 'Neighbourhoods'),
(61, 'neighbourhoods/city', 'Neighbourhoods', 'Neighbourhoods', 'Neighbourhoods', 'Neighbourhoods'),
(62, 'users/view_fb_popup', 'Facebook Signup', 'Facebook Signup', 'Facebook Signup', 'Facebook Signup'),
(63, 'contacts/request', 'Contact_Request', 'Contact Request', 'Contact Request', 'Contact Request'),
(64, 'statistics/view_statistics_graph', 'Statistics', 'Statistics', 'Statistics', 'Statistics'),
(65, 'account/security', 'Security', 'Security', 'Security', 'Security'),
(66, 'account/setting', 'Setting', 'Setting', 'Setting', 'Setting'),
(67, 'trips/request_sent', 'Request Sent', 'Request Sent', 'Request Sent', 'Request Sent');

-- --------------------------------------------------------

--
-- Table structure for table `neighbor_area`
--

CREATE TABLE `neighbor_area` (
  `id` int(50) NOT NULL,
  `city_id` int(50) NOT NULL,
  `area` varchar(80) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `neighbor_city`
--

CREATE TABLE `neighbor_city` (
  `id` int(50) NOT NULL,
  `Country` varchar(80) NOT NULL,
  `State` varchar(80) NOT NULL,
  `City` varchar(80) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `neigh_category`
--

CREATE TABLE `neigh_category` (
  `id` int(3) NOT NULL,
  `category` varchar(100) NOT NULL,
  `created` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `neigh_category`
--

INSERT INTO `neigh_category` (`id`, `category`, `created`) VALUES
(1, 'Great Transit', '1381458120'),
(2, 'Touristy', '1381458133'),
(3, 'Shopping', '1381458148'),
(4, 'Loved by Londoners', '1381458168');

-- --------------------------------------------------------

--
-- Table structure for table `neigh_city`
--

CREATE TABLE `neigh_city` (
  `id` int(3) NOT NULL,
  `city_name` varchar(100) NOT NULL,
  `city_desc` text NOT NULL,
  `around` text NOT NULL,
  `known` text NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `is_home` int(1) NOT NULL DEFAULT '0',
  `created` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `neigh_city`
--

INSERT INTO `neigh_city` (`id`, `city_name`, `city_desc`, `around`, `known`, `image_name`, `is_home`, `created`) VALUES
(1, 'London', 'Relentlessly enterprising and culturally diverse, all eyes are on London when this influential city takes the stage.', 'Public Transit', 'Pub culture, tea culture, the royal family, Big Ben, Shakespeare, wry humor, theatre, fashion and finance, fish and chips, Tate Modern, the Tube', 'London.jpg', 1, '1388864301'),
(2, 'New York', 'From dawn until dusk until dawn again, trendsetting New York City never sleeps and never lets up.', 'Car,Public,Transit', 'Bright lights, long nights, tall buildings, high fashion, Lady Liberty, Central Park, Broadway, Wall Street, museums, the cutting edge, setting trends', 'nyc-aerial-skyline.jpg', 1, '1474584770'),
(3, 'Paris', 'When it comes to culture, cuisine, and immutable pride, no city shines like The City of Light.', 'Car,Public,Transit', 'Romance, philosophy, culinary esteem, iconic fashion, iconic attitude, rich history, fine art, The Louvre, Notre Dame, Champs-ElysÃƒÆ’Ã‚Â©es, brooding perfection', 'Paris.jpg', 1, '1474584694'),
(4, 'Washington DC', 'Instead of skyscrapers, pillars of American history, culture, and politics dominate DC.', 'Car,Public,Transit', 'The White House, the President, politics, Capitol Hill, US history, American monuments, the National Mall, the Pentagon, Georgetown, suits and ties', '356_3834_552_2361_hero_USA_WashingtonDC_Skyline_CKW-22.jpg', 1, '1435608503'),
(5, 'Berlin', 'From underground clubs to skyscraping murals, Berlin is swiftly becoming EuropeÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢s epicenter for creative culture', 'Car,Public,Transit', 'The Berlin Wall, historical significance, Bauhaus, provocative street art, weekend-long parties, electronic music, artists\' squats, beer gardens', '2_1348_0_700_hero_skylines_berlin_neumann_02-cropped-2-1.jpg', 1, '1435608610'),
(6, 'Bangkok', 'Neon-clad and always congested, Bangkok honors traditional sentimentality and embraces urban sentiments', 'Public Transit', 'Temples, street food, floating markets, tuk-tuks, beautiful ladyboys, Khaosan Road, Thai massage, Thai boxing, royal palaces, warm smiles.', 'pbk-Landing-Leaderboard-1280.jpg', 1, '1474584644'),
(7, 'Los Angles', 'Los Angels', 'Car', 'Los', 'los-angeles.jpg', 1, '1474585072'),
(8, 'Thai', 'Thailand is a city which is know for vacation.', 'Public', 'Thailand vacation', 'boat_wooden_canoe_local_thailand_beach_holiday_experience2.jpg', 1, '1475367444'),
(9, 'Amsterdam', 'Amsterdam - a great city for construction', 'Car', 'Construction', 'HQ-Amsterdam-Wallpaper.jpg', 1, '1475367768'),
(10, 'Dubai', 'Dubai - a boon in the world', 'Car', 'Everything', '2001.jpg', 1, '1475368120');

-- --------------------------------------------------------

--
-- Table structure for table `neigh_city_place`
--

CREATE TABLE `neigh_city_place` (
  `id` int(4) NOT NULL,
  `city_id` int(4) NOT NULL,
  `city_name` varchar(100) NOT NULL,
  `place_name` varchar(100) NOT NULL,
  `quote` text NOT NULL,
  `short_desc` text NOT NULL,
  `long_desc` text NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `lat` varchar(25) NOT NULL,
  `lng` varchar(25) NOT NULL,
  `is_featured` int(1) NOT NULL,
  `created` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `neigh_city_place`
--

INSERT INTO `neigh_city_place` (`id`, `city_id`, `city_name`, `place_name`, `quote`, `short_desc`, `long_desc`, `image_name`, `lat`, `lng`, `is_featured`, `created`) VALUES
(1, 1, 'London', 'Westminster', 'Prove you\'re in London with pictures in Westminster.', 'History is etched into the stones that compose this neighborhood\'s famous clock tower, abbey, and parliament buildings.', 'Boasting more than a few London landmarks, Westminster is a distinct political and cultural epicenter. Westminster Abbey, Buckingham Palace (God Save the Queen), and the United KingdomÃ¢â‚¬â„¢s House of Parliament all share the cobblestoned lanes under Big BenÃ¢â‚¬â„¢s timely shadow. Perched along the north bank', '0_5616_651_3093_hero_UK_London_King_s_Cross_RD__2.jpg', '51.5096446', '-0.1585863', 1, '1381458492');

-- --------------------------------------------------------

--
-- Table structure for table `neigh_knowledge`
--

CREATE TABLE `neigh_knowledge` (
  `id` int(5) NOT NULL,
  `post_id` int(3) NOT NULL,
  `city_id` int(5) NOT NULL,
  `city` varchar(25) NOT NULL,
  `place_id` int(5) NOT NULL,
  `place` varchar(25) NOT NULL,
  `user_id` int(5) NOT NULL,
  `user_type` varchar(25) NOT NULL DEFAULT 'Guest',
  `room_id` int(5) NOT NULL,
  `room_title` text NOT NULL,
  `knowledge` text NOT NULL,
  `shown` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `neigh_knowledge`
--

INSERT INTO `neigh_knowledge` (`id`, `post_id`, `city_id`, `city`, `place_id`, `place`, `user_id`, `user_type`, `room_id`, `room_title`, `knowledge`, `shown`) VALUES
(1, 1, 1, 'London', 1, 'Westminster', 1, 'Guest', 0, '', 'We would definitely recommend this [neighbourhood] to anyone who wants to be centrally located and use the flat as a base to enjoy this vibrant city and all it has to offer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `neigh_photographer`
--

CREATE TABLE `neigh_photographer` (
  `id` int(3) NOT NULL,
  `city` varchar(100) NOT NULL,
  `place` varchar(100) NOT NULL,
  `photographer_name` text NOT NULL,
  `photographer_desc` text NOT NULL,
  `photographer_image` varchar(100) NOT NULL,
  `photographer_web` varchar(50) NOT NULL,
  `city_id` varchar(3) NOT NULL,
  `is_featured` varchar(1) NOT NULL,
  `created` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `neigh_photographer`
--

INSERT INTO `neigh_photographer` (`id`, `city`, `place`, `photographer_name`, `photographer_desc`, `photographer_image`, `photographer_web`, `city_id`, `is_featured`, `created`) VALUES
(1, 'London', 'Westminster', 'Duke', 'Rebecca Duke has been working as a photographer for the last decade, after attending Central St Martinâ€™s in London. Her work focuses on people, interiors and travel and has been published in The Sunday Times, Elle Decor and Conde Nast Traveller. Rebecca travels frequently for her work but loved work', 'no_avatar-xlarge.jpg', 'No Website', '1', '1', '1381460489');

-- --------------------------------------------------------

--
-- Table structure for table `neigh_place_category`
--

CREATE TABLE `neigh_place_category` (
  `id` int(5) NOT NULL,
  `city` varchar(100) NOT NULL,
  `place` varchar(100) NOT NULL,
  `category_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `neigh_place_category`
--

INSERT INTO `neigh_place_category` (`id`, `city`, `place`, `category_id`) VALUES
(5, 'London', 'Westminster', 1),
(6, 'London', 'Westminster', 2),
(7, 'London', 'Westminster', 3),
(8, 'London', 'Westminster', 4),
(9, 'Tokyo', 'Akihabara', 1),
(10, 'Tokyo', 'Akihabara', 2),
(11, 'Tokyo', 'Akihabara', 3),
(12, 'Tokyo', 'Akihabara', 4),
(13, 'Mexico City', 'Mexico City', 1);

-- --------------------------------------------------------

--
-- Table structure for table `neigh_post`
--

CREATE TABLE `neigh_post` (
  `id` int(3) NOT NULL,
  `city` varchar(100) NOT NULL,
  `place` varchar(100) NOT NULL,
  `image_title` text NOT NULL,
  `image_desc` text NOT NULL,
  `big_image1` varchar(100) NOT NULL,
  `small_image1` varchar(100) NOT NULL,
  `small_image2` varchar(100) NOT NULL,
  `small_image3` varchar(100) NOT NULL,
  `small_image4` varchar(100) NOT NULL,
  `small_image5` varchar(100) NOT NULL,
  `big_image2` varchar(100) NOT NULL,
  `big_image3` varchar(100) NOT NULL,
  `is_featured` int(1) NOT NULL,
  `created` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `neigh_post`
--

INSERT INTO `neigh_post` (`id`, `city`, `place`, `image_title`, `image_desc`, `big_image1`, `small_image1`, `small_image2`, `small_image3`, `small_image4`, `small_image5`, `big_image2`, `big_image3`, `is_featured`, `created`) VALUES
(1, 'London', 'Westminster', 'Britain\'s VIPs: Very Important People and Places', 'Westminster\'s regal appeal stems from more than old stones and gold-gilded gates.', '0_5760_0_3840_one_UK_London_King_s_Cross_RD__6.jpg', '0_5760_115_3725_two_UK_London_King_s_Cross_RD__7.jpg', '0_5760_115_3725_two_UK_London_King_s_Cross_RD__18.jpg', '765_4995_0_3840_three_UK_London_King_s_Cross_RD__11.jpg', '765_4995_0_3840_three_UK_London_King_s_Cross_RD__13.jpg', '765_4995_0_3840_three_UK_London_King_s_Cross_RD__9.jpg', '0_5760_115_3725_two_UK_London_King_s_Cross_RD__22.jpg', '0_5760_115_3725_two_UK_London_King_s_Cross_RD__25.jpg', 1, '1381458967');

-- --------------------------------------------------------

--
-- Table structure for table `neigh_tag`
--

CREATE TABLE `neigh_tag` (
  `id` int(5) NOT NULL,
  `city_id` int(5) NOT NULL,
  `city` varchar(25) NOT NULL,
  `place_id` int(5) NOT NULL,
  `place` varchar(25) NOT NULL,
  `user_id` int(5) NOT NULL,
  `tag` varchar(25) NOT NULL,
  `shown` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(111) NOT NULL,
  `page_name` varchar(111) NOT NULL,
  `page_title` varchar(111) NOT NULL,
  `page_url` varchar(111) NOT NULL,
  `is_footer` tinyint(4) NOT NULL,
  `is_under` varchar(25) NOT NULL,
  `page_content` text NOT NULL,
  `created` int(31) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `page_name`, `page_title`, `page_url`, `is_footer`, `is_under`, `page_content`, `created`) VALUES
(13, 'Help', 'Help', 'help', 0, '', '<div id=\"View_help\" class=\"inner_pad_top\">\n<ul>\n  		<li><a href=\"#\"> Need help on this page? </a></li>\n  		<li><a href=\"#\">Getting Started Guide</a></li>\n  		<li><a href=\"#\">How do I sign up?</a></li>\n  		<li><a href=\"#\">How do I host on Dropinn?</a></li>\n  		<li><a href=\"#\">How do I travel on Dropinn?</a></li>\n  		<li><a href=\"#\">Visit our Trust & Safety Center </a></li>\n  		<li><a href=\"#\">See all FAQs</a></li>\n  		\n  	</ul>\n</div>', 1323793245),
(12, 'Travel', 'Travel', 'travel', 0, '', '<div class=\"inner_header\"><h2>Travel</h2></div><h4>Aliquam vitae congue tortor</h4>\r\n<p>Praesent in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent dui nibh, placerat id placerat nec, facilisis vitae lorem.Ut at ante non quam posuere sollicitudin. Sed vel libero tellus. Nam aliquam dolor vitae risus lacinia tristique.</p>\r\n\r\n<h3>Nam aliquam dolor</h3>\r\n<p> Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\r\n\r\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\r\n\r\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>\r\n<div class=\"inner_terms\">\r\n<ul>\r\n   <li><a href=\"#\">Ut rhoncus imperdiet augue sit amet egestas</a></li>\r\n<li><a href=\"#\">Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</a></li>\r\n<li><a href=\"#\">Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</a></li>\r\n<li><a href=\"#\">Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</a></li>\r\n<li><a href=\"#\">Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</a></li>\r\n<li><a href=\"#\">Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</a></li>\r\n<li><a href=\"#\">Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</a></li>\r\n<li><a href=\"#\">Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</a></li>\r\n</ul>\r\n</div>\r\n<h3>Phasellus sem</h3>\r\n\r\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\r\n\r\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\r\n\r\n<h3>Nunc porttitor sagittis</h3>\r\n\r\n<p>Aliquam vitae congue tortor. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio. Ut elementum ante quis urna auctor sagittis. Nunc porttitor sagittis condimentum. Nullam laoreet elit quis quam lobortis aliquet. Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros.</p>\r\n\r\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\r\n\r\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\r\n\r\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\r\n\r\n\r\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\r\n<div class=\"inner_terms\">\r\n<ul>\r\n   <li><a href=\"#\">Ut rhoncus imperdiet augue sit amet egestas</a></li>\r\n<li><a href=\"#\">Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</a></li>\r\n<li><a href=\"#\">Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</a></li>\r\n<li><a href=\"#\">Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</a></li>\r\n<li><a href=\"#\">Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</a></li>\r\n<li><a href=\"#\">Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</a></li>\r\n<li><a href=\"#\">Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</a></li>\r\n<li><a href=\"#\">Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</a></li>\r\n</ul>\r\n</div>\r\n\r\n<h3>Nam aliquam dolor</h3>\r\n<p> Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\r\n\r\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\r\n\r\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>', 1323793245),
(11, 'Social Connections', 'Social Connections', 'social', 1, 'discover', '<div class=\"inner_header\"><h2>Social Connections</h2></div><h3>Nam aliquam dolor?</h3>\r\n<p> Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\r\n\r\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\r\n\r\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>\r\n\r\n<h3>Phasellus sem?</h3>\r\n\r\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\r\n\r\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\r\n\r\n<h3>Nunc porttitor sagittis?</h3>\r\n\r\n<p>Aliquam vitae congue tortor. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio. Ut elementum ante quis urna auctor sagittis. Nunc porttitor sagittis condimentum. Nullam laoreet elit quis quam lobortis aliquet. Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros.</p>\r\n<h3>Donec gravida nulla non ante semper fringilla in ante justo?</h3>\r\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\r\n<h3>Aliquam gravida nisl non libero ullamcorper placerat sed nisl lacus?</h3>\r\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\r\n<h3>Nunc porttitor sagittis?</h3>\r\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\r\n\r\n<h3>Nunc porttitor sagittis?</h3>\r\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>', 1323793245),
(10, 'Responsible Hosting', 'Responsible Hosting', 'responsible_hosting', 1, 'company', '<div class=\"inner_header\"><h2>Responsible Hosting</h2></div><h4>Aliquam vitae congue tortor</h4>\n<p>Praesent in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent dui nibh, placerat id placerat nec, facilisis vitae lorem.Ut at ante non quam posuere sollicitudin. Sed vel libero tellus. Nam aliquam dolor vitae risus lacinia tristique.</p>\n\n<h4>Integer velit nunc faucibus idmollir</h4>\n<div class=\"inner_terms\">\n<ul>\n   <li>Ut rhoncus imperdiet augue sit amet egestas</li>\n<li>Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</li>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</li>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</li>\n<li>Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</li>\n<li>Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</li>\n<li>Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n<li>Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n</ul>\n</div>\n\n<h4>Aliquam gravida nisl non libero ullamcorper placerat</h4>\n\n<div class=\"inner_terms\">\n<ul>\n  <li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</li>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</li>\n<li>Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</li>\n<li>Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</li>\n<li>Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n<li>Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n</ul>\n</div>\n\n\n<h4>Nam eget nisl feugiat augue egestas</h4>\n<div class=\"inner_terms\">\n<ul>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</li>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</li>\n<li>Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</li>\n<li>Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</li>\n<li>Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n<li>Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</li>\n<li>Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</li>\n<li>Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n\n</div>', 1323793245),
(9, 'Terms & Privacy', 'Terms & Privacy', 'terms', 1, 'company', '<div class=\"inner_header\"><h2>Terms & Privacy</h2></div><h4>Aliquam vitae congue tortor</h4>\n<p>Praesent in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent dui nibh, placerat id placerat nec, facilisis vitae lorem.Ut at ante non quam posuere sollicitudin. Sed vel libero tellus. Nam aliquam dolor vitae risus lacinia tristique.</p>\n\n<h3>Nam aliquam dolor</h3>\n<p> Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\n\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\n\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>\n\n<h3>Phasellus sem</h3>\n\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\n\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\n\n<h3>Nunc porttitor sagittis</h3>\n\n<p>Aliquam vitae congue tortor. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio. Ut elementum ante quis urna auctor sagittis. Nunc porttitor sagittis condimentum. Nullam laoreet elit quis quam lobortis aliquet. Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros.</p>\n\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\n\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\n\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\n\n\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\n<div class=\"inner_terms\">\n<ul>\n   <li>Ut rhoncus imperdiet augue sit amet egestas</li>\n<li>Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</li>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</li>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</li>\n<li>Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</li>\n<li>Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</li>\n<li>Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n<li>Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n</ul>\n</div>\n\n', 1323793245),
(8, 'Policies', 'Policies', 'policies', 1, 'company', '<div class=\"inner_header\"><h2>Policies</h2></div><h4>Aliquam vitae congue tortor</h4>\n<p>Praesent in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent dui nibh, placerat id placerat nec, facilisis vitae lorem.Ut at ante non quam posuere sollicitudin. Sed vel libero tellus. Nam aliquam dolor vitae risus lacinia tristique.</p>\n\n<h3>Nam aliquam dolor</h3>\n<p> Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\n\n\n<div class=\"inner_terms\">\n<ul>\n   <li>Ut rhoncus imperdiet augue sit amet egestas</li>\n<li>Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</li>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</li>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</li>\n<li>Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</li>\n<li>Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</li>\n<li>Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n<li>Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n</ul>\n</div>\n\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\n\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>\n<div class=\"inner_terms\">\n<ul>\n  <li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</li>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</li>\n<li>Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</li>\n<li>Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</li>\n<li>Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n<li>Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n</ul>\n</div>\n<h3>Phasellus sem</h3>\n\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\n\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\n\n<h3>Nunc porttitor sagittis</h3>\n\n<p>Aliquam vitae congue tortor. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio. Ut elementum ante quis urna auctor sagittis. Nunc porttitor sagittis condimentum. Nullam laoreet elit quis quam lobortis aliquet. Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros.</p>\n\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\n\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\n\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\n\n\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\n<div class=\"inner_terms\">\n<ul>\n   <li>Ut rhoncus imperdiet augue sit amet egestas</li>\n<li>Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</li>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</li>\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</li>\n<li>Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</li>\n<li>Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</li>\n<li>Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n<li>Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n</ul>\n</div>\n', 1323793245),
(6, 'About Us', 'About Us', 'about', 1, 'company', '<div class=\"inner_header\"><h2>About Us</h2></div><p>Praesent in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent dui nibh, placerat id placerat nec, facilisis vitae lorem.Ut at ante non quam posuere sollicitudin. Sed vel libero tellus. Nam aliquam dolor vitae risus lacinia tristique. Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\n<p>Aliquam vitae congue tortor. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio. Ut elementum ante quis urna auctor sagittis. Nunc porttitor sagittis condimentum. Nullam laoreet elit quis quam lobortis aliquet. Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros.</p>\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>', 1323793245),
(7, 'Press', 'Press', 'press', 1, 'company', '<div class=\"inner_header\"><h2>Press</h2></div><h4>Aliquam vitae congue tortor</h4>\n<p>Praesent in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent dui nibh, placerat id placerat nec, facilisis vitae lorem.Ut at ante non quam posuere sollicitudin. Sed vel libero tellus. Nam aliquam dolor vitae risus lacinia tristique.</p>\n\n<h3>Nam aliquam dolor</h3>\n<p> Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\n\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\n\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>\n<div class=\"inner_terms\">\n<ul>\n   <li>Ut rhoncus imperdiet augue sit amet egestas</li>\r\n<li>Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</li>\r\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</li>\r\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</li>\r\n<li>Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</li>\r\n<li>Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</li><li>Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\r\n<li>Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\n</ul>\n</div>\n<h3>Phasellus sem</h3>\n\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\n\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\n\n<h3>Nunc porttitor sagittis</h3>\n\n<p>Aliquam vitae congue tortor. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio. Ut elementum ante quis urna auctor sagittis. Nunc porttitor sagittis condimentum. Nullam laoreet elit quis quam lobortis aliquet. Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros.</p>\n\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\n\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\n\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\n\n\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\n<div class=\"inner_terms\">\n<ul>\n   <li><a href=\"#\">Ut rhoncus imperdiet augue sit amet egestas</a></li>\n<li><a href=\"#\">Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</a></li>\n<li><a href=\"#\">Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</a></li>\n<li><a href=\"#\">Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</a></li>\n<li><a href=\"#\">Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</a></li>\n<li><a href=\"#\">Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</a></li>\n<li><a href=\"#\">Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</a></li>\n<li><a href=\"#\">Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</a></li>\n</ul>\n</div>\n\n<h3>Nam aliquam dolor</h3>\n<p> Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\n\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\n\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>', 1323793245),
(5, 'Why Host?', 'Why Host?', 'why_host', 1, 'discover', '<div class=\"inner_header\"><h2>Why Host?</h2></div><p>Praesent in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent dui nibh, placerat id placerat nec, facilisis vitae lorem.Ut at ante non quam posuere sollicitudin. Sed vel libero tellus. Nam aliquam dolor vitae risus lacinia tristique. Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\n<p>Aliquam vitae congue tortor. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio. Ut elementum ante quis urna auctor sagittis. Nunc porttitor sagittis condimentum. Nullam laoreet elit quis quam lobortis aliquet. Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros.</p>\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>', 1323793245),
(4, 'Recommendation Help', 'Recommendation Help', 'recommendation_help', 0, '', '<div class=\"inner_header\"><h2>Recommendation Help</h2></div>\r\n\r\n<h4>Aliquam vitae congue tortor</h4>\r\n<p>Praesent in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent dui nibh, placerat id placerat nec, facilisis vitae lorem.Ut at ante non quam posuere sollicitudin. Sed vel libero tellus. Nam aliquam dolor vitae risus lacinia tristique.</p>\r\n\r\n<h3>Nam aliquam dolor</h3>\r\n<p> Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\r\n\r\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\r\n\r\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>\r\n\r\n<h3>Phasellus sem</h3>\r\n\r\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\r\n\r\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\r\n\r\n<h3>Nunc porttitor sagittis</h3>\r\n\r\n<p>Aliquam vitae congue tortor. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio. Ut elementum ante quis urna auctor sagittis. Nunc porttitor sagittis condimentum. Nullam laoreet elit quis quam lobortis aliquet. Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros.</p>\r\n\r\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\r\n\r\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\r\n\r\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\r\n\r\n\r\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\r\n<div class=\"inner_terms\">\r\n<ul>\r\n   <li>Ut rhoncus imperdiet augue sit amet egestas</li>\r\n<li>Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</li>\r\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</li>\r\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</li>\r\n<li>Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</li>\r\n<li>Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</li>\r\n<li>Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\r\n<li>Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\r\n</ul>\r\n</div>', 1323793186);
INSERT INTO `page` (`id`, `page_name`, `page_title`, `page_url`, `is_footer`, `is_under`, `page_content`, `created`) VALUES
(3, 'Photo Tips', 'Photo Tips', 'photo_tips', 0, '', '<div class=\"inner_header\"><h2>Photo Tips</h2></div>\r\n\r\n<h4>Aliquam vitae congue tortor</h4>\r\n<p>Praesent in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent dui nibh, placerat id placerat nec, facilisis vitae lorem.Ut at ante non quam posuere sollicitudin. Sed vel libero tellus. Nam aliquam dolor vitae risus lacinia tristique.</p>\r\n\r\n<h3>Nam aliquam dolor</h3>\r\n<p> Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\r\n\r\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\r\n\r\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>\r\n\r\n<h3>Phasellus sem</h3>\r\n\r\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\r\n\r\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\r\n\r\n<h3>Nunc porttitor sagittis</h3>\r\n\r\n<p>Aliquam vitae congue tortor. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio. Ut elementum ante quis urna auctor sagittis. Nunc porttitor sagittis condimentum. Nullam laoreet elit quis quam lobortis aliquet. Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros.</p>\r\n\r\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\r\n\r\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\r\n\r\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\r\n\r\n\r\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\r\n<div class=\"inner_terms\">\r\n<ul>\r\n   <li>Ut rhoncus imperdiet augue sit amet egestas</li>\r\n<li>Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</li>\r\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</li>\r\n<li>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</li>\r\n<li>Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</li>\r\n<li>Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</li>\r\n<li>Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\r\n<li>Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</li>\r\n</ul>\r\n</div>', 1323793059),
(2, 'Fun Company News', 'Fun Company News', 'fun_company_news', 0, '', '<div class=\"inner_header\"><h2>Fun Company News</h2></div><h4>Aliquam vitae congue tortor</h4>\n<p>Praesent in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent dui nibh, placerat id placerat nec, facilisis vitae lorem.Ut at ante non quam posuere sollicitudin. Sed vel libero tellus. Nam aliquam dolor vitae risus lacinia tristique.</p>\n\n<h3>Nam aliquam dolor</h3>\n<p> Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\n\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\n\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>\n<div class=\"inner_terms\">\n<ul>\n   <li><a href=\"#\">Ut rhoncus imperdiet augue sit amet egestas</a></li>\n<li><a href=\"#\">Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</a></li>\n<li><a href=\"#\">Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</a></li>\n<li><a href=\"#\">Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</a></li>\n<li><a href=\"#\">Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</a></li>\n<li><a href=\"#\">Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</a></li>\n<li><a href=\"#\">Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</a></li>\n<li><a href=\"#\">Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</a></li>\n</ul>\n</div>\n<h3>Phasellus sem</h3>\n\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\n\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\n\n<h3>Nunc porttitor sagittis</h3>\n\n<p>Aliquam vitae congue tortor. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio. Ut elementum ante quis urna auctor sagittis. Nunc porttitor sagittis condimentum. Nullam laoreet elit quis quam lobortis aliquet. Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros.</p>\n\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\n\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\n\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\n\n\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>\n<div class=\"inner_terms\">\n<ul>\n   <li><a href=\"#\">Ut rhoncus imperdiet augue sit amet egestas</a></li>\n<li><a href=\"#\">Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper</a></li>\n<li><a href=\"#\">Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel</a></li>\n<li><a href=\"#\">Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit.</a></li>\n<li><a href=\"#\">Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio.</a></li>\n<li><a href=\"#\">Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat.</a></li>\n<li><a href=\"#\">Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</a></li>\n<li><a href=\"#\">Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</a></li>\n</ul>\n</div>\n\n<h3>Nam aliquam dolor</h3>\n<p> Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\n\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\n\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>', 1323793001),
(1, 'Really Cool Destinations', 'Really Cool Destinations', 'really_cool_destinations', 0, '', '<div class=\"inner_header\"><h2>Really Cool Destinations</h2></div><p>Praesent in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent dui nibh, placerat id placerat nec, facilisis vitae lorem.Ut at ante non quam posuere sollicitudin. Sed vel libero tellus. Nam aliquam dolor vitae risus lacinia tristique. Sed vitae nibh et felis ornare accum. tiam tristique ornare erat et facilisis. Etiam pretium, massa ut commodo viverra, nunc magna vestibulum risus, a imperdiet quam leo ac mi.</p>\n<p>Nam eget nisl feugiat augue egestas tempus at fermentum tellus. Vestibulum vel orci ante, sed auctor mauris. Nulla a odio id nunc lobortis venenatis. Sed vestibulum elit at urna tincidunt pellentesque. Aenean tristique, massa ac faucibus adipiscing, nunc nulla aliquet orci, vitae pharetra enim erat sit amet magna. Ut pulvinar consequat purus in egestas. Phasellus imperdiet bibendum libero sit amet adipiscing.</p>\n<p>Nulla mauris tellus, aliquam rutrum consectetur eu, pulvinar sit amet est. Integer sodales vulputate arcu eget dictum. Suspendisse nibh dolor, vestibulum a euismod nec, tristique ac quam. Aliquam vitae dolor justo, non aliquet nisl. Maecenas accumsan convallis mattis.</p>\n<p>Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros. Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius.</p>\n<p>Phasellus sem tellus, imperdiet eu feugiat vel, laoreet non ligula. Pellentesque eleifend consequat augue eu hendrerit. Quisque vel turpis et lacus fermentum congue. Integer fringilla euismod dui, id vehicula ut. Pellentesque placerat dictum diam sit amet porta.</p>\n<p>Aliquam vitae congue tortor. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio. Ut elementum ante quis urna auctor sagittis. Nunc porttitor sagittis condimentum. Nullam laoreet elit quis quam lobortis aliquet. Duis suscipit interdum sapien, nec vulputate tortor dignissim et. Maecenas consequat rhoncus eros.</p>\n<p>Ut rhoncus imperdiet augue, sit amet egestas odio fermentum sed. Aliquam gravida nisl non libero ullamcorper placerat. Sed nisl lacus, auctor in posuere vitae, aliquam ut elit. Integer velit nunc, faucibus id mollis pharetra, eleifend at odio. Integer ullamcorper pretium varius. Donec gravida nulla non ante semper fringilla. In ante justo, sodales id condimentum sit amet, lobortis ut odio.</p>', 1323792509),
(14, 'Host Cancellation Policy', 'Host Cancellation Policy', 'host_cancellation_policy', 1, 'discover', '<div class=\"inner_header\"><h2>Host Cancellation Policy</h2></div><p>If you need to cancel a confirmed reservation, it&rsquo;s important to do so as soon as possible. Go to your reservations, find the reservation you need to cancel, and click Cancel. You should also contact your guest to apologize for the inconvenience.<br /><br />When a reservation&rsquo;s status is Canceled, you&rsquo;ll be recorded as the party responsible for initiating the cancellation in our system. Because host cancellations require additional customer support, and generate additional costs for securing last-minute accommodations, the following actions are applied:<br /><br />Your calendar will remain blocked for the dates of the reservation. Our system does this automatically, to prevent another guest from booking unavailable dates at your listing.<br /><br />You may be subject to a cancellation fee, if you cancel more than {limit} in a {month}-month period. This fee, taken from your next payout, will help offset the cost of securing last-minute accommodations for your guest. The fee will be {before_amount} for each cancellation after {limit} in a {month}-month period, or {within_amount} for each cancellation after {limit} in a {month}-month period, made on a reservation that begins in {days} days or less.<br /><br />If you cancel before a reservation begins, your guest will receive a full refund and you will not receive a payout. If you cancel an active reservation, your cancellation policy will no longer determine your payout, and your guest will be fully refunded for every night that they did not stay in your space.</p>', 1416840213);

-- --------------------------------------------------------

--
-- Table structure for table `page_popup`
--

CREATE TABLE `page_popup` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_popup`
--

INSERT INTO `page_popup` (`id`, `name`, `content`, `status`) VALUES
(11, 'search', '', 1),
(16, 'search', '', 1),
(17, 'step2', '', 1),
(18, 'home', '', 1),
(19, 'step4', '', 1),
(23, 'rooms', '', 1),
(27, 'rooms', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `paykey`
--

CREATE TABLE `paykey` (
  `paykey` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paykey`
--

INSERT INTO `paykey` (`paykey`) VALUES
('AP-9LF47559NN033983K');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` smallint(6) NOT NULL,
  `payment_name` varchar(30) NOT NULL,
  `is_enabled` smallint(6) NOT NULL DEFAULT '0',
  `is_live` smallint(6) NOT NULL DEFAULT '0',
  `is_payout` smallint(6) NOT NULL DEFAULT '0',
  `arrives_on` varchar(111) NOT NULL,
  `fees` varchar(30) NOT NULL,
  `currency` varchar(5) NOT NULL,
  `note` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `payment_name`, `is_enabled`, `is_live`, `is_payout`, `arrives_on`, `fees`, `currency`, `note`) VALUES
(2, 'Paypal', 1, 0, 1, 'Instant', 'None', 'USD', 'You can withdraw money from PayPal...\r\n<ul style=\"list-style-type: disc;list-style-position: inside;\">\r\n<li>to your local bank account.</li>\r\n<li>via paper check.</li>\r\n</ul>'),
(1, 'CreditCard', 1, 0, 1, '', '', '', ''),
(3, 'Stripe', 1, 0, 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_details`
--

CREATE TABLE `payment_details` (
  `id` int(11) NOT NULL,
  `payment_id` smallint(6) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(111) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_details`
--

INSERT INTO `payment_details` (`id`, `payment_id`, `code`, `name`, `value`) VALUES
(4, 2, 'PAYPAL_ID', 'Paypal Business Id', 'gatesiva44@gmail.com'),
(3, 1, 'CC_SIGNATURE', 'CC Signature', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AZSIKDxFGEpEBkweaNyiG.ilJ1UF'),
(2, 1, 'CC_PASSWORD', 'CC Password', '7R4U7VHZEPF4NGBK'),
(1, 1, 'CC_USER', 'CC Username', 'gatesiva44-facilitator_api1.gmail.com'),
(5, 3, 'BT_MERCHANT', 'BT Merchant Id', 'jdrhjptfq67xxyrq'),
(6, 3, 'BT_PUBLICKEY', 'BT Public Key', 'hc846bfhfy655xkc'),
(7, 3, 'BT_PRIVATEKEY', 'BT Private Key', 'b6c18672a8694e035ea2bc1edf056411'),
(8, 4, 'SecretKey', 'SecretKey', 'sk_test_Nr2Zq4SGJ6oqJGh9Rb3s9rxi'),
(9, 4, 'PublishableKey', 'PublishableKey', 'pk_test_mTyIoTTyYqxRRauS7dXXzAPk'),
(10, 4, 'LSecretKey', 'LSecretKey', 'sk_live_JBTornkJUvqBs9buZA3I94CW'),
(11, 4, 'LPublishableKey', 'LPublishableKey', 'pk_live_Cj2nLSwt7Liigrv13BxIF9o1'),
(12, 4, 'PublishableKey', 'PublishableKey', 'pk_test_mTyIoTTyYqxRRauS7dXXzAPk');

-- --------------------------------------------------------

--
-- Table structure for table `paymode`
--

CREATE TABLE `paymode` (
  `id` tinyint(4) NOT NULL,
  `mod_name` varchar(111) NOT NULL,
  `is_premium` tinyint(4) NOT NULL DEFAULT '0',
  `is_fixed` tinyint(4) NOT NULL DEFAULT '0',
  `fixed_amount` bigint(20) NOT NULL,
  `currency` varchar(25) NOT NULL,
  `percentage_amount` float NOT NULL,
  `modified_date` varchar(111) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paymode`
--

INSERT INTO `paymode` (`id`, `mod_name`, `is_premium`, `is_fixed`, `fixed_amount`, `currency`, `percentage_amount`, `modified_date`) VALUES
(1, 'Host Listing', 0, 1, 10, 'USD', 10, ''),
(2, 'Guest Booking', 1, 1, 50, 'USD', 50, ''),
(3, 'Host Accept The Reservation Request', 1, 0, 10, 'USD', 10, '');

-- --------------------------------------------------------

--
-- Table structure for table `payout_preferences`
--

CREATE TABLE `payout_preferences` (
  `id` int(50) NOT NULL,
  `user_id` int(111) NOT NULL,
  `country` varchar(7) NOT NULL,
  `payout_type` smallint(6) NOT NULL,
  `email` varchar(50) NOT NULL,
  `currency` varchar(7) NOT NULL,
  `is_default` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payout_preferences`
--

INSERT INTO `payout_preferences` (`id`, `user_id`, `country`, `payout_type`, `email`, `currency`, `is_default`) VALUES
(1, 1, 'IN', 2, 'admin@gmail.com', 'USD', 1);

-- --------------------------------------------------------

--
-- Table structure for table `paywhom`
--

CREATE TABLE `paywhom` (
  `id` int(11) NOT NULL,
  `whom` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paywhom`
--

INSERT INTO `paywhom` (`id`, `whom`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_bin
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE `price` (
  `id` int(11) NOT NULL,
  `night` int(11) NOT NULL,
  `week` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `guests` smallint(6) NOT NULL,
  `addguests` int(11) NOT NULL,
  `cleaning` int(11) NOT NULL,
  `security` int(11) NOT NULL,
  `previous_price` int(255) NOT NULL,
  `currency` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price`
--

INSERT INTO `price` (`id`, `night`, `week`, `month`, `guests`, `addguests`, `cleaning`, `security`, `previous_price`, `currency`) VALUES
(1, 250, 1445, 3888, 2, 5, 5, 0, 0, 'USD'),
(2, 221, 155, 258, 1, 5, 5, 0, 0, 'PHP'),
(3, 125, 693, 1890, 1, 5, 0, 0, 0, 'EUR');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) NOT NULL,
  `Fname` varchar(255) DEFAULT NULL,
  `Lname` varchar(255) DEFAULT NULL,
  `gender` varchar(25) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `live` text,
  `school` text NOT NULL,
  `work` text,
  `phnum` varchar(255) DEFAULT NULL,
  `describe` text,
  `verification_status` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `emergency_status` int(1) NOT NULL DEFAULT '0',
  `emergency_name` varchar(255) NOT NULL,
  `emergency_phone` varchar(15) NOT NULL,
  `emergency_email` varchar(255) NOT NULL,
  `country_code` varchar(50) NOT NULL,
  `emergency_relation` varchar(255) NOT NULL,
  `join_date` varchar(70) NOT NULL,
  `tmail` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `Fname`, `Lname`, `gender`, `dob`, `email`, `live`, `school`, `work`, `phnum`, `describe`, `verification_status`, `language`, `emergency_status`, `emergency_name`, `emergency_phone`, `emergency_email`, `country_code`, `emergency_relation`, `join_date`, `tmail`) VALUES
(1, NULL, NULL, '', '', 'systems@cogzidel.com', NULL, '', NULL, NULL, NULL, '', '', 0, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(200) NOT NULL,
  `email` text NOT NULL,
  `src` text,
  `ext` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_type`
--

CREATE TABLE `property_type` (
  `id` int(63) NOT NULL,
  `type` varchar(63) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_type`
--

INSERT INTO `property_type` (`id`, `type`) VALUES
(2, 'House'),
(1, 'Apartment'),
(3, 'Bed & Break Fast'),
(5, 'Cabin'),
(7, 'Castle'),
(8, 'Dorm'),
(9, 'Treehouse'),
(10, 'Boat'),
(4, 'Loft'),
(6, 'Villa'),
(11, 'Plane'),
(12, 'Parking Space'),
(13, 'Car'),
(14, 'Van'),
(15, 'Camper/RV'),
(16, 'Lgloo'),
(17, 'Lighthouse'),
(18, 'Yurt'),
(19, 'Tipi'),
(20, 'Cave'),
(21, 'Island'),
(22, 'Chalet'),
(23, 'Earth House'),
(24, 'Hut'),
(25, 'Train'),
(26, 'Tent'),
(27, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `recommends`
--

CREATE TABLE `recommends` (
  `id` bigint(20) NOT NULL,
  `userby` bigint(20) NOT NULL,
  `userto` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `relationship` int(1) NOT NULL,
  `is_approval` int(1) NOT NULL,
  `created` int(31) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reference_request`
--

CREATE TABLE `reference_request` (
  `id` int(10) NOT NULL,
  `userby` int(10) NOT NULL,
  `userto` int(10) NOT NULL,
  `status` int(1) NOT NULL,
  `created` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `id` int(111) NOT NULL,
  `invite_from` int(111) NOT NULL,
  `invite_to` int(111) NOT NULL,
  `join_date` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `referrals_amount`
--

CREATE TABLE `referrals_amount` (
  `id` int(111) NOT NULL,
  `user_id` int(111) NOT NULL,
  `count_trip` int(111) NOT NULL,
  `count_book` int(111) NOT NULL,
  `amount` varchar(111) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `referrals_booking`
--

CREATE TABLE `referrals_booking` (
  `id` int(11) NOT NULL,
  `payer_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `ref_amount` int(11) NOT NULL,
  `is_full` int(1) NOT NULL,
  `date` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `referral_management`
--

CREATE TABLE `referral_management` (
  `id` int(11) NOT NULL,
  `fixed_status` tinyint(4) NOT NULL,
  `fixed_amt` int(11) NOT NULL,
  `currency` varchar(25) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `trip_amt` bigint(20) NOT NULL,
  `trip_per` float NOT NULL,
  `rent_amt` bigint(20) NOT NULL,
  `rent_per` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referral_management`
--

INSERT INTO `referral_management` (`id`, `fixed_status`, `fixed_amt`, `currency`, `type`, `trip_amt`, `trip_per`, `rent_amt`, `rent_per`) VALUES
(1, 0, 1000, 'USD', 0, 0, 50, 1000, 50);

-- --------------------------------------------------------

--
-- Table structure for table `refund`
--

CREATE TABLE `refund` (
  `id` int(10) NOT NULL,
  `reservation_id` int(10) NOT NULL,
  `userto` int(10) NOT NULL,
  `payout_id` varchar(50) NOT NULL,
  `accept_status` int(1) NOT NULL,
  `created` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` bigint(20) NOT NULL,
  `transaction_id` varchar(50) NOT NULL,
  `list_id` int(111) NOT NULL,
  `userby` int(11) NOT NULL,
  `userto` int(111) NOT NULL,
  `checkin` varchar(50) NOT NULL,
  `checkout` varchar(50) NOT NULL,
  `no_quest` tinyint(4) NOT NULL,
  `currency` varchar(11) NOT NULL,
  `price` float NOT NULL,
  `topay` float NOT NULL,
  `admin_commission` float NOT NULL,
  `cleaning` float NOT NULL,
  `security` float NOT NULL,
  `extra_guest_price` float NOT NULL,
  `guest_count` int(5) NOT NULL,
  `coupon` varchar(25) NOT NULL,
  `coupon_amt` varchar(100) NOT NULL,
  `credit_type` tinyint(4) NOT NULL,
  `ref_amount` int(111) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `is_payed` tinyint(4) NOT NULL DEFAULT '0',
  `is_payed_host` int(1) NOT NULL DEFAULT '0',
  `is_payed_guest` int(1) NOT NULL DEFAULT '0',
  `payment_id` tinyint(4) NOT NULL,
  `payed_date` varchar(111) NOT NULL,
  `book_date` int(31) NOT NULL,
  `cancel_date` varchar(50) NOT NULL,
  `host_topay` float NOT NULL,
  `guest_topay` float NOT NULL,
  `host_penalty` float NOT NULL,
  `policy` int(3) NOT NULL,
  `contacts_offer` int(255) NOT NULL DEFAULT '0',
  `paypal_transactionid` varchar(500) NOT NULL,
  `paypal_token` varchar(500) NOT NULL,
  `paypal_payer_id` varchar(500) NOT NULL,
  `cc_cusid` varchar(255) NOT NULL,
  `per_night` varchar(255) NOT NULL,
  `reservation_key` varchar(255) NOT NULL,
  `via` varchar(255) NOT NULL,
  `contact_key` varchar(255) NOT NULL,
  `per_night_price` varchar(100) NOT NULL,
  `base_price` varchar(100) NOT NULL,
  `seasonal_dates_price` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reservation_status`
--

CREATE TABLE `reservation_status` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation_status`
--

INSERT INTO `reservation_status` (`id`, `name`) VALUES
(0, 'Payment Pending'),
(1, 'Pending'),
(2, 'Expired'),
(3, 'Accepted'),
(4, 'Declined'),
(5, 'Before Checkin Canceled by Host'),
(6, 'Before Checkin Canceled by Guet'),
(7, 'Checkin'),
(8, 'Awaiting Host Review'),
(9, 'Awaiting Travel Review'),
(10, 'Completed'),
(11, 'After Checkin Canceled by Host'),
(12, 'After Checkin Canceled by Guest'),
(13, 'Pending Reservation Canceled');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) NOT NULL,
  `userby` bigint(20) NOT NULL,
  `userto` bigint(20) NOT NULL,
  `reservation_id` bigint(20) NOT NULL,
  `list_id` bigint(20) NOT NULL,
  `review` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `feedback` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `cleanliness` smallint(6) NOT NULL,
  `communication` smallint(6) NOT NULL,
  `house_rules` smallint(6) NOT NULL,
  `accuracy` tinyint(4) NOT NULL,
  `checkin` tinyint(4) NOT NULL,
  `location` tinyint(4) NOT NULL,
  `value` tinyint(4) NOT NULL,
  `created` int(31) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `parent_id`, `name`) VALUES
(1, 0, 'User'),
(2, 0, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE `room_type` (
  `id` int(10) NOT NULL,
  `type` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`id`, `type`) VALUES
(1, 'Entire'),
(2, 'Private'),
(3, 'Shared');

-- --------------------------------------------------------

--
-- Table structure for table `saved_neigh`
--

CREATE TABLE `saved_neigh` (
  `id` int(5) NOT NULL,
  `city_id` int(5) NOT NULL,
  `city` varchar(100) NOT NULL,
  `place_id` int(5) NOT NULL,
  `place` varchar(100) NOT NULL,
  `user_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `seasonalprice`
--

CREATE TABLE `seasonalprice` (
  `id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `start_date` int(31) NOT NULL,
  `end_date` int(31) NOT NULL,
  `currency` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(12) UNSIGNED NOT NULL,
  `code` varchar(100) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `setting_type` char(1) CHARACTER SET utf8 NOT NULL,
  `value_type` char(1) CHARACTER SET utf8 NOT NULL,
  `int_value` int(12) DEFAULT NULL,
  `string_value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_value` text CHARACTER SET utf8,
  `created` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `code`, `name`, `setting_type`, `value_type`, `int_value`, `string_value`, `text_value`, `created`) VALUES
(1, 'SITE_TITLE', 'Site Title', 'S', 'S', 0, 'tools4less', NULL, 1503909291),
(2, 'SITE_SLOGAN', 'Site Slogan', 'S', 'S', 0, 'Rent nightly from real people in 15,543 cities in 185 countries.', NULL, 2013),
(3, 'SITE_STATUS', 'Site status', 'S', 'I', 0, '', NULL, 2013),
(4, 'OFFLINE_MESSAGE', 'Offline Message', 'S', 'T', 0, '', 'Updation is going on...we will run this system very soon', 2013),
(5, 'SITE_ADMIN_MAIL', 'Site Admin Mail', 'S', 'S', NULL, 'systems@cogzidel.com', NULL, 1503909291),
(6, 'SITE_FB_API_ID', 'Site Facebook API ID', 'S', 'S', NULL, '', NULL, 1503909291),
(7, 'SITE_FB_API_SECRET', 'Site Facebook Secret Key', 'S', 'S', NULL, '', NULL, 1503909291),
(8, 'SITE_GOOGLE_API_ID', 'Site Google API Key', 'S', 'S', NULL, 'AIzaSyCEyPrLqbbtMuzjHsqxuuW9LSUWGQ5eh7c', NULL, 1503909291),
(9, 'FRONTEND_LANGUAGE', 'Frontend Language', 'S', 'S', 1, 'en', 'en', 2013),
(10, 'SITE_LOGO', 'Site Logo', 'S', 'S', NULL, 'newlogo.png', NULL, 2013),
(11, 'META_KEYWORD', 'Meta Keyword', 'S', 'S', NULL, 'Dropinn', NULL, 2013),
(12, 'META_DESCRIPTION', 'Meta Description', 'S', 'S', NULL, 'Dropinn - Airbnb Clone', NULL, 2013),
(13, 'HOW_IT_WORKS', 'How It Works', 'S', 'S', 0, 'video.mp4', '', 2013),
(14, 'GOOGLE_ANALYTICS_CODE', 'Google Analytics Code', 'S', 'S', NULL, NULL, '', 2013),
(15, 'BACKEND_LANGUAGE', 'Backend Language', 'S', 'S', 1, 'en', 'en', 0),
(16, 'SITE_TWITTER_API_ID', 'Site Twitter API ID', 'S', 'S', NULL, '', NULL, 1503909291),
(17, 'SITE_TWITTER_API_SECRET', 'Site Twitter Secret Key', 'S', 'S', NULL, '', NULL, 1503909291),
(18, 'DI_LICENSE_KEY', 'License Key', 'S', 'S', NULL, '', NULL, 1364016667),
(19, 'DI_LICENSE_LOCAL_KEY', 'License Local Key', 'S', 'T', NULL, NULL, 'YToxMDp7czo4OiJjdXN0b21lciI7YTo2OntzOjI6ImlkIjtzOjE6IjIiO3M6MTU6%0AInByaW1hcnlfdXNlcl9pZCI7czoxOiIxIjtzOjQ6Im5hbWUiO3M6MjoiQ1QiO3M6%0AMTA6InZhdF9udW1iZXIiO3M6MDoiIjtzOjY6InN0YXR1cyI7czo3OiJlbmFibGVk%0AIjtzOjc6ImNyZWF0ZWQiO3M6MTA6IjEzMTEzMzY4NDciO31zOjQ6InVzZXIiO2E6%0AMTp7aTowO2E6MTQ6e3M6MjoiaWQiO3M6MToiMSI7czoxMDoic2Vzc2lvbl9pZCI7%0AczozMjoiY2U4N2MzMGRkYTg5YTY5MTU2Mzk1ZjU3YTA0YzNkMjciO3M6MTE6Imxh%0Ac3RfbG9nZ2VkIjtzOjEwOiIxMzEzNTEwNDQyIjtzOjEwOiJmaXJzdF9uYW1lIjtz%0AOjU6ImtyaXNoIjtzOjk6Imxhc3RfbmFtZSI7czoxMjoiQ2hlbGxha2thbm51Ijtz%0AOjg6InBhc3N3b3JkIjtzOjMyOiJlMTBhZGMzOTQ5YmE1OWFiYmU1NmUwNTdmMjBm%0AODgzZSI7czo4OiJ1c2VybmFtZSI7czo0OiJiYWxhIjtzOjU6ImVtYWlsIjtzOjI1%0AOiJiYWxha3Jpc2huYW50bmpAZ21haWwuY29tIjtzOjE3OiJzZWN1cml0eV9xdWVz%0AdGlvbiI7czoyNDoiV2hhdCB3YXMgeW91ciBmaXJzdCBqb2I%2FIjtzOjI0OiJzZWN1%0Acml0eV9xdWVzdGlvbl9hbnN3ZXIiO3M6NjoiYWdyaXlhIjtzOjg6Im1heF9yb3dz%0AIjtzOjE6IjUiO3M6MjU6ImhlbHBkZXNrX2Zsb29kX3Byb3RlY3Rpb24iO3M6ODoi%0ARGlzYWJsZWQiO3M6NzoiY3JlYXRlZCI7czoxMDoiMTMxMTMzNjg0NyI7czo2OiJz%0AdGF0dXMiO3M6NzoiZW5hYmxlZCI7fX1zOjE4OiJsaWNlbnNlX2tleV9zdHJpbmci%0AO3M6MjA6IkRyb3BJbm4tMjYyOTIyOWYxYjllIjtzOjg6Imluc3RhbmNlIjthOjU6%0Ae3M6OToiZGlyZWN0b3J5IjthOjE6e2k6MTg3ODtzOjU0OiIvaG9tZS9jb2d6aWRl%0AbHRlbXAvcHVibGljX2h0bWwvZGVtby9jbGllbnQvZHJvcGlubi0xNjYiO31zOjY6%0AImRvbWFpbiI7YToyOntpOjE4NzU7czoyNjoiZGVtby5jb2d6aWRlbHRlbXBsYXRl%0Acy5jb20iO2k6MTg3NjtzOjMwOiJ3d3cuZGVtby5jb2d6aWRlbHRlbXBsYXRlcy5j%0Ab20iO31zOjI6ImlwIjthOjE6e2k6MTg3NztzOjEzOiIyMDguMTA5Ljg3LjU3Ijt9%0AczoxNToic2VydmVyX2hvc3RuYW1lIjthOjE6e2k6MTg3OTtzOjM2OiJpcC0yMDgt%0AMTA5LTg3LTQyLmlwLnNlY3VyZXNlcnZlci5uZXQiO31zOjk6InNlcnZlcl9pcCI7%0AYToxOntpOjE4ODA7czo5OiIxMjcuMC4wLjEiO319czo3OiJlbmZvcmNlIjthOjU6%0Ae2k6MDtzOjY6ImRvbWFpbiI7aToxO3M6MjoiaXAiO2k6MjtzOjk6ImRpcmVjdG9y%0AeSI7aTozO3M6MTU6InNlcnZlcl9ob3N0bmFtZSI7aTo0O3M6OToic2VydmVyX2lw%0AIjt9czoxMzoiY3VzdG9tX2ZpZWxkcyI7YToxOntzOjA6IiI7Tjt9czoyMzoiZG93%0AbmxvYWRfYWNjZXNzX2V4cGlyZXMiO3M6MTA6IjEzNTUxNDgyMTYiO3M6MTU6Imxp%0AY2Vuc2VfZXhwaXJlcyI7czo1OiJuZXZlciI7czoxNzoibG9jYWxfa2V5X2V4cGly%0AZXMiO3M6MTA6IjEzNzU4NTUxOTkiO3M6Njoic3RhdHVzIjtzOjY6ImFjdGl2ZSI7%0AfXtzcGJhc31hM2YwM2QyNTNiNmFmMmExZjQxYTJhNGQxMmI1NDE4NntzcGJhc304%0AZTQ1ZTcxYWU0NjIyY2YwZWNlMjJkM2U2OTAzMjY4Ng%3D%3D', 1364016667),
(31, 'BANNER_VIDEO', 'Home page banner video', 'S', 'S', 0, 'http://www.youtube.com/watch?v=ab0TSkLe-E0', NULL, 0),
(20, 'NEXMO_API_SECRET', 'Nexmo API Secret', 'S', 'T', 0, 'c0a428fd', NULL, 0),
(21, 'NEXMO_API_KEY', 'Nexmo API Key', 'S', 'T', 0, 'af6792d3', NULL, 0),
(22, 'NEXMO_API_PHONE_NO', 'Nexmo API Registered Phone number', 'S', 'S', 0, '919597944025', NULL, 0),
(23, 'FAVICON_IMAGE', 'favicon image', 'S', 'S', NULL, 'logo.png', NULL, 2014),
(24, 'cloud_name', 'Cloud name', 'S', 'S', NULL, 'tools4less', NULL, 1503909642),
(25, 'cloud_api_secret', 'cloud_api_secret', 'S', 'S', NULL, 'mPE8F4Gel1Y0CMVbREncjbdorD0', NULL, 1503909291),
(26, 'cloud_api_key', 'cloud_api_key', 'S', 'S', 0, '783225867774139', NULL, 1503909291),
(27, 'sinch_name', 'Sinch name', 'S', 'S', NULL, '1', NULL, 1503909291),
(28, 'sinch_api_secret', 'sinch_api_secret', 'S', 'S', NULL, '1', NULL, 1503909291),
(29, 'sinch_api_key', 'sinch_api_key', 'S', 'S', 0, '1', NULL, 1503909291),
(30, 'SITE_GOOGLE_CLIENT_ID', 'Site Google Client Id', 'S', 'S', 0, '', NULL, 1503909291),
(32, 'MANAGE_VIDEO', 'manage_video', 'S', 'S', 0, 'home.webm', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings_extra`
--

CREATE TABLE `settings_extra` (
  `id` int(12) UNSIGNED NOT NULL,
  `code` varchar(100) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `setting_type` char(1) CHARACTER SET utf8 NOT NULL,
  `value_type` char(1) CHARACTER SET utf8 NOT NULL,
  `int_value` int(12) DEFAULT NULL,
  `string_value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_value` text CHARACTER SET utf8,
  `created` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE `statistics` (
  `id` int(25) NOT NULL,
  `list_id` int(25) NOT NULL,
  `page_view` int(25) NOT NULL,
  `date` int(25) NOT NULL,
  `month` varchar(100) NOT NULL,
  `year` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `theme_select`
--

CREATE TABLE `theme_select` (
  `id` int(11) NOT NULL,
  `color` varchar(25) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `theme_select`
--

INSERT INTO `theme_select` (`id`, `color`, `status`) VALUES
(1, 'red', 1),
(2, 'orange', 0),
(3, 'pink', 0),
(4, 'green', 0),
(5, 'yellow', 0);

-- --------------------------------------------------------

--
-- Table structure for table `toplocations`
--

CREATE TABLE `toplocations` (
  `id` int(11) NOT NULL,
  `category_id` smallint(6) NOT NULL,
  `name` varchar(111) NOT NULL,
  `search_code` varchar(111) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toplocations`
--

INSERT INTO `toplocations` (`id`, `category_id`, `name`, `search_code`) VALUES
(1, 1, 'Delhi', 'Delhi'),
(2, 1, 'Mumbai', 'Mumbai'),
(3, 1, 'Bangalore', 'Bangalore'),
(4, 1, 'Hyderabad', 'Hyderabad'),
(5, 1, 'Ahmedabad', 'Ahmedabad'),
(6, 1, 'Chennai', 'Chennai'),
(7, 1, 'Kolkata', 'Kolkata'),
(8, 1, 'Pune', 'Pune'),
(9, 2, 'New York', 'New York'),
(10, 2, 'San Francisco', 'San Francisco'),
(11, 2, 'London', 'London'),
(12, 2, 'Paris', 'Paris'),
(13, 2, 'Barcelona', 'Barcelona'),
(14, 2, 'Rome', 'Rome'),
(15, 2, 'Berlin', 'Berlin'),
(16, 2, 'Amsterdam', 'Amsterdam');

-- --------------------------------------------------------

--
-- Table structure for table `toplocation_categories`
--

CREATE TABLE `toplocation_categories` (
  `id` smallint(6) NOT NULL,
  `name` varchar(111) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toplocation_categories`
--

INSERT INTO `toplocation_categories` (`id`, `name`) VALUES
(1, 'India'),
(2, 'World');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '1',
  `ref_id` varchar(50) NOT NULL,
  `fb_id` bigint(20) NOT NULL,
  `twitter_id` bigint(20) NOT NULL,
  `google_id` bigint(20) NOT NULL,
  `file1` varchar(255) NOT NULL,
  `file2` varchar(255) NOT NULL,
  `coupon_code` varchar(50) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `via_login` varchar(25) NOT NULL,
  `facebook_verify` varchar(10) DEFAULT '0',
  `facebook_email` varchar(25) NOT NULL,
  `google_verify` varchar(10) DEFAULT '0',
  `google_email` varchar(25) NOT NULL,
  `email_verify` varchar(10) DEFAULT '0',
  `phone_verify` varchar(10) NOT NULL DEFAULT '0',
  `email_verification_code` varchar(50) DEFAULT '0',
  `phone_verification_code` int(10) NOT NULL,
  `referral_code` varchar(15) NOT NULL,
  `ref_total` bigint(20) NOT NULL,
  `ref_trip` bigint(20) NOT NULL,
  `ref_rent` bigint(20) NOT NULL,
  `refer_userid` varchar(100) NOT NULL,
  `trips_referral_code` varchar(15) NOT NULL,
  `list_referral_code` varchar(15) NOT NULL,
  `referral_amount` int(10) NOT NULL,
  `timezone` varchar(11) NOT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `newpass` varchar(34) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `newpass_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `newpass_time` datetime DEFAULT NULL,
  `last_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_login` int(31) NOT NULL,
  `created` int(31) NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `photo_status` int(11) NOT NULL,
  `shortlist` text NOT NULL,
  `superhost` tinyint(4) NOT NULL,
  `file1_path` varchar(500) NOT NULL,
  `file2_path` varchar(500) NOT NULL,
  `onesignal_id` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `ref_id`, `fb_id`, `twitter_id`, `google_id`, `file1`, `file2`, `coupon_code`, `username`, `password`, `email`, `via_login`, `facebook_verify`, `facebook_email`, `google_verify`, `google_email`, `email_verify`, `phone_verify`, `email_verification_code`, `phone_verification_code`, `referral_code`, `ref_total`, `ref_trip`, `ref_rent`, `refer_userid`, `trips_referral_code`, `list_referral_code`, `referral_amount`, `timezone`, `banned`, `ban_reason`, `newpass`, `newpass_key`, `newpass_time`, `last_ip`, `last_login`, `created`, `modified`, `photo_status`, `shortlist`, `superhost`, `file1_path`, `file2_path`, `onesignal_id`) VALUES
(1, 2, '21232f297a57a5a743894a0e4a801fc3', 0, 0, 0, '', '', '53738', 'admin', '$1$3yO8IpDW$GM9HnZ964w9taXrQK1dSx0', 'systems@cogzidel.com', '', '0', '', '0', '', '0', '0', '0', 0, '', 0, 0, 0, '', '', '', 0, '', 0, NULL, NULL, NULL, NULL, '27.250.3.186', 1503909555, 1503889491, '2017-08-28 08:39:15', 0, '1,2', 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_autologin`
--

CREATE TABLE `user_autologin` (
  `key_id` char(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `user_id` mediumint(8) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_notification`
--

CREATE TABLE `user_notification` (
  `id` bigint(11) NOT NULL,
  `periodic_offers` smallint(5) NOT NULL,
  `company_news` smallint(5) NOT NULL,
  `upcoming_reservation` smallint(5) NOT NULL,
  `new_review` smallint(5) NOT NULL,
  `leave_review` smallint(5) NOT NULL,
  `standby_guests` smallint(5) NOT NULL,
  `rank_search` smallint(5) NOT NULL,
  `user_id` smallint(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_notification`
--

INSERT INTO `user_notification` (`id`, `periodic_offers`, `company_news`, `upcoming_reservation`, `new_review`, `leave_review`, `standby_guests`, `rank_search`, `user_id`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_temp`
--

CREATE TABLE `user_temp` (
  `id` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(34) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `activation_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_wishlist`
--

CREATE TABLE `user_wishlist` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `list_id` int(10) NOT NULL,
  `wishlist_id` int(10) NOT NULL,
  `note` text NOT NULL,
  `created` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `privacy` int(1) NOT NULL,
  `created` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accept_pay`
--
ALTER TABLE `accept_pay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_key`
--
ALTER TABLE `admin_key`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `amnities`
--
ALTER TABLE `amnities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calendar`
--
ALTER TABLE `calendar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cancellation_policy`
--
ALTER TABLE `cancellation_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_info`
--
ALTER TABLE `contact_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_users`
--
ALTER TABLE `coupon_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_converter`
--
ALTER TABLE `currency_converter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discover`
--
ALTER TABLE `discover`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_settings`
--
ALTER TABLE `email_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `google_analytics`
--
ALTER TABLE `google_analytics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help`
--
ALTER TABLE `help`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `host_cancellation_penalty`
--
ALTER TABLE `host_cancellation_penalty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `host_cancellation_policy`
--
ALTER TABLE `host_cancellation_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ical_import`
--
ALTER TABLE `ical_import`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Invoice`
--
ALTER TABLE `Invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `joinus`
--
ALTER TABLE `joinus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `list`
--
ALTER TABLE `list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `list_pay`
--
ALTER TABLE `list_pay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `list_photo`
--
ALTER TABLE `list_photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_history`
--
ALTER TABLE `login_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_photo`
--
ALTER TABLE `map_photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_type`
--
ALTER TABLE `message_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metas`
--
ALTER TABLE `metas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neighbor_area`
--
ALTER TABLE `neighbor_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neighbor_city`
--
ALTER TABLE `neighbor_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neigh_category`
--
ALTER TABLE `neigh_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neigh_city`
--
ALTER TABLE `neigh_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neigh_city_place`
--
ALTER TABLE `neigh_city_place`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neigh_knowledge`
--
ALTER TABLE `neigh_knowledge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neigh_photographer`
--
ALTER TABLE `neigh_photographer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neigh_place_category`
--
ALTER TABLE `neigh_place_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neigh_post`
--
ALTER TABLE `neigh_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neigh_tag`
--
ALTER TABLE `neigh_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_popup`
--
ALTER TABLE `page_popup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_details`
--
ALTER TABLE `payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payout_preferences`
--
ALTER TABLE `payout_preferences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paywhom`
--
ALTER TABLE `paywhom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD KEY `email` (`email`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_type`
--
ALTER TABLE `property_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recommends`
--
ALTER TABLE `recommends`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reference_request`
--
ALTER TABLE `reference_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referrals_amount`
--
ALTER TABLE `referrals_amount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referrals_booking`
--
ALTER TABLE `referrals_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_management`
--
ALTER TABLE `referral_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refund`
--
ALTER TABLE `refund`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saved_neigh`
--
ALTER TABLE `saved_neigh`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seasonalprice`
--
ALTER TABLE `seasonalprice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_extra`
--
ALTER TABLE `settings_extra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme_select`
--
ALTER TABLE `theme_select`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toplocations`
--
ALTER TABLE `toplocations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toplocation_categories`
--
ALTER TABLE `toplocation_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_autologin`
--
ALTER TABLE `user_autologin`
  ADD PRIMARY KEY (`key_id`,`user_id`);

--
-- Indexes for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_temp`
--
ALTER TABLE `user_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_wishlist`
--
ALTER TABLE `user_wishlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accept_pay`
--
ALTER TABLE `accept_pay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_key`
--
ALTER TABLE `admin_key`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `amnities`
--
ALTER TABLE `amnities`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `calendar`
--
ALTER TABLE `calendar`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cancellation_policy`
--
ALTER TABLE `cancellation_policy`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact_info`
--
ALTER TABLE `contact_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coupon_users`
--
ALTER TABLE `coupon_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `currency_converter`
--
ALTER TABLE `currency_converter`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `discover`
--
ALTER TABLE `discover`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_settings`
--
ALTER TABLE `email_settings`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `google_analytics`
--
ALTER TABLE `google_analytics`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `help`
--
ALTER TABLE `help`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `host_cancellation_penalty`
--
ALTER TABLE `host_cancellation_penalty`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `host_cancellation_policy`
--
ALTER TABLE `host_cancellation_policy`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ical_import`
--
ALTER TABLE `ical_import`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `joinus`
--
ALTER TABLE `joinus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `list`
--
ALTER TABLE `list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `list_pay`
--
ALTER TABLE `list_pay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `list_photo`
--
ALTER TABLE `list_photo`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `login_history`
--
ALTER TABLE `login_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `map_photo`
--
ALTER TABLE `map_photo`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `message_type`
--
ALTER TABLE `message_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `metas`
--
ALTER TABLE `metas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `neighbor_area`
--
ALTER TABLE `neighbor_area`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `neighbor_city`
--
ALTER TABLE `neighbor_city`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `neigh_category`
--
ALTER TABLE `neigh_category`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `neigh_city`
--
ALTER TABLE `neigh_city`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `neigh_city_place`
--
ALTER TABLE `neigh_city_place`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `neigh_knowledge`
--
ALTER TABLE `neigh_knowledge`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `neigh_photographer`
--
ALTER TABLE `neigh_photographer`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `neigh_place_category`
--
ALTER TABLE `neigh_place_category`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `neigh_post`
--
ALTER TABLE `neigh_post`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `neigh_tag`
--
ALTER TABLE `neigh_tag`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `page_popup`
--
ALTER TABLE `page_popup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `payment_details`
--
ALTER TABLE `payment_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `payout_preferences`
--
ALTER TABLE `payout_preferences`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `property_type`
--
ALTER TABLE `property_type`
  MODIFY `id` int(63) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `recommends`
--
ALTER TABLE `recommends`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reference_request`
--
ALTER TABLE `reference_request`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `referrals_amount`
--
ALTER TABLE `referrals_amount`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `referrals_booking`
--
ALTER TABLE `referrals_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `refund`
--
ALTER TABLE `refund`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `saved_neigh`
--
ALTER TABLE `saved_neigh`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seasonalprice`
--
ALTER TABLE `seasonalprice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `settings_extra`
--
ALTER TABLE `settings_extra`
  MODIFY `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `statistics`
--
ALTER TABLE `statistics`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `theme_select`
--
ALTER TABLE `theme_select`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `toplocations`
--
ALTER TABLE `toplocations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `toplocation_categories`
--
ALTER TABLE `toplocation_categories`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_notification`
--
ALTER TABLE `user_notification`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_temp`
--
ALTER TABLE `user_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_wishlist`
--
ALTER TABLE `user_wishlist`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
