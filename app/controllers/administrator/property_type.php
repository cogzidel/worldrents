<?php
/**
 * DROPinn Admin List Controller Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		DROPinn
 * @subpackage	Controllers
 * @category	Admin property type
 * @author		Cogzidel Product Team
 * @version		Version 1.6
 * @link		http://www.cogzidel.com
 
 */
class Property_type extends CI_Controller
{
	function Property_type()
	{
		parent::__construct();
		
		$this->load->library('Table');
		$this->load->library('Pagination');
		$this->load->library('form_validation');
		
		$this->load->helper('form');
		$this->load->helper('url');
 	$this->load->helper('file');
		
		$this->load->model('Users_model');
		$this->load->model('Rooms_model');

		$this->path = realpath(APPPATH . '../images');	
		
		// Protect entire controller so only admin, 
		// and users that have granted role in permissions table can access it.
		$this->dx_auth->check_uri_permissions();
	}
	
	function index()
	{
		$query = $this->db->get('list');
 
		// Get offset and limit for page viewing
		$start = (int) $this->uri->segment(4,0);
		
	 // Number of record showing per page
		$row_count = 10;
		
		if($start > 0)
		   $offset			 = ($start-1) * $row_count;
		else
		   $offset			 =  $start * $row_count; 
		
		
		// Get all users
		$data['users'] = $this->db->order_by('id','asc')->get('list', $row_count, $offset)->result();
		
		// Pagination config
		$p_config['base_url']    = admin_url('property_type/index');
		$p_config['uri_segment'] = 4;
		$p_config['num_links']   = 5;
		$p_config['total_rows']  = $query->num_rows();
		$p_config['per_page']    = $row_count;
				
		// Init pagination
		$this->pagination->initialize($p_config);		
		// Create pagination links
		$data['pagination'] = $this->pagination->create_links2();
		
		
	$data['message_element'] = "administrator/view_lists";
	$this->load->view('administrator/admin_template', $data);
	}
	

public function view_all_property()
	{	
		//Get Groups
		 $this->load->model('property_model');
			$data['property']	=	$this->property_model->getproperty();
		
		//$data['area']   =   $this->place_model->getplace1();
		
		//Load View	
	 $data['message_element'] = "administrator/property_type/view_property";
		$this->load->view('administrator/admin_template', $data);
	   
	}

	function view_property()
	{


	$data['message_element'] = "administrator/view_add_property";
	$this->load->view('administrator/admin_template', $data);

	}
	
	
public function editproperty()
	{		
	
	$this->load->model('property_model');

	
		//Get id of the category	
	 $id = is_numeric($this->uri->segment(4))?$this->uri->segment(4):0;
		
		//Intialize values for library and helpers	
		$this->form_validation->set_error_delimiters($this->config->item('field_error_start_tag'), $this->config->item('field_error_end_tag'));
		
		if($this->input->post('submit'))
		{	
           	//Set rules
			$this->form_validation->set_rules('type','Type','required|trim|xss_clean');
						
			if($this->form_validation->run())
			{	
				  //prepare update data
				  $updateData                  	  	= array();	
			   $updateData['type']  		    = $this->input->post('type');
						
				  $check = $this->db->where('type',$updateData['type'])->get('property_type');
  
  if($check->num_rows() != 0)
  {

  	$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('Please give different one, its already entered.')));
	redirect_admin('property_type/editproperty/'.$id);
  }
				  $check_data = $this->db->where('id',$this->uri->segment(4))->get('property_type');
				  
				  if($check_data->num_rows() == 0)
				  {
				  	$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('This property type is already deleted.')));
				  	redirect_admin('property_type/view_all_property');
				  }
				  
				  //Edit Faq Category
				  $updateKey 							= array('property_type.id'=>$this->uri->segment(4));
				  
				  $this->property_model->updateproperty($updateKey,$updateData);
				  $old=$this->input->post('old');
				  $data=$this->db->where('type',$old)->update('sub_categories',$updateData);
				  // echo $old;exit;
				  if($data)	
				  {
				  //Notification message
				  $this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('success',translate_admin('Property type updated successfully')));
				  redirect_admin('property_type/view_all_property');
				}
		 	} 
		} //If - Form Submission End
		
		//Set Condition To Fetch The Faq Category
		$condition = array('property_type.id'=>$id);
			
	 //Get Groups
		$data['property']	=	$this->property_model->getproperty($condition);

         if($data['property']->num_rows() == 0)
          {
          	$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('This Property type is already deleted.')));
          	redirect_admin('property_type/view_all_property');
          }
			//Load View	
	 $data['message_element'] = "administrator/property_type/edit_property";
		$this->load->view('administrator/admin_template', $data);
   
	}

	
	
	public function delete_property()
	{	
	$this->load->model('property_model');
	$id = $this->uri->segment(4,0);
		
	if($id == 0)	
	{
		$getproperty	 =	$this->property_model->getproperty();
		$propertylist  =   $this->input->post('propertylist');
		if(!empty($propertylist))
		{	
				foreach($propertylist as $res)
				 {
					$condition = array('property_type.id'=>$res);
					$this->property_model->deleteproperty(NULL,$condition);
				 }
			} 
		else
		{
		$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('Please select Property type')));
	 redirect_admin('property_type/view_all_property');
		}
	}
	else
	{
		$getproperty	 =	$this->property_model->getproperty();
		$result = $this->db->where('property_id',$id)->get('list');
		if($result->num_rows() != 0)
		{
			//Notification message
		$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('This property type is used by some lists.')));
		redirect_admin('property_type/view_all_Property');
		}
		if($getproperty->num_rows() > 4)
		{
	$condition = array('property_type.id'=>$id);
	$this->property_model->deleteproperty(NULL,$condition);
	//Notification message
		$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('success',translate_admin('Property deleted successfully')));
		redirect_admin('property_type/view_all_Property');
	}
		else {
			//Notification message
		$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin("Minimum property type is 3. So you aren't able to delete.")));
		redirect_admin('property_type/view_all_Property');
		}
	}		
		//Notification message
		$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('success',translate_admin('Property deleted successfully')));
		redirect_admin('property_type/view_all_Property');
	}

    function addproperty()
  {
  $prop = $this->input->post('addproperty'); 
  $property=trim($prop);
   
  if(empty($property))
			{
			 $this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('Sorry, You have to select atleast one!')));
				redirect_admin('property_type');
			}else
			{
			$nul ="NULL";
			$data = array(
											'id'         => NULL,
											'type'       => $this->input->post('addproperty')
											
											);
			$this->Common_model->insertData('property_type',$data);
		
			echo "<p>Additional Property type added successfully</p>";
			
			}
			
  }
  
  
    function addproperties()
  {
  $prop = $this->input->post('addproperty'); 
  $property=trim($prop);
  
   $check = $this->db->where('type',$property)->get('property_type');
  
  if($check->num_rows() != 0)
  {
  	$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('Please give different one, its already entered.')));
	redirect_admin('property_type/view_property');
  }
  
  if(empty($property))
			{
			 $this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('Sorry, You have to fill all fields!')));
				redirect_admin('property_type/view_property');
			}else
			{
			$nul ="NULL";
			$data = array(
											'id'         => NULL,
											'type'       => $this->input->post('addproperty')
											
											);
			$this->Common_model->insertData('property_type',$data);
			
			 $this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('success',translate_admin('Property type added successfully!')));
			redirect_admin('property_type/view_all_property');
			
			}
			
  }
  

  public function view_all_category()
	{	
		//Get Groups
		 // $this->load->model('sub_categories');
		$data['sub_cat'] = $this->db->get('sub_categories');
			// $data['property']	=	$this->property_model->getproperty();
		
		//$data['area']   =   $this->place_model->getplace1();
		
		//Load View	
	 $data['message_element'] = "administrator/property_type/view_category";
		$this->load->view('administrator/admin_template', $data);
	   
	}

	public function delete_category($id)
	{	
	$id = $this->uri->segment(4,0);
	$this->db->where('id',$id)->delete('sub_categories');
	//Notification message
		$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('success',translate_admin('Subcategory deleted successfully')));
		redirect_admin('property_type/view_all_category');
	}

	public function editcategory()
	{		
		// $this->load->model('property_model');
	
	 $id = $this->uri->segment(4,0);
	 $this->form_validation->set_error_delimiters($this->config->item('field_error_start_tag'), $this->config->item('field_error_end_tag'));
		
		if($this->input->post('submit'))
		{	
           	//Set rules
			$this->form_validation->set_rules('type','Type','required|trim|xss_clean');
            $this->form_validation->set_rules('subcategory','subcategory','required|trim|xss_clean');						
			if($this->form_validation->run())
			{	
				  //prepare update data
				  $updateData                  	  	= array();	
			   $updateData['type']  		    = $this->input->post('type');
			   $updateData['sub_categorie']  		    = $this->input->post('subcategory');
					// $updateKey 							= array('sub_categories.id'=>$this->uri->segment(4));
				  
				 $data=$this->db->where('id',$id)->update('sub_categories',$updateData);
				  if($data)
				  {
				  	$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('success',translate_admin('Subcategory updated successfully')));
				  redirect_admin('property_type/view_all_category');
				  }
				}
			}
	$data['property']=$this->db->where('id',$id)->get('sub_categories');
	 $data['message_element'] = "administrator/property_type/edit_category";
		$this->load->view('administrator/admin_template', $data);
}
	
	function view_category()
	{

   $data['property']=$this->db->get('property_type');
	$data['message_element'] = "administrator/view_add_category";
	$this->load->view('administrator/admin_template', $data);

	}
	
		 function addsubcategories()
  {
  $prop = $this->input->post('addproperty'); 
  $property=trim($prop);
  $sub_cat = $this->input->post('sub_cat'); 
  $sub_category=trim($sub_cat);
  
   $check = $this->db->where('sub_categorie',$sub_category)->get('sub_categories');
  
  if($check->num_rows() != 0)
  {
  	$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('Please give different one, its already entered.')));
	redirect_admin('property_type/view_category');
  }
  
  if(empty($sub_category))
			{
			 $this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('Sorry, You have to fill all fields!')));
				redirect_admin('property_type/view_category');
			}else
			{
			$nul ="NULL";
			$data = array(
											'id'         => NULL,
											'type'       => $this->input->post('addproperty'),
											'sub_categorie'       => $this->input->post('sub_cat'),
											
											);
			$this->Common_model->insertData('sub_categories',$data);
			
			 $this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('success',translate_admin('Subcategory added successfully!')));
			redirect_admin('property_type/view_all_category');
			
			}
			

  }
  public function delete_category1()
	{	
	$this->load->model('property_model');
	$id = $this->uri->segment(4,0);
		
	if($id == 0)	
	{
		$getproperty	 =	$this->property_model->getcategory();
		$propertylist  =   $this->input->post('propertylist');
		if(!empty($propertylist))
		{	
				foreach($propertylist as $res)
				 {
					$condition = array('sub_categories.id'=>$res);
					$this->property_model->deletecategory(NULL,$condition);
				 }
			} 
		else
		{
		$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('Please select Subcategory')));
	 redirect_admin('property_type/view_all_category');
		}
	}
	else
	{
		$getproperty	 =	$this->property_model->getcategory();
		$result = $this->db->where('property_id',$id)->get('list');
		if($result->num_rows() != 0)
		{
			//Notification message
		$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin('This property type is used by some lists.')));
		redirect_admin('property_type/view_all_category');
		}
		if($getproperty->num_rows() > 4)
		{
	$condition = array('property_type.id'=>$id);
	$this->property_model->deletecategory(NULL,$condition);
	//Notification message
		$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('success',translate_admin('Subcategory deleted successfully')));
		redirect_admin('property_type/view_all_category');
	}
		else {
			//Notification message
		$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('error',translate_admin("Minimum Subcategory type is 3. So you aren't able to delete.")));
		redirect_admin('property_type/view_all_category');
		}
	}		
		//Notification message
		$this->session->set_flashdata('flash_message', $this->Common_model->admin_flash_message('success',translate_admin('Subcategory deleted successfully')));
		redirect_admin('property_type/view_all_category');
	}

  
	

	
}
?>
