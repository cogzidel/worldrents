<?php
/**
 * Dropinn system page Class
 *
 * Permits admin to handle the FAQ System of the site
 *
 * @package		Dropinn
 * @subpackage	Controllers
 * @category	Skills 
 * @author		Cogzidel Product Team
 * @version		Version 1.6
 * @created		December 22 2008
 * @link		http://www.cogzidel.com
 
 */
class Manage_cron extends CI_Controller
{
	function Manage_cron()
	{
		parent::__construct();
		
		$this->load->library('Table');
		$this->load->library('Pagination');
		$this->load->library('DX_Auth');
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->helper('url');
 		$this->load->helper('file');
				
		$this->load->model('Users_model');	
	    $this->load->model('Contacts_model');		
		
		// Protect entire controller so only admin, 
		// and users that have granted role in permissions table can access it.
		$this->dx_auth->check_uri_permissions();
	}
	
	function index()
	{
		if (!$this->dx_auth->is_logged_in())
		{
			redirect_admin('login','refresh');
		}else {
			$data['crons'] = $this->db->select('*')->get('cron');
			$data['message_element'] = "administrator/view_manage_cron";
			$this->load->view('administrator/admin_template', $data);
		}			
	}
}
?>