<script src="http://malsup.github.com/jquery.form.js"></script>
<div class="container-fluid top-sp body-color">
	<div class="load_div">
		 <div class="loader"></div> 
		 <i class="fa fa-check loader-tick" aria-hidden="true"></i>
	</div>
	<style>
	.loader-tick{
		color: #002e42;
		font-size: 110px;
		display:none;
		left: 45%;
	    position: absolute;
	    top: 50%;
	}
		.loader {
   -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    animation: 2s linear 0s normal none infinite running spin;
    background: #fff none repeat scroll 0 0;
    border-color:#002e42 #f3f3f3;
    border-image: none;
    border-radius: 50%;
    border-style: solid;
    border-width: 16px;
    height: 120px;
    left: 45%;
    position: absolute;
    top: 50%;
    width: 120px;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
.load_div {
	display:none;	
    background: #ffff none repeat scroll 0 0;
    height: 100%;
    opacity: 0.93;
    position: absolute;
    width: 98%;
    z-index: 99;
}
.search {
 	background-color: white;
    background-image: url("<?php echo base_url() ?>Cloud_data/images/search-icon.png");
    background-size: 20px auto;
    background-position: 10px 10px;
    background-repeat: no-repeat;
    border: 2px solid #ccc !important;
    border-radius: 4px !important;
    box-sizing: border-box;
    font-size: 16px !important;
    height: 45px !important;
    padding: 12px 20px 12px 40px !important;
    transition: width 0.4s ease-in-out 0s;
    width: 100% !important;
}

/* When the input field gets focus, change its width to 100% */
.search:focus {
	box-shadow: 5px 5px 6px -4px #000 !important;
    width: 100%;
}
.search_div, .nav-div {
    margin-bottom: 20px;
    position: relative;
}
<?php if(isset($_GET['search'])) { ?>
	#<?php echo $_GET['search'] ?> {
    animation: blinker 1s linear infinite;
}
@keyframes blinker {  
  50% { opacity: 0; }
}
<?php }?>

.tooltip {
    display: none;
    padding: 10px;
}

.input:hover .tooltip {
    background: #86b4c8;
    border-radius: 3px;
    bottom: -105px;
    color: white;
    display: inline;
    height: auto;
    left: 20px;
    line-height: 30px;
    opacity:1;
    position: absolute;
    width: auto;
    line-height: 1.5;
    top:100%;
}
@media screen and (max-width: 480px)  {
.input:hover .tooltip {
	 bottom: -182px !important;
}
}
@media screen and (min-width: 768px) and (max-width: 800px) {
.input:hover .tooltip {
	 bottom: -122px !important;
}
}
.input:hover .tooltip:before {
    display: block;
    content: "";
    position: absolute;
    top: -5px;
    width: 0; 
	height: 0; 
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	opacity:1;
	border-bottom: 5px solid #86b4c8;
}
	</style>
	<div class="">
	<div class="col-xs-12 col-md-12 col-sm-12">
	 <h1 class="page-header1"><?php echo translate_admin('Manage static images'); ?></h1>
	 </div>
	 <div class="col-xs-12 col-md-12 col-sm-12 nav-div">
	 <nav>
		<a href="<?php echo admin_url().'/settings/manage_image' ?>">Manage Image</a> |
		<?php
		if(isset($_GET['folder'])){ ?>
		<a href="<?php echo admin_url().'/settings/manage_image?folder='.$_GET['folder'] ?>"><?php echo $_GET['folder'] ?></a> |	
		<?php } 
		?>
		<?php
		if(isset($_GET['file'])){
		if(isset($_GET['folder'])){ ?>
		<a href="<?php echo admin_url().'/settings/manage_image?file='.$_GET['file'].'&folder='.$_GET['folder'] ?>"><?php echo $_GET['file'] ?></a>	
		<?php }else{ ?>
		<a href="<?php echo admin_url().'/settings/manage_image?file='.$_GET['file']?>"><?php echo $_GET['file'] ?></a>	
		<?php }		
		 ?>
		<?php } 
		?>
		</nav>
		</div>
	 <div class="col-xs-12 col-md-12 col-sm-12 search_div input">
	 	<?php $get_search = $this->db->get('file_directory')->result(); ?>	
	  <input list="search" oninput='onInput()' type="text" class="search" id="search_input" name="search" placeholder="Search.."> 
	   <span class="tooltip"><b>How To search ?</b></br>
	   	Type a page URL on search box.</br>
	   	Search box show the exact suggestion based on your search keywords.</br>
	   	Give search keywords should contain correct page URL</br>
	   	Examble <?php echo base_url() ?>'info/how_it_works', text box should give only after the base path "info/how_it_works" 
	   </span>
	  <datalist id="search">
	  	<?php foreach($get_search as $getsearch){  ?>
	  <option data-customvalue="<?php echo $getsearch->file ?>"  data-customsearch="<?php echo $getsearch->redirect_url ?>" value="<?php echo $getsearch->page_url ?>"><?php echo $getsearch->page_url ?></option>
	  	<?php } ?>
	</datalist>
	  </div> 
	  <script>
	  
	  $(document).ready(function()
	  {
	  	var divs = $(".allfiles");
		for(var i = 0; i < divs.length; i+=3) {
		divs.slice(i, i+3).wrapAll("<div class='new col-xs-12' style='padding:0px;'></div>");
		}
	  })	
	  
	  	function onInput() {
    var val = document.getElementById("search_input").value;
    var opts = document.getElementById('search').childNodes;
    for (var i = 0; i < opts.length; i++) {
      if (opts[i].value === val) {
      	var file =$('#search [value="' + opts[i].value + '"]').data('customvalue');
      	var redirect =$('#search [value="' + opts[i].value + '"]').data('customsearch');
      	$('#search_input').val('');
      	if(redirect == 'settings/manage_image'){
      	window.location.href = '<?php echo admin_url().'/' ?>'+redirect+'?search='+file;	
      	}else{
      	window.location.href = '<?php echo admin_url().'/' ?>'+redirect+'&search='+file;	
      	}
        
        break;
      }
    }
  }
	  </script>
	 
	<?php
	if(isset($files)){
	unset($files[0]);
	unset($files[1]);
	foreach($files as $file){ 
	if( strpos( $file, '.' ) == false ) { ?>
	<div class="col-xs-12 col-sm-4 allfiles">	
	<label class="folder_list"><a href="<?php echo admin_url().'/settings/manage_image?folder='.$file ?>"><?php echo $file ?></a><p>Folder</p></label>	
	
	</div>	
	<?php } ?>
		
	<?php } ?>
	<div class="col-xs-12 margin-set"></div>
	<?php foreach($files as $file){ 
	if( strpos( $file, '.' ) !== false ) { ?>
	<div class="col-xs-12 col-sm-4 allfiles">	
	<?php 
	if(isset($folder)){ ?>	
	<label id="<?php echo str_replace('.php', '', $file) ?>" class="files_list"><a href="<?php echo admin_url().'/settings/manage_image?file='.$file.'&folder='.$folder ?>"><?php echo $file ?></a><p>File</p></label>
	<?php }else{ ?> 
	<label id="<?php echo str_replace('.php', '', $file) ?>" class="files_list"><a href="<?php echo admin_url().'/settings/manage_image?file='.$file ?>"><?php echo $file ?></a><p>File</p></label>
	<?php } ?>
	</div>	
	<?php } ?>
		
	<?php } }else if(isset($url)){ ?>
	<div class="col-xs-12 no_image_div">	
	<div class="col-xs-12 alert alert-danger">
		<strong>Warning!</strong> There is no static images on this page.
	</div></div>	
	<?php 	$i = 0; 
	foreach($url->find('img') as $element){
		if( strpos( $element->src, '$' ) == false ){
		if( strpos( $element->src, 'Cloud_data' ) !== false ){ ?>
		<div class="col-xs-12 col-sm-3">	
			<script>
				$(document).ready(function(){
					$('.no_image_div').hide();
				})
			</script>
	<?php 
	$image = base_url().$element->src;
	if( strpos( $element->src, 'http' ) == false || strpos( $element->src, 'https' ) == false  ){
	if( strpos( $element, 'css_url()' ) !== false){
		$image = css_url().$element->src;
		$image = remove_character_image_manage($image);
	}else{
		$image = base_url().$element->src;
		$image = remove_character_image_manage($image);
	}
	}else{
		$image = $element->src;
		$image = remove_character_image_manage($image);
	}
	
	?>	
	<img src="<?php echo str_replace("<?php echo base_url(); ?>","",$image) ?>" class="list_image" /> 
	<form id="form_<?php echo $i ?>" action="<?php echo admin_url() ?>/settings/change_file_image" method="post" enctype="multipart/form-data">
	<input type="hidden" name="file_path" value="<?php echo $image ?>" />
	<input type="file" id="file" name="file" class="change_file" onchange="UploadFile(this,<?php echo $i ?>);" />
	<button class="change_image"><i class="fa fa-file-image-o" aria-hidden="true"></i>
		<p class="change_text">Change Image</p>
	</button></form>
	</div>		
		<?php }else{ ?> 
		<div class="col-xs-12 col-sm-3">	
			<script>
				$(document).ready(function(){
					$('.no_image_div').hide();
				})
			</script>
	<?php 
	if( strpos( $element->src, 'http' ) == false || strpos( $element->src, 'https' ) == false  ){
	if( strpos( $element->src, 'css_url()' ) !== false){
		$image = css_url().$element->src;
		$image = remove_character_image_manage($image);
		$image = remove_imgext_cld($image);
	}elseif( strpos( $element->src, 'cdn_url_images()' ) !== false){
		$image = cdn_url_images().$element->src;
		$image = remove_character_image_manage($image);
		$image = remove_imgext_cld($image);
	}else{
		$image = base_url().$element->src;
		$image = remove_character_image_manage($image);
		$image = remove_imgext_cld($image);
	}
	}else{
		$image = $element->src;
		$image = remove_character_image_manage($image);
		$image = remove_imgext_cld($image);
	}
	
	?>	
	<img src="<?php echo $image ?>" class="list_image" /> 
	<form id="form_<?php echo $i ?>" action="<?php echo admin_url() ?>/settings/change_clud_image" method="post" enctype="multipart/form-data">
	<input type="hidden" name="file_path" value="<?php echo $image ?>" />
	<input type="file" id="file" name="file" class="change_file" onchange="UploadFile(this,<?php echo $i ?>);"/>
	<button class="change_image"><i class="fa fa-file-image-o" aria-hidden="true"></i>
		<p class="change_text">Change Image</p>
	</button></form>
	</div>		
		<?php }
	?> 
	
	<?php } $i++; } } ?>
	</div>
	</div>
	<script>
	function UploadFile(file,id) {
		$('.load_div').fadeIn(500);
  if (file.value === '') {
    alert("Invalid File");
  } else {
   //document.getElementById('form_'+id).submit();
		            jQuery('#form_'+id).ajaxSubmit({ 
		                success:function (e){
		                	d = new Date();
$('#form_'+id).prev().attr("src", e+"?"+d.getTime());
$('.loader').delay(100).fadeOut(100);
$('.loader-tick').delay(200).fadeIn(500);
$('.load_div').delay(500).fadeOut(500);
		                },
		                complete:function (e){
		                },
		                resetForm: true 
		            }); 
		            return false; 
  }
}
	</script>
	<style>
.folder_list, .files_list {
    background: #ececec none repeat scroll 0 0;
    box-shadow: 2px 0 0 0 #000;
    color: #fff !important;
    padding: 20px 87px 20px 20px;
    width: 100%;
    word-wrap:break-word;
}
.folder_list a, .files_list a{
	color:#002e42;
}
.folder_list p, .files_list p {
    background: #86b4c8 none repeat scroll 0 0;
    padding: 20px;
    position: absolute;
    right: 15px;
    top: 0;
}
.margin-set{
	margin-top:20px;
	margin-bottom:20px;
}
.list_image{
    background: #d5d5d5 none repeat scroll 0 0;
    height: 200px;
    margin: 5px;
    width: 100%;
}
.
*::after, *::before {
    box-sizing: border-box;
}
*::after, *::before {
    box-sizing: border-box;
}
.alert.alert-warning {
    position: absolute;
    top: 100%;
    width: 98%;
}
.change_image {
	display:none;
    height: 200px;
    margin: 5px;
    opacity: 0.66;
    position: absolute;
    top: 0;
    width: 90%;
}
.change_image .fa {
    color: #033145;
    font-size: 50px;
}
.change_file:hover + .change_image{
	display:block;
    height: 200px;
    margin: 5px;
    opacity: 0.66;
    position: absolute;
    top: 0;
    width: 90%;
}
.change_image:hover{
	display:block;
    height: 200px;
    margin: 5px;
    opacity: 0.66;
    position: absolute;
    top: 0;
    width: 90%;
}
.change_file {
    height: 200px !important;
    margin: 5px;
    opacity: 0;
    position: absolute;
    top: 0;
    width: 90% !important;
    z-index: 9;
}
</style>