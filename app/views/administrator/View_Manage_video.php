
<!DOCTYPE HTML> 
  <html>
    <head>
    	<style>
    		.error{
    			color: #FF5964;
    		}
    	</style>
    	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
    	<script>
    		$(document).ready(function()
{
	$("#manage_video").validate({
     rules: {
                media: { 
                	required: true, 
                	alphanumeric: true 
                	},
                
            },
     messages: {
                  media: {
                  	required: "Please enter the API ID.",
                  	alphanumeric: "Please give valid API ID."
                  	},
                  
               }

});
});
    	</script>
  </head>

  <body>
<center>
<h2>Manage Videos</h2>	<br /><br />
<form method="post" id="manage_video" enctype="multipart/form-data">
    Select video to upload:<br /><br />
    <input type="file" name="media" id="fileToUpload" value=""><br />
    <input type="submit" value="Upload Video" name="submit">
    
</form>
</center>
  </body>
</html>