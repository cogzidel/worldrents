<script type="text/javascript">
        jQuery(document).ready(function() {  
        	 var p = jQuery("#gridcontainer").nextRange().BcalGetOp();
                if (p && p.datestrshow) {
                             jQuery("#txtdatetimeshow").text(p.datestrshow);
                               }
          
                    	//previous
        var p = jQuery("#gridcontainer").previousRange().BcalGetOp();
                 if (p && p.datestrshow) {
                           jQuery("#txtdatetimeshow").text(p.datestrshow);
                           }
           var view="week";          
          // var value_url = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
           var value_url = window.location.href.split( '/' );
var value_1 = value_url[ value_url.length - 1 ] ; // 2
var value_2 = value_url[ value_url.length - 2 ] ; // 2
var value_3 =  value_url[ value_url.length - 3 ] ; // projects
var value = value_1 ;
if(value_2 == "edit" || value_2 == "id")
{
	value = value_1 ;
}

if(value_1 == "preview" || value_1 == "preview#calendar")
{
      value = value_2 ;
}
           jQuery("#list_id").val(value);	
           var DATA_FEED_URL = "<?php echo base_url(); ?>php/datafeed.php";
            var op = {
                view: view,
                theme:1,
                showday: new Date(),
                EditCmdhandler:Edit,
                DeleteCmdhandler:Delete,
                ViewCmdhandler:View,    
                onWeekOrMonthToDay:wtd,
                onBeforeRequestData: cal_beforerequest,
                onAfterRequestData: cal_afterrequest,
                onRequestDataError: cal_onerror, 
                autoload:true,
                url: DATA_FEED_URL + "?method=list&list_id="+value,  
                quickAddUrl: DATA_FEED_URL + "?method=add&list_id="+value, 
                quickUpdateUrl: DATA_FEED_URL + "?method=update&list_id="+value,
                quickDeleteUrl: DATA_FEED_URL + "?method=remove&list_id="+value        
            };
            var $dv = jQuery("#calhead");
            var _MH = document.documentElement.clientHeight;
            var dvH = $dv.height() + 2;
            op.height = _MH - dvH;
            op.eventItems =[];    op.eventItems =[];

            var p = jQuery("#gridcontainer").bcalendar(op).BcalGetOp();
            setTimeout(function(){
            if (p && p.datestrshow) {          	
            	    jQuery("#txtdatetimeshow").text(p.datestrshow);	
     
            }},2000);
            jQuery("#caltoolbar").noSelect();
            
            jQuery("#hdtxtshow").datepicker({ picker: "#txtdatetimeshow", showtarget: jQuery("#txtdatetimeshow"),
            onReturn:function(r){                          
                            var p = jQuery("#gridcontainer").gotoDate(r).BcalGetOp();
                            if (p && p.datestrshow) {
                                jQuery("#txtdatetimeshow").text(p.datestrshow);
                            }
                     } 
            });
             jQuery("#hdtxtshow").datepicker({ picker: "#txtdatetimeshow", showtarget: jQuery("#txtdatetimeshow"),
            onReturn:function(r){                          
                            var p = jQuery("#gridcontainer").gotoDate(r).BcalGetOp();
                            if (p && p.datestrshow) {
                                jQuery("#txtdatetimeshow").text(p.datestrshow);
                            }
                   }
                   });
            function cal_beforerequest(type)
            {
            	 var p = jQuery("#gridcontainer").nextRange().BcalGetOp();
                if (p && p.datestrshow) {
                             jQuery("#txtdatetimeshow").text(p.datestrshow);
                               }
          
                    	//previous
        var p = jQuery("#gridcontainer").previousRange().BcalGetOp();
                 if (p && p.datestrshow) {
                           jQuery("#txtdatetimeshow").text(p.datestrshow);
                           }
                var t="Loading data...";
                switch(type)
                {
                    case 1:
                        t="Loading data...";
                        break;
                    case 2:                      
                    case 3:  
                    case 4:    
                        t="The request is being processed ...";                                   
                        break;
                }
                jQuery("#errorpannel").hide();
                jQuery("#loadingpannel").html(t).show();    
            }
            function cal_afterrequest(type)
            {
                switch(type)
                {
                    case 1:
                        jQuery("#loadingpannel").hide();
                        break;
                    case 2:
                    case 3:
                    case 4:
                        jQuery("#loadingpannel").html("Success!");
            window.setTimeout(function(){
                        	 $("#loadingpannel").hide();
                        	 if(type == 2)
                        	 {
                        	 	$("#cal_plus_after").show();  
                        	 	$("#cal_plus").hide(); 
                        	 	                        	 	/// add
                        	 }
                        	 	  jQuery('#cal_after').trigger('click');
          window.location.reload(window.location.href.lastIndexOf('/') + 1);

                        	 },2000);
                    break;
                }              
               
            }
            function cal_onerror(type,data)
            {
                jQuery("#errorpannel").show();
            }
            function Edit(data)
            {
              var eurl="<?php echo base_url();?>rooms/edit_calendar?list_id="+value+"&id={0}&start={2}&end={3}&isallday={4}&title={1}"; 	
              // var eurl="edit.php?id={0}&start={2}&end={3}&isallday={4}&title={1}";   
                if(data)
                {
                    var url = StrFormat(eurl,data);
                    OpenModelWindow(url,{ width: 600, height: 400, caption:"Manage  The Calendar",onclose:function(){
                       location.reload();
                    }});
                }
            }    
            function View(data)
            {
                var str = "";
                jQuery.each(data, function(i, item){
                    str += "[" + i + "]: " + item + "\n";
                });
                //alert(str);               
            }    
            function Delete(data,callback)
            {           
                
                jQuery.alerts.okButton="Ok";  
                jQuery.alerts.cancelButton="Cancel";  
                hiConfirm("Are You Sure to Delete this Event", 'Confirm',function(r){ r && callback(0);});           
            }
            function wtd(p)
            {
               if (p && p.datestrshow) {
                    jQuery("#txtdatetimeshow").text(p.datestrshow);
                }
                jQuery("#caltoolbar div.fcurrent").each(function() {
                    jQuery(this).removeClass("fcurrent");
                })
                jQuery("#showdaybtn").addClass("fcurrent");
            }
            //to show day view
            jQuery("#showdaybtn").click(function(e) {
                //document.location.href="#day";
                jQuery("#caltoolbar div.fcurrent").each(function() {
                    jQuery(this).removeClass("fcurrent");
                })
                jQuery(this).addClass("fcurrent");
                var p = jQuery("#gridcontainer").swtichView("day").BcalGetOp();
                if (p && p.datestrshow) {
                    jQuery("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            //to show week view
            jQuery("#showweekbtn").click(function(e) {
                //document.location.href="#week";
                jQuery("#caltoolbar div.fcurrent").each(function() {
                    jQuery(this).removeClass("fcurrent");
                })
                jQuery(this).addClass("fcurrent");
                var p = jQuery("#gridcontainer").swtichView("week").BcalGetOp();
                if (p && p.datestrshow) {
                    jQuery("#txtdatetimeshow").text(p.datestrshow);
                }

            });
            //to show month view
            jQuery("#showmonthbtn").click(function(e) {
                //document.location.href="#month";
                jQuery("#caltoolbar div.fcurrent").each(function() {
                    jQuery(this).removeClass("fcurrent");
                })
                jQuery(this).addClass("fcurrent");
            	setTimeout(function(e)
				{
					var p = jQuery("#gridcontainer").swtichView("month").BcalGetOp();
	                if (p && p.datestrshow) {
	                    jQuery("#txtdatetimeshow").text(p.datestrshow);
	                }
				},1000)
                var p = jQuery("#gridcontainer").nextRange().BcalGetOp();
                if (p && p.datestrshow) {
                             jQuery("#txtdatetimeshow").text(p.datestrshow);
                               }
          
                    	//previous
        var p = jQuery("#gridcontainer").previousRange().BcalGetOp();
                 if (p && p.datestrshow) {
                           jQuery("#txtdatetimeshow").text(p.datestrshow);
                           }
            });
            
            jQuery("#showreflashbtn").click(function(e){
                jQuery("#gridcontainer").reload();
            });
            
            //Add a new event
            jQuery("#faddbtn").click(function(e) {
                var url ="edit.php";
                OpenModelWindow(url,{ width: 500, height: 400, caption: "Create New Calendar"});
            });
            //go to today
            jQuery("#showtodaybtn").click(function(e) {
                var p = jQuery("#gridcontainer").gotoDate().BcalGetOp();
                if (p && p.datestrshow) {
                    jQuery("#txtdatetimeshow").text(p.datestrshow);
                }


            });
            //previous date range
            jQuery("#sfprevbtn").click(function(e) {
                var p = jQuery("#gridcontainer").previousRange().BcalGetOp();
                if (p && p.datestrshow) {
                    jQuery("#txtdatetimeshow").text(p.datestrshow);
                }

            });
            //next date range
            jQuery("#sfnextbtn").click(function(e) {
                var p = jQuery("#gridcontainer").nextRange().BcalGetOp();
                if (p && p.datestrshow) {
                    jQuery("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            
            
        });
           jQuery("#showmonthbtn").load(function(e) {
                //document.location.href="#month";
                jQuery("#caltoolbar div.fcurrent").each(function() {
                    jQuery(this).removeClass("fcurrent");
                })
                jQuery(this).addClass("fcurrent");
            	setTimeout(function(e)
				{
					var p = jQuery("#gridcontainer").swtichView("month").BcalGetOp();
	                if (p && p.datestrshow) {
	                    jQuery("#txtdatetimeshow").text(p.datestrshow);
	                }
				},1000)
                var p = jQuery("#gridcontainer").nextRange().BcalGetOp();
                if (p && p.datestrshow) {
                             jQuery("#txtdatetimeshow").text(p.datestrshow);
                               }
          
                    	//previous
        var p = jQuery("#gridcontainer").previousRange().BcalGetOp();
                 if (p && p.datestrshow) {
                           jQuery("#txtdatetimeshow").text(p.datestrshow);
                           }
            });
     
    </script>  