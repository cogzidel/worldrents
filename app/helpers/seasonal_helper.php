<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

function getDailyPrice($list_id,$date,$list_price)
{
		$CI     =& get_instance();
		$special=0;
		$CI->load->model('Common_model');	
		//edit calender
			$list_currency=$CI->db->where('id',$list_id)->get('list')->row()->currency;
		    //print_r($list_currency);
		//edit calender
		
		$conditions=array('list_id' => $list_id);
		$seasonal_query=$CI->Common_model->getTableData('seasonalprice',$conditions);
		//echo "<pre>";
		//print_r($seasonal_query);
	    if($seasonal_query -> num_rows() > 0)
		 {
				    
					$seasonal_result=$seasonal_query->result_array();
					
						foreach($seasonal_result as $res)
						{
								
							if($date == $res['start_date'] || $date == $res['end_date'] || ($date > $res['start_date'] && $date < $res['end_date']))
							{
									$price=$res['price'];
								  //edit calender
								  //	$list_currency = $CI->Common_model->getTableData('list',array('id'=>$list_id))->row()->currency;
									
									//print_r($res['currency']);
							        $price = $price;
								   //print_r($price);n getDailyPrice
									
									
								//	$price = get_currency_value_cal(get_currency_code(),$list_currency,$price);
								//end edit calender
									$special=1;
							}
					   }
		
                		
			   if($special == 0)
				
				//start edit calender
				
			   //$price=get_currency_value_lys($list_currency,get_currency_code(),$list_price);
				//print_r($date);
				  //print_r($list_price);
				  $price=$list_price;
			    //	$price=get_currency_value1($list_id,$list_price);
				//end edit calender

	         }
	        else 
	        {
					//start edit calender
					$price=$list_price;	
				
				    //$price=get_currency_value_lys($list_currency,get_currency_code(),$list_price);
				    //end edit calender
	          }
	          
	 
		return $price;
}

function get_new_price_calculation($checkin,$checkout,$timein,$timeout,$hprice,$Wprice,$Mprice,$price)
{
	$seas_av=0;
	$hour=(($timeout - $timein)/(3600));
	$date1 = new DateTime(date('Y-m-d H:i',$timein));
	$date2 = new DateTime(date('Y-m-d H:i',$timeout));
	$diff = $date2->diff($date1);
	$hours=$diff->format('%h');
	$days=$diff->format('%a');
	$minute=$diff->format('%I');
	if( $checkin == $checkout)
	{
		if($days==0 && $hour==24)
		{
			$price=$price;
		}
		else
		{
			$seas_av=1;
			$data['show_hour'] = $hour;
			$data['show_hprice'] = $hprice;
			$price=ceil($hprice*$hour);
		}
	}
	else
	{				
		if($minute==30)
		{
			$minprice=round($hprice/2);
		}
		else {
			$minprice=0;
		}
		$phour=$hprice;
		$data['show_hour'] = $hours;
		$data['show_hprice'] = $phour;
		$sprice=ceil($phour*$hours);
		$oprice  = ($price * ($days));
		$price=$oprice+$sprice+$minprice;
		if($hours>0)
		{
			$seas_av=1;
		}		
	}
	
	if($days >= 7 && $days < 30)
	{    
        if(!empty($Wprice))
		{
         $week_count  = floor($days / 7);
         $tempMod = (float)($days / 7);
         $tempMod = ($tempMod - (int)$tempMod)*7;
         $remaining = round($tempMod);
         $data['show_week'] = $week_count;
		 $data['show_day'] = $remaining;
		}
		else
		{
         $data['show_week'] = 0;
		 $data['show_day'] = $days;
		}

		if(!empty($Wprice))
		{
			$finalAmount     = $Wprice;
			$differNights    = $days - 7;
			$perDay          = $Wprice / 7;
			$per_night       = round($perDay, 2);
			if($differNights > 0)
			{
			  $addAmount     = $differNights * $per_night;
					$finalAmount   = $Wprice + $addAmount;
			}
			$price           = $finalAmount;
			$phour=$hprice;
		
			if($minute==30)
			{
				$minprice=round($hprice/2);
			}
			else {
				$minprice=0;
			}	
			$phour=$hprice;
			$data['show_hour'] = $hour;
			$data['show_hprice'] = $phour;
			$sprice=ceil($phour*$hours);
			//$oprice  = ($price * ($days));
			$price=$price+$sprice+$minprice;			
			if($hours>0)
			{
				$seas_av=1;
			}				
		}
	}
	else
	{
		$data['show_week'] = 0;
		$data['show_day'] = $days;
	}
			
			
	if($days >= 30)
	{
		if(!empty($Mprice))
		{
			$finalAmount     = $Mprice;
			$differNights    = $days - 30;
			$perDay          = $Mprice / 30;
			$per_night       = round($perDay, 2);
			if($differNights > 0)
			{
				$addAmount     = $differNights * $per_night;
				$finalAmount   = $Mprice + $addAmount;
			}
			$price           = $finalAmount;
			$phour=$hprice;

			if($minute==30)
			{
				$minprice=round($hprice/2);
			}
			else {
				$minprice=0;
			}	
			$phour=$hprice;
			$data['show_hour'] = $hour;
			$data['show_hprice'] = $phour;
			$sprice=ceil($phour*$hours);
			//$oprice  = ($price * ($days));
			$price=$price+$sprice+$minprice;
			if($hours>0)
			{
				$seas_av=1;
			}
		}
	}
	$data['Tdays']=$days;
	$data['Thour']=$hours;
	$data['seas_av']=$seas_av;
	$data['price']=$price;
	return $data;	
}

?>